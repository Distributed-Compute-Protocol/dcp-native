/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2021
 */

#if !BOOST_OS_WINDOWS

  #include <cstdlib>

int main() noexcept {
  return EXIT_FAILURE;
}

#endif
