/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#if BOOST_OS_WINDOWS

  #include "resource.h"

  #include <dcp/bitmap.hh>
  #include <dcp/graphics.hh>
  #include <dcp/logger.hh>
  #include <dcp/paths.hh>
  #include <dcp/process.hh>
  #include <dcp/resource.hh>
  #include <dcp/settings.hh>
  #include <dcp/settings/configuration.hh>
  #include <dcp/settings/configurator.hh>
  #include <dcp/string.hh>
  #include <dcp/supervisor.hh>
  #include <dcp/window.hh>
  #include <dcp/window/combo_box.hh>
  #include <dcp/window/dismissable_message_dialog.hh>
  #include <dcp/window/link.hh>
  #include <dcp/window/slider.hh>
  #include <dcp/window/static_control.hh>
  #include <dcp/window/tab_box.hh>

  #include <boost/algorithm/string.hpp>
  #include <boost/exception/diagnostic_information.hpp>
  #include <boost/filesystem/operations.hpp>
  #include <boost/format.hpp>
  #include <boost/nowide/convert.hpp>
  #include <boost/thread/scoped_thread.hpp>

  #include <windows.h> // Include before other windows headers
  #include <commctrl.h>
  #include <powerbase.h>
  #include <scrnsave.h>
  #include <uxtheme.h>

  #include <shared_mutex>

using namespace DCP::Settings;

namespace {

  enum Messages : UINT {
    /// Issued to destructively update the dialog UI.
    WM_UPDATE = WM_APP + 0x0001,
    /// Issued from the "add compute group" thread when completed.
    /// The LPARAM argument holds a non-zero number if there was an exception.
    /// The WPARAM argument holds a boolean indicating whether the compute group
    /// was added.
    WM_COMPUTE_GROUP_ADDED,
    /// Issued from the saving thread when completed.
    /// The LPARAM argument holds a non-zero number if the settings configurator
    /// failed to run.
    /// The WPARAM argument holds the settings configurator exit code.
    WM_SAVED
  };

  static boost::filesystem::path installDirectoryPath{};

  static std::shared_timed_mutex configurationMutex{};
  static boost::optional<Configuration> configuration{};

  DCP::Handle<HFONT, nullptr> getLicenseFont() {
    LOGFONT logfont{};
    logfont.lfCharSet = DEFAULT_CHARSET;
    logfont.lfHeight = DCP::Graphics::getFontHeight(6);
    logfont.lfPitchAndFamily = (FIXED_PITCH | FF_MODERN);

    return DCP::Handle<HFONT, nullptr>(
      CreateFontIndirect(&logfont), DeleteObject
    );
  }

  /// Show general error message (if possible) and exit with `EXIT_FAILURE`.
  void failWithMessage(HWND const window) {
    try {
      DCP::Window::MessageDialog{MB_OK | MB_ICONERROR}.show(window,
        DCP::Resource::loadString(IDS_INTERNAL_ERROR_CAPTION).data(),
        DCP::Resource::loadString(IDS_INTERNAL_ERROR_MESSAGE).data()
      );
    } catch (...) {}
    std::exit(EXIT_FAILURE);
  }

  /** Prompt to specify a payment address.
   *
   *  @return   `true` if the user chose to specify the address.
   */
  bool revisitMissingPayment(HWND const window) {
    return DCP::Window::MessageDialog{MB_YESNO | MB_ICONWARNING}.show(window,
      DCP::Resource::loadString(IDS_PAYMENT_ADDRESS_REQUIRED_CAPTION).data(),
      DCP::Resource::loadString(IDS_PAYMENT_ADDRESS_REQUIRED_MESSAGE).data()
    ) == IDYES;
  }

  /** Check power settings and prompt to adjust if necessary and permitted.
   *
   *  @return   `true` if a UI action was taken.
   */
  bool checkPowerSettings(
    HWND const window,
    DCP::Window::DismissableMessageDialog const &adjustPowerSettingsDialog
  ) {
    DCP::log(DCP::LogLevel::debug, "Getting power policy...");
    SYSTEM_POWER_POLICY powerPolicy{};
    DWORD const status = CallNtPowerInformation(SystemPowerPolicyAc,
      nullptr, 0, &powerPolicy, sizeof(powerPolicy)
    );
    if (status != ERROR_SUCCESS) {
      DCP::log(DCP::LogLevel::error, "CallNtPowerInformation() failed: %lu",
        status
      );
      return false;
    }

    DWORD const sleepTimeout{
      (powerPolicy.Idle.Action != PowerActionSleep) ? 0 :
      powerPolicy.IdleTimeout
    };
    DCP::log(DCP::LogLevel::debug, "Sleep timeout: %lu", sleepTimeout);

    DWORD const screenTimeout{powerPolicy.VideoTimeout};
    DCP::log(DCP::LogLevel::debug, "Screen timeout: %lu", screenTimeout);

    if (!sleepTimeout && !screenTimeout) {
      DCP::log(DCP::LogLevel::debug, "No sleep or screen timeout");
      return false;
    }
    DWORD const timeout{
      !sleepTimeout ? screenTimeout :
      !screenTimeout ? sleepTimeout :
      std::min(sleepTimeout, screenTimeout)
    };
    DCP::log(DCP::LogLevel::debug, "Timeout: %lu", timeout);

    int const result = adjustPowerSettingsDialog.show(window,
      DCP::Resource::loadString(IDS_ADJUST_POWER_SETTINGS_CAPTION).data(),
      boost::str(
        boost::format(
          DCP::Resource::loadString(IDS_ADJUST_POWER_SETTINGS_MESSAGE)
        ) % (timeout / 60)
      ).data()
    );
    if (result != IDYES) {
      DCP::log(DCP::LogLevel::debug,
        "User opted to not adjust power settings; result: %i", result
      );
      return false;
    }
    DCP::log(DCP::LogLevel::debug, "User opted to adjust power settings");

    auto controlExecutablePath = DCP::Paths::getSystemDirectoryPath();
    assert(!controlExecutablePath.empty());
    controlExecutablePath /= "control.exe";

    std::vector<std::string> const &arguments{
      "/name", "Microsoft.PowerOptions",
      "/page", "pagePlanSettings"
    };

    DCP::log(DCP::LogLevel::debug,
      "Running command to open settings control panel"
    );
    try {
      DCP::Process::command(
        controlExecutablePath.string().data(), arguments, false, true
      );
    } catch (...) {
      DCP::log(DCP::LogLevel::error,
        "The command `\"%s\" %s` threw exception: %s",
        controlExecutablePath.string().data(),
        DCP::String::joinQuoted(arguments).data(),
        boost::current_exception_diagnostic_information().data()
      );
      DCP::Window::MessageDialog{MB_OK | MB_ICONERROR}.show(window,
        DCP::Resource::loadString(
          IDS_ADJUST_POWER_SETTINGS_ERROR_CAPTION
        ).data(),
        DCP::Resource::loadString(
          IDS_ADJUST_POWER_SETTINGS_ERROR_MESSAGE
        ).data()
      );
    }
    DCP::log(DCP::LogLevel::debug,
      "Settings control panel opened successfully"
    );
    return true;
  }

  //! Handle messages for the "add compute group" dialog.
  INT_PTR handleAddComputeGroupMessage(
    HWND const dialog,
    UINT const message,
    WPARAM const wParam,
    LPARAM const lParam
  ) {
    assert(dialog);

    try {
      static boost::optional<boost::strict_scoped_thread<>> addingThread{};

      switch (message) {
      case WM_COMMAND:
        switch (HIWORD(wParam)) {
        case EN_CHANGE:
          switch (LOWORD(wParam)) {
          case IDC_COMPUTE_GROUP_JOIN_KEY:
            {
              auto const joinKeyField = reinterpret_cast<HWND>(lParam);
              assert(joinKeyField);
              std::string const &joinKey = DCP::Window::getText(joinKeyField);

              HWND const nameField = DCP::Window::tryToGetChild(
                dialog, IDC_COMPUTE_GROUP_NAME
              );
              assert(nameField);

              [[maybe_unused]] auto const result = SendMessage(
                nameField, EM_SETCUEBANNER, FALSE,
                reinterpret_cast<LPARAM>(boost::nowide::widen(joinKey).data())
              );
              assert(result);
            }
            return TRUE;
          }
          break;
        case BN_CLICKED:
          switch (LOWORD(wParam)) {
          case IDOK:
            {
              HWND const joinKeyField = DCP::Window::tryToGetChild(
                dialog, IDC_COMPUTE_GROUP_JOIN_KEY
              );
              assert(joinKeyField);
              std::string joinKey = DCP::Window::getText(joinKeyField);
              if (!DCP::Window::isReadOnly(joinKeyField)) {
                DCP::log(DCP::LogLevel::debug,
                  "Compute group join key text entered: %s", joinKey.data()
                );
                if (joinKey.empty()) {
                  DCP::Window::tryToSetFocus(joinKeyField);
                  DCP::Window::MessageDialog{MB_OK | MB_ICONERROR}.show(dialog,
                    DCP::Resource::loadString(
                      IDS_COMPUTE_GROUP_JOIN_KEY_REQUIRED_CAPTION
                    ).data(),
                    DCP::Resource::loadString(
                      IDS_COMPUTE_GROUP_JOIN_KEY_REQUIRED_MESSAGE
                    ).data()
                  );
                  return TRUE;
                }
              }
              boost::trim(joinKey);

              HWND const joinSecretField = DCP::Window::tryToGetChild(
                dialog, IDC_COMPUTE_GROUP_JOIN_SECRET
              );
              assert(joinSecretField);
              std::string joinSecret = DCP::Window::getText(
                joinSecretField
              );
              if (!DCP::Window::isReadOnly(joinSecretField)) {
                DCP::log(DCP::LogLevel::debug,
                  "Compute group join secret text entered: %s",
                  joinSecret.data()
                );
              }
              boost::trim(joinSecret);

              HWND const nameField = DCP::Window::tryToGetChild(
                dialog, IDC_COMPUTE_GROUP_NAME
              );
              assert(nameField);
              std::string name = DCP::Window::getText(nameField);
              if (name.empty()) {
                name = joinKey;
                DCP::log(DCP::LogLevel::debug,
                  "No compute group name text entered; join key used: %s",
                  name.data()
                );
              } else {
                DCP::log(DCP::LogLevel::debug,
                  "Compute group name text entered: %s", name.data()
                );
              }
              boost::trim(name);

              DCP::Window::setReadOnly(dialog, true);
              assert(!addingThread);
              addingThread.emplace(
                [
                  dialog,
                  joinKey{std::move(joinKey)},
                  joinSecret{std::move(joinSecret)},
                  name{std::move(name)}
                ]() noexcept {
                  WPARAM wParam{};
                  LPARAM lParam{};
                  try {
                    assert(
                      boost::filesystem::is_directory(installDirectoryPath)
                    );
                    auto const joinHash = DCP::Supervisor::hash(
                      installDirectoryPath, joinKey, joinSecret
                    );
                    bool computeGroupAdded = false;
                    {
                      std::lock_guard<std::shared_timed_mutex> lock{
                        configurationMutex
                      };
                      assert(configuration);
                      computeGroupAdded = configuration->addComputeGroup(name,
                        Configuration::ComputeGroup(
                          joinKey, joinSecret, joinHash
                        )
                      );
                    }
                    wParam = computeGroupAdded;
                  } catch (...) {
                    DCP::tryToLog(DCP::LogLevel::error,
                      "Exception when adding compute group: %s",
                      boost::current_exception_diagnostic_information().data()
                    );
                    lParam = 1;
                  }
                  DCP::Window::postMessage(
                    dialog, WM_COMPUTE_GROUP_ADDED, wParam, lParam
                  );
                }
              );
            }
            return TRUE;
          case IDCANCEL:
            DCP::Resource::closeModalDialog(dialog, TRUE);
            return TRUE;
          }
          break;
        }
        break;
      case WM_SETCURSOR:
        if (addingThread) {
          static HCURSOR const waitCursor = LoadCursor(NULL, IDC_WAIT);
          SetCursor(waitCursor);
          return TRUE;
        }
        break;
      case WM_COMPUTE_GROUP_ADDED:
        addingThread.reset();
        DCP::Window::setReadOnly(dialog, false);
        if (lParam) {
          DCP::Window::MessageDialog{MB_OK | MB_ICONERROR}.show(dialog,
            DCP::Resource::loadString(
              IDS_COMPUTE_GROUP_ADD_ERROR_CAPTION
            ).data(),
            DCP::Resource::loadString(
              IDS_COMPUTE_GROUP_ADD_ERROR_MESSAGE
            ).data()
          );
        } else if (!wParam) {
          DCP::Window::MessageDialog{MB_OK | MB_ICONERROR}.show(dialog,
            DCP::Resource::loadString(
              IDS_COMPUTE_GROUP_ADD_ERROR_CAPTION
            ).data(),
            DCP::Resource::loadString(
              IDS_COMPUTE_GROUP_ADD_DUPLICATE_MESSAGE
            ).data()
          );

          HWND const nameField = DCP::Window::tryToGetChild(
            dialog, IDC_COMPUTE_GROUP_NAME
          );
          assert(nameField);
          DCP::Window::tryToSetFocus(nameField);
        } else {
          DCP::Resource::closeModalDialog(dialog, TRUE);
        }
        return TRUE;
      case WM_DESTROY:
        addingThread.reset();
        return TRUE;
      }
    } catch (...) {
      DCP::tryToLog(DCP::LogLevel::error,
        "Exception in handleAddComputeGroupMessage(): %s",
        boost::current_exception_diagnostic_information().data()
      );
      failWithMessage(dialog);
    }
    return FALSE;
  }

  /// Handle tab-level messages.
  INT_PTR handleTabMessage(
    HWND const tab,
    UINT const message,
    WPARAM const wParam,
    LPARAM const lParam
  ) {
    assert(tab);

    try {
      HWND const window = DCP::Window::tryToGetParent(tab);
      assert(window);

      switch (message) {
      case WM_INITDIALOG:
        if (
          HRESULT const result = EnableThemeDialogTexture(tab,
            ETDT_USETABTEXTURE
          );
          result != S_OK
        ) {
          throw std::system_error(
            std::error_code(result, std::system_category()),
            "EnableThemeDialogTexture() failed"
          );
        }
        break;
      case WM_COMMAND:
        switch (HIWORD(wParam)) {
        case LBN_SELCHANGE:
          switch (LOWORD(wParam)) {
          case IDC_COMPUTE_GROUP_LIST:
            return DCP::Window::postMessage(window, WM_UPDATE,
              IDC_COMPUTE_GROUP_REMOVE_BUTTON
            );
          }
          break;
        case BN_CLICKED:
          switch (LOWORD(wParam)) {
          case IDC_IDENTITY_KEYSTORE_BUTTON:
            if (
              auto const &filePath = DCP::Window::chooseFile(
                tab, "Keystore Files\0*.KEYSTORE\0"
              );
              !filePath.empty()
            ) {
              DCP::log(DCP::LogLevel::debug,
                "Selected identity keystore file \"%s\"",
                filePath.string().data()
              );
              bool success = false;
              {
                std::lock_guard<std::shared_timed_mutex> lock{
                  configurationMutex
                };
                assert(configuration);
                success = configuration->setIdentity(filePath);
              }
              if (success) {
                return DCP::Window::postMessage(window, WM_UPDATE,
                  IDC_IDENTITY_ADDRESS
                );
              }
              DCP::Window::MessageDialog{MB_OK | MB_ICONERROR}.show(window,
                DCP::Resource::loadString(IDS_KEYSTORE_ERROR_CAPTION).data(),
                DCP::Resource::loadString(IDS_KEYSTORE_ERROR_MESSAGE).data()
              );
              DCP::Window::tryToSetFocus(
                DCP::Window::tryToGetChild(tab, IDC_IDENTITY_KEYSTORE_BUTTON)
              );
            }
            return TRUE;
          case IDC_IDENTITY_RESET_BUTTON:
            {
              std::lock_guard<std::shared_timed_mutex> lock{configurationMutex};
              assert(configuration);
              configuration->resetIdentity();
            }
            return DCP::Window::postMessage(window, WM_UPDATE,
              IDC_IDENTITY_ADDRESS
            );
          case IDC_PAYMENT_KEYSTORE_BUTTON:
            if (
              boost::filesystem::path const &filePath = (
                DCP::Window::chooseFile(window, "Keystore Files\0*.KEYSTORE\0")
              );
              !filePath.empty()
            ) {
              DCP::log(DCP::LogLevel::debug,
                "Selected payment keystore file \"%s\"",
                filePath.string().data()
              );
              bool success = false;
              {
                std::lock_guard<std::shared_timed_mutex> lock{
                  configurationMutex
                };
                assert(configuration);
                success = configuration->setPayment(filePath);
              }
              if (success) {
                return DCP::Window::postMessage(window, WM_UPDATE,
                  IDC_PAYMENT_ADDRESS
                );
              }
              DCP::Window::MessageDialog{MB_OK | MB_ICONERROR}.show(window,
                DCP::Resource::loadString(IDS_KEYSTORE_ERROR_CAPTION).data(),
                DCP::Resource::loadString(IDS_KEYSTORE_ERROR_MESSAGE).data()
              );
              DCP::Window::tryToSetFocus(
                DCP::Window::tryToGetChild(tab, IDC_PAYMENT_KEYSTORE_BUTTON)
              );
            }
            return TRUE;
          case IDC_PAYMENT_RESET_BUTTON:
            {
              std::lock_guard<std::shared_timed_mutex> lock{configurationMutex};
              assert(configuration);
              configuration->resetPayment();
            }
            return DCP::Window::postMessage(window, WM_UPDATE,
              IDC_PAYMENT_ADDRESS
            );
          case IDC_COMPUTE_GROUP_ADD_BUTTON:
            {
              [[maybe_unused]] INT_PTR const result{
                DCP::Resource::openModalDialog(
                  IDD_ADD_COMPUTE_GROUP, tab, handleAddComputeGroupMessage
                )
              };
              assert(result);
            }
            return DCP::Window::postMessage(window, WM_UPDATE,
              IDC_COMPUTE_GROUP_LIST
            );
          case IDC_COMPUTE_GROUP_REMOVE_BUTTON:
            {
              HWND const computeGroupList = DCP::Window::tryToGetChild(
                tab, IDC_COMPUTE_GROUP_LIST
              );
              assert(computeGroupList);

              auto result = SendMessage(computeGroupList, LB_GETSELCOUNT, 0, 0);
              assert(result != LB_ERR);
              size_t count = result;
              if (!count) {
                assert(false &&
                  "It should not be possible to click Remove when no compute"
                  " group is selected."
                );
                return TRUE;
              }

              std::vector<int> items(count, LB_ERR);
              result = SendMessage(
                computeGroupList, LB_GETSELITEMS, count, (LPARAM)&items.front()
              );
              assert(result != LB_ERR && (size_t)result == count);

              result = SendMessage(computeGroupList, LB_GETTEXTLEN, 0, 0);
              assert(result != LB_ERR);
              std::string name(result + 1, '\0');

              while (count) {
                size_t const position = items[--count];

                result = SendMessage(computeGroupList, LB_GETTEXT,
                  static_cast<WPARAM>(position),
                  reinterpret_cast<LPARAM>(&name.front())
                );
                assert(result != LB_ERR);

                {
                  std::lock_guard<std::shared_timed_mutex> lock{
                    configurationMutex
                  };
                  assert(configuration);
                  if (!configuration->removeComputeGroup(name.data())) {
                    assert(false);
                  }
                }

                result = SendMessage(computeGroupList, LB_DELETESTRING,
                  static_cast<WPARAM>(position), 0
                );
                assert(result != LB_ERR);
              }
            }
            return DCP::Window::postMessage(window, WM_UPDATE,
              IDC_COMPUTE_GROUP_REMOVE_BUTTON
            );
          case IDC_SCREENSAVER_BITMAP_BUTTON:
            if (
              auto filePath = DCP::Window::chooseFile(
                window, "Bitmap Files\0*.BMP\0"
              );
              !filePath.empty()
            ) {
              bool success = false;
              {
                std::lock_guard<std::shared_timed_mutex> lock{
                  configurationMutex
                };
                assert(configuration);
                success = configuration->setScreensaverBitmapFilePath(
                  std::move(filePath)
                );
              }
              if (success) {
                return DCP::Window::postMessage(window, WM_UPDATE,
                  IDC_SCREENSAVER_PREVIEW
                );
              }
              DCP::Window::MessageDialog{MB_OK | MB_ICONERROR}.show(window,
                DCP::Resource::loadString(
                  IDS_SCREENSAVER_BITMAP_ERROR_CAPTION
                ).data(),
                DCP::Resource::loadString(
                  IDS_SCREENSAVER_BITMAP_ERROR_MESSAGE
                ).data()
              );
              DCP::Window::tryToSetFocus(
                DCP::Window::tryToGetChild(tab, IDC_SCREENSAVER_BITMAP_BUTTON)
              );
            }
            return TRUE;
          case IDC_SCREENSAVER_BACKGROUND_BUTTON:
            {
              boost::optional<DCP::Color::Value> backgroundColor{};
              {
                std::shared_lock<std::shared_timed_mutex> lock{
                  configurationMutex
                };
                assert(configuration);
                backgroundColor = (
                  configuration->getScreensaverBackgroundColor()
                );
              }
              backgroundColor = DCP::Window::chooseColor(window,
                backgroundColor ? *backgroundColor :
                DCP::Resource::loadColor(IDR_BACKGROUND_COLOR, IDR_COLOR_TYPE)
              );
              if (backgroundColor) {
                {
                  std::lock_guard<std::shared_timed_mutex> lock{
                    configurationMutex
                  };
                  assert(configuration);
                  configuration->setScreensaverBackgroundColor(
                    *backgroundColor
                  );
                }
                return DCP::Window::postMessage(window, WM_UPDATE,
                  IDC_SCREENSAVER_PREVIEW
                );
              }
            }
            return TRUE;
          case IDC_SCREENSAVER_RESET_BUTTON:
            {
              std::lock_guard<std::shared_timed_mutex> lock{configurationMutex};
              assert(configuration);
              configuration->resetScreensaverBitmap();
              configuration->resetScreensaverBackgroundColor();
            }
            return DCP::Window::postMessage(window, WM_UPDATE,
              IDC_SCREENSAVER_PREVIEW
            );
          case IDC_SCHEDULER_RESET_BUTTON:
            {
              std::lock_guard<std::shared_timed_mutex> lock{configurationMutex};
              assert(configuration);
              configuration->setSchedulerUrl("");
            }
            return DCP::Window::postMessage(window, WM_UPDATE,
              IDC_SCHEDULER_URL
            );
          case IDC_EVALUATOR_WEBGPU_CHECKBOX:
            if (
              HWND const control = reinterpret_cast<HWND>(lParam);
              DCP::Window::getChecked(control)
            ) {
              bool const confirmed = (
                DCP::Window::MessageDialog{MB_YESNO | MB_ICONWARNING}.show(tab,
                  DCP::Resource::loadString(
                    IDS_EVALUATOR_WEBGPU_WARNING_CAPTION
                  ).data(),
                  DCP::Resource::loadString(
                    IDS_EVALUATOR_WEBGPU_WARNING_MESSAGE
                  ).data()
                ) == IDYES
              );
              DCP::Window::setChecked(control, confirmed);
            }
            return TRUE;
          }
          break;
        }
        break;
      default:
        try {
          if (DCP::Window::Link::handleMessage(message, wParam, lParam)) {
            return TRUE;
          }
        } catch (...) {
          DCP::log(DCP::LogLevel::error,
            "Error opening link in default browser: %s",
            boost::current_exception_diagnostic_information().data()
          );
          DCP::Window::MessageDialog{MB_OK | MB_ICONERROR}.show(window,
            DCP::Resource::loadString(IDS_LINK_ERROR_CAPTION).data(),
            DCP::Resource::loadString(IDS_LINK_ERROR_MESSAGE).data()
          );
        }
        break;
      }
    } catch (...) {
      DCP::tryToLog(DCP::LogLevel::error, "Exception in handleTabMessage(): %s",
        boost::current_exception_diagnostic_information().data()
      );
      failWithMessage(tab);
    }
    return FALSE;
  }

}

extern "C" BOOL WINAPI ScreenSaverConfigureDialog(
  HWND const window,
  UINT const message,
  WPARAM wParam,
  LPARAM const lParam
) {
  static_assert(DCP_serviceName && *DCP_serviceName);
  assert(window);

  try {
    // Ensure static logger is instantiated before trying to log.
    // Turn off stderr logging, which causes problems for the screensaver.
    static auto const &executablePath = DCP::Paths::getExecutablePath();
    static auto &logger = DCP::Logger::getInstance(
      executablePath.stem().string(), -1
    );
    try {
      // If adding or removing a value here, update the tab box initialization.
      struct TabBoxIndex {
        enum Enumeration {
          Keystores,
          ComputeGroups,
          Screensaver,
          Advanced,
          About
        };
      };

      static DCP::Handle<HICON, nullptr> icon{};
      static DCP::Color::Value defaultBackgroundColor{};
      static DCP::Handle<HBITMAP, nullptr> defaultBitmap{};
      static DCP::Handle<HFONT, nullptr> licenseFont{};
      static boost::optional<DCP::Window::TabBox> tabBox{};
      static boost::optional<
        DCP::Window::ComboBox
      > publicComputeGroupComboBox{};
      static boost::optional<
        DCP::Window::DismissableMessageDialog
      > adjustPowerSettingsDialog{};
      static boost::optional<DCP::Window::Slider> loggingSlider{};
      static boost::optional<boost::strict_scoped_thread<>> savingThread{};

      switch (message) {
      case WM_INITDIALOG:
        assert(boost::filesystem::exists(executablePath));
        installDirectoryPath = DCP::Paths::getInstallDirectoryPath(
          executablePath.parent_path()
        );
        assert(boost::filesystem::is_directory(installDirectoryPath));

        {
          INITCOMMONCONTROLSEX icce{};
          icce.dwSize = sizeof(icce);
          icce.dwICC = ICC_TAB_CLASSES;
          InitCommonControlsEx(&icce);
        }

        configuration.emplace();
        assert(configuration);

        logger.setMaxSystemLogLevel(configuration->getMaxSystemLogLevel());
        logger.log(DCP::LogLevel::info, "Running \"%s\", version %s...",
          executablePath.filename().string().data(), DCP_version
        );

        icon = DCP::Resource::loadIcon(IDI_ICON);
        assert(icon);
        DCP::Window::setIcon(window, icon);

        defaultBackgroundColor = DCP::Resource::loadColor(
          IDR_BACKGROUND_COLOR, IDR_COLOR_TYPE
        );
        assert(defaultBackgroundColor);

        defaultBitmap = DCP::Resource::loadBitmap(IDB_BITMAP);
        assert(defaultBitmap);

        assert(!tabBox);
        tabBox.emplace(DCP::Window::tryToGetChild(window, IDC_TAB));
        assert(tabBox);

        // If adding or removing a tab here, update TabBoxIndex::Enumeration.
        tabBox->appendTab(IDD_KEYSTORES,
          DCP::Resource::loadString(IDS_TAB_KEYSTORES), handleTabMessage
        );
        tabBox->appendTab(IDD_COMPUTE_GROUPS,
          DCP::Resource::loadString(IDS_TAB_COMPUTE_GROUPS), handleTabMessage
        );
        tabBox->appendTab(IDD_SCREENSAVER,
          DCP::Resource::loadString(IDS_TAB_SCREENSAVER), handleTabMessage
        );
        tabBox->appendTab(IDD_ADVANCED,
          DCP::Resource::loadString(IDS_TAB_ADVANCED), handleTabMessage
        );
        tabBox->appendTab(IDD_ABOUT,
          DCP::Resource::loadString(IDS_TAB_ABOUT), handleTabMessage
        );
        assert(tabBox->getTab(TabBoxIndex::Keystores));
        assert(tabBox->getTab(TabBoxIndex::ComputeGroups));
        assert(tabBox->getTab(TabBoxIndex::Screensaver));
        assert(tabBox->getTab(TabBoxIndex::Advanced));
        assert(tabBox->getTab(TabBoxIndex::About));
        tabBox->selectTab(TabBoxIndex::Keystores);

        assert(
          static_cast<int>(
            Configuration::ComputeGroup::IncludePublic::Always
          ) == 0
        );
        assert(
          static_cast<int>(
            Configuration::ComputeGroup::IncludePublic::Fallback
          ) == 1
        );
        assert(
          static_cast<int>(
            Configuration::ComputeGroup::IncludePublic::Never
          ) == 2
        );
        publicComputeGroupComboBox.emplace(
          DCP::Window::tryToGetChild(
            tabBox->getTab(TabBoxIndex::ComputeGroups),
            IDC_COMPUTE_GROUP_PUBLIC_COMBOBOX
          ),
          std::vector<char const *>{
            DCP::Resource::loadString(
              IDS_COMPUTE_GROUP_PUBLIC_ALWAYS_CAPTION
            ).data(),
            DCP::Resource::loadString(
              IDS_COMPUTE_GROUP_PUBLIC_FALLBACK_CAPTION
            ).data(),
            DCP::Resource::loadString(
              IDS_COMPUTE_GROUP_PUBLIC_NEVER_CAPTION
            ).data()
          }
        );
        assert(publicComputeGroupComboBox);
        publicComputeGroupComboBox->setSelection(0);

        // The GUID was uniquely generated with uuidgen.
        adjustPowerSettingsDialog.emplace(
          "93BD04C6-5EAC-48E6-B465-C1638C894D0E", IDNO,
          MB_YESNO | MB_ICONWARNING
        );
        assert(adjustPowerSettingsDialog);

        assert(!loggingSlider);
        assert(tabBox->getTab(TabBoxIndex::Advanced));
        loggingSlider.emplace(
          DCP::Window::tryToGetChild(
            tabBox->getTab(TabBoxIndex::Advanced), IDC_LOGGING_SLIDER
          ),
          static_cast<std::uint16_t>(0),
          boost::numeric_cast<std::uint16_t>(DCP::LogLevel::trace + 1)
        );

        {
          std::string const &license = DCP::Resource::loadFile(
            IDR_LICENSE_FILE, IDR_FILE_TYPE
          );
          HWND const control = DCP::Window::tryToGetChild(
            tabBox->getTab(TabBoxIndex::About), IDC_LICENSE
          );
          assert(control);

          DCP::Window::setFont(control, getLicenseFont());
          DCP::Window::setText(control, license.data());
        }

        {
          HWND const saveButton = DCP::Window::tryToGetChild(window, IDOK);
          assert(saveButton);
          Button_SetElevationRequiredState(saveButton, TRUE);
        }
        return DCP::Window::postMessage(window, WM_UPDATE);
      case WM_DPICHANGED:
        {
          assert(tabBox);
          HWND const control = DCP::Window::tryToGetChild(
            tabBox->getTab(TabBoxIndex::About), IDC_LICENSE
          );
          assert(control);
          DCP::Window::setFont(control, getLicenseFont());
        }
        return TRUE;
      case WM_SETCURSOR:
        if (savingThread) {
          static HCURSOR const waitCursor = LoadCursor(NULL, IDC_WAIT);
          SetCursor(waitCursor);
          return TRUE;
        }
        break;
      case WM_UPDATE:
        assert(tabBox);
        assert(tabBox->getTab(TabBoxIndex::Keystores));
        assert(tabBox->getTab(TabBoxIndex::Screensaver));
        assert(tabBox->getTab(TabBoxIndex::Advanced));
        {
          int const item = boost::numeric_cast<int>(wParam);

          if (!item || item == IDC_IDENTITY_ADDRESS) {
            HWND const identityAddressField = DCP::Window::tryToGetChild(
              tabBox->getTab(TabBoxIndex::Keystores), IDC_IDENTITY_ADDRESS
            );
            assert(identityAddressField);

            std::shared_lock<std::shared_timed_mutex> lock{configurationMutex};
            assert(configuration);
            DCP::Keystore const *const keystore = (
              configuration->getIdentityKeystore()
            );
            DCP::Window::setText(identityAddressField,
              keystore ? keystore->getAddress().data() : ""
            );
          }

          if (!item || item == IDC_PAYMENT_ADDRESS) {
            HWND const paymentAddressField = DCP::Window::tryToGetChild(
              tabBox->getTab(TabBoxIndex::Keystores), IDC_PAYMENT_ADDRESS
            );
            assert(paymentAddressField);

            std::shared_lock<std::shared_timed_mutex> lock{configurationMutex};
            assert(configuration);
            DCP::Window::setText(
              paymentAddressField, configuration->getPaymentAddress().data()
            );
          }

          if (!item || item == IDC_COMPUTE_GROUP_LIST) {
            HWND const computeGroupList = DCP::Window::tryToGetChild(
              tabBox->getTab(TabBoxIndex::ComputeGroups), IDC_COMPUTE_GROUP_LIST
            );
            assert(computeGroupList);

            SendMessage(computeGroupList, LB_RESETCONTENT, 0, 0);

            {
              std::shared_lock<std::shared_timed_mutex> lock{
                configurationMutex
              };
              assert(configuration);

              auto const &computeGroups = configuration->getComputeGroups();
              for (auto const &computeGroup: computeGroups) {
                char const *const name = computeGroup.first.data();
                DCP::log(DCP::LogLevel::debug,
                  "Adding compute group to list: %s", name
                );
                auto result = SendMessage(
                  computeGroupList, LB_ADDSTRING, 0,
                  reinterpret_cast<LPARAM>(name)
                );
                if (result == LB_ERR) {
                  DCP::log(DCP::LogLevel::error,
                    "Could not add compute group to list: %s", name
                  );
                }
              }
            }

            DCP::Window::postMessage(window, WM_UPDATE,
              IDC_COMPUTE_GROUP_REMOVE_BUTTON
            );
          }

          if (!item || item == IDC_COMPUTE_GROUP_PUBLIC_COMBOBOX) {
            assert(publicComputeGroupComboBox);

            auto includePublicComputeGroup = (
              Configuration::ComputeGroup::IncludePublic::Always
            );
            {
              std::shared_lock<std::shared_timed_mutex> lock{
                configurationMutex
              };
              assert(configuration);
              includePublicComputeGroup = (
                configuration->getIncludePublicComputeGroup()
              );
            }
            publicComputeGroupComboBox->setSelection(
              (size_t)includePublicComputeGroup
            );
          }

          if (!item || item == IDC_COMPUTE_GROUP_REMOVE_BUTTON) {
            HWND const computeGroupRemoveButton = DCP::Window::tryToGetChild(
              tabBox->getTab(TabBoxIndex::ComputeGroups),
              IDC_COMPUTE_GROUP_REMOVE_BUTTON
            );
            assert(computeGroupRemoveButton);

            HWND const computeGroupList = DCP::Window::tryToGetChild(
              tabBox->getTab(TabBoxIndex::ComputeGroups), IDC_COMPUTE_GROUP_LIST
            );
            assert(computeGroupList);

            auto const result = SendMessage(
              computeGroupList, LB_GETSELCOUNT, 0, 0
            );
            assert(result != LB_ERR);
            if (result > 0) {
              DCP::Window::setReadOnly(computeGroupRemoveButton, false);
            } else {
              if (DCP::Window::hasFocus(computeGroupRemoveButton)) {
                DCP::Window::tryToSetFocus(computeGroupList);
              }
              DCP::Window::setReadOnly(computeGroupRemoveButton, true);
            }
          }

          if (!item || item == IDC_SCREENSAVER_PREVIEW) {
            HWND const field = DCP::Window::tryToGetChild(
              tabBox->getTab(TabBoxIndex::Screensaver), IDC_SCREENSAVER_PREVIEW
            );
            assert(field);

            std::shared_lock<std::shared_timed_mutex> lock{configurationMutex};
            assert(configuration);

            boost::optional<DCP::Color::Value> backgroundColor{
              configuration->getScreensaverBackgroundColor()
            };
            if (!backgroundColor) {
              backgroundColor = defaultBackgroundColor;
            }
            assert(backgroundColor);

            DCP::Handle<HBITMAP, nullptr> bitmap{
              configuration->getScreensaverBitmap()
            };
            if (!bitmap) {
              bitmap = defaultBitmap;
            }
            assert(bitmap);

            RECT const fieldClientRect{DCP::Window::getClientRect(field)};
            SIZE const fieldClientSize{
              fieldClientRect.right - fieldClientRect.left,
              fieldClientRect.bottom - fieldClientRect.top
            };

            DCP::Handle<HDC, nullptr> fieldDC{DCP::Graphics::getDC(field)};

            DCP::Handle<HDC, nullptr> sourceDC{
              DCP::Graphics::createCompatibleDC(fieldDC)
            };
            DCP::Handle<HGDIOBJ, nullptr> oldSourceObject{
              DCP::Graphics::selectObject(sourceDC, bitmap)
            };

            HBITMAP const targetBitmap = CreateCompatibleBitmap(sourceDC,
              fieldClientSize.cx, fieldClientSize.cy
            );
            if (!targetBitmap) {
              throw std::runtime_error("CreateCompatibleBitmap() failed");
            }

            DCP::Handle<HDC, nullptr> targetDC{
              DCP::Graphics::createCompatibleDC(fieldDC)
            };
            try {
              DCP::Handle<HGDIOBJ, nullptr> oldTargetObject{
                DCP::Graphics::selectObject(targetDC, targetBitmap)
              };

              if (
                !FillRect(targetDC, &fieldClientRect,
                  DCP::Handle<HBRUSH, nullptr>{
                    CreateSolidBrush(*backgroundColor), DeleteObject
                  }
                )
              ) {
                throw std::runtime_error("FillRect() failed");
              }

              SIZE const bitmapSize = DCP::Bitmap::getSize(bitmap);
              SIZE const targetBitmapSize = DCP::Graphics::scaleToFit(
                bitmapSize, fieldClientSize, 0.75
              );
              DCP::Graphics::setBitmapStretchMode(targetDC, HALFTONE);
              if (
                !StretchBlt(
                  targetDC,
                  (fieldClientSize.cx / 2) - (targetBitmapSize.cx / 2),
                  (fieldClientSize.cy / 2) - (targetBitmapSize.cy / 2),
                  targetBitmapSize.cx, targetBitmapSize.cy,
                  sourceDC, 0, 0, bitmapSize.cx, bitmapSize.cy,
                  SRCCOPY
                )
              ) {
                throw std::runtime_error("StretchBlt() failed");
              }
              DCP::log(DCP::LogLevel::debug,
                "Stretch-blitted the %li x %li bitmap to %li x %li",
                bitmapSize.cx, bitmapSize.cy,
                targetBitmapSize.cx, targetBitmapSize.cy
              );
            } catch (...) {
              DeleteObject(targetBitmap);
              throw;
            }
            DCP::Window::StaticControl::setBitmap(field, targetBitmap);
          }

          if (!item || item == IDC_ADJUST_POWER_SETTINGS_CHECKBOX) {
            auto const adjustPowerSettingsCheckBox = DCP::Window::tryToGetChild(
              tabBox->getTab(TabBoxIndex::Screensaver),
              IDC_ADJUST_POWER_SETTINGS_CHECKBOX
            );
            assert(adjustPowerSettingsCheckBox);
            assert(adjustPowerSettingsDialog);
            DCP::Window::setChecked(adjustPowerSettingsCheckBox,
              !adjustPowerSettingsDialog->getDismissed()
            );
          }

          if (!item || item == IDC_SCHEDULER_URL) {
            HWND const schedulerUrlField = DCP::Window::tryToGetChild(
              tabBox->getTab(TabBoxIndex::Advanced), IDC_SCHEDULER_URL
            );
            assert(schedulerUrlField);

            std::shared_lock<std::shared_timed_mutex> lock{configurationMutex};
            assert(configuration);
            DCP::Window::setText(
              schedulerUrlField, configuration->getSchedulerUrl().data()
            );
          }

          if (!item || item == IDC_EVALUATOR_WEBGPU_CHECKBOX) {
            HWND const evaluatorWebGPUCheckBox = DCP::Window::tryToGetChild(
              tabBox->getTab(TabBoxIndex::Advanced),
              IDC_EVALUATOR_WEBGPU_CHECKBOX
            );
            assert(evaluatorWebGPUCheckBox);

            std::shared_lock<std::shared_timed_mutex> lock{configurationMutex};
            assert(configuration);
            DCP::Window::setChecked(
              evaluatorWebGPUCheckBox, configuration->getEvaluatorWebGPU()
            );
          }

          if (!item || item == IDC_LOGGING_SLIDER) {
            std::shared_lock<std::shared_timed_mutex> lock{configurationMutex};
            assert(configuration);
            int const logLevel = configuration->getMaxSystemLogLevel();
            uint16_t const loggingSliderPosition = (
              logLevel < 0 ? 0 : boost::numeric_cast<uint16_t>(logLevel + 1)
            );
            assert(loggingSlider);
            loggingSlider->setPosition(loggingSliderPosition);
          }
        }
        return TRUE;
      case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case IDOK:
          assert(tabBox);
          assert(tabBox->getTab(TabBoxIndex::Keystores));
          assert(tabBox->getTab(TabBoxIndex::Screensaver));
          assert(tabBox->getTab(TabBoxIndex::Advanced));
          {
            assert(!installDirectoryPath.empty());
            assert(boost::filesystem::is_directory(installDirectoryPath));

            static DCP::Time::Milliseconds const timeout = (
              10 * DCP::Time::millisecondsPerSecond
            );

            HWND const paymentAddressField = DCP::Window::tryToGetChild(
              tabBox->getTab(TabBoxIndex::Keystores), IDC_PAYMENT_ADDRESS
            );
            assert(paymentAddressField);
            std::string enteredPaymentAddress = DCP::Window::getText(
              paymentAddressField
            );
            if (!DCP::Window::isReadOnly(paymentAddressField)) {
              DCP::log(DCP::LogLevel::debug,
                "Payment address text entered: %s", enteredPaymentAddress.data()
              );
              if (enteredPaymentAddress.empty()) {
                tabBox->selectTab(TabBoxIndex::Keystores);
                if (revisitMissingPayment(window)) {
                  DCP::Window::tryToSetFocus(paymentAddressField);
                  return TRUE;
                }
              } else if (
                !DCP::Keystore::validateAddress(enteredPaymentAddress)
              ) {
                tabBox->selectTab(TabBoxIndex::Keystores);
                DCP::Window::MessageDialog{MB_OK | MB_ICONERROR}.show(window,
                  DCP::Resource::loadString(
                    IDS_KEYSTORE_ADDRESS_INVALID_CAPTION
                  ).data(),
                  DCP::Resource::loadString(IDS_KEYSTORE_ADDRESS_MESSAGE).data()
                );
                DCP::Window::tryToSetFocus(paymentAddressField);
                return TRUE;
              }
            }

            HWND const schedulerUrlField = DCP::Window::tryToGetChild(
              tabBox->getTab(TabBoxIndex::Advanced), IDC_SCHEDULER_URL
            );
            assert(schedulerUrlField);
            std::string enteredSchedulerUrl = DCP::Window::getText(
              schedulerUrlField
            );
            if (!DCP::Window::isReadOnly(schedulerUrlField)) {
              DCP::log(DCP::LogLevel::debug,
                "Scheduler URL text entered: %s", enteredSchedulerUrl.data()
              );
              if (
                !enteredSchedulerUrl.empty() &&
                !Configuration::validateUrl(enteredSchedulerUrl)
              ) {
                tabBox->selectTab(TabBoxIndex::Advanced);
                DCP::Window::MessageDialog{MB_OK | MB_ICONERROR}.show(window,
                  DCP::Resource::loadString(IDS_URL_INVALID_CAPTION).data(),
                  DCP::Resource::loadString(IDS_URL_MESSAGE).data()
                );
                DCP::Window::tryToSetFocus(schedulerUrlField);
                return TRUE;
              }
            }

            HWND const evaluatorWebGPUField = DCP::Window::tryToGetChild(
              tabBox->getTab(TabBoxIndex::Advanced),
              IDC_EVALUATOR_WEBGPU_CHECKBOX
            );
            assert(evaluatorWebGPUField);
            bool const enteredEvaluatorWebGPU = DCP::Window::getChecked(
              evaluatorWebGPUField
            );

            assert(loggingSlider);
            uint16_t const loggingSliderPosition{loggingSlider->getPosition()};
            int const enteredMaxSystemLogLevel{
              boost::numeric_cast<int>(loggingSliderPosition) - 1
            };

            assert(publicComputeGroupComboBox);
            auto const includePublicComputeGroup = static_cast<
              Configuration::ComputeGroup::IncludePublic
            >(publicComputeGroupComboBox->getSelection());
            if (
              includePublicComputeGroup ==
              Configuration::ComputeGroup::IncludePublic::Never
            ) {
              HWND const computeGroupList = DCP::Window::tryToGetChild(
                tabBox->getTab(TabBoxIndex::ComputeGroups),
                IDC_COMPUTE_GROUP_LIST
              );
              assert(computeGroupList);

              auto const result = SendMessage(
                computeGroupList, LB_GETCOUNT, 0, 0
              );
              assert(result != LB_ERR);
              size_t const computeGroupCount = result;
              if (computeGroupCount == 0) {
                tabBox->selectTab(TabBoxIndex::ComputeGroups);
                DCP::Window::MessageDialog{MB_OK | MB_ICONERROR}.show(window,
                  DCP::Resource::loadString(
                    IDS_COMPUTE_GROUP_REQUIRED_CAPTION
                  ).data(),
                  DCP::Resource::loadString(
                    IDS_COMPUTE_GROUP_REQUIRED_MESSAGE
                  ).data()
                );
                DCP::Window::tryToSetFocus(*publicComputeGroupComboBox);
                return TRUE;
              }
            }

            {
              std::lock_guard<std::shared_timed_mutex> lock{configurationMutex};
              assert(configuration);

              if (
                !configuration->setSchedulerUrl(std::move(enteredSchedulerUrl))
              ) {
                assert(false);
              }

              if (
                !configuration->setPaymentAddress(
                  std::move(enteredPaymentAddress)
                )
              ) {
                assert(false);
              }

              configuration->setEvaluatorWebGPU(enteredEvaluatorWebGPU);

              configuration->setMaxSystemLogLevel(enteredMaxSystemLogLevel);

              configuration->setIncludePublicComputeGroup(
                includePublicComputeGroup
              );
            }

            {
              HWND const adjustPowerSettingsCheckBox{
                DCP::Window::tryToGetChild(
                  tabBox->getTab(TabBoxIndex::Screensaver),
                  IDC_ADJUST_POWER_SETTINGS_CHECKBOX
                )
              };
              assert(adjustPowerSettingsCheckBox);
              assert(adjustPowerSettingsDialog);
              adjustPowerSettingsDialog->setDismissed(
                !DCP::Window::getChecked(adjustPowerSettingsCheckBox)
              );
            }

            DCP::Window::setReadOnly(window, true);
            assert(!savingThread);
            savingThread.emplace(
              [window]() noexcept {
                WPARAM wParam{};
                LPARAM lParam{};
                try {
                  std::lock_guard<std::shared_timed_mutex> lock{
                    configurationMutex
                  };
                  assert(configuration);
                  wParam = configuration->save(
                    installDirectoryPath, DCP_serviceName, timeout
                  );
                } catch (...) {
                  DCP::tryToLog(DCP::LogLevel::error,
                    "Settings configurator execution failed: %s",
                    boost::current_exception_diagnostic_information().data()
                  );
                  lParam = 1;
                }
                DCP::Window::postMessage(window, WM_SAVED, wParam, lParam);
              }
            );
          }
          return TRUE;
        case IDCANCEL:
          return DCP::Window::postMessage(window, WM_CLOSE);
        }
        break;
      case WM_SAVED:
        savingThread.reset();
        DCP::Window::setReadOnly(window, false);
        if (lParam || wParam) {
          std::string errorMessage = DCP::Resource::loadString(
            IDS_CONFIGURE_ERROR_INTRO_MESSAGE
          );
          errorMessage += "\n\n";

          if (lParam) {
            errorMessage += DCP::Resource::loadString(
              IDS_CONFIGURE_FAILURE_MESSAGE
            );
            errorMessage += '\n';
          } else if (wParam) {
            DCP::log(DCP::LogLevel::warning,
              "Settings configurator returned non-zero exit code: %lu",
              static_cast<long unsigned>(wParam)
            );

            // Log configurator errors that occur before its logger is set up.
            if (wParam & Configurator::ExitCode::argumentError) {
              DCP::log(DCP::LogLevel::error,
                "A required configurator argument was missing or invalid."
              );
            }

            // Pop any user-facing errors and add them to the error message.
            if (wParam & Configurator::ExitCode::fileCopyError) {
              wParam &= ~Configurator::ExitCode::fileCopyError;
              errorMessage += DCP::Resource::loadString(
                IDS_CONFIGURE_BITMAP_WRITE_ERROR_MESSAGE
              );
              errorMessage += '\n';
            }
            if (wParam & Configurator::ExitCode::settingWriteError) {
              wParam &= ~Configurator::ExitCode::settingWriteError;
              errorMessage += DCP::Resource::loadString(
                IDS_CONFIGURE_SETTING_WRITE_ERROR_MESSAGE
              );
              errorMessage += '\n';
            }
            if (wParam & Configurator::ExitCode::settingReadError) {
              wParam &= ~Configurator::ExitCode::settingReadError;
              errorMessage += DCP::Resource::loadString(
                IDS_CONFIGURE_SETTING_READ_ERROR_MESSAGE
              );
              errorMessage += '\n';
            }
            if (wParam & Configurator::ExitCode::screensaverUpdateError) {
              wParam &= ~Configurator::ExitCode::screensaverUpdateError;
              errorMessage += DCP::Resource::loadString(
                IDS_CONFIGURE_SCREENSAVER_UPDATE_ERROR_MESSAGE
              );
              errorMessage += '\n';
            }
            if (wParam & Configurator::ExitCode::serviceRestartError) {
              wParam &= ~Configurator::ExitCode::serviceRestartError;
              errorMessage += boost::str(
                boost::format(
                  DCP::Resource::loadString(
                    IDS_CONFIGURE_SERVICE_RESTART_ERROR_MESSAGE
                  )
                ) % DCP_serviceName
              );
              errorMessage += '\n';
            }
            if (wParam) {
              // Add an "internal error" catch-all for any additional,
              // non-user-facing errors.
              errorMessage += DCP::Resource::loadString(
                IDS_CONFIGURE_INTERNAL_ERROR_MESSAGE
              );
              errorMessage += '\n';
            }
          }

          errorMessage += '\n';
          errorMessage += DCP::Resource::loadString(
            IDS_CONFIGURE_ERROR_OUTRO_MESSAGE
          );

          SetForegroundWindow(window);
          DCP::Window::MessageDialog{MB_OK | MB_ICONERROR}.show(window,
            DCP::Resource::loadString(IDS_CONFIGURE_ERROR_CAPTION).data(),
            errorMessage.data()
          );
          return TRUE;
        }
        DCP::log(DCP::LogLevel::info, "Configurator executed successfully");
        assert(adjustPowerSettingsDialog);
        if (!checkPowerSettings(window, *adjustPowerSettingsDialog)) {
          DCP::Resource::closeModalDialog(window, TRUE);
        }
        return TRUE;
      case WM_CLOSE:
        assert(tabBox);
        assert(tabBox->getTab(TabBoxIndex::Keystores));
        if (
          Tree::get().first.tryToGet(
            Configuration::paymentAddressPath, Configuration::paymentAddressName
          ).first.empty()
        ) {
          tabBox->selectTab(TabBoxIndex::Keystores);
          if (revisitMissingPayment(window)) {
            DCP::Window::tryToSetFocus(
              DCP::Window::tryToGetChild(
                tabBox->getTab(TabBoxIndex::Keystores), IDC_PAYMENT_ADDRESS
              )
            );
            return TRUE;
          }
        }
        assert(adjustPowerSettingsDialog);
        if (!checkPowerSettings(window, *adjustPowerSettingsDialog)) {
          DCP::Resource::closeModalDialog(window, FALSE);
        }
        return TRUE;
      case WM_DESTROY:
        assert(tabBox);
        assert(tabBox->getTab(TabBoxIndex::Screensaver));
        savingThread.reset();
        {
          HWND const screensaverPreview = DCP::Window::tryToGetChild(
            tabBox->getTab(TabBoxIndex::Screensaver), IDC_SCREENSAVER_PREVIEW
          );
          assert(screensaverPreview);
          DCP::Window::StaticControl::setBitmap(screensaverPreview);
        }
        loggingSlider.reset();
        tabBox.reset();
        icon = {};
        configuration.reset();
        // Shut down and cleanly destruct static objects.
        // Without this, destructors of statics are not called.
        std::exit(EXIT_SUCCESS);
      default:
        if (tabBox && tabBox->handleMessage(message, wParam, lParam)) {
          return TRUE;
        }
      }
    } catch (...) {
      logger.tryToLog(DCP::LogLevel::error,
        "Exception in ScreenSaverConfigureDialog: %s",
        boost::current_exception_diagnostic_information().data()
      );
      failWithMessage(window);
    }
  } catch (...) {
    assert(false);
    failWithMessage(window);
  }
  return FALSE;
}

#endif
