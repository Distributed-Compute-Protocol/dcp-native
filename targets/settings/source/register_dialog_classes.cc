/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2021
 */

#if BOOST_OS_WINDOWS

  #include <windows.h> // Include before other windows headers
  #include <scrnsave.h>

extern "C" BOOL WINAPI RegisterDialogClasses(HANDLE const) {
  return TRUE;
}

#endif
