/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2021
 */

#if BOOST_OS_WINDOWS

  #include "resource.h"

  #include <dcp/bitmap.hh>
  #include <dcp/logger.hh>
  #include <dcp/paths.hh>
  #include <dcp/resource.hh>
  #include <dcp/sandbox.hh>
  #include <dcp/settings/configuration.hh>
  #include <dcp/window/screensaver.hh>

  #include <boost/exception/diagnostic_information.hpp>
  #include <boost/filesystem/operations.hpp>

  #include <windows.h> // Include before other windows headers
  #include <scrnsave.h>

using namespace DCP::Settings;

extern "C" LRESULT WINAPI ScreenSaverProc(
  HWND const window,
  UINT const message,
  WPARAM const wParam,
  LPARAM const lParam
) {
  try {
    // Ensure static logger is instantiated before trying to log.
    // Turn off stderr logging, which causes problems for the screensaver.
    static auto const &executablePath = DCP::Paths::getExecutablePath();
    static auto &logger = DCP::Logger::getInstance(
      executablePath.stem().string(), -1
    );
    try {
      static int unsigned const shutdownTimeoutSeconds{1U};

      static boost::optional<DCP::Sandbox> sandbox{};
      static boost::optional<DCP::Window::Screensaver> screensaver{};

      switch (message) {
      case WM_CREATE:
        assert(boost::filesystem::exists(executablePath));
        {
          Configuration const configuration{};

          logger.setMaxSystemLogLevel(configuration.getMaxSystemLogLevel());
          logger.log(DCP::LogLevel::info, "Running \"%s\", version %s...",
            executablePath.filename().string().data(),
            DCP_version
          );

          if (fChildPreview) {
            logger.log(DCP::LogLevel::info,
              "Preview mode; skipping evaluation."
            );
          } else {
            static auto const installDirectoryPath = (
              DCP::Paths::getInstallDirectoryPath(executablePath.parent_path())
            );
            assert(!installDirectoryPath.empty());
            assert(boost::filesystem::is_directory(installDirectoryPath));

            sandbox.emplace(installDirectoryPath,
              std::vector<std::string>{
                "--shutdown-timeout", std::to_string(shutdownTimeoutSeconds)
              }
            );
          }

          boost::optional<DCP::Color::Value> backgroundColor{
            configuration.getScreensaverBackgroundColor()
          };
          if (!backgroundColor) {
            backgroundColor = DCP::Resource::loadColor(
              IDR_BACKGROUND_COLOR, IDR_COLOR_TYPE
            );
          }
          assert(backgroundColor);

          DCP::Handle<HBITMAP, nullptr> bitmap{
            configuration.getScreensaverBitmap()
          };
          if (!bitmap) {
            bitmap = DCP::Resource::loadBitmap(IDB_BITMAP);
          }
          assert(bitmap);

          DCP::Time::Milliseconds const timeIncrement = 100;
          double const angleIncrement = 0.05;

          assert(!screensaver);
          screensaver.emplace(
            window, *backgroundColor, bitmap, timeIncrement, angleIncrement
          );
        }
        return 0;
      case WM_DESTROY:
        logger.tryToLog(DCP::LogLevel::debug, "Resetting screensaver...");
        screensaver.reset();
        logger.tryToLog(DCP::LogLevel::debug, "Screensaver reset");

        logger.tryToLog(DCP::LogLevel::debug, "Resetting sandboxes...");
        if (sandbox) {
          auto &process = sandbox->getProcess();
          process.interrupt();
          process.wait();
          sandbox.reset();
        }
        logger.tryToLog(DCP::LogLevel::debug, "Sandboxes reset");

        // Shut down and cleanly destruct static objects.
        // Without this, destructors of statics are not called.
        logger.tryToLog(DCP::LogLevel::info,
          "Screensaver stopped; shutting down process gracefully."
        );
        std::exit(EXIT_SUCCESS);
      default:
        if (
          screensaver && screensaver->handleMessage(message, wParam, lParam)
        ) {
          return 0;
        }
        return DefScreenSaverProc(window, message, wParam, lParam);
      }
    } catch (...) {
      logger.tryToLog(DCP::LogLevel::error, "Exception in ScreenSaverProc: %s",
        boost::current_exception_diagnostic_information().data()
      );
      std::exit(EXIT_FAILURE);
    }
  } catch (...) {
    assert(false);
    std::exit(EXIT_FAILURE);
  }
}

#endif
