/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       November 2023
 */

#include <dcp/service.hh>

#if BOOST_OS_WINDOWS

#include <dcp/logger.hh>
#include <dcp/process.hh>

#include <boost/exception/diagnostic_information.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>

namespace DCP {

  namespace {

    bool commandService(
      boost::filesystem::path const &serviceManagerPath,
      char const *const serviceName,
      char const *const commandName,
      boost::optional<Time::Milliseconds> const timeout = {}
    ) {
      assert(boost::filesystem::exists(serviceManagerPath));
      assert(serviceName && (*serviceName != '\0'));
      assert(commandName && (*commandName != '\0'));

      log(LogLevel::debug, "Commanding service '%s' to %s",
        serviceName, commandName
      );
      try {
        auto const exitCode = Process::command(
          serviceManagerPath.string().data(), {commandName, serviceName},
          timeout, true, false
        );
        if (exitCode != EXIT_SUCCESS) {
          tryToLog(LogLevel::error,
            "Commanding service '%s' to %s returned exit code %lu",
            serviceName, commandName, exitCode
          );
          return false;
        }
      } catch (...) {
        tryToLog(LogLevel::error, "Error commanding service '%s' to %s: %s",
          serviceName, commandName,
          boost::current_exception_diagnostic_information().data()
        );
        return false;
      }
      return true;
    }

  }

  Service::Service(
    boost::filesystem::path const &installDirectoryPath,
    std::string name
  ) :
  managerPath{installDirectoryPath / "nssm.exe"},
  name{std::move(name)}
  {
    assert(boost::filesystem::is_directory(installDirectoryPath));

    if (!boost::filesystem::exists(this->managerPath)) {
      throw std::invalid_argument(
        boost::str(
          boost::format("The service manager program \"%s\" does not exist.")
          % this->managerPath.string()
        )
      );
    }
  }

  bool Service::restart(boost::optional<Time::Milliseconds> const timeout) {
    return commandService(managerPath, name.data(), "restart", timeout);
  }

}

#endif
