/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       November 2023
 */

#if !defined(DCP_Service_)
  #define DCP_Service_

  #if BOOST_OS_WINDOWS

    #include <dcp/time.hh>

    #include <boost/filesystem/path.hpp>
    #include <boost/optional.hpp>

namespace DCP {

  struct Service final {

    explicit Service(
      boost::filesystem::path const &installDirectoryPath,
      std::string name
    );

    bool restart(boost::optional<Time::Milliseconds> timeout = {});

  private:

    boost::filesystem::path managerPath;

    std::string name;

  };

}

  #endif

#endif
