# Copyright (c) 2022 Distributive Corp. All Rights Reserved.

set(DCP_UNINSTALLER_BUNDLE_DISPLAY_NAME "Uninstall DCP Worker")

set(DCP_UNINSTALLER_BUNDLE_DISPLAY_OK "OK")

set(DCP_UNINSTALLER_BUNDLE_DISPLAY_PROMPT
  "This application will uninstall DCP Worker."
)

set(DCP_UNINSTALLER_BUNDLE_DISPLAY_PROGRESS_DESCRIPTION
  "Uninstalling DCP Worker..."
)
set(DCP_UNINSTALLER_BUNDLE_DISPLAY_PROGRESS_DESCRIPTION_COMPONENTS
  "Removing components..."
)
set(DCP_UNINSTALLER_BUNDLE_DISPLAY_PROGRESS_DESCRIPTION_FILES
  "Deleting files and directories..."
)
set(DCP_UNINSTALLER_BUNDLE_DISPLAY_PROGRESS_DESCRIPTION_SERVICE
  "Stopping service..."
)
set(DCP_UNINSTALLER_BUNDLE_DISPLAY_PROGRESS_DESCRIPTION_SHORTCUTS
  "Deleting shortcuts..."
)

string(
  CONCAT DCP_UNINSTALLER_BUNDLE_DISPLAY_COMPLETION_ERROR
  "The installation may now be in an inconsistent state."
  " Either re-run the uninstaller, or re-install the software."
)
set(DCP_UNINSTALLER_BUNDLE_DISPLAY_COMPLETION_ERROR_CANCELLED
  "DCP Worker uninstallation was interrupted by the user."
)
set(DCP_UNINSTALLER_BUNDLE_DISPLAY_COMPLETION_ERROR_UNKNOWN
  "There was an error uninstalling DCP Worker"
)
