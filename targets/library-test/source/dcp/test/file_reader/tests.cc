/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       April 2021
 */

#include <dcp/file_reader.hh>
#include <dcp/test.hh>

#include <boost/exception/diagnostic_information.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/format.hpp>
#include <boost/thread/scoped_thread.hpp>

#if BOOST_OS_UNIX || BOOST_OS_MACOS
  #include <sys/file.h>
  #include <sys/stat.h>
#endif

#include <condition_variable>
#include <fstream>
#include <thread>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(FileReader)

  namespace {

    // NOLINTNEXTLINE(performance-enum-size)
    enum struct Success : int {
      indeterminate = -1,
      no,
      yes
    };

    struct TemporaryFile final {

      explicit TemporaryFile(
        std::string contents = "",
        bool const readOnly = false
      ) :
      path(
        boost::filesystem::temp_directory_path() /
        boost::filesystem::unique_path()
      ),
      readOnly(readOnly),
      contents(std::move(contents))
      {
        {
          std::ofstream out{path.native()};
          out << this->contents;
          out.flush();
        }

        if (readOnly) {
          boost::filesystem::permissions(path,
            boost::filesystem::perms::remove_perms | readOnlyPermissions
          );
        }
      }

      TemporaryFile(TemporaryFile const &) = delete;
      TemporaryFile(TemporaryFile const &&) = delete;

      TemporaryFile &operator =(TemporaryFile const &) = delete;
      TemporaryFile &operator =(TemporaryFile const &&) = delete;

      ~TemporaryFile() {
        try {
          boost::filesystem::permissions(path,
            boost::filesystem::perms::add_perms | readOnlyPermissions
          );

          if (!boost::filesystem::remove(path)) {
            BOOST_FAIL(
              boost::str(
                boost::format("Failed to delete temporary file %s") % path
              )
            );
          }
        } catch (...) {
          BOOST_FAIL(
            boost::str(
              boost::format("Error deleting temporary file %s: %s")
              % path
              % boost::current_exception_diagnostic_information()
            )
          );
        }
      }

      [[nodiscard]]
      boost::filesystem::path const &getPath() const {
        return path;
      }

      [[nodiscard]]
      bool isReadOnly() const {
        return readOnly;
      }

      [[nodiscard]]
      std::string const &getContents() const {
        return contents;
      }

    private:

      static auto const readOnlyPermissions = (
        boost::filesystem::perms::owner_write |
        boost::filesystem::perms::others_write |
        boost::filesystem::perms::group_write
      );

      boost::filesystem::path const path;
      bool const readOnly;
      std::string const contents;

    };

    /** Sleep for approximately the specified duration, updated to the actual
     *  duration.
     *
     *  Because std::this_thread::sleep_for() sometimes sleeps for a bit less
     *  than the full duration, set the duration to the actual time slept.
     */
    void sleepFor(std::chrono::milliseconds &duration) {
      auto const start = std::chrono::steady_clock::now();
      std::this_thread::sleep_for(duration);
      auto const end = std::chrono::steady_clock::now();
      duration = std::chrono::duration_cast<std::chrono::milliseconds>(
        end - start
      );
    }
  }

  BOOST_AUTO_TEST_SUITE(tests)

  /// Confirm that FileReader works on a read-only file.
  BOOST_AUTO_TEST_CASE(readOnly, *boost::unit_test::timeout(timeout)) {
    TemporaryFile const temporaryFile{"Test", true};

    DCP::FileReader fileReader{temporaryFile.getPath()};
    BOOST_CHECK_EQUAL(
      std::string{fileReader.getData()},
      temporaryFile.getContents()
    );
    BOOST_CHECK_EQUAL(fileReader.getSize(), temporaryFile.getContents().size());
  }

  /// Confirm that FileReader does not wait on a shared lock.
  BOOST_AUTO_TEST_CASE(sharedLock, *boost::unit_test::timeout(timeout)) {
    std::chrono::milliseconds duration{1000};
    TemporaryFile const temporaryFile{"Test"};
    Success success{Success::indeterminate};
    std::string failure{};
    std::chrono::steady_clock::time_point start{};
    std::chrono::steady_clock::time_point end{};
    {
      std::mutex mutex{};
      std::condition_variable cv{};

      boost::strict_scoped_thread<> thread(
        [
          &temporaryFile, &success, &failure, &mutex, &cv, &start, &duration
        ]() noexcept {
          try {
            DCP::FileReader fileReader{temporaryFile.getPath()};
            {
              std::lock_guard<std::mutex> lock(mutex);
              success = Success::yes;
            }
            start = std::chrono::steady_clock::now();
            cv.notify_one();
            sleepFor(duration);
          } catch (...) {
            std::lock_guard<std::mutex> lock(mutex);
            failure = boost::str(
              boost::format("Error for file %s: %s")
              % temporaryFile.getPath()
              % boost::current_exception_diagnostic_information()
            );
            success = Success::no;
          }
        }
      );

      {
        std::unique_lock<std::mutex> lock(mutex);
        cv.wait(lock,
          [&success]() {
            return success != Success::indeterminate;
          }
        );
        BOOST_REQUIRE_MESSAGE(success == Success::yes, failure);
      }

      DCP::FileReader fileReader{temporaryFile.getPath()};
      end = std::chrono::steady_clock::now();

      BOOST_CHECK_EQUAL(
        std::string{fileReader.getData()}, temporaryFile.getContents()
      );
      BOOST_CHECK_EQUAL(
        fileReader.getSize(), temporaryFile.getContents().size()
      );
    }
    BOOST_CHECK_MESSAGE(success == Success::yes, failure);

    BOOST_REQUIRE(start <= end);
    auto const elapsed = std::chrono::duration_cast<
      std::chrono::milliseconds
    >(end - start);
    BOOST_CHECK_LT(elapsed.count(), duration.count());
  }

  /// Confirm that FileReader waits until an exclusive lock completes.
  BOOST_AUTO_TEST_CASE(exclusiveLock, *boost::unit_test::timeout(timeout)) {
    std::chrono::milliseconds duration{1000};
    TemporaryFile const temporaryFile{"Test"};
    Success success{Success::indeterminate};
    std::string failure{};
    std::chrono::steady_clock::time_point start{};
    std::chrono::steady_clock::time_point end{};
    {
      std::mutex mutex{};
      std::condition_variable cv{};

      boost::strict_scoped_thread<> thread(
        [
          &temporaryFile, &success, &failure, &mutex, &cv, &start, &duration
        ]() noexcept {
          try {

#if BOOST_OS_UNIX || BOOST_OS_MACOS

            // Open the file.
            errno = 0;
            DCP::Handle<int, -1> const handle(
              open(
                temporaryFile.getPath().string().data(), O_WRONLY | O_CLOEXEC
              ),
              close
            );
            if (handle == -1) {
              std::lock_guard<std::mutex> lock(mutex);
              failure = boost::str(
                boost::format("open() failed for file %s: %s")
                % temporaryFile.getPath()
                % std::error_code(errno, std::system_category()).message()
              );
              success = Success::no;
              return;
            }

            // Lock the file.
            errno = 0;
            if (flock(handle, LOCK_EX | LOCK_NB) != 0) {
              {
                std::lock_guard<std::mutex> lock(mutex);
                failure = boost::str(
                  boost::format("flock() failed for file %s: %s")
                  % temporaryFile.getPath()
                  % std::error_code(errno, std::system_category()).message()
                );
                success = Success::no;
              }
              cv.notify_one();
              return;
            }

#elif BOOST_OS_WINDOWS

            // Open the file with an exclusive lock.
            SetLastError(ERROR_SUCCESS);
            DCP::Handle<HANDLE, INVALID_HANDLE_VALUE> const handle(
              CreateFile(temporaryFile.getPath().string().data(),
                GENERIC_WRITE, 0, NULL, OPEN_EXISTING, NULL, NULL
              ),
              CloseHandle
            );
            if (handle == INVALID_HANDLE_VALUE) {
              {
                std::lock_guard<std::mutex> lock(mutex);
                failure = boost::str(
                  boost::format("CreateFile() failed for file %s: %s")
                  % temporaryFile.getPath()
                  % std::error_code(
                    GetLastError(), std::system_category()
                  ).message()
                );
                success = Success::no;
              }
              cv.notify_one();
              return;
            }

#else
  #error Platform not supported.
#endif

            {
              std::lock_guard<std::mutex> lock(mutex);
              success = Success::yes;
            }
            start = std::chrono::steady_clock::now();
            cv.notify_one();
            sleepFor(duration);
          } catch (...) {
            {
              std::lock_guard<std::mutex> lock(mutex);
              failure = boost::str(
                boost::format("Error for file %s: %s")
                % temporaryFile.getPath()
                % boost::current_exception_diagnostic_information()
              );
              success = Success::no;
            }
            cv.notify_one();
          }
        }
      );

      {
        std::unique_lock<std::mutex> lock(mutex);
        cv.wait(lock,
          [&success]() {
            return success != Success::indeterminate;
          }
        );
        BOOST_REQUIRE_MESSAGE(success == Success::yes, failure);
      }

      DCP::FileReader fileReader{temporaryFile.getPath()};
      end = std::chrono::steady_clock::now();

      BOOST_CHECK_EQUAL(
        std::string{fileReader.getData()}, temporaryFile.getContents()
      );
      BOOST_CHECK_EQUAL(
        fileReader.getSize(), temporaryFile.getContents().size()
      );
    }
    BOOST_CHECK_MESSAGE(success == Success::yes, failure);

    BOOST_REQUIRE(start <= end);
    auto const elapsed = std::chrono::duration_cast<
      std::chrono::milliseconds
    >(end - start);
    BOOST_CHECK_GE(elapsed.count(), duration.count());
  }

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
