/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#include <dcp/number.hh>
#include <dcp/test.hh>

#include <cstdint>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Number)
  BOOST_AUTO_TEST_SUITE(clampTests)

  BOOST_AUTO_TEST_CASE(minUnsignedToSigned,
    *boost::unit_test::timeout(timeout)
  ) {
    using From = uint64_t;
    using To = int64_t;
    constexpr auto from = std::numeric_limits<From>::lowest();
    auto const to = DCP::Number::clamp<To>(from);
    BOOST_CHECK_EQUAL(to, 0);
  }

  BOOST_AUTO_TEST_CASE(minSignedToUnsigned,
    *boost::unit_test::timeout(timeout)
  ) {
    using From = int64_t;
    using To = uint64_t;
    constexpr auto from = std::numeric_limits<From>::lowest();
    auto const to = DCP::Number::clamp<To>(from);
    BOOST_CHECK_EQUAL(to, std::numeric_limits<To>::lowest());
  }

  BOOST_AUTO_TEST_CASE(minSignedToNarrowerSigned,
    *boost::unit_test::timeout(timeout)
  ) {
    using From = int64_t;
    using To = int8_t;
    constexpr auto from = std::numeric_limits<From>::lowest();
    auto const to = DCP::Number::clamp<To>(from);
    BOOST_CHECK_EQUAL(to, std::numeric_limits<To>::lowest());
  }

  BOOST_AUTO_TEST_CASE(minSignedToWiderSigned,
    *boost::unit_test::timeout(timeout)
  ) {
    using From = int8_t;
    using To = int64_t;
    constexpr auto from = std::numeric_limits<From>::lowest();
    auto const to = DCP::Number::clamp<To>(from);
    BOOST_CHECK_EQUAL(to, from);
  }

  BOOST_AUTO_TEST_CASE(maxToNarrower, *boost::unit_test::timeout(timeout)) {
    using From = uint64_t;
    using To = uint8_t;
    constexpr auto from = std::numeric_limits<From>::max();
    auto const to = DCP::Number::clamp<To>(from);
    BOOST_CHECK_EQUAL(to, std::numeric_limits<To>::max());
  }

  BOOST_AUTO_TEST_CASE(maxToWider, *boost::unit_test::timeout(timeout)) {
    using From = uint8_t;
    using To = uint64_t;
    constexpr auto from = std::numeric_limits<From>::max();
    auto const to = DCP::Number::clamp<To>(from);
    BOOST_CHECK_EQUAL(to, from);
  }

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
