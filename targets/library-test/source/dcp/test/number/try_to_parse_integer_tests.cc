/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#include <dcp/number.hh>
#include <dcp/test.hh>

#include <cstdint>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Number)
  BOOST_AUTO_TEST_SUITE(tryToParseIntegerTests)

  BOOST_AUTO_TEST_CASE(minimum, *boost::unit_test::timeout(timeout)) {
    auto string = std::to_string(std::numeric_limits<char signed>::lowest());
    char signed charSigned{0};
    BOOST_CHECK(DCP::Number::tryToParseInteger(string, charSigned));
    BOOST_CHECK_EQUAL(charSigned, std::numeric_limits<char signed>::lowest());
    char unsigned charUnsigned{0};
    BOOST_CHECK(DCP::Number::tryToParseInteger(string, charUnsigned));
    BOOST_CHECK_EQUAL(charUnsigned, 0);
    string.push_back('0');
    BOOST_CHECK(DCP::Number::tryToParseInteger(string, charSigned));
    BOOST_CHECK_EQUAL(charSigned, std::numeric_limits<char signed>::lowest());
    BOOST_CHECK(DCP::Number::tryToParseInteger(string, charUnsigned));
    BOOST_CHECK_EQUAL(charUnsigned, 0);
    intmax_t signedInteger{0};
    BOOST_CHECK(DCP::Number::tryToParseInteger(string, signedInteger));
    BOOST_CHECK_EQUAL(signedInteger,
      std::numeric_limits<char signed>::lowest() * 10
    );
  }

  BOOST_AUTO_TEST_CASE(maximum, *boost::unit_test::timeout(timeout)) {
    auto string = std::to_string(std::numeric_limits<char unsigned>::max());
    char unsigned charUnsigned{0};
    BOOST_CHECK(DCP::Number::tryToParseInteger(string, charUnsigned));
    BOOST_CHECK_EQUAL(charUnsigned, std::numeric_limits<char unsigned>::max());
    char signed charSigned{0};
    BOOST_CHECK(DCP::Number::tryToParseInteger(string, charSigned));
    BOOST_CHECK_EQUAL(charSigned, std::numeric_limits<char signed>::max());
    string.push_back('0');
    BOOST_CHECK(DCP::Number::tryToParseInteger(string, charUnsigned));
    BOOST_CHECK_EQUAL(charUnsigned, std::numeric_limits<char unsigned>::max());
    BOOST_CHECK(DCP::Number::tryToParseInteger(string, charSigned));
    BOOST_CHECK_EQUAL(charSigned, std::numeric_limits<char signed>::max());
    uintmax_t unsignedInteger{0};
    BOOST_CHECK(DCP::Number::tryToParseInteger(string, unsignedInteger));
    BOOST_CHECK_EQUAL(unsignedInteger,
      std::numeric_limits<char unsigned>::max() * 10
    );
  }

  BOOST_AUTO_TEST_CASE(underflow, *boost::unit_test::timeout(timeout)) {
    auto string = std::to_string(std::numeric_limits<intmax_t>::lowest());
    intmax_t signedInteger{0};
    BOOST_CHECK(DCP::Number::tryToParseInteger(string, signedInteger));
    BOOST_CHECK_EQUAL(signedInteger, std::numeric_limits<intmax_t>::lowest());
    string.push_back('0');
    BOOST_CHECK(!DCP::Number::tryToParseInteger(string, signedInteger));
  }

  BOOST_AUTO_TEST_CASE(overflow, *boost::unit_test::timeout(timeout)) {
    auto string = std::to_string(std::numeric_limits<uintmax_t>::max());
    uintmax_t unsignedInteger{0};
    BOOST_CHECK(DCP::Number::tryToParseInteger(string, unsignedInteger));
    BOOST_CHECK_EQUAL(unsignedInteger, std::numeric_limits<uintmax_t>::max());
    string.push_back('0');
    BOOST_CHECK(!DCP::Number::tryToParseInteger(string, unsignedInteger));
  }

  BOOST_AUTO_TEST_CASE(invalid, *boost::unit_test::timeout(timeout)) {
    size_t size{0};
    BOOST_CHECK(!DCP::Number::tryToParseInteger("NaN", size));
  }

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
