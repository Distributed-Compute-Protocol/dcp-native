/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       July 2024
 */

#include <dcp/string.hh>
#include <dcp/test.hh>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(String)
  BOOST_AUTO_TEST_SUITE(tests)

  BOOST_AUTO_TEST_CASE(joinQuoted, *boost::unit_test::timeout(timeout)) {
    std::vector<std::string> const strings{
      "baseline",
      "with space",
      "with \"balanced quotes\"",
      "with \"unbalanced quote"
    };
    auto const joined = DCP::String::joinQuoted(strings, ",");
    std::string const expected{
      "\"baseline\","
      "\"with space\","
      "\"with \\\"balanced quotes\\\"\","
      "\"with \\\"unbalanced quote\""
    };
    BOOST_CHECK_EQUAL(joined, expected);
  }

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
