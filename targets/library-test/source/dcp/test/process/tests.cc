/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       July 2024
 */

#include <dcp/process.hh>
#include <dcp/test.hh>

#include <boost/format.hpp>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Process)
  BOOST_AUTO_TEST_SUITE(tests)

#if BOOST_OS_WINDOWS

  BOOST_AUTO_TEST_CASE(command, *boost::unit_test::timeout(timeout)) {
    auto const exitCode = DCP::Process::command(
      getExecutablePath().string().data(), {"--version"}, {}, false, false
    );
    BOOST_CHECK_EQUAL(exitCode, 0);
  }

  BOOST_AUTO_TEST_CASE(commandError, *boost::unit_test::timeout(timeout)) {
    auto const exitCode = DCP::Process::command(
      getExecutablePath().string().data(), {"--not-an-option"}, {}, false, false
    );
    BOOST_CHECK_NE(exitCode, 0);
  }

  BOOST_AUTO_TEST_CASE(execute, *boost::unit_test::timeout(timeout)) {
    std::string output{};
    std::string error{};
    auto exitCode = DCP::Process::execute(
      getExecutablePath(), {"--version"}, output, error
    );
    BOOST_CHECK_EQUAL(exitCode, 0);
    BOOST_CHECK(output.empty());
    BOOST_CHECK(!error.empty());
    auto expected = boost::str(
      boost::format("Boost.Test module in executable '%s'")
      % getExecutablePath().filename().string()
    );
    BOOST_CHECK_EQUAL(error.substr(0, expected.size()), expected);

    exitCode = DCP::Process::execute(
      getExecutablePath(), {"--run_test=Process/tests/command"}, output, error
    );
    BOOST_CHECK_EQUAL(exitCode, 0);
    BOOST_CHECK(!output.empty());
    BOOST_CHECK(!error.empty());
    expected = "Running 1 test case...";
    BOOST_CHECK_EQUAL(output.substr(0, expected.size()), expected);

    output = DCP::Process::execute(getExecutablePath(), {"--version"});
    BOOST_CHECK(output.empty());

    output = DCP::Process::execute(
      getExecutablePath(), {"--run_test=Process/tests/command"}
    );
    BOOST_CHECK(!output.empty());
    BOOST_CHECK_EQUAL(output.substr(0, expected.size()), expected);
  }

  BOOST_AUTO_TEST_CASE(executeError, *boost::unit_test::timeout(timeout)) {
    std::string output{};
    std::string error{};
    auto const exitCode = DCP::Process::execute(
      getExecutablePath(), {"--not-an-option"}, output, error
    );
    BOOST_CHECK_NE(exitCode, 0);
    BOOST_CHECK(output.empty());
    BOOST_CHECK(!error.empty());
    std::string const expected{
      "An unrecognized parameter in the argument not-an-option"
    };
    BOOST_CHECK_EQUAL(error.substr(0, expected.size()), expected);
  }

  BOOST_AUTO_TEST_CASE(executeException, *boost::unit_test::timeout(timeout)) {
    try {
      std::string const output{
        DCP::Process::execute(getExecutablePath(), {"--not-an-option"})
      };
      BOOST_FAIL("An exception should have been thrown.");
    } catch ([[maybe_unused]] std::runtime_error const &exception) {}
  }

#endif

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
