/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       September 2021
 */

#include <dcp/read_buffer.hh>
#include <dcp/test.hh>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(ReadBuffer)
  BOOST_AUTO_TEST_SUITE(tests)

  BOOST_AUTO_TEST_CASE(general, *boost::unit_test::timeout(timeout)) {
    std::string expected(static_cast<size_t>(4096U * 8192U), '*');
    expected.push_back('\n');

    DCP::ReadBuffer readBuffer{};
    char const *remainingData = expected.data();
    size_t remainingSize = expected.size();
    auto const read = [&remainingData, &remainingSize](
      char *memory, size_t &size
    ) {
      bool const isMoreToRead = (size < remainingSize);
      size = std::min(size, remainingSize);
      strncpy(memory, remainingData, size);
      remainingData += size;
      remainingSize -= size;
      return isMoreToRead;
    };
    while (readBuffer.push(read)) {}
    char const *const actual = readBuffer.pop();
    BOOST_REQUIRE(actual);
    BOOST_CHECK(0 == strncmp(actual, expected.data(), expected.size() - 1U));
  }

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
