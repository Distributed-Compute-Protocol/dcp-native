/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#include <dcp/test.hh>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Logger)
  BOOST_AUTO_TEST_SUITE(logTests)

  BOOST_AUTO_TEST_CASE(longString, *boost::unit_test::timeout(timeout)) {
    // Log a string whose size is the documented maximum for ReportEvent().
    size_t const size = 31839;
    std::string const string(size, '1');
    BOOST_REQUIRE_EQUAL(string.size(), size);
    BOOST_REQUIRE(DCP::tryToLog(DCP::LogLevel::info, string.data()));

    // Log a string double that size to ensure that the limit is exercised.
    // The actual maximum size on Windows 10 appears to be 31879.
    static_assert(
      std::numeric_limits<size_t>::max() - size >= size, "Overflow"
    );
    size_t const largerSize = size * 2;
    std::string const largerString(largerSize, '2');
    BOOST_REQUIRE_EQUAL(largerString.size(), largerSize);
    BOOST_CHECK(DCP::tryToLog(DCP::LogLevel::info, largerString.data()));
  }

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
