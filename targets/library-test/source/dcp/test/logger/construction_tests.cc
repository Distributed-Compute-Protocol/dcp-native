/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#include <dcp/test.hh>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Logger)
  BOOST_AUTO_TEST_SUITE(constructionTests)

  BOOST_AUTO_TEST_CASE(mismatchedName, *boost::unit_test::timeout(timeout)) {
    BOOST_REQUIRE_EQUAL(
      DCP::Logger::getInstance().getName(),
      getExecutablePath().stem().string()
    );

    try {
      DCP::Logger::getInstance("different name");
      BOOST_FAIL("An exception should have been thrown.");
    } catch (std::invalid_argument const &) {}
  }

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
