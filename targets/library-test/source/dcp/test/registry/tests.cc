/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#include <dcp/test.hh>

#if BOOST_OS_WINDOWS

  #include <dcp/registry.hh>

  #include <boost/uuid/random_generator.hpp>
  #include <boost/uuid/uuid.hpp>
  #include <boost/uuid/uuid_io.hpp>

  #define testRootSubkeyBranch \
  "Software\\" DCP_companyName "\\" DCP_productName "\\"
  #define testRootSubkeyLeaf "test"
  #define testRootSubkey testRootSubkeyBranch "\\" testRootSubkeyLeaf

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Registry)

  namespace {

    static HKEY const testRootKey = HKEY_CURRENT_USER;
    DCP::Registry::Key testKey{};

    struct Fixture {
      Fixture() {
        auto [key, error] = DCP::Registry::tryToGetKey(
          testRootKey, testRootSubkey, true
        );
        BOOST_REQUIRE(!error);
        BOOST_REQUIRE(DCP::Registry::keyExists(key));
        testKey = std::move(key);
      }

      virtual ~Fixture() {
        BOOST_REQUIRE_NE(testKey, DCP::Registry::Key{});
        BOOST_REQUIRE(DCP::Registry::removeKey(testKey));
        BOOST_REQUIRE(!DCP::Registry::keyExists(testKey));
      }
    };

    std::string generateName() {
      boost::uuids::uuid uuid = boost::uuids::random_generator()();
      std::string name = boost::uuids::to_string(uuid);
      DCP::log(DCP::LogLevel::info, "Testing registry key: %s", name.data());
      return name;
    }

    void removeKey(HKEY const key, char const *const subkey = "") {
      BOOST_REQUIRE(subkey);

      BOOST_CHECK(DCP::Registry::keyExists(key, subkey));
      BOOST_CHECK(DCP::Registry::removeKey(key, subkey));
      BOOST_CHECK(!DCP::Registry::keyExists(key, subkey));
    }

    void addStringValue(
      char const *const string,
      HKEY const key,
      char const *const subkey,
      char const *const name
    ) {
      BOOST_REQUIRE(string);
      BOOST_REQUIRE(subkey);
      BOOST_REQUIRE(name);
      BOOST_REQUIRE(!DCP::Registry::valueExists(key, subkey, name));

      DCP::Registry::setStringValue(string, key, subkey, name);
      BOOST_CHECK(DCP::Registry::valueExists(key, subkey, name));
      auto const &pair = DCP::Registry::tryToGetStringValue(key, subkey, name);
      BOOST_CHECK_EQUAL(pair.first.data(), string);
      BOOST_CHECK(!pair.second);
    }

    void removeStringValue(
      HKEY const key,
      char const *const subkey,
      char const *const name
    ) {
      BOOST_REQUIRE(subkey);
      BOOST_REQUIRE(name);
      BOOST_REQUIRE(DCP::Registry::valueExists(key, subkey, name));

      BOOST_CHECK(DCP::Registry::removeValue(key, subkey, name));
      BOOST_CHECK(!DCP::Registry::valueExists(key, subkey, name));
    }

    void testStringValue(
      char const *const string,
      HKEY const key,
      char const *const subkey,
      char const *const name
    ) {
      BOOST_REQUIRE(string);
      BOOST_REQUIRE(subkey);
      BOOST_REQUIRE(name);

      addStringValue(string, key, subkey, name);
      removeStringValue(key, subkey, name);
    }

    /// Create keys under `key`, then iterate and confirm.
    void createKeys(HKEY const key, size_t const count) {
      for (size_t index = 0; index < count; ++index) {
        std::string subkey = std::to_string(index);
        addStringValue("", key, subkey.data(), "");
      }
      DCP::Registry::SubkeyIterator iterator(key);
      BOOST_CHECK_EQUAL(iterator.getCount(), count);
      for (size_t index = 0; index < count; ++index) {
        std::string subkey = iterator.getNext();
        BOOST_CHECK_EQUAL(subkey, std::to_string(index));
      }
    }

    /// Create string values under `key`, then iterate and confirm.
    void createStringValues(HKEY const key, size_t const count) {
      for (size_t index = 0; index < count; ++index) {
        std::string name = std::to_string(index);
        addStringValue(name.data(), key, "", name.data());
      }
      DCP::Registry::NameIterator iterator(key);
      BOOST_CHECK_EQUAL(iterator.getCount(), count);
      for (size_t index = 0; index < count; ++index) {
        std::string name = iterator.getNext();
        BOOST_CHECK_EQUAL(name, std::to_string(index));
      }
    }

  }

  BOOST_FIXTURE_TEST_SUITE(tests, Fixture)

  BOOST_AUTO_TEST_CASE(key, *boost::unit_test::timeout(timeout)) {
    std::string testSubkey = generateName();
    BOOST_REQUIRE(!DCP::Registry::keyExists(testKey, testSubkey.data()));
    auto pair = DCP::Registry::tryToGetKey(testKey, testSubkey.data());
    BOOST_REQUIRE_EQUAL(pair.first, DCP::Registry::Key{});
    BOOST_REQUIRE(pair.second);

    auto [key, error] = DCP::Registry::tryToGetKey(
      testKey, testSubkey.data(), true
    );
    BOOST_REQUIRE(!error);
    BOOST_CHECK_NE(key, DCP::Registry::Key{});
    BOOST_CHECK(DCP::Registry::keyExists(key));
    pair = DCP::Registry::tryToGetKey(testKey, testSubkey.data());
    BOOST_CHECK_NE(pair.first, DCP::Registry::Key{});
    BOOST_CHECK(!pair.second);

    {
      auto const &duplicate = DCP::Registry::tryToGetKey(
        testKey, testSubkey.data(), true
      );
      BOOST_REQUIRE(!duplicate.second);
      BOOST_CHECK_NE(duplicate.first, DCP::Registry::Key{});
      BOOST_CHECK(DCP::Registry::keyExists(duplicate.first));
      pair = DCP::Registry::tryToGetKey(testKey, testSubkey.data());
      BOOST_CHECK_NE(pair.first, DCP::Registry::Key{});
      BOOST_CHECK(!pair.second);
    }

    createKeys(key, 5);

    std::vector<std::string> subkeys{"", generateName()};
    std::vector<std::string> strings{
      "",
      "TEST",
      (
        "Bacon ipsum dolor amet quis officia turducken, reprehenderit ham"
        " prosciutto cupim sed pork loin. Boudin culpa quis tongue duis enim"
        " fatback short loin elit meatball rump aute cupim drumstick excepteur."
        " Sint filet mignon ut, proident irure in ball tip ipsum nostrud. Ham"
        " hock pancetta consectetur kielbasa fatback lorem. Capicola kielbasa"
        " pork belly jowl, non tempor do pariatur reprehenderit boudin biltong"
        " in qui. Alcatra deserunt sirloin strip steak."
      )
    };
    for (std::string const &subkey: subkeys) {
      for (std::string const &string: strings) {
        testStringValue(
          string.data(), key, subkey.data(), generateName().data()
        );
      }
    }

    createStringValues(key, 5);

    BOOST_CHECK(DCP::Registry::removeKey(key));
    BOOST_CHECK(!DCP::Registry::keyExists(key));
    pair = DCP::Registry::tryToGetKey(key);
    BOOST_CHECK_EQUAL(pair.first, DCP::Registry::Key{});
    BOOST_CHECK(pair.second);
    BOOST_CHECK(!DCP::Registry::removeKey(key));
  }

  BOOST_AUTO_TEST_CASE(subkeys, *boost::unit_test::timeout(timeout)) {
    size_t const count = 5;
    createKeys(testKey, count);
    for (size_t index = 0; index < count; ++index) {
      removeKey(testKey, std::to_string(index).data());
    }
  }

  BOOST_AUTO_TEST_CASE(names, *boost::unit_test::timeout(timeout)) {
    size_t const count = 5;
    createStringValues(testKey, count);
    for (size_t index = 0; index < count; ++index) {
      removeStringValue(testKey, "", std::to_string(index).data());
    }
  }

  BOOST_AUTO_TEST_CASE(rename, *boost::unit_test::timeout(timeout)) {
    size_t const count = 5;

    BOOST_REQUIRE(DCP::Registry::keyExists(testKey));
    createKeys(testKey, count);

  #define testRootRenamedSubkeyLeaf testRootSubkeyLeaf " (renamed)"
  #define testRootRenamedSubkey \
  testRootSubkeyBranch "\\" testRootRenamedSubkeyLeaf

    DCP::Registry::renameKey(testRootRenamedSubkeyLeaf, testKey);
    BOOST_CHECK(!DCP::Registry::keyExists(testRootKey, testRootSubkey));
    BOOST_CHECK(DCP::Registry::keyExists(testRootKey, testRootRenamedSubkey));

    auto const renamed = DCP::Registry::tryToGetKey(
      testRootKey, testRootRenamedSubkey
    );
    BOOST_CHECK_NE(renamed.first, DCP::Registry::Key{});
    BOOST_CHECK(!renamed.second);
    DCP::Registry::SubkeyIterator iterator(renamed.first);
    BOOST_CHECK_EQUAL(iterator.getCount(), count);
    for (size_t index = 0; index < count; ++index) {
      auto const subkey = iterator.getNext();
      BOOST_CHECK_EQUAL(subkey, std::to_string(index));
    }

    DCP::Registry::renameKey(
      testRootSubkeyLeaf, testRootKey, testRootRenamedSubkey
    );
    BOOST_CHECK(!DCP::Registry::keyExists(testRootKey, testRootRenamedSubkey));
    BOOST_CHECK(DCP::Registry::keyExists(testRootKey, testRootSubkey));

    for (size_t index = 0; index < count; ++index) {
      std::string const oldName{std::to_string(index)};
      std::string const newName{std::to_string(count + index)};
      BOOST_REQUIRE(DCP::Registry::keyExists(testKey, oldName.data()));
      DCP::Registry::renameKey(newName.data(), testKey, oldName.data());
      BOOST_CHECK(!DCP::Registry::keyExists(testKey, oldName.data()));
      BOOST_CHECK(DCP::Registry::keyExists(testKey, newName.data()));
    }

  #undef testRootRenamedSubkeyLeaf
  #undef testRootRenamedSubkey

  }

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}

#else

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Registry)
  BOOST_AUTO_TEST_SUITE(tests)

  BOOST_AUTO_TEST_CASE(empty) {}

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}

#endif
