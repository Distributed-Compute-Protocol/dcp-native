/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       December 2020
 */

#include <dcp/handle.hh>
#include <dcp/test.hh>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Handle)
  BOOST_AUTO_TEST_SUITE(tests)

  BOOST_AUTO_TEST_CASE(moveConstructor, *boost::unit_test::timeout(timeout)) {
    using Handle = DCP::Handle<int, -1>;
    size_t destroyed = 0;
    auto const destroy = [&destroyed](int value) {
      BOOST_REQUIRE_NE(value, -1);
      ++destroyed;
    };
    BOOST_REQUIRE_EQUAL(static_cast<int>(Handle{}), -1);
    {
      Handle mover(1, destroy);
      Handle recipient{std::move(mover)};
      BOOST_CHECK_EQUAL(destroyed, 0);
#if !defined(__clang_analyzer__)
      // NOLINTNEXTLINE(bugprone-use-after-move, hicpp-invalid-access-moved)
      BOOST_CHECK_EQUAL(mover, Handle{});
#endif
      BOOST_CHECK_EQUAL(static_cast<int>(recipient), 1);
      BOOST_CHECK_EQUAL(destroyed, 0);
    }
    BOOST_CHECK_EQUAL(destroyed, 1);
  }

  BOOST_AUTO_TEST_CASE(moveAssignment, *boost::unit_test::timeout(timeout)) {
    using Handle = DCP::Handle<int, -1>;
    size_t destroyed = 0;
    auto const destroy = [&destroyed](int value) {
      BOOST_REQUIRE_NE(value, -1);
      ++destroyed;
    };
    BOOST_REQUIRE_EQUAL(static_cast<int>(Handle{}), -1);
    {
      Handle mover(1, destroy);
      Handle recipient(2, destroy);
      BOOST_CHECK_EQUAL(destroyed, 0);
      recipient = std::move(mover);
  #if !defined(__clang_analyzer__)
      // NOLINTNEXTLINE(bugprone-use-after-move, hicpp-invalid-access-moved)
      BOOST_CHECK_EQUAL(mover, Handle{});
  #endif
      BOOST_CHECK_EQUAL(static_cast<int>(recipient), 1);
      BOOST_CHECK_EQUAL(destroyed, 1);
    }
    BOOST_CHECK_EQUAL(destroyed, 2);
    {
      Handle recipient(3, destroy);
      recipient = {};
      BOOST_CHECK_EQUAL(static_cast<int>(recipient), -1);
      BOOST_CHECK_EQUAL(destroyed, 3);
    }
    BOOST_CHECK_EQUAL(destroyed, 3);
  }

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
