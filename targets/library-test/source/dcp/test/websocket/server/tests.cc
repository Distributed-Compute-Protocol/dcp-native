/**
 *  @file
 *  @author     Severn Lortie, severn@distributive.network
 *  @date       May 2024
 */

#include <dcp/socket.hh>
#include <dcp/test.hh>
#include <dcp/websocket/server.hh>

#include <boost/asio.hpp>
#include <boost/beast/core.hpp>
#include <boost/thread/scoped_thread.hpp>

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <string>
#include <thread>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Websocket)
  BOOST_AUTO_TEST_SUITE(Server)
  BOOST_AUTO_TEST_SUITE(tests)

  BOOST_AUTO_TEST_CASE(echoMessage, *boost::unit_test::timeout(timeout)) {
    std::atomic<bool> quitNow{false};
    std::condition_variable cv{};
    std::mutex mutex{};
    bool websocketServerReady{false};
    auto const address = DCP::Socket::getAvailableAddress();
    BOOST_REQUIRE(address);
    short unsigned const freePort{address->getPort()};
    boost::strict_scoped_thread<> thread(
      [&freePort, &mutex, &websocketServerReady, &quitNow, &cv]() {
        DCP::Websocket::Server websocketServer{freePort};
        websocketServer.setReadCallback(
          [&websocketServer](boost::beast::flat_buffer &buffer) {
            websocketServer.send(
              boost::beast::buffers_to_string(buffer.data())
            );
          }
        );
        websocketServer.acceptConnection();
        {
          std::lock_guard<std::mutex> lock{mutex};
          websocketServerReady = true;
          cv.notify_one();
        }
        while (!quitNow) {
          try {
            websocketServer.pollEventLoop();
            std::this_thread::yield();
          } catch (boost::beast::error_code &error_code) {
            if (error_code != boost::asio::error::operation_aborted) {
              tryToLog(LogLevel::error, error_code.message().data());
              BOOST_FAIL(
                "Polling the Websocket Server threw unexpected exception"
              );
            }
          }
        }
      }
    );
    std::unique_lock<std::mutex> lock{mutex};
    cv.wait(lock,
      [&websocketServerReady] {
        return websocketServerReady;
      }
    );
    boost::asio::io_context io_context{};
    boost::asio::ip::tcp::resolver resolver{io_context};
    boost::beast::websocket::stream<boost::asio::ip::tcp::socket> websocket{
      io_context
    };
    std::string const host{"localhost"};
    auto const resolvedHost = resolver.resolve(host, std::to_string(freePort));
    boost::asio::ip::tcp::endpoint tcpEndpoint{
      boost::asio::connect(websocket.next_layer(), resolvedHost)
    };

    std::string const websocketHost{host + std::to_string(tcpEndpoint.port())};
    websocket.handshake(websocketHost, "/");
    std::string const message{"Hello Websocket Server!"};
    websocket.write(boost::asio::buffer(message));
    boost::beast::flat_buffer response;
    websocket.read(response);
    websocket.close(boost::beast::websocket::close_code::normal);
    BOOST_CHECK_EQUAL(
      message,
      boost::beast::buffers_to_string(response.data())
    );
    quitNow = true;
  }

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
