/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       November 2020
 */

#include <dcp/keystore.hh>
#include <dcp/test.hh>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Keystore)
  BOOST_AUTO_TEST_SUITE(tests)

  BOOST_AUTO_TEST_CASE(validateAddress, *boost::unit_test::timeout(timeout)) {
    std::string address{};
    BOOST_CHECK(!DCP::Keystore::validateAddress(address));

#define addressString "0123456789abcdef0123456789abcdefdeadbeef"
    address = addressString;
    BOOST_CHECK(DCP::Keystore::validateAddress(address));
    BOOST_CHECK_EQUAL(address, "0x" addressString);

    BOOST_CHECK(DCP::Keystore::validateAddress(address));
#undef addressString
  }

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
