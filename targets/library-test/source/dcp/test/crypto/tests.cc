/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       May 2021
 */

#include <dcp/crypto.hh>
#include <dcp/test.hh>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Crypto)
  BOOST_AUTO_TEST_SUITE(tests)

#if BOOST_OS_WINDOWS

  BOOST_AUTO_TEST_CASE(encryptAndDecrypt, *boost::unit_test::timeout(timeout)) {
    // https://generator.lorem-ipsum.info/_volapuek
    std::string const string{
      "Eprofetobs-li jamod loenio obe äl! Us drinön kupriniki ledutoti tim, kü"
      " läs dotolsöv fidom-li obinofs. Va lak lups seaton zitevon, ol klu"
      " benomeugik distukons koap. Pla bü bims notükon sinik."
    };
    std::string const encrypted{DCP::Crypto::encrypt(string)};
    BOOST_CHECK_NE(encrypted, string);
    std::string const decrypted{DCP::Crypto::decrypt(encrypted)};
    BOOST_CHECK_EQUAL(decrypted, string);
  }

  BOOST_AUTO_TEST_CASE(encryptEmpty, *boost::unit_test::timeout(timeout)) {
    try {
      [[maybe_unused]] std::string const encrypted{DCP::Crypto::encrypt("")};
      BOOST_FAIL("Encrypting an empty string should throw invalid_argument");
    } catch ([[maybe_unused]] std::invalid_argument const &) {
      return;
    } catch (...) {
      BOOST_FAIL("Encrypting an empty string should throw invalid_argument");
    }
  }

  BOOST_AUTO_TEST_CASE(decryptEmpty, *boost::unit_test::timeout(timeout)) {
    try {
      [[maybe_unused]] std::string const decrypted{DCP::Crypto::decrypt("")};
      BOOST_FAIL("Decrypting an empty string should throw invalid_argument");
    } catch ([[maybe_unused]] std::invalid_argument const &) {
      return;
    } catch (...) {
      BOOST_FAIL("Decrypting an empty string should throw invalid_argument");
    }
  }

  BOOST_AUTO_TEST_CASE(decryptInvalidBinary,
    *boost::unit_test::timeout(timeout)
  ) {
    try {
      [[maybe_unused]] std::string const decrypted{
        DCP::Crypto::decrypt("dead")
      };
      BOOST_FAIL("Decrypting invalid binary should throw system_error");
    } catch ([[maybe_unused]] std::system_error const &) {
      return;
    } catch (...) {
      BOOST_FAIL("Decrypting invalid binary should throw system_error");
    }
  }

#else

  BOOST_AUTO_TEST_CASE(empty) {}

#endif

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
