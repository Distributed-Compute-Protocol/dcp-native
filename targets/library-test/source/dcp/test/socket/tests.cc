/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       February 2020
 */

#include <dcp/socket.hh>
#include <dcp/test.hh>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Socket)
  BOOST_AUTO_TEST_SUITE(tests)

#if BOOST_OS_WINDOWS

  BOOST_AUTO_TEST_CASE(wsaErrorCodes, *boost::unit_test::timeout(timeout)) {
    std::error_code errorCode{WSANOTINITIALISED, std::system_category()};
    BOOST_CHECK_EQUAL(errorCode.value(), WSANOTINITIALISED);
    BOOST_CHECK_EQUAL(errorCode.message(),
      "Either the application has not called WSAStartup, or WSAStartup failed."
    );
  }

#else

  BOOST_AUTO_TEST_CASE(badPipe, *boost::unit_test::timeout(timeout)) {
    // Create pipe.
    int fds[2];
    if (pipe(fds) != 0) {
      throw std::system_error(
        std::error_code(errno, std::system_category()),
        "pipe() failed; cannot test"
      );
    }

    // Close the input end of the pipe.
    close(fds[0]);

    // Attempt to write, and assert that EPIPE results.
    size_t sz = 1;
    try {
      DCP::Socket::write(fds[1], "a", sz);
    } catch (std::system_error const &error) {
      BOOST_CHECK_EQUAL(error.code().value(), EPIPE);
    }
    BOOST_CHECK_EQUAL(sz, 1);

    close(fds[1]);
  }

#endif

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
