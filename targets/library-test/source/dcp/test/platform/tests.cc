/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       February 2022
 */

#include <dcp/platform.hh>
#include <dcp/test.hh>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Platform)
  BOOST_AUTO_TEST_SUITE(tests)

  BOOST_AUTO_TEST_CASE(getName, *boost::unit_test::timeout(timeout)) {
    std::string name = DCP::Platform::getName();
  #if BOOST_OS_WINDOWS
    BOOST_CHECK_EQUAL(name, "win32");
  #elif BOOST_OS_MACOS
    BOOST_CHECK_EQUAL(name, "darwin");
  #elif BOOST_OS_UNIX
    BOOST_CHECK_EQUAL(name, "linux");
  #else
    #error Platform not supported.
  #endif
  }

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
