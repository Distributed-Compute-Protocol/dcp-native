/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#include <dcp/evaluator.hh>
#include <dcp/test.hh>

#include <boost/exception/diagnostic_information.hpp>
#include <boost/thread/scoped_thread.hpp>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Evaluator)
  BOOST_AUTO_TEST_SUITE(constructionTests)

  BOOST_AUTO_TEST_CASE(invalidOutput, *boost::unit_test::timeout(timeout)) {
    boost::strict_scoped_thread<> thread(
      []() {
        try {
          DCP::Evaluator::getInstance();
          BOOST_FAIL("An exception should have been thrown.");
        } catch (std::invalid_argument const &) {}
      }
    );
  }

#if !BOOST_OS_WINDOWS

  BOOST_AUTO_TEST_CASE(mismatchedOutput, *boost::unit_test::timeout(timeout)) {
    boost::strict_scoped_thread<> thread(
      []() {
        try {
          auto const socketPair = DCP::Socket::createPair();

          DCP::Evaluator::getInstance(socketPair[1]);

          DCP::Evaluator &evaluator = DCP::Evaluator::getInstance();
          BOOST_CHECK(evaluator.getOutput() == socketPair[1]);

          try {
            DCP::Evaluator::getInstance(DCP::Socket::Handle(fileno(stdout)));
            BOOST_FAIL("An exception should have been thrown.");
          } catch (std::invalid_argument const &) {}
        } catch (...) {
          DCP::tryToLog(DCP::LogLevel::warning,
            "Unexpected exception in thread: %s",
            boost::current_exception_diagnostic_information().data()
          );
          BOOST_FAIL("An exception was unexpectedly thrown");
        }
      }
    );
  }

  BOOST_AUTO_TEST_CASE(mismatchedWebGPU, *boost::unit_test::timeout(timeout)) {
    boost::strict_scoped_thread<> thread(
      []() {
        try {
          bool const webGPU{};
          DCP::Evaluator &evaluator = DCP::Evaluator::getInstance(
            DCP::Socket::Handle(fileno(stdout)), webGPU
          );
          try {
            BOOST_REQUIRE_EQUAL(webGPU, evaluator.getWebGPU());
            DCP::Evaluator::getInstance(
              DCP::Socket::Handle(fileno(stdout)), !webGPU
            );
            BOOST_FAIL("An exception should have been thrown.");
          } catch (std::invalid_argument const &) {}
        } catch (...) {
          DCP::tryToLog(DCP::LogLevel::warning,
            "Unexpected exception in thread: %s",
            boost::current_exception_diagnostic_information().data()
          );
          BOOST_FAIL("An exception was unexpectedly thrown");
        }
      }
    );
  }

  BOOST_AUTO_TEST_CASE(mismatchedDebugging,
    *boost::unit_test::timeout(timeout)
  ) {
    boost::strict_scoped_thread<> thread(
      []() {
        try {
          DCP::Evaluator &evaluator = DCP::Evaluator::getInstance(
            DCP::Socket::Handle(fileno(stdout))
          );
          try {
            BOOST_REQUIRE(!evaluator.getDebuggingPort());
            auto const address = DCP::Socket::getAvailableAddress();
            BOOST_REQUIRE(address);
            DCP::Evaluator::getInstance(
              DCP::Socket::Handle(fileno(stdout)), {}, {}, address->getPort()
            );
            BOOST_FAIL("An exception should have been thrown.");
          } catch (std::invalid_argument const &) {}
        } catch (...) {
          DCP::tryToLog(DCP::LogLevel::warning,
            "Unexpected exception in thread: %s",
            boost::current_exception_diagnostic_information().data()
          );
          BOOST_FAIL("An exception was unexpectedly thrown");
        }
      }
    );
  }

  BOOST_AUTO_TEST_CASE(mismatchedDebuggingPort,
    *boost::unit_test::timeout(timeout)
  ) {
    boost::strict_scoped_thread<> thread(
      []() {
        try {
          auto const address = DCP::Socket::getAvailableAddress();
          BOOST_REQUIRE(address);
          short unsigned const port{address->getPort()};
          DCP::Evaluator &evaluator = DCP::Evaluator::getInstance(
            DCP::Socket::Handle(fileno(stdout)), {}, {}, port
          );
          try {
            BOOST_REQUIRE(!!evaluator.getDebuggingPort());
            BOOST_REQUIRE_EQUAL(port, *evaluator.getDebuggingPort());
            DCP::Evaluator::getInstance(
              DCP::Socket::Handle(fileno(stdout)), {}, {}, port + 1
            );
            BOOST_FAIL("An exception should have been thrown.");
          } catch (std::invalid_argument const &) {}
        } catch (...) {
          DCP::tryToLog(DCP::LogLevel::warning,
            "Unexpected exception in thread: %s",
            boost::current_exception_diagnostic_information().data()
          );
          BOOST_FAIL("An exception was unexpectedly thrown");
        }
      }
    );
  }

  BOOST_AUTO_TEST_CASE(mismatchedOptions, *boost::unit_test::timeout(timeout)) {
    boost::strict_scoped_thread<> thread(
      []() {
        try {
          DCP::Evaluator &evaluator = DCP::Evaluator::getInstance(
            DCP::Socket::Handle(fileno(stdout))
          );
          try {
            std::vector<std::string> implementationOptions{
              "--max-old-space-size=2048",
              "--use-strict"
            };
            BOOST_REQUIRE(
              implementationOptions != evaluator.getImplementationOptions()
            );
            DCP::Evaluator::getInstance(
              DCP::Socket::Handle(fileno(stdout)),
              {}, {}, {}, implementationOptions
            );
            BOOST_FAIL("An exception should have been thrown.");
          } catch (std::invalid_argument const &) {}
        } catch (...) {
          DCP::tryToLog(DCP::LogLevel::warning,
            "Unexpected exception in thread: %s",
            boost::current_exception_diagnostic_information().data()
          );
          BOOST_FAIL("An exception was unexpectedly thrown");
        }
      }
    );
  }

#endif

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
