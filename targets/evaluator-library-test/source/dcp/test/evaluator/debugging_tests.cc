/**
 *  @file
 *  @author     Severn Lortie, severn@distributive.network
 *  @date       June 2024
 */

#include <dcp/evaluator.hh>
#include <dcp/test.hh>
#include <dcp/write_buffer.hh>

#include <boost/asio.hpp>
#include <boost/beast/core.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/thread/scoped_thread.hpp>
#include <boost/beast/websocket.hpp>

#include <condition_variable>
#include <mutex>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Evaluator)
  BOOST_AUTO_TEST_SUITE(debuggingTests)

  BOOST_AUTO_TEST_CASE(connection, *boost::unit_test::timeout(timeout)) {
    std::condition_variable cv{};
    std::mutex mutex{};
    bool evaluatorReady{false};
    auto const outputSocket = DCP::Socket::Handle(fileno(stdout));
    auto const socketPair = DCP::Socket::createPair();
    auto const address = DCP::Socket::getAvailableAddress();
    BOOST_REQUIRE(address);
    short unsigned const freePort{address->getPort()};
    boost::strict_scoped_thread<> thread(
      [
        &socketPair, &outputSocket, &evaluatorReady, &mutex, &cv, &freePort
      ]() noexcept {
        try {
          auto &evaluator = DCP::Evaluator::getInstance(
            outputSocket, false, {}, freePort
          );
          {
            std::lock_guard<std::mutex> lock{mutex};
            evaluatorReady = true;
            cv.notify_one();
          }
          evaluator.evaluateStream(socketPair[1]);
        } catch (...) {
          DCP::tryToLog(DCP::LogLevel::warning,
            "Unexpected exception in thread: %s",
            boost::current_exception_diagnostic_information().data()
          );
          BOOST_FAIL("An exception was unexpectedly thrown");
        }
      }
    );
    std::unique_lock<std::mutex> lock{mutex};
    cv.wait(lock,
      [&evaluatorReady] {
        return evaluatorReady;
      }
    );

    boost::asio::io_context io_context{};
    boost::asio::ip::tcp::resolver resolver{io_context};
    boost::beast::websocket::stream<boost::asio::ip::tcp::socket> websocket{
      io_context
    };
    std::string const host{"localhost"};
    auto const resolvedHost = resolver.resolve(host, std::to_string(freePort));
    boost::asio::ip::tcp::endpoint const tcpEndpoint{
      boost::asio::connect(websocket.next_layer(), resolvedHost)
    };
    std::string const websocketHost{host + std::to_string(tcpEndpoint.port())};
    websocket.handshake(websocketHost, "/");
    DCP::Socket::send(socketPair[0], "die()\n");
  }

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
