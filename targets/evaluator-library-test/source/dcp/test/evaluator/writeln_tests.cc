/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#include <dcp/evaluator.hh>
#include <dcp/test.hh>

#include <boost/exception/diagnostic_information.hpp>
#include <boost/thread/scoped_thread.hpp>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Evaluator)
  BOOST_AUTO_TEST_SUITE(writelnTests)

  BOOST_AUTO_TEST_CASE(i18n, *boost::unit_test::timeout(timeout)) {
    boost::strict_scoped_thread<> thread(
      []() {
        try {
          auto const socketPair = DCP::Socket::createPair();

          auto &evaluator = DCP::Evaluator::getInstance(socketPair[1]);
          BOOST_CHECK(evaluator.evaluateString(
            "writeln((88888888).toLocaleString())")
          );

          std::string const expected = "88,888,888\n";
          std::vector<char> actual(expected.size() + 1U, '\0');
          size_t actualSize = actual.size();
          DCP::Socket::read(socketPair[0], &actual.front(), actualSize);
          BOOST_CHECK_EQUAL(actualSize, expected.size());
          BOOST_CHECK_EQUAL(actual.data(), expected.data());
        } catch (...) {
          DCP::tryToLog(DCP::LogLevel::warning,
            "Unexpected exception in thread: %s",
            boost::current_exception_diagnostic_information().data()
          );
          BOOST_FAIL("An exception was unexpectedly thrown");
        }
      }
    );
  }

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
