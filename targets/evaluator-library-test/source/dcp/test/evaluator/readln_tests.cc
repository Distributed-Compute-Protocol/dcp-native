/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#include <dcp/evaluator.hh>
#include <dcp/test.hh>

#include <boost/exception/diagnostic_information.hpp>
#include <boost/thread/scoped_thread.hpp>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Evaluator)
  BOOST_AUTO_TEST_SUITE(readlnTests)

  BOOST_AUTO_TEST_CASE(compileError, *boost::unit_test::timeout(timeout)) {
    boost::strict_scoped_thread<> thread(
      []() {
        try {
          auto const socketPair = DCP::Socket::createPair();

          auto &evaluator = DCP::Evaluator::getInstance(socketPair[1]);

          // The next line is invalid; confirm no crash (DCP-3521)
          BOOST_CHECK(!evaluator.evaluateString("writeln("));
        } catch (...) {
          DCP::tryToLog(DCP::LogLevel::warning,
            "Unexpected exception in thread: %s",
            boost::current_exception_diagnostic_information().data()
          );
          BOOST_FAIL("An exception was unexpectedly thrown");
        }
      }
    );
  }

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
