/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       December 2021
 */

#include <dcp/pipe.hh>

#if BOOST_OS_WINDOWS

  #include <system_error>

namespace DCP {

  Pipe::Pipe() {
    SECURITY_ATTRIBUTES sa = {};
    sa.nLength = sizeof(SECURITY_ATTRIBUTES);
    sa.bInheritHandle = TRUE;
    sa.lpSecurityDescriptor = NULL;

    HANDLE readHandle;
    HANDLE writeHandle;
    SetLastError(ERROR_SUCCESS);
    if (!CreatePipe(&readHandle, &writeHandle, &sa, 0)) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "CreatePipe() failed"
      );
    }
    readEnd = Handle<HANDLE, nullptr>(readHandle, CloseHandle);
    writeEnd = Handle<HANDLE, nullptr>(writeHandle, CloseHandle);
  }

  void Pipe::read(std::string &string) {
    for (;; ) {
      DWORD bytesAvailable = 0;
      SetLastError(ERROR_SUCCESS);
      if (!PeekNamedPipe(readEnd, NULL, 0, NULL, &bytesAvailable, NULL)) {
        throw std::system_error(
          std::error_code(GetLastError(), std::system_category()),
          "PeekNamedPipe() failed"
        );
      }
      if (!bytesAvailable) {
        break;
      }

      char buffer[1024];
      DWORD bytesRead = 0;
      SetLastError(ERROR_SUCCESS);
      if (
        !ReadFile(
          readEnd, buffer,
          std::min((DWORD)(sizeof(buffer) - 1), bytesAvailable),
          &bytesRead, NULL
        )
      ) {
        throw std::system_error(
          std::error_code(GetLastError(), std::system_category()),
          "ReadFile() failed"
        );
      }
      if (!bytesRead) {
        break;
      }
      buffer[bytesRead] = 0;
      string += buffer;
    }
  }

  HANDLE Pipe::getReadEnd() const {
    return readEnd;
  }

  HANDLE Pipe::getWriteEnd() const {
    return writeEnd;
  }

}

#endif
