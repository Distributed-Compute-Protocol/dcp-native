/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2021
 */

#include <dcp/bitmap.hh>

#if BOOST_OS_WINDOWS

  #include <dcp/logger.hh>

namespace DCP::Bitmap {

  Handle<HBITMAP, nullptr> loadFile(boost::filesystem::path const &filePath) {
    if (filePath.empty()) {
      return {};
    }

    SetLastError(ERROR_SUCCESS);
    Handle<HBITMAP, nullptr> bitmap(
      (HBITMAP)LoadImage(NULL, filePath.string().data(),
        IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE
      ),
      DeleteObject
    );
    if (!bitmap) {
      log(LogLevel::info, "LoadImage() failed for bitmap file \"%s\": %s",
        filePath.string().data(),
        std::error_code(GetLastError(), std::system_category()).message().data()
      );
    }
    return bitmap;
  }

  SIZE getSize(HBITMAP const bitmap) {
    BITMAP bitmapInfo = {};
    if (!GetObject(bitmap, sizeof(bitmapInfo), &bitmapInfo)) {
      throw std::runtime_error("GetObject() failed");
    }
    return {bitmapInfo.bmWidth, bitmapInfo.bmHeight};
  }

}

#endif
