/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       August 2020
 */

#include <dcp/paths.hh>

#if BOOST_OS_WINDOWS

  #include <dcp/handle.hh>
  #include <dcp/logger.hh>
  #include <dcp/settings.hh>

  #include <boost/filesystem/operations.hpp>
  #include <boost/format.hpp>
  #include <boost/nowide/convert.hpp>

  #include <stdexcept>

namespace DCP::Paths {

  boost::filesystem::path getSpecialDirectoryPath(REFKNOWNFOLDERID id) {
    PWSTR wideString = nullptr;
    HRESULT const result = SHGetKnownFolderPath(id, 0, nullptr, &wideString);
    Handle<PWSTR, nullptr> handle(wideString, CoTaskMemFree);
    if (result != S_OK) {
      throw std::runtime_error(
        boost::str(
          boost::format("SHGetKnownFolderPath() failed: %l") % result
        )
      );
    }

    boost::filesystem::path directoryPath{boost::nowide::narrow(wideString)};
    if (directoryPath.empty()) {
      throw std::runtime_error("SHGetKnownFolderPath() returned an empty path");
    }
    return directoryPath;
  }

  boost::filesystem::path getInstallDirectoryPath(
    boost::filesystem::path const &defaultDirectoryPath
  ) {
    assert(
      defaultDirectoryPath.empty() ||
      boost::filesystem::is_directory(defaultDirectoryPath)
    );

    auto const &string = Settings::Tree::get().first.tryToGet("", "").first;
    if (string.empty() || !boost::filesystem::is_directory(string)) {
      log(LogLevel::info,
        "Install directory not found; using default directory \"%s\"",
        defaultDirectoryPath.string().data()
      );
      return defaultDirectoryPath;
    }
    boost::filesystem::path installDirectoryPath{string};
    log(LogLevel::info, "Using install directory \"%s\"",
      installDirectoryPath.string().data()
    );
    return installDirectoryPath;
  }

  boost::filesystem::path const &getExecutablePath() {
    static auto const executablePath = boost::filesystem::absolute(__argv[0]);
    assert(!executablePath.empty());
    return executablePath;
  }

  boost::filesystem::path getProgramDataDirectoryPath() {
    auto programDataDirectoryPath = getSpecialDirectoryPath(
      FOLDERID_ProgramData
    );
    assert(!programDataDirectoryPath.empty());
    assert(!std::string{DCP_companyName}.empty());
    programDataDirectoryPath /= DCP_companyName;
    assert(!std::string{DCP_productName}.empty());
    programDataDirectoryPath /= DCP_productName;
    return programDataDirectoryPath;
  }

  boost::filesystem::path getSystemDirectoryPath() {
    return getSpecialDirectoryPath(FOLDERID_System);
  }

}

#endif
