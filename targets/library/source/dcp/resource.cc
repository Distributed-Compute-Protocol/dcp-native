/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       August 2020
 */

#include <dcp/resource.hh>

#if BOOST_OS_WINDOWS

  #include <boost/nowide/convert.hpp>

  #include <system_error>

namespace DCP::Resource {

  namespace {

    HMODULE getModule() {
      SetLastError(ERROR_SUCCESS);
      if (static HMODULE const module = GetModuleHandle(nullptr)) {
        return module;
      }
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "GetModuleHandle() failed"
      );
    }

    std::string loadUserDefined(
      int unsigned const id,
      int unsigned const type
    ) {
      HMODULE const module = getModule();
      assert(module);

      SetLastError(ERROR_SUCCESS);
      HRSRC const resourceInfo = FindResource(
        module, MAKEINTRESOURCE(id), MAKEINTRESOURCE(type)
      );
      if (!resourceInfo) {
        throw std::system_error(
          std::error_code(GetLastError(), std::system_category()),
          "FindResource() failed"
        );
      }

      SetLastError(ERROR_SUCCESS);
      HGLOBAL const resource = LoadResource(module, resourceInfo);
      if (!resource) {
        throw std::system_error(
          std::error_code(GetLastError(), std::system_category()),
          "LoadResource() failed"
        );
      }

      SetLastError(ERROR_SUCCESS);
      DWORD const size = SizeofResource(module, resourceInfo);
      if (!size) {
        throw std::system_error(
          std::error_code(GetLastError(), std::system_category()),
          "SizeofResource() failed"
        );
      }

      char const *const data = static_cast<const char *>(
        LockResource(resource)
      );
      if (!data) {
        throw std::runtime_error("LockResource() failed");
      }

      return std::string(data, data + size);
    }

  }

  Color::Value loadColor(int unsigned const id, int unsigned const type) {
    auto const &string = loadUserDefined(id, type);
    return Color::fromString(string);
  }

  Handle<HBITMAP, nullptr> loadBitmap(int unsigned const id) {
    HMODULE const module = getModule();
    assert(module);

    SetLastError(ERROR_SUCCESS);
    Handle<HBITMAP, nullptr> bitmap(
      LoadBitmap(module, MAKEINTRESOURCE(id)),
      DeleteObject
    );
    if (!bitmap) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "LoadBitmap() failed"
      );
    }
    return bitmap;
  }

  Handle<HICON, nullptr> loadIcon(int unsigned const id) {
    HMODULE const module = getModule();
    assert(module);

    SetLastError(ERROR_SUCCESS);
    Handle<HICON, nullptr> icon(
      (HICON)LoadImage(module, MAKEINTRESOURCE(id), IMAGE_ICON, 0, 0,
        LR_DEFAULTCOLOR | LR_DEFAULTSIZE
      ),
      DestroyIcon
    );
    if (!icon) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "LoadImage() failed"
      );
    }
    return icon;
  }

  std::string loadString(int unsigned const id) {
    HMODULE const module = getModule();
    assert(module);

    wchar_t const *string = nullptr;
    SetLastError(ERROR_SUCCESS);
    size_t const size = LoadStringW(module, id,
      // `LoadStringW` takes a `wchar_t` buffer that it will populate with a
      // `const wchar_t *` pointing to the string resource. For this buffer, use
      // the stack memory that contains the string pointer.
      reinterpret_cast<wchar_t *>(&string),
      0
    );
    if (!size) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "LoadStringW() failed"
      );
    }
    return boost::nowide::narrow(std::wstring(string, size));
  }

  std::string loadFile(int unsigned const id, int unsigned const type) {
    return loadUserDefined(id, type);
  }

  HWND createDialog(
    int unsigned const id,
    HWND const parent,
    DLGPROC const callback
  ) {
    HMODULE const module = getModule();
    assert(module);

    SetLastError(ERROR_SUCCESS);
    HWND const dialog{
      CreateDialog(module, MAKEINTRESOURCE(id), parent, callback)
    };
    if (!dialog) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "CreateDialog() failed"
      );
    }
    return dialog;
  }

  INT_PTR openModalDialog(
    int unsigned const id,
    HWND const parent,
    DLGPROC const callback
  ) {
    HMODULE const module = getModule();
    assert(module);

    SetLastError(ERROR_SUCCESS);
    INT_PTR result = DialogBoxParam(
      module, MAKEINTRESOURCE(id), parent, callback, NULL
    );
    if (!result) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "CreateDialog() failed"
      );
    }
    return result;
  }

  void closeModalDialog(HWND const dialog, INT_PTR const result) {
    SetLastError(ERROR_SUCCESS);
    if (!EndDialog(dialog, result)) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "EndDialog() failed"
      );
    }
  }

}

#endif
