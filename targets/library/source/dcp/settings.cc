/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       October 2023
 */

#include <dcp/settings.hh>

#if BOOST_OS_WINDOWS

namespace DCP::Settings {

  std::pair<Tree, std::error_code> Tree::get(
    char const *const path,
    bool const writeable
  ) noexcept {
    assert(path);

    auto pair = Registry::tryToGetKey(
      HKEY_LOCAL_MACHINE, "Software\\" DCP_companyName "\\" DCP_productName,
      writeable
    );
    if (!pair.second && (*path != '\0')) {
      assert(pair.first != Registry::Key{});
      pair = Registry::tryToGetKey(pair.first, path, writeable);
    }
    return {Tree(pair.first), pair.second};
  }

  size_t Tree::NameIterator::getCount() const noexcept {
    return iterator.getCount();
  }

  std::string Tree::NameIterator::getNext() {
    return iterator.getNext();
  }

  size_t Tree::PathIterator::getCount() const noexcept {
    return iterator.getCount();
  }

  std::string Tree::PathIterator::getNext() {
    return iterator.getNext();
  }

  bool Tree::exists(
    char const *const path,
    char const *const name
  ) const noexcept {
    assert(path);
    assert(name);

    return Registry::valueExists(key, path, name);
  }

  std::pair<std::string, std::error_code> Tree::tryToGet(
    char const *const path,
    char const *const name
  ) const {
    assert(path);
    assert(name);

    return Registry::tryToGetStringValue(key, path, name);
  }

  void Tree::set(
    char const *const path,
    char const *const name,
    std::string value,
    bool const overwrite
  ) {
    assert(path);
    assert(name);

    // If not overwriting, this is subject to a "time-of-check to time-of-use"
    // race, but there isn't an easy way around given the Windows Registry API.
    if (overwrite || !exists(path, name)) {
      Registry::setStringValue(value.data(), key, path, name);
    }
  }

  void Tree::unset(
    char const *const path,
    char const *const name
  ) {
    assert(path);
    assert(name);

    if (*name == '\0') {
      Registry::removeKey(key, path);
    } else {
      Registry::removeValue(key, path, name);
    }
  }

  Tree::Tree(Registry::Key key) noexcept :
  key{std::move(key)}
  {}

}

#endif
