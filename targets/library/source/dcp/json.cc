/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2021
 */

#include <dcp/json.hh>

#include <boost/json/src.hpp>

#include <istream>

namespace DCP::JSON {

  boost::json::value read(std::istream &is, boost::system::error_code &ec) {
    boost::json::stream_parser p;
    std::string line;
    while (std::getline(is, line)) {
      p.write(line, ec);
      if (ec) {
        return nullptr;
      }
    }
    p.finish(ec);
    if (ec) {
      return nullptr;
    }
    return p.release();
  }

}
