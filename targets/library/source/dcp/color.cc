/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       August 2020
 */

#include <dcp/color.hh>

#if BOOST_OS_WINDOWS

  #include <iomanip>
  #include <sstream>

namespace DCP::Color {

  Value fromString(std::string const &string) {
    return std::stoul(string, 0, 16);
  }

  std::string toString(Value const color) {
    std::stringstream ss;
    ss << "0x" << std::setfill('0') << std::setw(8) << std::hex;
    ss << color;
    return ss.str();
  }

}

#endif
