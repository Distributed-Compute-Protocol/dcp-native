/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2020
 */

#include <dcp/write_buffer.hh>

#include <dcp/logger.hh>

#include <boost/thread/thread.hpp>

namespace DCP {

  void WriteBuffer::pop(Write const &write) {
    if (queue.empty()) {
      return;
    }

    std::string const &string = queue.front();
    size_t const size = string.size();
    assert(offset < size);

    size_t writeSize = size - offset;
    write(string.data() + offset, writeSize);

    log(LogLevel::trace, "Write Buffer: Advancing offset %zu by %zu byte(s)",
      offset, writeSize
    );
    offset += writeSize;
    if (offset >= size) {
      assert(offset == size);
      log(LogLevel::trace,
        "Write Buffer: Popping string from queue and resetting offset to 0"
      );
      queue.pop();
      offset = 0;
    }
  }

  void WriteBuffer::push(std::string &&string) {
    log(LogLevel::trace, "Write Buffer: Pushing %zu byte(s)", string.size());
    queue.emplace(std::move(string));
  }

  bool WriteBuffer::isEmpty() const {
    return queue.empty();
  }

  void WriteBuffer::send(
    SOCKET const socket,
    char const *const string,
    Time::Milliseconds const timeout
  ) {
    assert(string);

    push(string);
    while (
      !isEmpty() && !Socket::wait({{}, {socket}, {}}, timeout).output.empty()
    ) {
      pop(
        [socket](char const *memory, size_t &size) {
          assert(memory);
          Socket::write(socket, memory, size);
        }
      );
      boost::this_thread::yield();
    }
    assert(isEmpty());
  }

  WriteBuffer::~WriteBuffer() = default;

}
