/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       December 2019
 */

#include <dcp/log_level.hh>

#if BOOST_OS_UNIX || BOOST_OS_MACOS
  #include <syslog.h>

namespace DCP::LogLevel {

  static_assert(
    (
      emergency == LOG_EMERG &&
      alert == LOG_ALERT &&
      critical == LOG_CRIT &&
      error == LOG_ERR &&
      warning == LOG_WARNING &&
      notice == LOG_NOTICE &&
      info == LOG_INFO &&
      debug == LOG_DEBUG
    ),
    "Log levels do not conform to standard syslog priority levels."
  );

}

#endif
