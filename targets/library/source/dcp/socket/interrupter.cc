/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#include <dcp/socket/interrupter.hh>

#include <dcp/logger.hh>

namespace DCP::Socket {

  Interrupter::Interrupter() :
  sockets{
#if BOOST_OS_WINDOWS
    {Handle(INVALID_SOCKET), createSocket()}
#else
    createPair()
#endif
  }
  {
    if (sockets[1] == INVALID_SOCKET) {
      throw std::runtime_error(
        "Interrupter output socket could not be created"
      );
    }
#if BOOST_OS_WINDOWS
    log(LogLevel::debug, "Created interrupter socket %" DCP_Socket_print,
      static_cast<SOCKET>(sockets[1])
    );
#else
    if (sockets[0] == INVALID_SOCKET) {
      throw std::runtime_error(
        "Interrupter input socket could not be created"
      );
    }
    log(LogLevel::debug,
      "Created interrupter sockets"
      " %" DCP_Socket_print " and %" DCP_Socket_print,
      static_cast<SOCKET>(sockets[0]),
      static_cast<SOCKET>(sockets[1])
    );
#endif
  }

  Interrupter::operator SOCKET() const {
    return sockets[1];
  }

  bool Interrupter::invoke() {
    tryToLog(LogLevel::debug, "Invoking interrupter...");
#if BOOST_OS_WINDOWS
    // Close the original socket and create a new one for the next invoke.
    // This will throw if the socket cannot be created.
    sockets[1] = createSocket();
    return true;
#else
    // Send any byte to the socket.
    size_t size{1U};
    Socket::write(sockets[0], "\0", size);
    return (0U < size);
#endif
  }

  Interrupter::~Interrupter() = default;

}
