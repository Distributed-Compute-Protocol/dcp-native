/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       February 2022
 */

#include <dcp/platform.hh>

#include <boost/algorithm/string/case_conv.hpp>

#if !BOOST_OS_WINDOWS
  #include <sys/utsname.h>
#endif

#include <system_error>

namespace DCP::Platform {

  std::string getName() {
#if BOOST_OS_WINDOWS
    return "win32";
#else
    utsname buffer{};
    errno = 0;
    if (uname(&buffer) == -1) {
      throw std::system_error(
        std::error_code(errno, std::system_category()), "uname() failed"
      );
    }
    std::string name{buffer.sysname};
    boost::algorithm::to_lower(name);
    return name;
#endif
  }

}
