/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       December 2019
 */

#include <dcp/socket.hh>

#include <dcp/logger.hh>
#include <dcp/number.hh>
#include <dcp/read_buffer.hh>
#include <dcp/signal.hh>
#include <dcp/socket/interrupter.hh>
#include <dcp/write_buffer.hh>

#include <socketpair.h>

#include <boost/format.hpp>
#include <boost/thread.hpp>

#if BOOST_OS_WINDOWS
  #include <winsock2.h>
  #include <ws2tcpip.h>
#else
  #include <arpa/inet.h>
  #include <fcntl.h>
  #include <netdb.h>
  #include <unistd.h>
#endif

#include <cerrno>
#include <csignal>
#include <stdexcept>

namespace DCP::Socket {

  /// Reset the last socket error.
  void resetError() {
#if BOOST_OS_WINDOWS
    WSASetLastError(0);
#else
    errno = 0;
#endif
  }

  /// Get the last socket error.
  int getError() {
    return (
#if BOOST_OS_WINDOWS
      WSAGetLastError()
#else
      errno
#endif
    );
  }

  /** A singleton class that manages socket library
   *  initialization/deinitialization and exposes portable socket operations.
   */
  struct Implementation final {

    /** Get the socket implementation instance, constructing on the initial
     *  call.
     *
     *  @throw  std::runtime_error
     *          An error occurred during initialization.
     */
    static Implementation const &getInstance() {
      static Implementation const implementation;
      return implementation;
    }

    Implementation(Implementation const &) = delete;
    Implementation(Implementation const &&) = delete;

    Implementation &operator =(Implementation const &) = delete;
    Implementation &operator =(Implementation const &&) = delete;

    template <typename... Arguments>
    [[nodiscard]]
    SOCKET create(Arguments &&... arguments) const {
      resetError();
      auto const result = socket(std::forward<Arguments>(arguments)...);
      if (result == INVALID_SOCKET) {
        throw std::system_error(
          std::error_code(getError(), std::system_category()),
          "socket() failed"
        );
      }
      return result;
    }

    [[nodiscard]]
    std::array<Handle, 2> createPair() const {
      SOCKET pair[2]{INVALID_SOCKET, INVALID_SOCKET};

      resetError();
      if (dumb_socketpair(pair, true) != 0) {
        throw std::system_error(
          std::error_code(getError(), std::system_category()),
          "socketpair() failed"
        );
      }

      Handle handle0(pair[0], Socket::close);
      assert(handle0 != INVALID_SOCKET);
      unblock(handle0);
#if !BOOST_OS_WINDOWS
      setCloseOnExec(handle0);
#endif

      Handle handle1(pair[1], Socket::close);
      assert(handle1 != INVALID_SOCKET);
      unblock(handle1);
#if !BOOST_OS_WINDOWS
      setCloseOnExec(handle1);
#endif

      return {handle0, handle1};
    }

    void unblock(SOCKET const socket) const {
#if BOOST_OS_WINDOWS
      ULONG nonBlock{1};
      resetError();
      if (ioctlsocket(socket, FIONBIO, &nonBlock) != 0) {
        throw std::system_error(
          std::error_code(getError(), std::system_category()),
          "ioctlsocket() failed"
        );
      }
#else
      resetError();
      int flags{fcntl(socket, F_GETFL, 0)};
      if (flags == -1) {
        throw std::system_error(
          std::error_code(getError(), std::system_category()),
          "fcntl(F_GETFL) failed"
        );
      }
      flags |= O_NONBLOCK;

      resetError();
      if (fcntl(socket, F_SETFL, flags) == -1) {
        throw std::system_error(
          std::error_code(getError(), std::system_category()),
          "fcntl(F_SETFL) failed"
        );
      }
#endif
    }

#if !BOOST_OS_WINDOWS
    void setCloseOnExec(SOCKET const socket, bool const value = true) const {
      resetError();
      int flags{fcntl(socket, F_GETFD, 0)};
      if (flags == -1) {
        throw std::system_error(
          std::error_code(getError(), std::system_category()),
          "fcntl(F_GETFD) failed"
        );
      }
      flags = value ? (flags | FD_CLOEXEC) : (flags & ~FD_CLOEXEC);

      resetError();
      if (fcntl(socket, F_SETFD, flags) == -1) {
        throw std::system_error(
          std::error_code(getError(), std::system_category()),
          "fcntl(F_SETFL) failed"
        );
      }
    }
#endif

    [[nodiscard]]
    // NOLINTNEXTLINE(readability-function-cognitive-complexity)
    SetTuple wait(
      SetTuple sockets,
      Time::Milliseconds const timeout
    ) const {
#if BOOST_OS_WINDOWS
      int const nfds{};
#else
      int const nfds{
        std::max(
          {
            sockets.input.empty() ? -1 : *std::max_element(
              sockets.input.cbegin(), sockets.input.cend()
            ),
            sockets.output.empty() ? -1 : *std::max_element(
              sockets.output.cbegin(), sockets.output.cend()
            ),
            sockets.error.empty() ? -1 : *std::max_element(
              sockets.error.cbegin(), sockets.error.cend()
            )
          }
        ) + 1
      };
      assert(nfds > 0);
#endif

      fd_set rfds;
      FD_ZERO(&rfds); // NOLINT(readability-isolate-declaration)
      for (SOCKET input: sockets.input) {
        FD_SET(input, &rfds);
      }

      fd_set wfds;
      FD_ZERO(&wfds); // NOLINT(readability-isolate-declaration)
      for (SOCKET output: sockets.output) {
        FD_SET(output, &wfds);
      }

      fd_set efds;
      FD_ZERO(&efds); // NOLINT(readability-isolate-declaration)
      for (SOCKET error: sockets.error) {
        FD_SET(error, &efds);
      }

      timeval tv{};
      tv.tv_sec = Number::clamp<decltype(tv.tv_sec)>(
        timeout / Time::millisecondsPerSecond
      );
      tv.tv_usec = Number::clamp<decltype(tv.tv_usec)>(
        (
          timeout % Time::millisecondsPerSecond
        ) * Time::microsecondsPerMillisecond
      );

      resetError();
      if (int fds{select(nfds, &rfds, &wfds, &efds, &tv)}; fds < 0) {
        auto const error = getError();
        if (
#if BOOST_OS_WINDOWS
          error == WSAEINTR
#else
          error == EAGAIN || error == EINTR
#endif
        ) {
          return {};
        }
        throw std::system_error(
          std::error_code(error, std::system_category()), "select() failed"
        );
      }

      for (
        auto i = sockets.input.begin();
        i != sockets.input.end();
        FD_ISSET(*i, &rfds) ? ++i : (i = sockets.input.erase(i))
      ) {}
      for (
        auto i = sockets.output.begin();
        i != sockets.output.end();
        FD_ISSET(*i, &wfds) ? ++i : (i = sockets.output.erase(i))
      ) {}
      for (
        auto i = sockets.error.begin();
        i != sockets.error.end();
        FD_ISSET(*i, &efds) ? ++i : (i = sockets.error.erase(i))
      ) {}
      return sockets;
    }

    bool read(SOCKET const input, char *const memory, size_t &size) const {
      assert(memory);
      resetError();
#if BOOST_OS_WINDOWS
      int result{recv(input, memory, boost::numeric_cast<int>(size), 0)};
#else
      ssize_t result{::read(input, memory, size)};
#endif
      if (!result) {
        size = 0;
        return false;
      }
      if (result < 0) {
        if (
          auto const error = getError();
#if BOOST_OS_WINDOWS
          error != WSAEWOULDBLOCK && error != WSAEINTR
#else
          error != EWOULDBLOCK && error != EINTR
#endif
        ) {
          throw std::system_error(
            std::error_code(error, std::system_category()),
#if BOOST_OS_WINDOWS
            "recv() failed"
#else
            "read() failed"
#endif
          );
        }
        result = 0;
      }
      size = static_cast<size_t>(result);
      return true;
    }

    void write(
      SOCKET const output,
      char const *const memory,
      size_t &size
    ) const {
      assert(memory);
      resetError();
#if BOOST_OS_WINDOWS
      int result{::send(output, memory, boost::numeric_cast<int>(size), 0)};
#else
      ssize_t result{::write(output, memory, size)};
#endif
      if (result < 0) {
        if (
          auto const error = getError();
#if BOOST_OS_WINDOWS
          error != WSAEWOULDBLOCK && error != WSAEINTR
#else
          error != EWOULDBLOCK && error != EINTR
#endif
        ) {
          throw std::system_error(
            std::error_code(error, std::system_category()),
#if BOOST_OS_WINDOWS
            "send() failed"
#else
            "write() failed"
#endif
          );
        }
        result = 0;
      }
      size = static_cast<size_t>(result);
    }

    bool receive(
      SOCKET socket,
      std::string &string,
      Time::Milliseconds const timeout
    ) const {
      while (wait({{socket}, {}, {}}, timeout).input.empty()) {
        log(LogLevel::debug, "Waiting until socket is ready to read...");
        boost::this_thread::yield();
      }

      ReadBuffer readBuffer{};
      return readBuffer.receive(socket, string, timeout);
    }

    void send(
      SOCKET socket,
      char const *const string,
      Time::Milliseconds const timeout
    ) const {
      assert(string);

      while (wait({{}, {socket}, {}}, timeout).output.empty()) {
        log(LogLevel::debug, "Waiting until socket is ready to write...");
        boost::this_thread::yield();
      }

      WriteBuffer writeBuffer{};
      writeBuffer.send(socket, string, timeout);
    }

    [[nodiscard]]
    boost::optional<Address> getAddress(SOCKET const socket) const {
      sockaddr_storage storage{};
#if BOOST_OS_WINDOWS
      using socklen_t = int;
#endif
      socklen_t storageSize{sizeof(storage)};
      resetError();
      auto const status = getsockname(
        socket, reinterpret_cast<sockaddr *>(&storage), &storageSize
      );
      if (status != 0) {
        log(LogLevel::notice, "getsockname() failed: %s",
          std::error_code(getError(), std::system_category()).message().data()
        );
        return boost::none;
      }
      switch (storage.ss_family) {
      case AF_INET:
        {
          auto const *const sa = reinterpret_cast<sockaddr_in *>(&storage);
          assert(sa);
          char ip[INET_ADDRSTRLEN];
          resetError();
          if (!inet_ntop(AF_INET, &(sa->sin_addr), ip, INET_ADDRSTRLEN)) {
            break;
          }
          short unsigned const port{ntohs(sa->sin_port)};
          log(LogLevel::debug,
            "Socket %" DCP_Socket_print " has IPv4 address: %s:%hu",
            socket, ip, port
          );
          return Address(ip, port, false);
        }
      case AF_INET6:
        {
          auto const *const sa = reinterpret_cast<sockaddr_in6 *>(&storage);
          assert(sa);
          char ip[INET6_ADDRSTRLEN];
          resetError();
          if (!inet_ntop(AF_INET6, &(sa->sin6_addr), ip, INET6_ADDRSTRLEN)) {
            break;
          }
          short unsigned const port{ntohs(sa->sin6_port)};
          log(LogLevel::debug,
            "Socket %" DCP_Socket_print " has IPv6 address [%s]:%hu",
            socket, ip, port
          );
          return Address(ip, port, true);
        }
      default:
        log(LogLevel::notice, "getsockname() gave unexpected family: %s",
          std::to_string(storage.ss_family).data()
        );
        return boost::none;
      }
      log(LogLevel::notice, "inet_ntop() failed: %s",
        std::error_code(getError(), std::system_category()).message().data()
      );
      return boost::none;
    }

    [[nodiscard]]
    boost::optional<Address> getAvailableAddress(
      char const *const host = nullptr
    ) const {
      Handle const socket(get(host, "0", bind), Socket::close);
      if (socket == INVALID_SOCKET) {
        log(LogLevel::error, "Could not create socket");
        return boost::none;
      }
      return getAddress(socket);
    }

    bool listen(
      char const *const host,
      char const *const port,
      std::function<void (Handle, short unsigned)> const &listenCallback,
      std::function<bool (Handle)> const &acceptCallback,
      Interrupter *const interrupter,
      Time::Milliseconds const timeout
    ) const {
      log(LogLevel::debug,
        "Attempting to listen at host \"%s\"%s on port \"%s\"%s...",
        host ? host : "", host ? "" : " (null)",
        port ? port : "", port ? "" : " (null)"
      );

      // Create a TCP socket, listening on the given port.
      Handle const listenSocket(get(host, port, bind), Socket::close);
      if (listenSocket == INVALID_SOCKET) {
        log(LogLevel::notice, "Could not create listening socket");
        return false;
      }
      log(LogLevel::debug, "Created listening socket %" DCP_Socket_print,
        static_cast<SOCKET>(listenSocket)
      );

      // Configure the TCP socket as non-blocking, and close-on-exec on POSIX.
      unblock(listenSocket);
#if !BOOST_OS_WINDOWS
      setCloseOnExec(listenSocket);
#endif

      // Get the bound port.
      auto const boundAddress = getAddress(listenSocket);
      if (!boundAddress) {
        log(LogLevel::notice, "listen() failed: getAddress() returned none");
        return false;
      }

      // Listen on the bound port.
      resetError();
      if (auto const status = ::listen(listenSocket, SOMAXCONN); status != 0) {
        log(LogLevel::notice, "listen() failed: %s",
          std::error_code(getError(), std::system_category()).message().data()
        );
        return false;
      }
      log(LogLevel::info,
        boundAddress->isIPV6() ?
        "Listening at IPv6 address [%s]:%hu..." :
        "Listening at IPv4 address %s:%hu...",
        boundAddress->getIP().data(), boundAddress->getPort()
      );
      listenCallback(listenSocket, boundAddress->getPort());

      // Accept client sockets and call the accept callback for each.
      // On timeout, call the accept callback with INVALID_SOCKET.
      for (bool shouldContinue = true; shouldContinue; ) {
        Handle acceptSocket{};

        SetTuple waitSockets{{listenSocket}, {}, {}};
        if (interrupter) {
          waitSockets.input.insert(*interrupter);
        }

        if (
          auto const &readySockets = wait(waitSockets, timeout);
          readySockets.input.find(listenSocket) != readySockets.input.end()
        ) {
          resetError();
          acceptSocket = Handle(
            accept(listenSocket, nullptr, nullptr),
            Socket::shutDownAndClose
          );
          if (acceptSocket == INVALID_SOCKET) {
            log(LogLevel::notice, "accept() failed: %s",
              std::error_code(
                getError(), std::system_category()
              ).message().data()
            );
          }
        }

        if (acceptSocket == INVALID_SOCKET) {
          boost::this_thread::yield();
        } else {
          log(LogLevel::debug, "Accepted connection socket %" DCP_Socket_print,
            static_cast<SOCKET>(acceptSocket)
          );
          Socket::unblock(acceptSocket);
        }
        shouldContinue = acceptCallback(std::move(acceptSocket));
      }

      return true;
    }

    Handle connect(char const *const host, char const *const port) const {
      // Create a TCP socket, connected to the given host and port.
      Handle socket(get(host, port, ::connect), Socket::shutDownAndClose);
      if (socket == INVALID_SOCKET) {
        log(LogLevel::notice, "Could not create connection socket");
        return socket;
      }
      log(LogLevel::debug, "Created connection socket %" DCP_Socket_print,
        static_cast<SOCKET>(socket)
      );

      // Configure the TCP socket as non-blocking, and close-on-exec on POSIX.
      unblock(socket);
#if !BOOST_OS_WINDOWS
      setCloseOnExec(socket);
#endif

      return socket;
    }

#if !BOOST_OS_WINDOWS
    void duplicate(SOCKET const socket, SOCKET const destination) const {
      resetError();
      if (dup2(socket, destination) == -1) {
        throw std::system_error(
          std::error_code(getError(), std::system_category())
        );
      }
    }
#endif

    bool close(SOCKET const socket) const noexcept {
      if (socket == INVALID_SOCKET) {
        return false;
      }
      tryToLog(LogLevel::debug, "Closing socket %" DCP_Socket_print, socket);

      resetError();
      if (
#if BOOST_OS_WINDOWS
        closesocket(socket) != 0
#else
        ::close(socket) != 0
#endif
      ) {
        tryToLog(LogLevel::debug,
          "Failed to close socket %" DCP_Socket_print ": %s", socket,
          std::error_code(getError(), std::system_category()).message().data()
        );
        return false;
      }
      tryToLog(LogLevel::debug,
        "Socket %" DCP_Socket_print " closed successfully", socket
      );
      return true;
    }

    bool shutDown(SOCKET const socket) const noexcept {
      if (socket == INVALID_SOCKET) {
        return false;
      }
      tryToLog(LogLevel::debug,
        "Shutting down socket %" DCP_Socket_print, socket
      );

      resetError();
      if (
        shutdown(socket,
#if BOOST_OS_WINDOWS
          SD_SEND
#else
          SHUT_WR
#endif
        ) != 0
      ) {
        tryToLog(LogLevel::debug,
          "Failed to shut down socket % " DCP_Socket_print ": %s", socket,
          std::error_code(getError(), std::system_category()).message().data()
        );
        return false;
      }
      tryToLog(LogLevel::debug,
        "Socket %" DCP_Socket_print " shut down successfully", socket
      );
      return true;
    }

    bool shutDownAndClose(SOCKET const socket) const noexcept {
      bool success{true};
      if (!shutDown(socket)) {
        success = false;
      }
      if (!close(socket)) {
        success = false;
      }
      return success;
    }

    ~Implementation() {
      tryToLog(LogLevel::debug, "Socket Implementation: destructing");
#if BOOST_OS_WINDOWS
      WSACleanup();
#endif
    }

  private:

    /** Construct the socket implementation.
     *
     *  @throw  std::runtime_error
     *          An error occurred during initialization.
     */
    Implementation() {
#if BOOST_OS_WINDOWS
      WSADATA wsaData;
      auto const result = WSAStartup(MAKEWORD(2, 2), &wsaData);
      if (result != NO_ERROR) {
        log(LogLevel::error,
          (
            "Socket Implementation: construction failed;"
            " WSAStartup() error: %i"
          ),
          result
        );
        WSACleanup();
        throw std::runtime_error(
          boost::str(boost::format("WSAStartup() failed: %i") % result)
        );
      }
#endif
      log(LogLevel::debug, "Socket Implementation: constructed");
    }

    /** Return a shared `addrinfo` pointer, or null on failure.
     *
     *  @param  host
     *          The host name or address; can be null.
     *  @param  port
     *          The port name or number; can be null.
     */
    std::shared_ptr<addrinfo> getAddrInfo(
      char const *const host,
      char const *const port
    ) const {
      addrinfo hints{};
      hints.ai_family = AF_UNSPEC;
      hints.ai_flags = AI_PASSIVE;
      hints.ai_protocol = IPPROTO_TCP;
      hints.ai_socktype = SOCK_STREAM;

      addrinfo *result{};
      if (auto const error = getaddrinfo(host, port, &hints, &result)) {
        log(LogLevel::notice, "getaddrinfo() failed: %s", gai_strerror(error));
      }
      return {
        result,
        [](addrinfo *ai) {
          if (ai) {
            freeaddrinfo(ai);
          }
        }
      };
    }

    /** Return the first socket for the given host and port for which `action`
     *  is called successfully.
     *
     *  @param  host
     *          The host name or address; can be null.
     *  @param  port
     *          The port name or number; can be null.
     *  @param  action
     *          The action to call on the resulting socket. Takes the socket,
     *          the address, and the address length and returns 0 on success
     *          (or sets the last error number, retrievable by @ref getError()).
     *          The function should not throw; otherwise, the socket will not
     *          be closed.
     */
    template <typename Action>
    SOCKET get(
      char const *const host,
      char const *const port,
      Action const &action
    ) const {
      SOCKET result{INVALID_SOCKET};
      if (auto addrInfo = getAddrInfo(host, port)) {
        for (
          addrinfo *addr{addrInfo.get()};
          addr && (result == INVALID_SOCKET);
          addr = addr->ai_next
        ) {
#if BOOST_OS_WINDOWS
          assert(addr->ai_addrlen <= std::numeric_limits<int>::max());
          auto const addrLength = boost::numeric_cast<int>(addr->ai_addrlen);
#else
          socklen_t const addrLength{addr->ai_addrlen};
#endif

          // Create the socket.
          resetError();
          result = socket(
            addr->ai_family, addr->ai_socktype, addr->ai_protocol
          );
          if (result == INVALID_SOCKET) {
            log(LogLevel::notice, "socket() failed: %s",
              std::error_code(
                getError(), std::system_category()
              ).message().data()
            );
          }

          // Set socket options.
          {
            int option{};

            // Allow both IPv4 and IPv6.
            option = 0;
            setsockopt(result, IPPROTO_IPV6, IPV6_V6ONLY,
              reinterpret_cast<char *>(&option), sizeof(option)
            );

            // See https://stackoverflow.com/a/14388707 for a full breakdown of
            // these options. Note: any setsockopt() failures are non-fatal.
            option = 1;
            setsockopt(result, SOL_SOCKET, SO_REUSEADDR,
              reinterpret_cast<char *>(&option), sizeof(option)
            );
#if defined(SO_REUSEPORT)
            option = 1;
            setsockopt(result, SOL_SOCKET, SO_REUSEPORT,
              reinterpret_cast<char *>(&option), sizeof(option)
            );
#endif
#if defined(SO_EXCLUSIVEADDRUSE)
            option = 1;
            setsockopt(result, SOL_SOCKET, SO_EXCLUSIVEADDRUSE,
              reinterpret_cast<char *>(&option), sizeof(option)
            );
#endif
          }

          // Perform the action on the socket.
          resetError();
          if (action(result, addr->ai_addr, addrLength) != 0) {
            log(LogLevel::notice, "Socket get action failed: %s",
              std::error_code(
                getError(), std::system_category()
              ).message().data()
            );
            result = INVALID_SOCKET;
          }
        }
      } else {
        log(LogLevel::notice,
          R"(Could not get address info for host "%s" and port "%s")",
          host ? host : "",
          port ? port : ""
        );
      }
      return result;
    }

#if !BOOST_OS_WINDOWS
    /// Ignore SIGPIPE for the scope of the Socket implementation.
    Signal::Handler signalHandler{Signal::Handler(SIG_IGN, SIGPIPE)};
#endif

  };

  std::array<Handle, 2> createPair() {
    return Implementation::getInstance().createPair();
  }

  void unblock(SOCKET const socket) {
    Implementation::getInstance().unblock(socket);
  }

#if !BOOST_OS_WINDOWS
  void setCloseOnExec(SOCKET const socket, bool const value) {
    Implementation::getInstance().setCloseOnExec(socket, value);
  }
#endif

  SetTuple wait(SetTuple sockets, Time::Milliseconds const timeout) {
    return Implementation::getInstance().wait(std::move(sockets), timeout);
  }

  bool read(SOCKET const input, char *const memory, size_t &size) {
    return Implementation::getInstance().read(input, memory, size);
  }

  void write(SOCKET const output, char const *const memory, size_t &size) {
    Implementation::getInstance().write(output, memory, size);
  }

  void send(
    SOCKET const socket,
    char const *const string,
    Time::Milliseconds const timeout
  ) {
    Implementation::getInstance().send(socket, string, timeout);
  }

  bool receive(
    SOCKET const socket,
    std::string &string,
    Time::Milliseconds const timeout
  ) {
    return Implementation::getInstance().receive(socket, string, timeout);
  }

  boost::optional<Address> getAddress(SOCKET const socket) {
    return Implementation::getInstance().getAddress(socket);
  }

  boost::optional<Address> getAvailableAddress(char const *const host) {
    return Implementation::getInstance().getAvailableAddress(host);
  }

  bool listen(
    char const *const host,
    char const *const port,
    std::function<void (Handle, short unsigned)> const &listenCallback,
    std::function<bool (Handle)> const &acceptCallback,
    Interrupter *const interrupter,
    Time::Milliseconds const timeout
  ) {
    return Implementation::getInstance().listen(
      host, port, listenCallback, acceptCallback, interrupter, timeout
    );
  }

  Handle connect(char const *const host, char const *const port) {
    return Implementation::getInstance().connect(host, port);
  }

#if !BOOST_OS_WINDOWS
  void duplicate(SOCKET const socket, SOCKET const destination) {
    Implementation::getInstance().duplicate(socket, destination);
  }
#endif

  bool close(SOCKET const socket) noexcept {
    return Implementation::getInstance().close(socket);
  }

  bool shutDown(SOCKET const socket) noexcept {
    return Implementation::getInstance().shutDown(socket);
  }

  bool shutDownAndClose(SOCKET const socket) noexcept {
    return Implementation::getInstance().shutDownAndClose(socket);
  }

#if BOOST_OS_WINDOWS
  Handle Interrupter::createSocket() {
    return Handle(
      Implementation::getInstance().create(AF_INET, SOCK_DGRAM, 0), close
    );
  }
#endif

}
