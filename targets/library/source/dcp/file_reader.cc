/**
 *  @file
 *  @author     Wes Garland, wes@sparc.network
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2018
 */

#include <dcp/file_reader.hh>

#include <dcp/logger.hh>

#include <cassert>

#if BOOST_OS_UNIX || BOOST_OS_MACOS

  #include <boost/numeric/conversion/cast.hpp>

  #include <sys/file.h>
  #include <sys/mman.h>
  #include <sys/stat.h>
  #include <unistd.h>

  #include <cerrno>
  #include <system_error>
  #include <thread>

namespace DCP {

  /** The Unix file reader implementation.
   *
   *  @note
   *    The `boost::interprocess::file_lock` Unix implementation currently uses
   *    `fcntl`, which doesn't have the desired locking characteristics and does
   *    not interoperate well with `fs-ext`, whose `flock()` function works well
   *    on Linux and macOS, but whose `fcntl()` function produced an error
   *    (`EINVAL`) in macOS testing.
   */
  struct FileReader::Implementation final {

    /** Construct the Unix file reader implementation.
     *
     *  @throw  std::system_error
     *          There was an error opening, inspecting, or memory mapping the
     *          file.
     */
    explicit Implementation(boost::filesystem::path const &filePath) :
    handle(
      [&filePath]() {
        errno = 0;
        int const fd = open(filePath.string().data(), O_RDONLY | O_CLOEXEC);
        if (fd == -1) {
          throw std::system_error(
            std::error_code(errno, std::system_category()), filePath.string()
          );
        }
        return Handle<int, -1>(fd, close);
      }()
    )
    {
      // Lock file.
      for (int lastError = 0;; ) {
        errno = 0;
        if (flock(handle, LOCK_SH | LOCK_NB) != 0) {
          if (int const error = errno; error != lastError) {
            log(LogLevel::warning, "flock() failed for file \"%s\": %s",
              filePath.string().data(),
              std::error_code(error, std::system_category()).message().data()
            );
            lastError = error;
          }
          std::this_thread::sleep_for(std::chrono::seconds(1));
          continue;
        }
        break;
      }

      // Get file size.
      errno = 0;
      {
        using stat = struct stat;
        stat sb{};
        if (fstat(handle, &sb) < 0) {
          throw std::system_error(
            std::error_code(errno, std::system_category()), filePath.string()
          );
        }
        size = boost::numeric_cast<size_t>(sb.st_size);
      }

      // Get memory map.
      errno = 0;
      data = mmap(
        nullptr, size, PROT_READ, MAP_SHARED | MAP_NORESERVE, handle, 0
      );
      // NOLINTNEXTLINE(performance-no-int-to-ptr)
      if (data == MAP_FAILED) {
        throw std::system_error(
          std::error_code(errno, std::system_category()), filePath.string()
        );
      }
    }

    Implementation(Implementation const &) = delete;
    Implementation(Implementation const &&) = delete;

    Implementation &operator =(Implementation const &) = delete;
    Implementation &operator =(Implementation const &&) = delete;

    [[nodiscard]]
    char const *getData() const {
      assert(data);
      return static_cast<const char *>(data);
    }

    [[nodiscard]]
    size_t getSize() const {
      return size;
    }

    ~Implementation() {
      munmap(data, size);
    }

  private:

    Handle<int, -1> handle;
    void *data = nullptr;
    size_t size = 0;

  };

}

#elif BOOST_OS_WINDOWS

  #include <dcp/handle.hh>

  #include <boost/format.hpp>
  #include <boost/numeric/conversion/cast.hpp>

  #include <windows.h>

namespace DCP {

  /// The Windows file reader implementation.
  struct FileReader::Implementation final {

    /** Construct the Windows file reader implementation.
     *
     *  @throw  std::system_error
     *          There was an error opening, inspecting, or memory mapping the
     *          file.
     */
    explicit Implementation(boost::filesystem::path const &filePath) :
    file(
      [&filePath]() {
        for (DWORD lastError = ERROR_SUCCESS;; ) {
          SetLastError(ERROR_SUCCESS);
          HANDLE const handle = CreateFile(filePath.string().data(),
            GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
            FILE_ATTRIBUTE_READONLY | FILE_FLAG_SEQUENTIAL_SCAN,
            NULL
          );
          if (handle == INVALID_HANDLE_VALUE) {
            if (DWORD const error = GetLastError(); error != lastError) {
              log(LogLevel::warning, "CreateFile() failed for file \"%s\": %s",
                filePath.string().data(),
                std::error_code(
                  GetLastError(), std::system_category()
                ).message().data()
              );
              lastError = error;
            }
            std::this_thread::sleep_for(std::chrono::seconds(1));
            continue;
          }
          return Handle<HANDLE, INVALID_HANDLE_VALUE>(handle, CloseHandle);
        }
      }()
    ),
    mapping(
      [&filePath, this]() {
        SetLastError(ERROR_SUCCESS);
        HANDLE const handle = CreateFileMapping(
          file, NULL, PAGE_READONLY, 0, 0, NULL
        );
        if (!handle) {
          throw std::system_error(
            std::error_code(GetLastError(), std::system_category()),
            boost::str(
              boost::format("CreateFileMapping() failed for file %s") % filePath
            )
          );
        }
        return Handle<HANDLE, nullptr>(handle, CloseHandle);
      }()
    ),
    data(
      [&filePath, this]() {
        SetLastError(ERROR_SUCCESS);
        char const *result = static_cast<const char *>(
          MapViewOfFile(mapping, FILE_MAP_READ, 0, 0, 0)
        );
        if (!result) {
          throw std::system_error(
            std::error_code(GetLastError(), std::system_category()),
            boost::str(
              boost::format("MapViewOfFile() failed for file %s") % filePath
            )
          );
        }
        return Handle<const char *, nullptr>(result, UnmapViewOfFile);
      }()
    ),
    size(
      [&filePath, this]() {
        SetLastError(ERROR_SUCCESS);
        DWORD const result = GetFileSize(file, NULL);
        if (result == INVALID_FILE_SIZE) {
          throw std::system_error(
            std::error_code(GetLastError(), std::system_category()),
            boost::str(
              boost::format("GetFileSize() failed for file %s") % filePath
            )
          );
        }
        return boost::numeric_cast<size_t>(result);
      }()
    )
    {}

    Implementation(Implementation const &) = delete;
    Implementation(Implementation const &&) = delete;

    Implementation &operator =(Implementation const &) = delete;
    Implementation &operator =(Implementation const &&) = delete;

    [[nodiscard]]
    char const *getData() const {
      return data;
    }

    [[nodiscard]]
    size_t getSize() const {
      return size;
    }

    ~Implementation() = default;

  private:

    Handle<HANDLE, INVALID_HANDLE_VALUE> file;
    Handle<HANDLE, nullptr> mapping;
    Handle<const char *, nullptr> data;
    size_t size;

  };

}

#else
  #error Platform not supported.
#endif

namespace DCP {

  FileReader::FileReader(boost::filesystem::path const &filePath) :
  implementation(std::make_unique<Implementation>(filePath))
  {}

  char const *FileReader::getData() const {
    assert(implementation);
    return implementation->getData();
  }

  size_t FileReader::getSize() const {
    assert(implementation);
    return implementation->getSize();
  }

  FileReader::~FileReader() = default;

}
