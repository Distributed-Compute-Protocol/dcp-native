/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       May 2020
 */

#include <dcp/graphics.hh>

#if BOOST_OS_WINDOWS

  #include <dcp/logger.hh>

  #include <stdexcept>
  #include <system_error>

namespace DCP::Graphics {

  Handle<HDC, nullptr> getDC(HWND const window) {
    if (HDC const dc = GetDC(window)) {
      return Handle<HDC, nullptr>(
        dc,
        [window](HDC windowDC) {
          ReleaseDC(window, windowDC);
        }
      );
    }
    throw std::runtime_error("GetDC() failed");
  }

  Handle<HDC, nullptr> createCompatibleDC(HDC const dc) {
    if (HDC const memoryDC = CreateCompatibleDC(dc)) {
      return Handle<HDC, nullptr>(memoryDC, DeleteDC);
    }
    throw std::runtime_error("CreateCompatibleDC() failed");
  }

  int getFontHeight(int const height) {
    static int const dpiFontReference{72};
    int const dpiX{GetDeviceCaps(DCP::Graphics::getDC(0), LOGPIXELSX)};
    return -MulDiv(height, dpiX, dpiFontReference);
  }

  Handle<HGDIOBJ, nullptr> selectObject(HDC const dc, HGDIOBJ const object) {
    HGDIOBJ const result = SelectObject(dc, object);
    if (!result || result == HGDI_ERROR) {
      throw std::runtime_error("SelectObject() failed");
    }
    return Handle<HGDIOBJ, nullptr>(
      result,
      [dc](HGDIOBJ oldObject) {
        assert(oldObject && oldObject != HGDI_ERROR);
        SelectObject(dc, oldObject);
      }
    );
  }

  void setBitmapStretchMode(HDC const dc, int const mode) {
    int const result = SetStretchBltMode(dc, mode);
    if (!result || result == ERROR_INVALID_PARAMETER) {
      throw std::runtime_error("SetStretchBltMode() failed");
    }
    if (mode == HALFTONE && !SetBrushOrgEx(dc, 0, 0, NULL)) {
      throw std::runtime_error("SetBrushOrgEx() failed");
    }
  }

  SIZE scaleToFit(
    SIZE size,
    SIZE const containerSize,
    double const maxContainerFraction
  ) {
    double const aspect{(double)size.cx / (double)size.cy};
    double const containerFractionX{(double)size.cx / (double)containerSize.cx};
    double const containerFractionY{(double)size.cy / (double)containerSize.cy};
    if (containerFractionX > containerFractionY) {
      if (containerFractionX > maxContainerFraction) {
        size.cx = static_cast<LONG>(
          (double)containerSize.cx * maxContainerFraction
        );
        size.cy = static_cast<LONG>((double)size.cx / aspect);
      }
    } else {
      if (containerFractionY > maxContainerFraction) {
        size.cy = static_cast<LONG>(
          (double)containerSize.cy * maxContainerFraction
        );
        size.cx = static_cast<LONG>((double)size.cy * aspect);
      }
    }
    return size;
  }

  Handle<HBRUSH, nullptr> createSolidBrush(Color::Value const color) {
    if (HBRUSH const brush = CreateSolidBrush(color)) {
      return Handle<HBRUSH, nullptr>(brush, DeleteObject);
    }
    throw std::runtime_error("CreateSolidBrush() failed");
  }

}

#endif
