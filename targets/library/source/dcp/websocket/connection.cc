/**
 *  @file
 *  @author     Severn Lortie, severn@distributive.network
 *  @date       April 2024
 */

#include <dcp/websocket/connection.hh>
#include <dcp/logger.hh>

#include <boost/beast/core/ostream.hpp>

namespace DCP::Websocket {

  Connection::Connection(
    boost::beast::websocket::stream<boost::asio::ip::tcp::socket> &&websocket
  ) :
  websocket{std::move(websocket)}
  {
    log(LogLevel::debug, "Websocket connection: constructed");
  }

  void Connection::setReadCallback(
    std::function<void (boost::beast::flat_buffer &)> callback
  ) {
    readCallback = std::move(callback);
    readLoop();
  }

  void Connection::setDisconnectCallback(std::function<void ()> callback) {
    disconnectCallback = std::move(callback);
  }

  boost::asio::detail::socket_type Connection::getSocket() {
    return websocket.next_layer().native_handle();
  }

  // NOLINTNEXTLINE(misc-no-recursion)
  void Connection::send(
    std::string const &message,
    std::function<void ()> callback
  ) {
    sendBuffer.clear();
    boost::beast::ostream(sendBuffer) << message;
    websocket.text(websocket.got_text());
    auto self = shared_from_this();
    websocket.async_write(
      sendBuffer.data(),
      // NOLINTNEXTLINE(misc-no-recursion)
      [self, callback{std::move(callback)}](
        boost::beast::error_code error,
        [[maybe_unused]] std::size_t bytesTransferred
      ) {
        assert(self);
        if (error) {
          self->handleError(error);
          return;
        }
        callback();
      }
    );
  }

  // NOLINTNEXTLINE(misc-no-recursion)
  void Connection::readLoop() {
    auto self = shared_from_this();
    websocket.async_read(readBuffer,
      // NOLINTNEXTLINE(misc-no-recursion)
      [self](
        boost::beast::error_code error,
        [[maybe_unused]] std::size_t bytesTransferred
      ) {
        assert(self);
        if (error) {
          self->handleError(error);
          return;
        }
        self->readCallback(self->readBuffer);
        self->readBuffer.clear();
        self->readLoop();
      }
    );
  }

  void Connection::handleError(boost::beast::error_code const error) {
    if (
      error == boost::beast::websocket::error::closed ||
      error == boost::asio::error::eof ||
      error == boost::asio::error::operation_aborted ||
      error == boost::asio::error::connection_reset ||
      error == boost::asio::error::not_connected
    ) {
      disconnectCallback();
      return;
    }
    throw boost::beast::system_error(error);
  }

}
