/**
 *  @file
 *  @author     Severn Lortie, severn@distributive.network
 *  @date       April 2024
 */

#include <dcp/websocket/server.hh>
#include <dcp/logger.hh>

#include <boost/asio.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/make_shared.hpp>
#include <boost/utility/in_place_factory.hpp>

namespace DCP::Websocket {

  Server::Server(short unsigned const port) :
  port{port},
  acceptor(
    ioContext,
    boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)
  )
  {
#if BOOST_OS_WINDOWS
    boost::asio::detail::win_thread::set_terminate_threads(true);
#endif
    acceptor.non_blocking(true);
  }

  void Server::setDisconnectCallback(std::function<void ()> callback) {
    disconnectCallback = std::move(callback);
  }

  void Server::setReadCallback(
    std::function<void (boost::beast::flat_buffer &)> callback
  ) {
    readCallback = std::move(callback);
  }

  void Server::send(std::string message) {
    sendQueue.emplace(std::move(message));
    if (sendLoopActive || !connection) {
      return;
    }
    sendLoop();
  }

  void Server::acceptConnection() {
    socket = boost::asio::ip::tcp::socket(ioContext);
    acceptor.async_accept(
      *socket,
      [this](boost::system::error_code const error) {
        if (error) {
          tryToLog(LogLevel::error, error.message().data());
          return;
        }
        // A shared pointer is needed here to keep the websocket alive until
        // the async_accept handler is invoked and it can be moved into the
        // Connection instance.
        auto websocket = std::make_shared<
          boost::beast::websocket::stream<boost::asio::ip::tcp::socket>
        >(std::move(*socket));
        assert(websocket);
        websocket->async_accept(
          [this, websocket](boost::beast::error_code const error) {
            if (error) {
              tryToLog(LogLevel::error, error.message().data());
              return;
            }
            connection = boost::make_shared<Connection>(std::move(*websocket));
            assert(connection);
            connection->setDisconnectCallback(
              [this]() {
                handleDisconnect();
              }
            );
            connection->setReadCallback(readCallback);
          }
        );
        socket.reset();
      }
    );
  }

  void Server::handleDisconnect() {
    assert(connection);
    if (disconnectCallback) {
      disconnectCallback();
    }
    connection.reset();
    acceptConnection();
  }

  void Server::sendLoop() {
    if (sendQueue.empty() || !connection) {
      sendLoopActive = false;
      return;
    }
    sendLoopActive = true;
    assert(connection);
    connection->send(sendQueue.front(),
      [this]() {
        sendQueue.pop();
        sendLoop();
      }
    );
  }

  short unsigned Server::getPort() const {
    return port;
  }

  boost::asio::detail::socket_type Server::getSocket() {
    if (connection) {
      return connection->getSocket();
    }
    return acceptor.native_handle();
  }

  void Server::pollEventLoop() {
    ioContext.poll();
    ioContext.restart();
  }

}
