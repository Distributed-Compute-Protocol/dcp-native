/**
 *  @file
 *  @author     Wes Garland, wes@sparc.network
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2018
 */

#include <dcp/read_buffer.hh>

#include <dcp/logger.hh>
#include <dcp/memory.hh>

#include <boost/thread/thread.hpp>

#include <algorithm>
#include <cassert>
#include <cstring>
#include <stdexcept>

namespace DCP {

  size_t const ReadBuffer::minAllocationSz = static_cast<size_t>(1024U * 1024U);
  size_t const ReadBuffer::powAllocationSz = 2U;
  size_t const ReadBuffer::minAvailableSz = 32U;
  size_t const ReadBuffer::maxWasteSz = (minAllocationSz * 64U) - 1U;

  ReadBuffer::ReadBuffer() :
  memSz{minAllocationSz},
  mem{static_cast<char *>(malloc(memSz))}
  {
    if (!mem) {
      throw std::bad_alloc();
    }
    try {
      ln = mem;
      p = ln;
      log(LogLevel::trace, "Read Buffer (%zu, %p, %zu, %zu): Initialized",
        memSz, mem, ln - mem, p - ln
      );
    } catch (...) {
      free(mem);
      throw;
    }
  }

  char const *ReadBuffer::pop() {
    char const *line = nullptr;
    assert(mem && ln && p && mem <= ln && ln <= p);
    char *s = ln;
    for (; s < p && *s && *s != '\n'; ++s) {}
    if (s < p) {
      *s = '\0';
      log(LogLevel::trace,
        "Read Buffer (%zu, %p, %zu, %zu): Found end of line",
        memSz, mem, ln - mem, p - ln
      );

      line = ln;

      ln = s + 1;
      log(LogLevel::trace,
        "Read Buffer (%zu, %p, %zu, %zu): Advanced to next line",
        memSz, mem, ln - mem, p - ln
      );
    }
    return line;
  }

  bool ReadBuffer::push(Read const &read) {
    bool result = true;

    // Free space at the front of the buffer, if possible.
    assert(
      mem && ln && p && mem <= ln && ln <= p &&
      static_cast<size_t>(p - mem) <= memSz
    );
    {
      auto const wasteSz = static_cast<size_t>(ln - mem);
      if (wasteSz > maxWasteSz) {
        memmove(mem, ln, static_cast<size_t>(p - ln));
        ln -= wasteSz;
        p -= wasteSz;
        log(LogLevel::trace,
          "Read Buffer (%zu, %p, %zu, %zu): Trimmed %zu byte(s)",
          memSz, mem, ln - mem, static_cast<size_t>(p - ln), wasteSz
        );
      }
    }

    // Allocate additional buffer space if necessary (and possible).
    // In the event of allocation failure, continue to read whatever possible.
    assert(
      mem && ln && p && mem <= ln && ln <= p &&
      static_cast<size_t>(p - mem) <= memSz
    );
    if ((memSz - static_cast<size_t>(p - mem)) < minAvailableSz) {
      size_t const memSzNew = memSz * powAllocationSz;
      if (memSzNew <= memSz) { // Overflow
        log(LogLevel::warning,
          "Read Buffer (%zu, %p, %zu, %zu):"
          " Could not increment line buffer size beyond %zu byte(s);"
          " next read at %zu",
          memSz, mem, ln - mem, p - ln, memSz, p - mem
        );
      } else if (
        auto *const memNew = static_cast<char *>(realloc(mem, memSzNew))
      ) {
        p = memNew + (p - mem);
        ln = memNew + (ln - mem);
        mem = memNew;
        memSz = memSzNew;
        log(LogLevel::trace,
          "Read Buffer (%zu, %p, %zu, %zu): Reallocated",
          memSz, mem, ln - mem, p - ln
        );
      } else {
        log(LogLevel::warning,
          "Read Buffer (%zu, %p, %zu, %zu):"
          " Insufficient memory for reallocation to %zu byte(s);"
          " next read at %zu",
          memSz, mem, ln - mem, p - ln, memSzNew, p - mem
        );
      }
    }

    // Read data into the buffer.
    // If there is no room available in the buffer for reading, throw.
    assert(
      mem && ln && p && mem <= ln && ln <= p &&
      static_cast<size_t>(p - mem) <= memSz
    );
    if (size_t readSz = (memSz - static_cast<size_t>(p - mem))) {
      log(LogLevel::trace,
        "Read Buffer (%zu, %p, %zu, %zu): Reading with %zu byte(s) available",
        memSz, mem, ln - mem, p - ln, readSz
      );

      result = read(p, readSz);

      p += readSz;
      log(LogLevel::trace,
        "Read Buffer (%zu, %p, %zu, %zu): Advanced p by %zu byte(s)",
        memSz, mem, ln - mem, p - ln, readSz
      );
    } else {
      throw std::overflow_error("Buffer space exceeded");
    }

    return result;
  }

  bool ReadBuffer::receive(
    SOCKET const socket,
    std::string &string,
    Time::Milliseconds const timeout
  ) {
    bool result = true;
    string.clear();
    while (!Socket::wait({{socket}, {}, {}}, timeout).input.empty()) {
      result = push(
        [socket](char *memory, size_t &size) {
          assert(memory);
          try {
            return Socket::read(socket, memory, size);
          } catch (std::system_error const &) {
            log(LogLevel::debug,
              "System error reading socket %" DCP_Socket_print, socket
            );
            return false;
          }
        }
      );
      if (!result) {
        break;
      }
      boost::this_thread::yield();
    }
    if (char const *const line = pop()) {
      string = line;
    }
    return result;
  }

  ReadBuffer::~ReadBuffer() {
    free(mem);
  }

}
