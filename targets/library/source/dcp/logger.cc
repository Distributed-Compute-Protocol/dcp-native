/**
 *  @file
 *  @author     Wes Garland, wes@sparc.network
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2018
 */

#include <dcp/logger.hh>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>

#if BOOST_OS_UNIX || BOOST_OS_MACOS
  #include <syslog.h>
  #include <unistd.h>
#endif

#include <stdexcept>

namespace DCP {

  namespace {

    /// Prefix the string with the process ID, thread ID, and log level.
    std::string prefixString(int const logLevel, char const *const string) {
      assert(string);

      return boost::str(
        boost::format("%lu: %s: %i: %s")
#if defined(_POSIX_VERSION)
        % getpid()
#elif BOOST_OS_WINDOWS
        % GetCurrentProcessId()
#endif
        % boost::lexical_cast<std::string>(boost::this_thread::get_id())
        % logLevel
        % string
      );
    }

    /// Print a log message to standard error, appending a newline.
    void print(char const *const string) {
      assert(string);

      clearerr(stderr);
      if (fprintf(stderr, "%s\n", string) < 0) {
        throw std::system_error(
          std::error_code(ferror(stderr), std::system_category()),
          "fprintf() failed"
        );
      }
    }

  }

  int const Logger::defaultMaxLogLevel = (
#if defined(NDEBUG)
  #if defined(_POSIX_VERSION)
    !isatty(fileno(stdin)) ? LogLevel::notice :
  #endif
    LogLevel::info
#else
    LogLevel::debug
#endif
  );

  int const Logger::defaultMaxSystemLogLevel = (
#if defined(NDEBUG)
    LogLevel::info
#else
    LogLevel::debug
#endif
  );

  Logger &Logger::getInstance(
    std::string const &name,
    int const maxLogLevel,
    int const maxSystemLogLevel
  ) {
    static Logger logger(name, maxLogLevel, maxSystemLogLevel);
    if (!name.empty() && name != logger.name) {
      throw std::invalid_argument(
        boost::str(
          boost::format(
            R"(Invalid name "%s"; already instantiated with name "%s")"
          ) % name % logger.name
        )
      );
    }
    return logger;
  }

  Logger::Logger(
    std::string name,
    int const maxLogLevel,
    int const maxSystemLogLevel
  ) :
  name(
    [name{std::move(name)}]() {
      if (name.empty()) {
        throw std::invalid_argument(
          "The logger name cannot be empty;"
          " this may be because `log` was called before `Logger::getInstance`"
        );
      }
      return name;
    }()
  ),
#if BOOST_OS_WINDOWS
  eventLog(
    [](char const *const name) {
      SetLastError(ERROR_SUCCESS);
      Handle<HANDLE, nullptr> handle(
        RegisterEventSource(nullptr, name),
        DeregisterEventSource
      );
      if (!handle) {
        throw std::system_error(
          std::error_code(GetLastError(), std::system_category()),
          "RegisterEventSource() failed"
        );
      }
      return handle;
    }(this->name.data())
  ),
#endif
  maxLogLevel(maxLogLevel),
  maxSystemLogLevel(maxSystemLogLevel)
  {
    try {
#if defined(_POSIX_VERSION)
      openlog(this->name.data(), LOG_PID, LOG_LOCAL7);
#endif

      this->log(LogLevel::debug,
        "Logger: constructed with maximum log level %i (system maximum: %i)",
        maxLogLevel, maxSystemLogLevel
      );
    } catch (...) {
#if defined(_POSIX_VERSION)
      closelog();
#endif
      throw;
    }
  }

  std::string const &Logger::getName() const {
    return name;
  }

  bool Logger::report(int const logLevel, char const *string) const {
    assert(string);

    if (logLevel < 0 || maxSystemLogLevel < logLevel) {
      return false;
    }

#if BOOST_OS_WINDOWS
    // Map the log level to Windows event type:
    // https://docs.microsoft.com/en-ca/windows/win32/eventlog/event-types
    WORD eventType = EVENTLOG_INFORMATION_TYPE;
    switch (logLevel) {
    case LogLevel::emergency:
      [[fallthrough]];
    case LogLevel::alert:
      [[fallthrough]];
    case LogLevel::critical:
      [[fallthrough]];
    case LogLevel::error:
      eventType = EVENTLOG_ERROR_TYPE;
      break;
    case LogLevel::warning:
      [[fallthrough]];
    case LogLevel::notice:
      eventType = EVENTLOG_WARNING_TYPE;
      break;
    default:
      break;
    }

    // Truncate the string to the maximum allowed size.
    size_t const maximumSize = 31839;
    std::string truncatedString;
    if (strlen(string) > maximumSize) {
      truncatedString.append(string, maximumSize);
      string = truncatedString.data();
    }

    // Report the event to the event log.
    SetLastError(ERROR_SUCCESS);
    if (
      !ReportEvent(
        eventLog, eventType, 0, 1000, nullptr, 1, 0, &string, nullptr
      )
    ) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "ReportEvent() failed"
      );
    }
    return true;
#elif defined(_POSIX_VERSION)
    syslog(
      // Log levels greater than debug are undefined.
      (logLevel > LogLevel::debug) ? LogLevel::debug : logLevel,
      "%s", string
    );
    return true;
#else
    throw std::runtime_error("No system logging available on this platform");
#endif
  }

  bool Logger::log(int const logLevel, char const *const string) const {
    assert(string);

    std::string const &prefixedString = prefixString(logLevel, string);

    bool const printed = (logLevel >= 0 && logLevel <= maxLogLevel);
    if (printed) {
      print(maxLogLevel < LogLevel::debug ? string : prefixedString.data());
    }

    bool const reported = report(logLevel, prefixedString.data());

    return printed || reported;
  }

  bool Logger::tryToLog(
    int const logLevel,
    char const *const string
  ) const noexcept {
    assert(string);

    bool printed = false;
    bool reported = false;
    try {
      std::string prefixedString;
      try {
        prefixedString = prefixString(logLevel, string);
      } catch (...) {
        assert(false);
      }
      try {
        printed = (logLevel >= 0 && logLevel <= maxLogLevel);
        if (printed) {
          print(
            (maxLogLevel < LogLevel::debug || prefixedString.empty()) ? string :
            prefixedString.data()
          );
        }
      } catch (...) {
        assert(false);
      }
      try {
        reported = report(logLevel, prefixedString.data());
      } catch (...) {
        assert(false);
      }
    } catch (...) {
      assert(false);
    }
    return printed || reported;
  }

  Logger::~Logger() {
    this->tryToLog(LogLevel::debug, "Logger: destructing");
#if defined(_POSIX_VERSION)
    closelog();
#endif
  }

}
