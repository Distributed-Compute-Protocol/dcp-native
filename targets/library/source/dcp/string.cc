/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       December 2019
 */

#include <dcp/string.hh>

#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>

namespace DCP::String {

  namespace {

    /// Functor used in @ref String::escape.
    struct EscapeCharacters final {

      template <typename Match>
      std::string operator ()(Match const &match) const {
        std::string s;
        for (
          typename Match::const_iterator i = match.begin();
          i != match.end();
          ++i
        ) {
          s += boost::str(boost::format("\\x%02x") % static_cast<int>(*i));
        }
        return s;
      }

    };

  }

  void escape(std::string &string) {
    boost::find_format_all(
      string, boost::token_finder(!boost::is_print()), EscapeCharacters{}
    );
  }

}
