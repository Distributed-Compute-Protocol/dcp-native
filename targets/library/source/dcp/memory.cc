/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       May 2020
 */

#include <dcp/memory.hh>

#if BOOST_OS_WINDOWS
  #include <windows.h>
#elif BOOST_OS_UNIX || BOOST_OS_MACOS
  #include <unistd.h>
#endif

#include <system_error>

#if BOOST_OS_WINDOWS

namespace DCP::Memory {

  long unsigned getPageSize() noexcept {
    SYSTEM_INFO sysInfo;
    GetNativeSystemInfo(&sysInfo);
    assert(sysInfo.dwPageSize);
    return sysInfo.dwPageSize;
  }

}

#elif BOOST_OS_UNIX || BOOST_OS_MACOS

namespace DCP::Memory {

  long unsigned getPageSize() noexcept {
    static auto const pageSize = static_cast<long unsigned>(
      sysconf(_SC_PAGE_SIZE)
    );
    assert(pageSize);
    return pageSize;
  }

}

#endif
