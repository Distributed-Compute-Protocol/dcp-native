/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#include <dcp/time.hh>

#include <boost/numeric/conversion/cast.hpp>

#include <chrono>

namespace DCP::Time {

  Milliseconds now() {
    auto const milliseconds = std::chrono::duration_cast<
      std::chrono::milliseconds
    >(std::chrono::system_clock::now().time_since_epoch()).count();
    return boost::numeric_cast<Milliseconds>(milliseconds);
  }

}
