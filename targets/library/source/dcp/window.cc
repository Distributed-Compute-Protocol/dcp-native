/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2021
 */

#include <dcp/window.hh>

#if BOOST_OS_WINDOWS

  #include <dcp/logger.hh>

  #include <boost/numeric/conversion/cast.hpp>

  #include <commctrl.h>
  #include <commdlg.h>

namespace DCP::Window {

  std::string getClassName(HWND const window) {
    assert(window);

    // The maximum length of the class name is given here:
    // https://docs.microsoft.com/en-ca/windows/win32/api/winuser/ns-winuser-wndclassa
    int const capacity = 256;
    std::string name(capacity, '\0');

    SetLastError(ERROR_SUCCESS);
    int const length = GetClassName(window, &name.front(), capacity);
    if (!length) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "GetClassName() failed"
      );
    }
    assert(strlen(name.data()) == length);
    name.resize(length);
    return name;
  }

  HWND tryToGetParent(HWND const window) {
    assert(window);

    SetLastError(ERROR_SUCCESS);
    HWND const parent = GetParent(window);
    if (!parent) {
      log(LogLevel::notice, "GetParent() failed: %s",
        std::error_code(GetLastError(), std::system_category())
      );
    }
    return parent;
  }

  HWND tryToGetChild(HWND const window, int const id) {
    assert(window);

    SetLastError(ERROR_SUCCESS);
    HWND const child = GetDlgItem(window, id);
    if (!child) {
      log(LogLevel::notice, "GetDlgItem() failed: %s",
        std::error_code(GetLastError(), std::system_category())
      );
    }
    return child;
  }

  RECT getClientRect(HWND const window) {
    assert(window);

    RECT clientRect = {};
    SetLastError(ERROR_SUCCESS);
    if (!GetClientRect(window, &clientRect)) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "GetClientRect() failed"
      );
    }
    return clientRect;
  }

  bool postMessage(
    HWND const window,
    UINT const message,
    WPARAM const wParam,
    LPARAM const lParam
  ) noexcept {
    assert(window);

    SetLastError(ERROR_SUCCESS);
    if (!PostMessage(window, message, wParam, lParam)) {
      tryToLog(LogLevel::warning, "PostMessage() failed",
        std::error_code(GetLastError(), std::system_category()).message().data()
      );
      return false;
    }
    return true;
  }

  bool hasFocus(HWND const window) {
    assert(window);

    return window == GetFocus();
  }

  bool tryToSetFocus(HWND const window) {
    assert(window);

    SetLastError(ERROR_SUCCESS);
    if (!SetFocus(window)) {
      log(LogLevel::notice, "SetFocus() failed: %s",
        std::error_code(
          GetLastError(), std::system_category()
        ).message().data()
      );
      return false;
    }
    return true;
  }

  bool isReadOnly(HWND const window) {
    assert(window);

    return GetWindowLongPtr(window, GWL_STYLE) & ES_READONLY;
  }

  void setReadOnly(HWND const window, bool const readOnly) {
    assert(window);

    EnableWindow(window, !readOnly);
  }

  void setIcon(HWND const window, HICON const icon) {
    assert(window);
    assert(icon);

    SendMessage(window, WM_SETICON, ICON_SMALL, reinterpret_cast<LPARAM>(icon));
    SendMessage(window, WM_SETICON, ICON_BIG, reinterpret_cast<LPARAM>(icon));
  }

  void setFont(HWND const window, HFONT const font) {
    assert(window);

    SendMessage(window, WM_SETFONT, reinterpret_cast<WPARAM>(font), TRUE);
  }

  void setText(HWND const window, char const *const text) {
    assert(window);

    SendMessage(window, WM_SETTEXT, 0, (LPARAM)text);
  }

  std::string getText(HWND const window) {
    assert(window);

    size_t const textSize = SendMessage(window, WM_GETTEXTLENGTH, 0, 0);
    if (!textSize) {
      return "";
    }
    std::string text(textSize, '\0');
    SendMessage(window, WM_GETTEXT, text.size() + 1, (LPARAM)&text.front());
    return text;
  }

  void setChecked(HWND const window, bool const checked) {
    assert(window);

    SendMessage(window, BM_SETCHECK, checked ? BST_CHECKED : BST_UNCHECKED, 0);
  }

  bool getChecked(HWND const window) {
    assert(window);

    LRESULT const result = SendMessage(window, BM_GETCHECK, 0, 0);
    return (result == BST_CHECKED);
  }

  boost::filesystem::path chooseFile(
    HWND const window,
    char const *const filter
  ) {
    assert(window);

    char buffer[MAX_PATH];
    buffer[0] = '\0';

    OPENFILENAME ofn{};
    ofn.lStructSize = sizeof(ofn);
    ofn.hwndOwner = window;
    ofn.lpstrFile = buffer;
    ofn.nMaxFile = sizeof(buffer);
    ofn.lpstrFilter = filter;
    ofn.nFilterIndex = 1;
    ofn.lpstrFileTitle = NULL;
    ofn.nMaxFileTitle = 0;
    ofn.lpstrInitialDir = NULL;
    ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

    if (!GetOpenFileName(&ofn)) {
      log(LogLevel::notice,
        "GetOpenFileName() failed; CommDlgExtendedError() returned: %lu",
        CommDlgExtendedError()
      );
      return {};
    }
    return ofn.lpstrFile;
  }

  boost::optional<Color::Value> chooseColor(
    HWND const window,
    Color::Value const initialColor
  ) {
    assert(window);

    static Color::Value customColors[16]{};

    CHOOSECOLOR cc{};
    cc.lStructSize = sizeof(cc);
    cc.hwndOwner = window;
    cc.lpCustColors = customColors;
    cc.rgbResult = initialColor;
    cc.Flags = CC_FULLOPEN | CC_RGBINIT;

    if (!ChooseColor(&cc)) {
      log(LogLevel::notice,
        "ChooseColor() failed; CommDlgExtendedError() returned: %lu",
        CommDlgExtendedError()
      );
      return {};
    }
    return cc.rgbResult;
  }

  Handle<UINT_PTR, 0> createTimer(
    HWND const window,
    Time::Milliseconds const timeIncrement
  ) {
    assert(window);

    SetLastError(ERROR_SUCCESS);
    UINT_PTR const result = SetTimer(
      window, 1, boost::numeric_cast<UINT>(timeIncrement), nullptr
    );
    if (!result) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "SetTimer() failed"
      );
    }
    return Handle<UINT_PTR, 0>(
      result,
      [window](UINT_PTR windowTimer) {
        KillTimer(window, windowTimer);
      }
    );
  }

}

#endif
