/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       December 2021
 */

#include <dcp/process.hh>

#include <dcp/logger.hh>
#if BOOST_OS_WINDOWS
  #include <dcp/paths.hh>
  #include <dcp/pipe.hh>
#endif
#include <dcp/signal.hh>
#include <dcp/string.hh>

#include <boost/algorithm/string.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>

#include <cinttypes>
#include <system_error>
#if !BOOST_OS_WINDOWS
  #include <thread>
#endif

#if BOOST_OS_MACOS
  #include <signal.h>
#elif BOOST_OS_LINUX
  #include <sys/wait.h>
#elif BOOST_OS_WINDOWS
  #include <shellapi.h>
#endif

namespace DCP {

#if BOOST_OS_WINDOWS

  namespace {

    /** A thread-local singleton that manages COM library initialization and
     *  deinitialization, needed by ShellExecuteEx.
     */
    struct COM final {

      static COM const &getInstance() {
        thread_local COM const com{};
        return com;
      }

      COM(COM const &) = delete;
      COM(COM const &&) = delete;

      COM &operator =(COM const &) = delete;
      COM &operator =(COM const &&) = delete;

      Handle<HANDLE, nullptr> command(
        char const *const command,
        std::vector<std::string> const &arguments,
        bool const wait,
        bool const administrate,
        bool const show
      ) const {
        auto const argumentsString = String::joinQuoted(arguments);

        SHELLEXECUTEINFO info{};
        info.cbSize = sizeof(info);
        info.fMask = SEE_MASK_FLAG_NO_UI | (wait ? SEE_MASK_NOCLOSEPROCESS : 0);
        info.hwnd = nullptr;
        info.lpVerb = administrate ? "runas" : "open";
        info.lpFile = command;
        info.lpParameters = argumentsString.data();
        info.lpDirectory = nullptr;
        info.nShow = show ? SW_SHOW : SW_HIDE;
        info.hInstApp = nullptr;

        SetLastError(ERROR_SUCCESS);
        if (!ShellExecuteEx(&info)) {
          throw std::system_error(
            std::error_code(GetLastError(), std::system_category()),
            "ShellExecuteEx() failed"
          );
        }

        return Handle<HANDLE, nullptr>(info.hProcess, CloseHandle);
      }

    private:

      COM() {
        // Required by ShellExecuteEx().
        HRESULT const result = CoInitializeEx(
          NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE
        );
        switch (result) {
        case S_OK:
          log(LogLevel::debug,
            "COM library successfully initialized on this thread"
          );
          break;
        case S_FALSE:
          log(LogLevel::warning,
            "COM library already initialized on this thread"
          );
          break;
        case RPC_E_CHANGED_MODE:
          log(LogLevel::warning,
            "COM library already initialized differently on this thread"
          );
          break;
        default:
          throw std::runtime_error(
            boost::str(
              boost::format("CoInitializeEx() returned error: %li") % result
            )
          );
        }
      }

      ~COM() {
        CoUninitialize();
        tryToLog(LogLevel::debug,
          "COM library uninitialization called on this thread"
        );
      }

    };

    struct Interrupt final {

      explicit Interrupt(DWORD const id) :
      id{id}
      {
        // The CreateProcess call in this file is configured (by default) such
        // that child processes inherit the console of this process. However,
        // in cases where this process does not have a console (eg. if linked
        // with /SUBSYSTEM:WINDOWS), the console of the child process must be
        // explicitly attached, if possible.
        if (!GetConsoleWindow()) {
          log(LogLevel::debug, "Attaching to console of process %lu", id);
          SetLastError(ERROR_SUCCESS);
          // This can fail with access denied in some cases where
          // GenerateConsoleCtrlEvent() will still work, so just log the error
          // and throw if GenerateConsoleCtrlEvent() fails.
          if (!AttachConsole(id)) {
            log(LogLevel::error,
              "AttachConsole() failed for process %lu: %s", id,
              std::error_code(
                GetLastError(), std::system_category()
              ).message().data()
            );
            return;
          }
          attached = true;
        }
      }

      Interrupt(Interrupt const &) = delete;
      Interrupt(Interrupt const &&) = delete;

      Interrupt &operator =(Interrupt const &) = delete;
      Interrupt &operator =(Interrupt const &&) = delete;

      void operator ()() const {
        log(LogLevel::debug, "Issuing Ctrl+C event to process %lu", id);
        SetLastError(ERROR_SUCCESS);
        if (!GenerateConsoleCtrlEvent(CTRL_C_EVENT, id)) {
          throw std::system_error(
            std::error_code(GetLastError(), std::system_category()),
            boost::str(
              boost::format(
                "GenerateConsoleCtrlEvent() failed for process %lu"
              ) % id
            )
          );
        }
      }

      ~Interrupt() {
        if (attached) {
          tryToLog(LogLevel::debug,
            "Detaching from console of process %lu", id
          );
          SetLastError(ERROR_SUCCESS);
          if (!FreeConsole()) {
            tryToLog(LogLevel::error,
              "FreeConsole() failed for process %lu: %s", id,
              std::error_code(
                GetLastError(), std::system_category()
              ).message().data()
            );
          }
        }
      }

    private:

      DWORD id;

      bool attached{false};

    };

    /**
     *  @param  handle
     *          The handle to wait for. The handle must be valid and cannot be
     *          that of an abandoned mutex.
     *  @param  timeout
     *          The optional timeout in milliseconds.
     *  @return `true` if wait completed.
     */
    bool waitForSingleObject(
      HANDLE const handle,
      boost::optional<Time::Milliseconds> const timeout = {}
    ) noexcept {
      SetLastError(ERROR_SUCCESS);
      DWORD const result = WaitForSingleObject(
        handle, timeout ? (DWORD)*timeout : INFINITE
      );
      switch (result) {
      case WAIT_FAILED:
        // This is not well documented, but appears to occur if the function
        // call itself fails; this can happen if the handle is invalid or there
        // is a permissions issue. If this is the case, then the preconditions
        // of the function were violated.
        tryToLog(LogLevel::error,
          "WaitForSingleObject() returned WAIT_FAILED: %s",
          std::error_code(GetLastError(), std::system_category()).message().data()
        );
        assert(false);
        return false;
      case WAIT_ABANDONED:
        // This should only occur if one of the handles is an abandoned mutex,
        // which is a violation of the function preconditions.
        tryToLog(LogLevel::error,
          "WaitForSingleObject() returned WAIT_ABANDONED"
        );
        assert(false);
        return false;
      case WAIT_OBJECT_0:
        return true;
      case WAIT_TIMEOUT:
        return false;
      default:
        // No other return values should be possible, according to the
        // documentation.
        tryToLog(LogLevel::error,
          "WaitForSingleObject() returned unexpected value"
        );
        assert(false);
        return false;
      }
    }

    /**
     *  @param  handles
     *          A vector of handles whose size cannot exceed
     *          MAXIMUM_WAIT_OBJECTS. All handles must be valid, and none can be
     *          that of an abandoned mutex.
     *  @param  all
     *          Whether to wait for all handles, vs. just waiting for the first
     *          (default: false)
     *  @param  timeout
     *          The optional timeout in milliseconds.
     *  @return Index of handle whose wait completeed first, if any.
     */
    boost::optional<size_t> waitForMultipleObjects(
      std::vector<HANDLE> const &handles,
      bool const all = false,
      boost::optional<Time::Milliseconds> const timeout = {}
    ) noexcept {
      static_assert(MAXIMUM_WAIT_OBJECTS <= std::numeric_limits<DWORD>::max());
      assert(handles.size() <= MAXIMUM_WAIT_OBJECTS);
      DWORD const count = static_cast<DWORD>(handles.size());

      SetLastError(ERROR_SUCCESS);
      DWORD const result = WaitForMultipleObjects(
        count, &handles.front(), all, timeout ? (DWORD)*timeout : INFINITE
      );
      switch (result) {
      case WAIT_FAILED:
        // This is not well documented, but appears to occur if the function
        // call itself fails; this can happen if a handle is invalid or there
        // is a permissions issue. If this is the case, then the preconditions
        // of the function were violated.
        tryToLog(LogLevel::error,
          "WaitForMultipleObjects() returned WAIT_FAILED: %s",
          std::error_code(GetLastError(), std::system_category()).message().data()
        );
        return {};
      case WAIT_TIMEOUT:
        assert(timeout);
        return {};
      default:
        // If a wait completed, return the index of the first completed handle.
        static_assert(WAIT_OBJECT_0 == 0);
        if (result < count) {
          return static_cast<size_t>(result);
        }

        // This should only occur if one of the handles is an abandoned mutex,
        // which is a violation of the function preconditions.
        static_assert(WAIT_ABANDONED_0 >= MAXIMUM_WAIT_OBJECTS);
        if (result >= WAIT_ABANDONED_0 && result < (WAIT_ABANDONED_0 + count)) {
          tryToLog(LogLevel::error,
            "WaitForMultipleObjects() returned WAIT_ABANDONED_0 + %lu",
            WAIT_ABANDONED_0 - result
          );
          assert(false);
        }

        // No other return values should be possible, according to the
        // documentation.
        tryToLog(LogLevel::error,
          "WaitForMultipleObjects() returned unexpected value"
        );
        assert(false);
        return {};
      }
    }

    /** Gets the exit code for the given process handle.
     *
     *  @return None if the process is still running, or the exit code if done.
     *  @throw  Exception if there was an error getting the exit code.
     */
    boost::optional<DWORD> getProcessExitCode(HANDLE const handle) {
      DWORD exitCode{};
      SetLastError(ERROR_SUCCESS);
      if (!GetExitCodeProcess(handle, &exitCode)) {
        throw std::system_error(
          std::error_code(GetLastError(), std::system_category()),
          "GetExitCodeProcess() failed"
        );
      }
      if (exitCode == STILL_ACTIVE) {
        return {};
      }
      return {exitCode};
    }

  }

  void Process::command(
    char const *const command,
    std::vector<std::string> const &arguments,
    bool const administrate,
    bool const show
  ) {
    COM::getInstance().command(command, arguments, false, administrate, show);
  }

  long unsigned Process::command(
    char const *const command,
    std::vector<std::string> const &arguments,
    boost::optional<Time::Milliseconds> const timeout,
    bool const administrate,
    bool const show
  ) {
    assert(!timeout || *timeout <= std::numeric_limits<DWORD>::max());

    auto const handle = COM::getInstance().command(
      command, arguments, true, administrate, show
    );
    if (!handle) {
      // From https://learn.microsoft.com/en-us/windows/win32/api/shellapi/ns-shellapi-shellexecuteinfoa :
      // In some cases, such as when execution is satisfied through a DDE
      // conversation, no handle will be returned.
      return EXIT_SUCCESS;
    }

    if (!waitForSingleObject(handle, timeout)) {
      throw std::runtime_error("Timed out waiting for process");
    }

    auto const exitCode = getProcessExitCode(handle);
    assert(exitCode);
    return *exitCode;
  }

  long unsigned Process::execute(
    boost::filesystem::path const &executablePath,
    std::vector<std::string> const &arguments,
    std::string &output,
    std::string &error
  ) {
    Pipe outputPipe{};
    Pipe errorPipe{};
    Process::IO io(errorPipe.getWriteEnd(), outputPipe.getWriteEnd());

    Process process(executablePath, arguments, {}, {}, {}, &io);
    process.wait();

    errorPipe.read(error);
    outputPipe.read(output);

    auto const exitCode = process.getExitCode();
    assert(exitCode);
    return *exitCode;
  }

  std::string Process::execute(
    boost::filesystem::path const &executablePath,
    std::vector<std::string> const &arguments
  ) {
    std::string output{};
    std::string error{};
    auto const exitCode = Process::execute(
      executablePath, arguments, output, error
    );
    if (exitCode != EXIT_SUCCESS || !error.empty()) {
      auto const argumentsString = String::joinQuoted(arguments);
      if (exitCode != EXIT_SUCCESS) {
        throw std::runtime_error(
          boost::str(
            boost::format("Command `\"%s\" %s` produced error code: %lu")
            % executablePath.string().data()
            % argumentsString.data()
            % exitCode
          )
        );
      }
      if (!error.empty()) {
        log(LogLevel::warning, "Command `\"%s\" %s` produced error output: %s",
          executablePath.string().data(), argumentsString.data(), error.data()
        );
      }
    }
    return output;
  }

  Handle<HANDLE, nullptr> Process::createJob() {
    SetLastError(ERROR_SUCCESS);
    HANDLE const jobHandle = CreateJobObject(nullptr, nullptr);
    if (!jobHandle) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "CreateJobObject() failed"
      );
    }

    JOBOBJECT_EXTENDED_LIMIT_INFORMATION info{};
    info.BasicLimitInformation.LimitFlags = JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE;
    SetInformationJobObject(jobHandle, JobObjectExtendedLimitInformation,
      &info, sizeof(info)
    );

    return Handle<HANDLE, nullptr>(jobHandle, CloseHandle);
  }

  Handle<HANDLE, nullptr> Process::createEvent() {
    SECURITY_ATTRIBUTES security{};
    security.nLength = sizeof(security);
    security.lpSecurityDescriptor = nullptr;
    security.bInheritHandle = true;

    SetLastError(ERROR_SUCCESS);
    HANDLE const eventHandle = CreateEvent(&security, true, false, nullptr);
    if (!eventHandle) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "CreateEvent() failed"
      );
    }
    return Handle<HANDLE, nullptr>(eventHandle, CloseHandle);
  }

  boost::optional<long unsigned> Process::getExitCode() const {
    return getProcessExitCode(handle);
  }

  // References:
  // - https://devblogs.microsoft.com/oldnewthing/20111216-00/?p=8873
  // - https://devblogs.microsoft.com/oldnewthing/20230209-00/?p=107812
  Process::Process(
    boost::filesystem::path const &executablePath,
    std::vector<std::string> const &arguments,
    Handle<HANDLE, nullptr> jobHandle,
    Handle<HANDLE, nullptr> startEventHandle,
    std::vector<HANDLE> additionalHandles,
    IO const *const io
  ) :
  jobHandle{std::move(jobHandle)},
  startEventHandle{std::move(startEventHandle)}
  {
    assert(boost::filesystem::exists(executablePath));

    DWORD attributeCount{};

    HANDLE job = this->jobHandle;
    if (job) {
      ++attributeCount;
    }

    HANDLE startEvent = this->startEventHandle;
    if (startEvent) {
      additionalHandles.push_back(startEvent);
      ++attributeCount;
    }
    if (additionalHandles.size() >= (0xFFFFFFFF / sizeof(HANDLE))) {
      throw std::system_error(
        std::error_code(ERROR_INVALID_PARAMETER, std::system_category()),
        "Too many additional handles"
      );
    }

    log(LogLevel::debug, "Getting process attribute list size...");
    SIZE_T attributeListSize{};
    SetLastError(ERROR_SUCCESS);
    if (
      !InitializeProcThreadAttributeList(
        NULL, attributeCount, 0, &attributeListSize
      ) && (GetLastError() != ERROR_INSUFFICIENT_BUFFER)
    ) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "First InitializeProcThreadAttributeList() failed"
      );
    }

    log(LogLevel::debug, "Allocating process attribute list...");
    std::unique_ptr<char[]> attributeListBuffer(new char[attributeListSize]);
    auto attributeList = reinterpret_cast<LPPROC_THREAD_ATTRIBUTE_LIST>(
      attributeListBuffer.get()
    );
    if (!attributeList) {
      throw std::runtime_error("Process attribute list allocation failed");
    }

    log(LogLevel::debug, "Initializing process attribute list...");
    SetLastError(ERROR_SUCCESS);
    if (
      !InitializeProcThreadAttributeList(
        attributeList, attributeCount, 0, &attributeListSize
      )
    ) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "Second InitializeProcThreadAttributeList() failed"
      );
    }
    Handle<LPPROC_THREAD_ATTRIBUTE_LIST, nullptr> attributeListHandle(
      attributeList, DeleteProcThreadAttributeList
    );

    if (job) {
      log(LogLevel::debug, "Adding job to process attribute job list...");
      SetLastError(ERROR_SUCCESS);
      if (
        !UpdateProcThreadAttribute(
          attributeList, 0,
          PROC_THREAD_ATTRIBUTE_JOB_LIST,
          &job, sizeof(job),
          nullptr, nullptr
        )
      ) {
        throw std::system_error(
          std::error_code(GetLastError(), std::system_category()),
          "UpdateProcThreadAttribute() failed for job list"
        );
      }
    }

    if (!additionalHandles.empty()) {
      log(LogLevel::debug,
        "Adding %zu handles to process attribute handle list...",
        additionalHandles.size()
      );
      SetLastError(ERROR_SUCCESS);
      if (
        !UpdateProcThreadAttribute(
          attributeList, 0,
          PROC_THREAD_ATTRIBUTE_HANDLE_LIST,
          &additionalHandles.front(), additionalHandles.size() * sizeof(HANDLE),
          nullptr, nullptr
        )
      ) {
        throw std::system_error(
          std::error_code(GetLastError(), std::system_category()),
          "UpdateProcThreadAttribute() failed for handle list"
        );
      }
    }

    log(LogLevel::debug, "Setting program path and arguments string...");
    boost::filesystem::path programPath{};
    std::string argumentsString{};
    {
      std::ostringstream argumentsStream{};

      auto const writeArguments = [&]() {
        argumentsStream << std::quoted(executablePath.string());
        for (auto const &argument: arguments) {
          argumentsStream << ' ' << std::quoted(argument);
        }
      };

      auto extension = executablePath.extension().string();
      boost::algorithm::to_lower(extension);
      if (extension == ".bat") {
        programPath = Paths::getSystemDirectoryPath() / "cmd.exe";
        assert(boost::filesystem::exists(programPath));

        argumentsStream << std::quoted(programPath.string());
        // Quoting the script path for cmd.exe is described here:
        // https://stackoverflow.com/a/356014
        argumentsStream << " /s /c \"";
        writeArguments();
        argumentsStream << '"';
      } else {
        programPath = executablePath;

        writeArguments();
      }

      argumentsString = argumentsStream.str();
    }

    DWORD const flags = EXTENDED_STARTUPINFO_PRESENT | (
      io ? CREATE_NEW_CONSOLE : CREATE_NEW_PROCESS_GROUP
    );

    STARTUPINFOEX sie{};
    sie.lpAttributeList = attributeList;
    auto &si = sie.StartupInfo;
    si.cb = sizeof(sie);
    // Prevent the window from being shown.
    si.dwFlags |= STARTF_USESHOWWINDOW;
    si.wShowWindow = SW_HIDE;
    if (io) {
      // Use custom stdin/stdout/stderr handles.
      si.dwFlags |= STARTF_USESTDHANDLES;
      si.hStdInput = io->input;
      si.hStdOutput = io->output;
      si.hStdError = io->error;
    }

    PROCESS_INFORMATION pi{};

    log(LogLevel::debug, "Calling CreateProcess()...");
    SetLastError(ERROR_SUCCESS);
    if (
      !CreateProcess(
        programPath.string().data(), &argumentsString.front(),
        nullptr, nullptr,
        true,
        flags,
        nullptr,
        nullptr,
        &si, &pi
      )
    ) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "CreateProcess() failed"
      );
    }
    this->id = pi.dwProcessId;
    this->threadID = pi.dwThreadId;
    this->handle = Handle<HANDLE, nullptr>(pi.hProcess, CloseHandle);
    this->threadHandle = Handle<HANDLE, nullptr>(pi.hThread, CloseHandle);
    this->attached = true;

    if (job) {
      log(LogLevel::debug,
        "Confirming that process %lu is in the job...", this->id
      );
      BOOL isInJob{};
      SetLastError(ERROR_SUCCESS);
      if (!IsProcessInJob(pi.hProcess, job, &isInJob)) {
        throw std::system_error(
          std::error_code(GetLastError(), std::system_category()),
          "IsProcessInJob() failed"
        );
      }
      if (!isInJob) {
        throw std::runtime_error("The process was not added to the job");
      }
    }

    log(LogLevel::debug, "Created process %lu", this->id);
  }

  bool Process::waitToStart() noexcept {
    assert(handle);

    bool result = true;
    if (startEventHandle) {
      tryToLog(LogLevel::debug,
        "Waiting for process %lu to fire start event...", id
      );
      auto const index = waitForMultipleObjects({handle, startEventHandle});
      assert(index);
      if (*index == 0) {
        tryToLog(LogLevel::info, "Process %lu already exited", id);
        result = false;
      } else {
        assert(*index == 1);
        tryToLog(LogLevel::debug, "Process %lu start event fired", id);
      }
    } else {
      tryToLog(LogLevel::warning, "No start event created for process %lu", id);
    }
    started = true;
    return result;
  }

  bool Process::isRunning() const noexcept {
    return !getExitCode();
  }

  bool Process::wait(
    boost::optional<Time::Milliseconds> const timeout
  ) const noexcept {
    assert(handle);

    return waitForSingleObject(handle, timeout);
  }

  void Process::interrupt() {
    assert(handle);

    if (!waitToStart()) {
      return;
    }

    Interrupt interrupt(id);
    interrupt();
  }

  void Process::terminate() {
    assert(handle);

    if (!waitToStart()) {
      return;
    }

    SetLastError(ERROR_SUCCESS);
    if (!TerminateProcess(handle, EXIT_SUCCESS)) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "TerminateProcess() failed"
      );
    }
  }

  void Process::kill() {
    if (!waitToStart()) {
      return;
    }

    auto const command = boost::str(
      boost::format("taskkill /pid %s /f /t >nul 2>&1") % id
    );
  #pragma warning(push)
  #pragma warning(disable: 28159) // Deprecation warning
    auto const exitCode = WinExec(command.data(), SW_HIDE);
  #pragma warning(pop)
    if (exitCode) {
      tryToLog(LogLevel::warning,
        "Killing task %lu returned non-zero exit code: %u", id, exitCode
      );
    } else {
      tryToLog(LogLevel::info, "Process %lu killed", id);
    }
  }

#else

  Process::Process(
    boost::filesystem::path const &executablePath,
    std::vector<std::string> const &arguments,
    char *const *const environment,
    std::function<bool ()> const &childCallback
  ) {
    assert(boost::filesystem::exists(executablePath));

    // Construct the executable path string to allow `data()` pointer usage.
    auto const &executablePathString = executablePath.string();

    // Construct the array of argument pointers expected by `execve`.
    // Do this in the parent because destructors won't be called in the child.
    std::vector<char const *> argumentPointers{};
    argumentPointers.reserve(arguments.size() + 2U);
    argumentPointers.push_back(executablePathString.data());
    for (auto const &argument: arguments) {
      argumentPointers.push_back(argument.data());
    }
    argumentPointers.push_back(nullptr);

    // Make a best attempt to flush buffers prior to fork().
    // Unfortunately, the POSIX standard doesn't require that this blocks until
    // completion.
    sync();

    // Fork and `exec` the child process.
    // Note that constructors are *not* called when the child process exits:
    // either the `exec` succeeds, or `_exit()` is called on error.
    errno = 0;
    this->id = fork();
    if (this->id < 0) { // Error
      throw std::system_error(
        std::error_code(errno, std::system_category()), "fork() failed"
      );
    }
    if (this->id == 0) { // Child
      try {
        if (childCallback()) {
          errno = 0;
          if (
            execve(
              executablePathString.data(),
              // NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast)
              const_cast<char *const *>(&argumentPointers.front()),
              environment
            ) == -1
          ) {
            tryToLog(LogLevel::error, "execve() failed: %s",
              std::error_code(errno, std::system_category()).message().data()
            );
          }
        } else {
          tryToLog(LogLevel::debug, "Child callback returned false");
        }
      } catch (...) {
        tryToLog(LogLevel::error, "Exception in child process: %s",
          boost::current_exception_diagnostic_information().data()
        );
      }
      _exit(EXIT_FAILURE);
    }
    this->attached = true;

    log(LogLevel::debug, "Created process %lu", this->id);
  }

  bool Process::isRunning() const noexcept {
    errno = 0;
    if (::kill(id, 0) == -1) {
      assert(errno == ESRCH);
      return false;
    }
    return true;
  }

  bool Process::wait(
    boost::optional<Time::Milliseconds> const timeout
  ) const noexcept {
    int options = WUNTRACED;
    if (timeout) {
      options |= WNOHANG;
    }

    auto const start = std::chrono::steady_clock::now();

    for (;; std::this_thread::yield()) {
      int status{};

      errno = 0;
      auto const result = waitpid(id, &status, options);
      if (result == -1) {
        switch (errno) {
        case EINTR:
          tryToLog(LogLevel::debug, "Process was interrupted.");
          continue;
        case ECHILD:
          tryToLog(LogLevel::debug, "Process was ended.");
          return true;
        default:
          // According to the documentation, no other failures should be
          // possible given the arguments provided.
          tryToLog(LogLevel::error,
            "waitpid() failed for process %lu: %s", id,
            std::error_code(errno, std::system_category()).message().data()
          );
          assert(false);
        }
      } else if (timeout && result == 0) {
        auto const finish = std::chrono::steady_clock::now();
        auto const elapsed = (
          std::chrono::duration_cast<std::chrono::milliseconds>(finish - start)
        ).count();
        if (static_cast<DCP::Time::Milliseconds>(elapsed) >= *timeout) {
          return false;
        }
        continue;
      }
      assert(result == id);

      if (WIFEXITED(status)) {
        tryToLog(LogLevel::debug,
          "Process exited with code %i", WEXITSTATUS(status)
        );
      } else if (WIFSIGNALED(status)) {
        tryToLog(LogLevel::debug,
          "Process terminated by signal %i", WTERMSIG(status)
        );
      } else if (WIFSTOPPED(status)) {
        tryToLog(LogLevel::debug,
          "Process stopped by signal %i", WSTOPSIG(status)
        );
      } else {
        tryToLog(LogLevel::warning, "Unknown process status: %i", status);
      }
      return true;
    }
  }

  // NOLINTNEXTLINE(readability-make-member-function-const)
  void Process::interrupt() {
    Signal::send(id, SIGINT);
  }

  // NOLINTNEXTLINE(readability-make-member-function-const)
  void Process::terminate() {
    Signal::send(id, SIGTERM);
  }

  // NOLINTNEXTLINE(readability-make-member-function-const)
  void Process::kill() {
    Signal::send(id, SIGKILL);
  }

#endif

  void Process::detach() {
    attached = false;
  }

  Process::~Process() {
    if (attached) {
      tryToLog(LogLevel::debug, "Waiting for process %lu...", id);
      wait();
    }
  }

}
