/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2021
 */

#include <dcp/window/slider.hh>

#if BOOST_OS_WINDOWS

  #include <dcp/logger.hh>
  #include <dcp/number.hh>

  #include <commctrl.h>

namespace DCP::Window {

  Slider::Slider(HWND const control, uint16_t const low, uint16_t const high) :
  control(control),
  low(low),
  high(high)
  {
    assert(control);

    if (high < low) {
      throw std::invalid_argument(
        "The maximum cannot be lower than the minimum"
      );
    }
    SendMessage(control, TBM_SETRANGE, TRUE, MAKELONG(low, high));
  }

  void Slider::setPosition(uint16_t position) {
    assert(low <= high);
    if (position < low) {
      log(LogLevel::warning, "Clamping position %lu to minimum %lu",
        position, low
      );
      position = low;
    } else if (position > high) {
      log(LogLevel::warning, "Clamping position %lu to maximum %lu",
        position, high
      );
      position = high;
    }
    SendMessage(control, TBM_SETPOSNOTIFY, 0, position);
  }

  uint16_t Slider::getPosition() const {
    LRESULT const result = SendMessage(control, TBM_GETPOS, 0, 0);
    uint16_t const position = Number::clamp<uint16_t>(result);
    assert(low <= position && position <= high);
    return position;
  }

}

#endif
