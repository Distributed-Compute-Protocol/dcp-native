/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2021
 */

#include <dcp/window/link.hh>

#if BOOST_OS_WINDOWS

  #include <dcp/logger.hh>
  #include <dcp/process.hh>
  #include <dcp/window.hh>

  #include <boost/exception/diagnostic_information.hpp>
  #include <boost/nowide/convert.hpp>

  #include <commctrl.h>

namespace DCP::Window::Link {

  bool handleMessage(
    UINT const message,
    [[maybe_unused]] WPARAM const wParam,
    LPARAM const lParam
  ) {
    switch (message) {
    case WM_NOTIFY:
      assert(lParam);
      switch (((LPNMHDR)lParam)->code) {
      case NM_CLICK:
        [[fallthrough]];
      case NM_RETURN:
        if (
          std::string const &className = getClassName(
            ((LPNMHDR)lParam)->hwndFrom
          );
          className == "SysLink"
        ) {
          log(LogLevel::debug, "Link %u (class: %s) clicked",
            ((LPNMHDR)lParam)->idFrom, className.data()
          );
          auto const url = boost::nowide::narrow(((PNMLINK)lParam)->item.szUrl);
          log(LogLevel::debug, "Opening link: %s", url.data());
          Process::command(url.data(), {}, false, true);
          return true;
        }
        break;
      }
      break;
    }
    return false;
  }

}

#endif
