/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#include <dcp/window/screensaver.hh>

#if BOOST_OS_WINDOWS

  #include <dcp/bitmap.hh>
  #include <dcp/graphics.hh>
  #include <dcp/logger.hh>
  #include <dcp/window.hh>

  #include <boost/math/constants/constants.hpp>

namespace DCP::Window {

  namespace {

    /** Get the rectangle for the bitmap.
     *
     *  Positions the bitmap by applying a lemniscate of Gerono to the given
     *  angle, with origin at center, causing the bitmap to move along an
     *  infinity-shaped path.
     */
    RECT getBitmapRect(
      RECT const &containerRect,
      SIZE const &size,
      double const angle
    ) {
      static double const tau = boost::math::constants::two_pi<double>();

      int const xCenter = (containerRect.right + containerRect.left) / 2;
      int const yCenter = (containerRect.top + containerRect.bottom) / 2;

      // Determine the factor to multiply by.
      // The lemniscate is twice as wide as high.
      double const factor = std::min(
        (
          (double)(containerRect.right - containerRect.left) - (double)size.cx
        ) / 2.0,
        (double)(containerRect.bottom - containerRect.top) - (double)size.cy
      );

      double const radians = angle * tau / 360.0;
      double const xOffset = sin(radians) * factor;
      double const yOffset = cos(radians) * xOffset;
      int const x = xCenter - (int)((size.cx / 2.0) - xOffset);
      int const y = yCenter - (int)((size.cy / 2.0) - yOffset);

      RECT bitmapRect = {};
      bitmapRect.left = x;
      bitmapRect.top = y;
      bitmapRect.right = x + size.cx;
      bitmapRect.bottom = y + size.cy;
      return bitmapRect;
    }

  }

  Screensaver::Screensaver(
    HWND const window,
    Color::Value const backgroundColor,
    Handle<HBITMAP, nullptr> const &bitmap,
    Time::Milliseconds const timeIncrement,
    double const angleIncrement
  ) :
  angleIncrement(angleIncrement),
  windowTimer(createTimer(window, timeIncrement)),
  windowClientRect(getClientRect(window)),
  windowDC(Graphics::getDC(window)),
  bitmap(
    bitmap ? bitmap : throw std::invalid_argument("Bitmap cannot be null")
  ),
  sourceDC(Graphics::createCompatibleDC(windowDC)),
  oldSourceObject(Graphics::selectObject(sourceDC, bitmap)),
  bitmapSourceSize(Bitmap::getSize(bitmap)),
  bitmapSize(
    Graphics::scaleToFit(
      bitmapSourceSize, {
        windowClientRect.right - windowClientRect.left,
        windowClientRect.bottom - windowClientRect.top
      }, 0.75
    )
  ),
  bitmapRect(getBitmapRect(windowClientRect, bitmapSize, angle)),
  background(Graphics::createSolidBrush(backgroundColor))
  {
    Graphics::setBitmapStretchMode(windowDC, HALFTONE);
    log(LogLevel::debug, "Screensaver: constructed");
  }

  void Screensaver::clear() {
    if (!FillRect(windowDC, &windowClientRect, background)) {
      tryToLog(LogLevel::trace, "FillRect() failed");
    }
  }

  void Screensaver::update() {
    RECT const oldBitmapRect = bitmapRect;

    angle += angleIncrement;
    if (angle >= 180.0) {
      angle -= 360.0;
    }

    bitmapRect = getBitmapRect(windowClientRect, bitmapSize, angle);

    // Erase the parts of the old rect that are not inside the new bitmap rect.
    {
      RECT xDeltaRect{};
      RECT const bitmapRectX{
        bitmapRect.left, oldBitmapRect.top,
        bitmapRect.right, oldBitmapRect.bottom
      };
      if (SubtractRect(&xDeltaRect, &oldBitmapRect, &bitmapRectX)) {
        if (!FillRect(windowDC, &xDeltaRect, background)) {
          tryToLog(LogLevel::trace, "FillRect() failed for x delta");
        }
      }
    }
    {
      RECT yDeltaRect{};
      RECT const bitmapRectY{
        oldBitmapRect.left, bitmapRect.top,
        oldBitmapRect.right, bitmapRect.bottom
      };
      if (SubtractRect(&yDeltaRect, &oldBitmapRect, &bitmapRectY)) {
        if (!FillRect(windowDC, &yDeltaRect, background)) {
          tryToLog(LogLevel::trace, "FillRect() failed for y delta");
        }
      }
    }

    if (
      !StretchBlt(
        windowDC,
        bitmapRect.left, bitmapRect.top,
        bitmapRect.right - bitmapRect.left,
        bitmapRect.bottom - bitmapRect.top,
        sourceDC, 0, 0, bitmapSourceSize.cx, bitmapSourceSize.cy,
        SRCCOPY
      )
    ) {
      tryToLog(LogLevel::trace, "StretchBlt() failed");
    }
  }

  bool Screensaver::handleMessage(
    UINT const message,
    [[maybe_unused]] WPARAM const wParam,
    [[maybe_unused]] LPARAM const lParam
  ) {
    switch (message) {
    case WM_ERASEBKGND:
      clear();
      return true;
    case WM_TIMER:
      update();
      return true;
    }
    return false;
  }

  Screensaver::~Screensaver() {
    tryToLog(LogLevel::debug, "Screensaver: destructing");
  }

}

#endif
