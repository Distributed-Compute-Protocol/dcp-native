/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2021
 */

#include <dcp/window/tab_box.hh>

#if BOOST_OS_WINDOWS

  #include <dcp/resource.hh>

  #include <commctrl.h>

namespace DCP::Window {

  TabBox::TabBox(HWND const control) :
  control((assert(control), control)),
  parent(DCP::Window::tryToGetParent(control))
  {
    assert(parent);
  }

  size_t TabBox::getTabCount() const {
    return tabs.size();
  }

  void TabBox::appendTab(
    UINT const id,
    std::string name,
    DLGPROC const callback
  ) {
    if (name.empty()) {
      throw std::invalid_argument("The name cannot be empty.");
    }

    {
      TCITEM tci{};
      tci.iImage = -1;
      tci.mask = TCIF_TEXT | TCIF_IMAGE;
      tci.pszText = &name.front();
      TabCtrl_InsertItem(control, tabs.size(), &tci);
    }

    // This gets destroyed when the parent window is destroyed.
    HWND const tab = Resource::createDialog(id, parent, callback);
    assert(tab);
    tabs.push_back(tab);

    RECT clientRect{};
    GetClientRect(control, &clientRect);
    TabCtrl_AdjustRect(control, FALSE, &clientRect);
    MapWindowPoints(control, parent, (LPPOINT)&clientRect, 2);
    SetWindowPos(tab, HWND_TOP,
      clientRect.left, clientRect.top,
      clientRect.right - clientRect.left, clientRect.bottom - clientRect.top,
      SWP_HIDEWINDOW
    );
  }

  void TabBox::selectTab(size_t const index) {
    assert(index < tabs.size());
    TabCtrl_SetCurSel(control, index);
    updateSelection();
  }

  HWND TabBox::getTab(size_t const index) const {
    assert(index < tabs.size());
    return tabs[index];
  }

  void TabBox::updateSelection() {
    int const selectedIndex = TabCtrl_GetCurSel(control);
    for (size_t index = 0; index < tabs.size(); ++index) {
      ShowWindow(tabs[index], (selectedIndex == index) ? SW_SHOW : SW_HIDE);
    }
  }

  bool TabBox::handleMessage(
    UINT const message,
    [[maybe_unused]] WPARAM const wParam,
    LPARAM const lParam
  ) {
    switch (message) {
    case WM_NOTIFY:
      switch (((LPNMHDR)lParam)->code) {
      case TCN_SELCHANGE:
        updateSelection();
        return true;
      }
      break;
    }
    return false;
  }

}

#endif
