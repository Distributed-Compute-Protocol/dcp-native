/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       February 2021
 */

#include <dcp/window/dismissable_message_dialog.hh>

#if BOOST_OS_WINDOWS

  #include <shlwapi.h>

  #include <system_error>

namespace DCP::Window {

  HKEY const DismissableMessageDialog::key = HKEY_CURRENT_USER;

  char const *const DismissableMessageDialog::subkey = (
    "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\"
    "DontShowMeThisDialogAgain"
  );

  char const *const DismissableMessageDialog::string = "NO";

  int DismissableMessageDialog::show(
    HWND const parent,
    char const *const caption,
    char const *const message
  ) const {
    SetLastError(ERROR_SUCCESS);
    int const result = SHMessageBoxCheck(
      parent, message, caption, type, defaultResult, name.data()
    );
    if (result == -1) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "SHMessageBoxCheck() failed"
      );
    }
    return result;
  }

  void DismissableMessageDialog::setDismissed(bool const dismissed) {
    if (dismissed) {
      Registry::setStringValue(string, key, subkey, name.data());
    } else {
      Registry::removeValue(key, subkey, name.data());
    }
  }

  bool DismissableMessageDialog::getDismissed() const {
    return (
      Registry::tryToGetStringValue(key, subkey, name.data()).first == string
    );
  }

}

#endif
