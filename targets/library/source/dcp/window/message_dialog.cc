/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       February 2021
 */

#include <dcp/window/message_dialog.hh>

#if BOOST_OS_WINDOWS

  #include <system_error>

namespace DCP::Window {

  int MessageDialog::show(
    HWND const parent,
    char const *const caption,
    char const *const message
  ) const {
    SetLastError(ERROR_SUCCESS);
    int const result = MessageBox(parent, message, caption, type);
    if (!result) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "MessageBox() failed"
      );
    }
    return result;
  }

  MessageDialog::~MessageDialog() = default;

}

#endif
