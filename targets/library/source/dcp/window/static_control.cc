/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2021
 */

#include <dcp/window/static_control.hh>

#if BOOST_OS_WINDOWS

  #include <commctrl.h>

namespace DCP::Window::StaticControl {

  HBITMAP setBitmap(HWND const control, HBITMAP const bitmap) noexcept {
    assert(control);

    HBITMAP const oldBitmap = (HBITMAP)SendMessage(control, STM_SETIMAGE,
      (WPARAM)IMAGE_BITMAP, (LPARAM)bitmap
    );
    InvalidateRect(control, NULL, TRUE);
    if (oldBitmap && oldBitmap != bitmap) {
      DeleteObject(oldBitmap);
    }
    HBITMAP const newBitmap = (HBITMAP)SendMessage(control, STM_GETIMAGE,
      (WPARAM)IMAGE_BITMAP, NULL
    );
    if (newBitmap != bitmap) {
      DeleteObject(bitmap);
      return newBitmap;
    }
    return bitmap;
  }

}

#endif
