/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       April 2020
 */

#include <dcp/registry.hh>

#include "boost/nowide/convert.hpp"

#if BOOST_OS_WINDOWS

  #include <boost/numeric/conversion/cast.hpp>

namespace DCP::Registry {

  NameIterator::NameIterator(HKEY const key) :
  Iterator(key)
  {
    auto const status = RegQueryInfoKey(key, nullptr, nullptr, nullptr, nullptr,
      nullptr, nullptr, &count, &maxSize, nullptr, nullptr, nullptr
    );
    if (status != ERROR_SUCCESS) {
      throw std::system_error(
        std::error_code(status, std::system_category()),
        "RegQueryInfoKey() failed"
      );
    }
    if (maxSize >= std::numeric_limits<DWORD>::max()) {
      throw std::overflow_error("Maximum registry name length too long");
    }
    buffer.assign(maxSize + 1U, '\0');
  }

  std::string NameIterator::getNext() {
    assert(index < count);

    auto size = boost::numeric_cast<DWORD>(buffer.size());
    buffer[0] = '\0';
    auto const status = RegEnumValue(key, index++, &buffer.front(), &size,
      nullptr, nullptr, nullptr, nullptr
    );
    if (status != ERROR_SUCCESS) {
      throw std::system_error(
        std::error_code(status, std::system_category()),
        "RegEnumValue() failed"
      );
    }
    return buffer.data();
  }

  SubkeyIterator::SubkeyIterator(HKEY const key) :
  Iterator(key)
  {
    auto const status = RegQueryInfoKey(key, nullptr, nullptr, nullptr,
      &count, &maxSize, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr
    );
    if (status != ERROR_SUCCESS) {
      throw std::system_error(
        std::error_code(status, std::system_category()),
        "RegQueryInfoKey() failed"
      );
    }
    if (maxSize >= std::numeric_limits<DWORD>::max()) {
      throw std::overflow_error("Maximum registry subkey length too long");
    }
    buffer.assign(maxSize + 1U, '\0');
  }

  std::string SubkeyIterator::getNext() {
    assert(index < count);

    auto size = boost::numeric_cast<DWORD>(buffer.size());
    buffer[0] = '\0';
    auto const status = RegEnumKeyEx(key, index++, &buffer.front(), &size,
      nullptr, nullptr, nullptr, nullptr
    );
    if (status != ERROR_SUCCESS) {
      throw std::system_error(
        std::error_code(status, std::system_category()),
        "RegEnumKeyEx() failed"
      );
    }
    return buffer.data();
  }

  bool keyExists(HKEY const key, char const *const subkey) noexcept {
    assert(key != Key{});
    assert(subkey);

    return tryToGetKey(key, subkey).first != Key{};
  }

  void renameKey(
    char const *const newName,
    HKEY const key,
    char const *const subkey
  ) {
    auto const status = RegRenameKey(key,
      boost::nowide::widen(subkey).data(),
      boost::nowide::widen(newName).data()
    );
    if (status != ERROR_SUCCESS) {
      throw std::system_error(
        std::error_code(status, std::system_category()), "RegRenameKey() failed"
      );
    }
  }

  std::pair<Key, std::error_code> tryToGetKey(
    HKEY const key,
    char const *const subkey,
    bool const writeable
  ) noexcept {
    assert(subkey);

    REGSAM access{KEY_READ | KEY_WOW64_64KEY};
    if (writeable) {
      access |= KEY_WRITE;
    }

    HKEY result{nullptr};
    auto status = RegOpenKeyEx(key, subkey, 0, access, &result);
    if (!result && writeable) {
      // Note that RegCreateKeyEx will return the existing key if it now exists
      // since the above check, unless access is denied.
      status = RegCreateKeyEx(key, subkey, 0, nullptr,
        REG_OPTION_NON_VOLATILE, access, nullptr, &result, nullptr
      );
    }
    if (status != ERROR_SUCCESS) {
      return {{}, std::error_code(status, std::system_category())};
    }
    return {Key(result, RegCloseKey), {}};
  }

  bool removeKey(HKEY const key, char const *const subkey) {
    assert(key != Key{});
    assert(subkey);

    if (!keyExists(key, subkey)) {
      return false;
    }
    auto status = RegDeleteTree(key, subkey);
    if (status != ERROR_SUCCESS) {
      throw std::system_error(
        std::error_code(status, std::system_category()),
        "RegDeleteTree() failed"
      );
    }
    if (keyExists(key, subkey)) {
      status = RegDeleteKey(key, subkey);
      if (status != ERROR_SUCCESS) {
        throw std::system_error(
          std::error_code(status, std::system_category()),
          "RegDeleteKey() failed"
        );
      }
    }
    return true;
  }

  bool valueExists(
    HKEY const key,
    char const *const subkey,
    char const *const name
  ) noexcept {
    assert(key != Key{});
    assert(subkey);
    assert(name);

    auto const subkeyKey = tryToGetKey(key, subkey).first;
    return (
      subkeyKey != Key{} && RegQueryValueEx(
        subkeyKey, name, nullptr, nullptr, nullptr, nullptr
      ) == ERROR_SUCCESS
    );
  }

  void setStringValue(
    char const *const string,
    HKEY const key,
    char const *const subkey,
    char const *const name
  ) {
    assert(string);
    assert(subkey);
    assert(name);

    auto const status = RegSetKeyValue(key,
      *subkey ? subkey : nullptr,
      *name ? name : nullptr,
      REG_SZ, string,
      boost::numeric_cast<DWORD>(strlen(string) + 1)
    );
    if (status != ERROR_SUCCESS) {
      throw std::system_error(
        std::error_code(status, std::system_category()),
        "RegSetKeyValue() failed"
      );
    }
  }

  std::pair<std::string, std::error_code> tryToGetStringValue(
    HKEY const key,
    char const *const subkey,
    char const *const name
  ) {
    assert(subkey);
    assert(name);

    DWORD size{};
    DWORD type{REG_SZ};
    auto result = RegGetValue(key, subkey, name, RRF_RT_REG_SZ, &type,
      nullptr, &size
    );
    if (result == ERROR_SUCCESS) {
      assert(size);
      assert(type == REG_SZ);
      std::string buffer(size, '\0'); // Can throw
      result = RegGetValue(key, subkey, name, RRF_RT_REG_SZ, &type,
        (LPBYTE)&buffer.front(), &size
      );
      if (result == ERROR_SUCCESS) {
        // Construct returned string from C string to trim after null.
        return {buffer.data(), {}};
      }
    }
    return {{}, std::error_code(result, std::system_category())};
  }

  bool removeValue(
    HKEY const key,
    char const *const subkey,
    char const *const name
  ) {
    assert(subkey);
    assert(name);

    if (!valueExists(key, subkey, name)) {
      return false;
    }
    auto const status = RegDeleteKeyValue(key, subkey, name);
    if (status != ERROR_SUCCESS) {
      throw std::system_error(
        std::error_code(status, std::system_category()),
        "RegDeleteKeyValue() failed"
      );
    }
    return true;
  }

}

#endif
