/**
 *  @file
 *  @author     Wes Garland, wes@sparc.network
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2018
 */

#include <dcp/signal.hh>

#include <cerrno>
#include <csignal>
#include <cstring>

namespace DCP::Signal {

  boost::optional<Handler::Callback>
  Handler::registerCallback(int number, Callback callback) {
    errno = 0;
    Callback result = signal(number, callback);
    // NOLINTNEXTLINE(performance-no-int-to-ptr)
    if (result == SIG_ERR) {
      std::error_code const errorCode(errno, std::system_category());
      std::string const &errorMessage = errorCode.message();
      log(LogLevel::error, "Signal %i: Error setting signal callback %s: %s",
        number, String::fromPointer(&callback).data(), errorMessage.data()
      );
      return {};
    }
    log(LogLevel::debug, "Signal %i: Replaced callback %s with callback %s",
      number,
      String::fromPointer(&result).data(),
      String::fromPointer(&callback).data()
    );
    return result;
  }

  void Handler::deinitialize() {
    for (auto pair: original) {
      registerCallback(pair.first, pair.second);
    }
  }

  Handler::~Handler() {
    tryToLog(LogLevel::debug, "Signal Handler: destructing");
    try {
      deinitialize();
    } catch (...) {
      assert(false);
    }
  }

  #if !BOOST_OS_WINDOWS

  bool send(pid_t const process, int const signal) {
    errno = 0;
    if (kill(process, signal) == -1) {
      switch (errno) {
      case ESRCH:
        log(LogLevel::info, "The process no longer exists.");
        return false;
      default:
        throw std::system_error(
          std::error_code(errno, std::system_category()), "kill() failed"
        );
      }
    }
    return true;
  }

  #endif

}
