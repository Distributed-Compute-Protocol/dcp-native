/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       May 2021
 */

#include <dcp/crypto.hh>

#if BOOST_OS_WINDOWS

  #include <dcp/handle.hh>

  #include <boost/algorithm/hex.hpp>
  #include <boost/numeric/conversion/cast.hpp>

  #include <windows.h> // Include before other windows headers
  #include <dpapi.h>

namespace DCP::Crypto {

  namespace {

    struct Blob {

      // NOLINTNEXTLINE(hicpp-explicit-conversions)
      Blob(DATA_BLOB const blob = {}) :
      data(
        reinterpret_cast<char *>(blob.pbData),
        [](char *const data) {
          SetLastError(ERROR_SUCCESS);
          if (LocalFree(static_cast<HLOCAL>(data)) != NULL) {
            throw std::system_error(
              std::error_code(GetLastError(), std::system_category()),
              "LocalFree() failed"
            );
          }
        }
      ),
      size(boost::numeric_cast<size_t>(blob.cbData))
      {}

      // NOLINTNEXTLINE(hicpp-explicit-conversions)
      operator DATA_BLOB() const {
        DATA_BLOB blob{};
        blob.pbData = reinterpret_cast<BYTE *>(static_cast<char *>(data));
        blob.cbData = static_cast<DWORD>(size);
        return blob;
      }

      // NOLINTNEXTLINE(hicpp-explicit-conversions)
      operator std::string() const {
        return std::string(getData(), getSize());
      }

      char const *getData() const {
        return data;
      }

      size_t getSize() const {
        return size;
      }

    private:

      DCP::Handle<char *, nullptr> data;
      size_t size;

    };

  }

  std::string encrypt(std::string string) {
    // Do not encrypt empty strings.
    if (string.empty()) {
      throw std::invalid_argument("Encrypting empty strings is not supported.");
    }

    // Populate input blob from input string.
    DATA_BLOB input{};
    input.pbData = reinterpret_cast<BYTE *>(&string.front());
    input.cbData = boost::numeric_cast<DWORD>(string.size());

    // Encrypt into binary output blob.
    DATA_BLOB output{};
    SetLastError(ERROR_SUCCESS);
    if (
      !CryptProtectData(
        &input, NULL, NULL, NULL, NULL, CRYPTPROTECT_UI_FORBIDDEN, &output
      )
    ) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "CryptProtectData() failed"
      );
    }
    Blob blob{output};
    assert(blob.getSize());
    assert(blob.getData());

    // Convert binary output blob to hex string, whereby each nybble of each
    // binary byte is converted to a hex char. Since there are 2 hex chars per
    // binary byte, reserve space accordingly (eg. 0x0F -> "0F").
    string.clear();
    if (blob.getSize() > (std::numeric_limits<size_t>::max() >> 1U)) {
      throw std::invalid_argument("The string is too big to encrypt.");
    }
    size_t const size = (blob.getSize() << 1U);
    assert(size);
    string.reserve(size);
    boost::algorithm::hex(blob.getData(), blob.getData() + blob.getSize(),
      std::back_inserter(string)
    );
    assert(string.size() == size);
    return string;
  }

  std::string decrypt(std::string const &encrypted) {
    // Reverse the hex conversion performed by `encrypt()` to restore the
    // original binary byte array. Since there are half the number of binary
    // bytes than hex chars, reserve space accordingly (eg. "0F" -> 0x0F).
    std::string string{};
    size_t const size = (encrypted.size() >> 1U);
    if (!size) {
      throw std::invalid_argument("The encrypted string input is invalid");
    }
    string.reserve(size);
    boost::algorithm::unhex(encrypted.data(), std::back_inserter(string));
    assert(string.size() == size);

    // Populate input blob from binary input string.
    DATA_BLOB input{};
    input.pbData = reinterpret_cast<BYTE *>(&string.front());
    input.cbData = boost::numeric_cast<DWORD>(string.size());

    // Decrypt into output blob.
    DATA_BLOB output{};
    SetLastError(ERROR_SUCCESS);
    if (
      !CryptUnprotectData(
        &input, NULL, NULL, NULL, NULL, CRYPTPROTECT_UI_FORBIDDEN, &output
      )
    ) {
      throw std::system_error(
        std::error_code(GetLastError(), std::system_category()),
        "CryptUnprotectData() failed"
      );
    }
    Blob blob{output};
    assert(blob.getSize());
    assert(blob.getData());

    // Return the blob, converting to string.
    return blob;
  }

}

#endif
