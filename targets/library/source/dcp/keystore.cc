/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       October 2020
 */

#include <dcp/keystore.hh>

#include <dcp/json.hh>
#include <dcp/logger.hh>

#include <boost/exception/diagnostic_information.hpp>

#include <fstream>

namespace DCP {

  bool Keystore::validateAddress(std::string &string) {
    static size_t const size = 40;
    static std::string const prefix{"0x"};

    size_t const offset = (string.rfind(prefix, 0) == 0) ? prefix.size() : 0;
    if (string.size() - offset != size) {
      return false;
    }
    auto iterator = string.begin();
    if (offset) {
      if (offset != prefix.size()) {
        return false;
      }
      std::advance(iterator,
        static_cast<decltype(iterator)::difference_type>(offset)
      );
    }
    if (!std::all_of(iterator, string.end(), isxdigit)) {
      return false;
    }
    if (!offset) {
      string.insert(0, prefix);
    }
    for (auto &digit: string) {
      digit = static_cast<char>(tolower(digit));
    }
    return true;
  }

  boost::optional<Keystore> Keystore::parse(std::string const &string) {
    try {
      std::stringstream ss{};
      ss << string.data();
      boost::system::error_code ec;
      boost::json::value json = JSON::read(ss, ec);
      if (json.is_null()) {
        log(LogLevel::warning,
          "Failed to parse keystore from string \"%s\": %s",
          string.data(), ec.message().data()
        );
        return {};
      }
      boost::json::object const &object = json.as_object();
      std::string address{object.at("address").as_string().data()};
      if (validateAddress(address)) {
        return Keystore{string, std::move(address)};
      }
    } catch (...) {
      log(LogLevel::warning, "Failed to parse keystore from string \"%s\": %s",
        string.data(), boost::current_exception_diagnostic_information().data()
      );
    }
    return {};
  }

  boost::optional<Keystore> Keystore::read(
    boost::filesystem::path const &filePath
  ) {
    std::ostringstream oss{};
    {
      std::ifstream in{filePath.string()};
      if (!in) {
        log(LogLevel::warning, "Failed to read keystore from file \"%s\"",
          filePath.string().data()
        );
        return {};
      }
      oss << in.rdbuf();
    }
    return parse(oss.str());
  }

}
