/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       May 2020
 */

#if !defined(DCP_Graphics_)
  #define DCP_Graphics_

  #if BOOST_OS_WINDOWS

    #include <dcp/color.hh>
    #include <dcp/handle.hh>

    #include <windows.h>

namespace DCP::Graphics {

  /// Get and configure the device context for the window.
  Handle<HDC, nullptr> getDC(HWND window);

  /// Create a compatible device context.
  Handle<HDC, nullptr> createCompatibleDC(HDC dc);

  int getFontHeight(int height);

  /** Select the object.
   *
   *  @return A handle to the old object, which is restored on destroy.
   */
  Handle<HGDIOBJ, nullptr> selectObject(HDC dc, HGDIOBJ object);

  /** Set the image stretch mode for the device context.
   *
   *  @param  dc
   *          The device context.
   *  @param  mode
   *          The mode, as defined in the
   *          [SetStretchBltMode documentation](https://docs.microsoft.com/en-us/windows/win32/api/wingdi/nf-wingdi-setstretchbltmode).
   */
  void setBitmapStretchMode(HDC dc, int mode);

  /** Scale the size to fit the container size, preserving aspect ratio.
   *  If either dimension takes up too much space, scale it down,
   *  then scale the other dimension to preserve source aspect ratio.
   */
  SIZE scaleToFit(SIZE size, SIZE containerSize, double maxContainerFraction);

  /// Create a solid brush with the given color.
  Handle<HBRUSH, nullptr> createSolidBrush(Color::Value color);

}

  #endif
#endif
