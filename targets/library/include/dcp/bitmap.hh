/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2021
 */

#if !defined(DCP_Bitmap_)
  #define DCP_Bitmap_

  #if BOOST_OS_WINDOWS

    #include <dcp/handle.hh>

    #include <boost/filesystem/path.hpp>

    #include <windows.h>

namespace DCP::Bitmap {

  /// Load the bitmap from the file, or return null.
  Handle<HBITMAP, nullptr> loadFile(boost::filesystem::path const &filePath);

  /// Get the bitmap size.
  SIZE getSize(HBITMAP bitmap);

}

  #endif
#endif
