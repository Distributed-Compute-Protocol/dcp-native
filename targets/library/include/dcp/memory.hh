/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       May 2020
 */

#if !defined(DCP_Memory_)
  #define DCP_Memory_

namespace DCP::Memory {

  long unsigned getPageSize() noexcept;

}

#endif
