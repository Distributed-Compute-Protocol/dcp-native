/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       November 2023
 */

#if !defined(DCP_Argument_)
  #define DCP_Argument_

  #include <ostream>

/// Utilities related to argc/argv parsing.
namespace DCP::Argument {

  template <
    typename Output,
    typename Name,
    typename Usage,
    typename Description,
    typename... Options
  >
  void printHelp(
    Output &output,
    Name const &name,
    Usage const &usage,
    Description const &description,
    Options const &... options
  ) {
    if (!usage.empty()) {
      output << "Usage: " << name << ' ' << usage << std::endl;
      output << std::endl;
    }
    if (!description.empty()) {
      output << description << std::endl;
      output << std::endl;
    }
    ((output << options << std::endl), ...);
  }

}

#endif
