/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       December 2021
 */

#if !defined(DCP_Pipe_)
  #define DCP_Pipe_

  #if BOOST_OS_WINDOWS

    #include <dcp/handle.hh>

    #include <windows.h>

    #include <string>

namespace DCP {

  struct Pipe {

    Pipe();

    void read(std::string &string);

    HANDLE getReadEnd() const;

    HANDLE getWriteEnd() const;

  private:

    Handle<HANDLE, nullptr> readEnd{};

    Handle<HANDLE, nullptr> writeEnd{};

  };

}

  #endif
#endif
