/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2021
 */

#if !defined(DCP_JSON_)
  #define DCP_JSON_

  #include <boost/json/error.hpp>
  #include <boost/json/value.hpp>

namespace DCP::JSON {

  /// Return a null value on error and sets the error code.
  boost::json::value read(std::istream &is, boost::system::error_code &ec);

}

#endif
