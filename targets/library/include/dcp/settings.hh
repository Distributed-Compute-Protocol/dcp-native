/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       October 2023
 */

#if !defined(DCP_Settings_)
  #define DCP_Settings_

#if BOOST_OS_WINDOWS
  #include <dcp/registry.hh>
#endif

  #include <string>
  #include <system_error>

namespace DCP::Settings {

  /// A class that allows storage and retrieval of individual settings.
  struct Tree final {

    /** Instantiate a Settings::Tree.
     *
     *  @param  path
     *          The path of the setting to use as the root of the Settings::Tree
     *          object, for only including a settings subset, or the empty
     *          string (default) to include all settings.
     *  @param  writable
     *          Whether to open underlying settings with write permissions and
     *          create if not found; may fail if insufficient permissions
     *          (default: false).
     */
    static std::pair<Tree, std::error_code> get(
      char const *path = "",
      bool writable = false
    ) noexcept;

    struct Iterator {

      explicit Iterator(Tree const &tree) :
      tree(tree)
      {}

      Iterator(Iterator const &) = delete;
      Iterator(Iterator const &&) = delete;

      Iterator &operator =(Iterator const &) = delete;
      Iterator &operator =(Iterator const &&) = delete;

      virtual size_t getCount() const noexcept = 0;

      virtual std::string getNext() = 0;

      virtual ~Iterator() = default;

    protected:

      Tree const &getTree() const {
        return tree;
      }

    private:

      Tree const &tree;

    };

    struct NameIterator final : Iterator {

      template <typename... Arguments>
      explicit NameIterator(Arguments &&... arguments) :
      Iterator(std::forward<Arguments>(arguments)...)
  #if BOOST_OS_WINDOWS
      , iterator(getTree().key)
  #endif
      {}

      size_t getCount() const noexcept override;

      std::string getNext() override;

    private:

  #if BOOST_OS_WINDOWS

      Registry::NameIterator iterator;

  #endif

    };

    struct PathIterator final : Iterator {

      template <typename... Arguments>
      explicit PathIterator(Arguments &&... arguments) :
      Iterator(std::forward<Arguments>(arguments)...)
  #if BOOST_OS_WINDOWS
      , iterator(getTree().key)
  #endif
      {}

      size_t getCount() const noexcept override;

      std::string getNext() override;

    private:

  #if BOOST_OS_WINDOWS

      Registry::SubkeyIterator iterator;

  #endif

    };

    bool exists(
      char const *path,
      char const *name
    ) const noexcept;

    std::pair<std::string, std::error_code> tryToGet(
      char const *path,
      char const *name
    ) const;

    void set(
      char const *path,
      char const *name,
      std::string value,
      bool overwrite = true
    );

    void unset(
      char const *path,
      char const *name
    );

  private:

#if BOOST_OS_WINDOWS

    explicit Tree(Registry::Key key) noexcept;

    Registry::Key key{};

#endif

  };

}

#endif
