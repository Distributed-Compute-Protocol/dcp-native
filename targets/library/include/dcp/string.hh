/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       December 2019
 */

#if !defined(DCP_String_)
  #define DCP_String_

  #include <cstdio>
  #include <iomanip>
  #include <sstream>
  #include <system_error>

namespace DCP::String {

  /// Convert printf-style format string and arguments to a string.
  template <typename... Arguments>
  std::string format(
    char const *const formatString,
    Arguments const &... arguments
  ) {
    assert(formatString);

    // Get the number of bytes, not including the null terminator.
    errno = 0;
  #if BOOST_OS_UNIX || BOOST_OS_MACOS
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wformat-nonliteral"
  #endif
    int count = std::snprintf(nullptr, 0, formatString, arguments...);
  #if BOOST_OS_UNIX || BOOST_OS_MACOS
    #pragma GCC diagnostic pop
  #endif
    if (count < 0) {
      throw std::system_error(
        std::error_code(errno, std::system_category()), "snprintf() failed"
      );
    }
    assert(count >= 0);

    // Populate and return the buffer with the entire string (plus null
    // terminator).
    std::string buffer(static_cast<size_t>(count) + 1U, '\0');
    errno = 0;
  #if BOOST_OS_UNIX || BOOST_OS_MACOS
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wformat-nonliteral"
  #endif
    int const result = std::snprintf(
      &buffer.front(), buffer.size(), formatString, arguments...
    );
  #if BOOST_OS_UNIX || BOOST_OS_MACOS
    #pragma GCC diagnostic pop
  #endif
    if (result < count) {
      throw std::system_error(
        std::error_code(errno, std::system_category()),
        "snprintf() failed to write all bytes"
      );
    }
    return buffer;
  }

  /** Represent a data or function pointer as a string.
   *
   *  This is useful for logging, since function pointers aren't supported by
   *  the @c \%p `printf` format specifier for data pointers.
   *
   *  @return A unique string representing the pointer.
   *  @note   This is based on the address, but does not take endianness into
   *          account.
   */
  template <typename Type>
  std::string fromPointer(Type const *p) {
    std::stringstream ss;
    auto const *const cp = reinterpret_cast<char unsigned const *>(p);
    for (size_t i = 0; i < sizeof(p); ++i) {
      ss << std::hex << std::setw(2) << std::setfill('0');
      ss << static_cast<unsigned>(cp[i]);
    }
    return ss.str();
  }

  /// Escape all non-printable characters, in place, for C++.
  void escape(std::string &string);

  /// Join the quoted strings with inner quotes escaped.
  template <typename Strings>
  std::string joinQuoted(
    Strings const &strings,
    char const *const delimiter = " "
  ) {
    char const *separator{""};
    std::ostringstream quotedAndJoined{};
    for (auto const &string: strings) {
      quotedAndJoined << separator << std::quoted(string);
      separator = delimiter;
    }
    return quotedAndJoined.str();
  }

}

#endif
