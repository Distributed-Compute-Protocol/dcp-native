/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       August 2020
 */

#if !defined(DCP_Color_)
  #define DCP_Color_

  #if BOOST_OS_WINDOWS

    #include <boost/optional.hpp>

    #include <windows.h>

    #include <string>

namespace DCP::Color {

  using Value = COLORREF;

  /// Throws if string cannot be parsed as a color.
  Value fromString(std::string const &string);

  std::string toString(Value const color);

}

  #endif
#endif
