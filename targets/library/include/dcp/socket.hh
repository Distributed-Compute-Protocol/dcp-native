/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       December 2019
 */

#if !defined(DCP_Socket_)
  #define DCP_Socket_

  #if !(BOOST_OS_UNIX || BOOST_OS_MACOS || BOOST_OS_WINDOWS)
    #error Platform not supported.
  #endif

  #include <dcp/handle.hh>
  #include <dcp/socket/address.hh>
  #include <dcp/time.hh>

  #include <boost/optional.hpp>

  #if BOOST_OS_WINDOWS
    #include <windows.h>
  #endif

  #include <array>
  #include <cinttypes>
  #include <functional>
  #include <set>
  #include <string>

  #if BOOST_OS_WINDOWS
    #define INVALID_SOCKET (SOCKET)(~0) // From winsock.h
    #define DCP_Socket_print PRIu64
using SOCKET = UINT_PTR; // From winsock.h
  #else
    #define DCP_Socket_print "i" // NOLINT(cppcoreguidelines-macro-usage)
using SOCKET = int; // POSIX socket descriptor type
static SOCKET const INVALID_SOCKET{-1};
  #endif

namespace DCP::Socket {

  /// A socket handle value type.
  using Handle = DCP::Handle<SOCKET, INVALID_SOCKET>;

  struct Interrupter;

  /// A tuple of socket sets.
  struct SetTuple final {
    std::set<SOCKET> input{};
    std::set<SOCKET> output{};
    std::set<SOCKET> error{};
  };

  /// The default amount of time for a wait to block.
  static Time::Milliseconds const defaultTimeout{
    60 * Time::millisecondsPerSecond
  };

  /// Create a connected pair of unblocked TCP sockets that close on destruct.
  std::array<Handle, 2> createPair();

  /** Make the socket non-blocking.
   *
   *  @throw  std::system_error
   *          An error occurred while configuring the socket as non-blocking.
   */
  void unblock(SOCKET socket);

#if !BOOST_OS_WINDOWS
  /// Either turn on or turn off "close-on-exec" for the socket.
  void setCloseOnExec(SOCKET socket, bool value = true);
#endif

  /** Wait for the sockets to be ready to read or write.
   *
   *  @param  sockets
   *          The sockets to wait for.
   *  @param  timeout
   *          The maximum time to block, in milliseconds.
   *  @return The ready sockets.
   *  @throw  std::system_error
   *          An unrecoverable error occurred when waiting for data.
   */
  SetTuple wait(
    SetTuple sockets,
    Time::Milliseconds timeout = defaultTimeout
  );

  /** Read data directly from the socket into the memory provided.
   *
   *  @param  input
   *          The socket to read from.
   *  @param  memory
   *          The memory pointer to read into.
   *  @param  size
   *          The size of the memory; on return, set to the number of bytes
   *          actually read.
   *  @return `true` if the connection is still open, or `false` if it was
   *          closed normally.
   *  @throw  std::system_error
   *          An unrecoverable error occurred when reading data.
   */
  bool read(SOCKET input, char *memory, size_t &size);

  /** Write data directly to the socket from the memory provided.
   *
   *  @param  output
   *          The socket to write to.
   *  @param  memory
   *          The memory pointer to read from.
   *  @param  size
   *          The size of the memory; on return, set to the number of bytes
   *          actually written.
   *  @throw  std::system_error
   *          An unrecoverable error occurred when writing data.
   */
  void write(SOCKET output, char const *memory, size_t &size);

  /** Wait until socket is ready to read, then perform a buffered read.
   *
   *  @note   The terminating newline is stripped from the received string.
   */
  bool receive(
    SOCKET socket,
    std::string &string,
    Time::Milliseconds timeout = defaultTimeout
  );

  /** Wait until socket is ready to write, then perform a buffered write.
   *
   *  @note   A terminating newline is required on the sent string.
   */
  void send(
    SOCKET socket,
    char const *string,
    Time::Milliseconds timeout = defaultTimeout
  );

  /** Get the address for the socket.
   *
   *  @return The socket address, if it could successfully be obtained.
   *  @note   Must only be called on a bound socket; behaviour is unspecified
   *          for an unbound socket.
   */
  [[nodiscard]]
  boost::optional<Address> getAddress(SOCKET socket);

  /** Get an available address.
   *
   *  @param  host
   *          The local host name or address, or null for
   *          `INADDR_ANY`/`IN6ADDR_ANY_INIT` (IPv4/IPv6).
   *  @return An available local socket address, if possible.
   *  @note   Susceptible to TOCTOU; the local address may be come unavailable
   *          between getting and using.
   */
  [[nodiscard]]
  boost::optional<Address> getAvailableAddress(char const *host = nullptr);

  /** Block until either a connection arrives on the specified port or the
   *  timeout expires, then pass the socket (or `INVALID_SOCKET` if the
   *  timeout expired) to the accept callback and block again if the accept
   *  callback returns `true`.
   *
   *  @param  host
   *          The host name or address to listen on, in string form, or null for
   *          `INADDR_ANY`/`IN6ADDR_ANY_INIT` (IPv4/IPv6).
   *  @param  port
   *          The port to listen on, in string form.
   *  @param  listenCallback
   *          A callback that takes the listen socket and the bound port.
   *  @param  acceptCallback
   *          A callback that takes an accepted socket (or `INVALID_SOCKET`
   *          if none was accepted before the timeout) and returns `true` to
   *          continue or `false` to break. Since the loop is blocked while
   *          this is called, it should return quickly.
   *  @param  interrupter
   *          The listen interrupter; when invoked, wakes the blocking listen.
   *  @param  timeout
   *          The timeout, in milliseconds, before re-entering the loop; the
   *          accept callback will be called after this amount of time passes,
   *          regardless of whether there is a connection ready.
   *  @return `false` if listening was unsuccessful.
   */
  bool listen(
    char const *host,
    char const *port,
    std::function<void (Handle, short unsigned)> const &listenCallback,
    std::function<bool (Handle)> const &acceptCallback,
    Interrupter *interrupter = nullptr,
    Time::Milliseconds timeout = defaultTimeout
  );

  /** Connect to a host.
   *
   *  @param  host
   *          The host to connect to, in string form.
   *  @param  port
   *          The port to connect to, in string form.
   *  @return A socket handle that shuts down the connection when it goes
   *          out of scope.
   */
  Handle connect(char const *host, char const *port);

#if !BOOST_OS_WINDOWS
  void duplicate(SOCKET socket, SOCKET destination);
#endif

  bool close(SOCKET socket) noexcept;

  bool shutDown(SOCKET socket) noexcept;

  bool shutDownAndClose(SOCKET socket) noexcept;

}

#endif
