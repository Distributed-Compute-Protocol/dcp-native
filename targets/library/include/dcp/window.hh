/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2021
 */

#if !defined(DCP_Window_)
  #define DCP_Window_

  #if BOOST_OS_WINDOWS

    #include <dcp/color.hh>
    #include <dcp/handle.hh>
    #include <dcp/time.hh>

    #include <boost/filesystem/path.hpp>

    #include <windows.h>

namespace DCP::Window {

  std::string getClassName(HWND window);

  /// Return the parent, or return `nullptr` on failure.
  HWND tryToGetParent(HWND window);

  /// Return the child, or return `nullptr` on failure.
  HWND tryToGetChild(HWND window, int id);

  RECT getClientRect(HWND window);

  bool postMessage(
    HWND window,
    UINT message,
    WPARAM wParam = {},
    LPARAM lParam = {}
  ) noexcept;

  bool hasFocus(HWND window);

  /// Set focus and return `true`, or return `false` on failure.
  bool tryToSetFocus(HWND window);

  bool isReadOnly(HWND window);

  void setReadOnly(HWND window, bool readOnly);

  void setIcon(HWND window, HICON icon);

  void setFont(HWND window, HFONT font);

  void setText(HWND window, char const *text);

  std::string getText(HWND window);

  void setChecked(HWND window, bool checked = true);

  bool getChecked(HWND window);

  boost::filesystem::path chooseFile(HWND window, char const *filter);

  boost::optional<Color::Value> chooseColor(
    HWND window,
    Color::Value initialColor
  );

  /// Create a timer associated with the window and return the handle.
  Handle<UINT_PTR, 0> createTimer(
    HWND window,
    Time::Milliseconds timeIncrement
  );

}

  #endif
#endif
