/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#if !defined(DCP_Socket_Interrupter_)
  #define DCP_Socket_Interrupter_

  #include <dcp/socket.hh>

namespace DCP::Socket {

  /** A socket that can be specified as a @ref wait() input such that waiting
   *  is interrupted when @ref invoke() is called (from any thread).
   */
  struct Interrupter final {

    Interrupter();

    Interrupter(Interrupter const &) = delete;
    Interrupter(Interrupter const &&) = delete;

    Interrupter &operator =(Interrupter const &) = delete;
    Interrupter &operator =(Interrupter const &&) = delete;

    // NOLINTNEXTLINE(hicpp-explicit-conversions)
    operator SOCKET() const;

    /** Invoke the interrupter.
     *
     *  @return `false` if the interrupter could not be invoked successfully.
     */
    bool invoke();

    ~Interrupter();

  private:

  #if BOOST_OS_WINDOWS
    /// Create a basic, unbound socket.
    Handle createSocket();
  #endif

    std::array<Handle, 2> sockets{};

  };

}

#endif
