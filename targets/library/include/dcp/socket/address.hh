/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       June 2024
 */

#if !defined(DCP_Socket_Address_)
  #define DCP_Socket_Address_

  #include <string>

namespace DCP::Socket {

  /** A socket address comprised of an IP address, a port, and a flag indicating
   *  whether the IP address is IPv4 or IPv6.
   */
  struct Address final {
    explicit Address(
      std::string ip,
      short unsigned const port,
      bool const ipv6
    ) :
    ip{std::move(ip)},
    port{port},
    ipv6{ipv6}
    {}

    std::string const &getIP() const {
      return ip;
    }

    short unsigned getPort() const {
      return port;
    }

    bool isIPV6() const {
      return ipv6;
    }

  private:

    std::string ip{};
    short unsigned port{};
    bool ipv6{};
  };

}

#endif
