/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       October 2020
 */

#if !defined(DCP_Keystore_)
  #define DCP_Keystore_

  #include <boost/filesystem/path.hpp>
  #include <boost/optional.hpp>

namespace DCP {

  struct Keystore final {

    /// Normalize and validate the address, or return false if invalid.
    static bool validateAddress(std::string &string);

    /// Parse the keystore from a JSON string.
    static boost::optional<Keystore> parse(std::string const &string);

    /// Read the keystore from a JSON file.
    static boost::optional<Keystore> read(
      boost::filesystem::path const &filePath
    );

    /// Return the original JSON string.
    [[nodiscard]]
    std::string const &stringify() const {
      return keystore;
    }

    /// Return the normalized address.
    [[nodiscard]]
    std::string const &getAddress() const {
      return address;
    }

  private:

    explicit Keystore(std::string keystore, std::string address) :
    keystore(std::move(keystore)),
    address(std::move(address))
    {}

    std::string keystore{};

    std::string address{};

  };

}

#endif
