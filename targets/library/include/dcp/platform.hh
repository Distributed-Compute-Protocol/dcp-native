/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       February 2022
 */

#if !defined(DCP_Platform_)
  #define DCP_Platform_

  #include <string>

namespace DCP::Platform {

  /** Gets the lower-case name of the platform.
   *
   *  @details
   *    Corresponds to Node's process.platform:
   *    https://nodejs.org/api/process.html#processplatform
   */
  std::string getName();

}

#endif
