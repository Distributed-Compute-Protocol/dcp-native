/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       August 2020
 */

#if !defined(DCP_Resource_)
  #define DCP_Resource_

  #if BOOST_OS_WINDOWS

    #include <dcp/color.hh>
    #include <dcp/handle.hh>

    #include <string>

    #include <windows.h>

namespace DCP::Resource {

  Color::Value loadColor(int unsigned id, int unsigned type);

  Handle<HBITMAP, nullptr> loadBitmap(int unsigned id);

  Handle<HICON, nullptr> loadIcon(int unsigned id);

  std::string loadString(int unsigned id);

  std::string loadFile(int unsigned id, int unsigned type);

  HWND createDialog(int unsigned id, HWND parent, DLGPROC callback);

  INT_PTR openModalDialog(int unsigned id, HWND parent, DLGPROC callback);

  void closeModalDialog(HWND dialog, INT_PTR result);

}

  #endif
#endif
