/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#if !defined(DCP_Time_)
  #define DCP_Time_

  #include <cstdint>

namespace DCP::Time {

  /// A time type denoting milliseconds since the epoch.
  using Milliseconds = uint64_t;

  static Milliseconds const millisecondsPerSecond = UINT64_C(1000);
  static uint64_t const microsecondsPerMillisecond = UINT64_C(1000);

  /**
   *  @return The number of milliseconds since the epoch.
   */
  Milliseconds now();

}

#endif
