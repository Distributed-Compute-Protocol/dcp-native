/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       December 2020
 */

#if !defined(DCP_Test_)
  #define DCP_Test_

  #include <dcp/logger.hh>

  #include <boost/filesystem/operations.hpp>
  #include <boost/test/data/test_case.hpp>
  #include <boost/test/debug.hpp>
  #include <boost/test/unit_test.hpp>

namespace DCP::Test {

  /// The default test timeout, in seconds.
  static int unsigned const timeout = 600;

  inline boost::filesystem::path getExecutablePath() {
    static auto const executablePath = boost::filesystem::absolute(
      boost::unit_test::framework::master_test_suite().argv[0]
    );
    assert(boost::filesystem::exists(executablePath));
    assert(!executablePath.parent_path().empty());
    assert(boost::filesystem::is_directory(executablePath.parent_path()));
    return executablePath;
  }

  struct GlobalFixture final {
    GlobalFixture() {
      // Disable spurious leak messages on Windows.
      boost::debug::detect_memory_leaks(false);

      // Configure boost to log all unit test messages.
      boost::unit_test::unit_test_log.set_threshold_level(
        boost::unit_test::log_level::log_successful_tests
      );

      // Initialize logger.
      Logger::getInstance(getExecutablePath().stem().string());
    }
  };

  BOOST_TEST_GLOBAL_FIXTURE(GlobalFixture);

}

#endif
