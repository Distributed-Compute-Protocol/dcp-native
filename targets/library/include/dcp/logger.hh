/**
 *  @file
 *  @author     Wes Garland, wes@sparc.network
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2018
 */

#if !defined(DCP_Logger_)
  #define DCP_Logger_

  #include <dcp/handle.hh>
  #include <dcp/log_level.hh>
  #include <dcp/string.hh>

  #if BOOST_OS_WINDOWS
    #include <windows.h>
  #endif

  #include <atomic>

namespace DCP {

  /** A thread-safe, singleton logger.
   *
   *  For each logging function, a log level can be a LogLevel value, a greater
   *  value to specify extra verbosity, or a negative value to mean "don't log".
   *
   *  Call @ref DCP::tryToLog() from `noexcept` code or other critical code
   *  where logging is expendable, and prefer calling @ref DCP::log() everywhere
   *  else so that exceptions are not unnecessarily buried.
   *
   *  Note that logging in hot loops should be done with care, since it could
   *  add a performance penalty.
   */
  struct Logger final {

    /// The default maximum log level for printing to standard error.
    static int const defaultMaxLogLevel;

    /// The default maximum log level for reporting to the system logger.
    static int const defaultMaxSystemLogLevel;

    /** Get the static instance of the Logger, constructing on the initial call.
     *
     *  @note   Arguments are only required on the initial call and are ignored
     *          on subsequent calls.
     *  @param  name
     *          The name to use for the system logger.
     *  @param  maxLogLevel
     *          The maximum log level to print to standard error.
     *  @param  maxSystemLogLevel
     *          The maximum log level to report to the system logger.
     */
    static Logger &getInstance(
      std::string const &name = {},
      int maxLogLevel = defaultMaxLogLevel,
      int maxSystemLogLevel = defaultMaxSystemLogLevel
    );

    Logger(Logger const &) = delete;
    Logger(Logger const &&) = delete;

    Logger &operator =(Logger const &) = delete;
    Logger &operator =(Logger const &&) = delete;

    /// Get the system logger name.
    [[nodiscard]]
    std::string const &getName() const;

    /// Get the maximum log level.
    [[nodiscard]]
    int getMaxLogLevel() const {
      return maxLogLevel;
    }

    /// Get the maximum system log level.
    [[nodiscard]]
    int getMaxSystemLogLevel() const {
      return maxSystemLogLevel;
    }

    /// Set the maximum system log level.
    void setMaxSystemLogLevel(int level) {
      maxSystemLogLevel = level;
    }

    /// Return `true` if the provided log level is currently logged anywhere.
    [[nodiscard]]
    bool willLog(int level) const {
      return (maxLogLevel >= level) && (maxSystemLogLevel >= level);
    }

    /** Print a log message to standard error and report to the system logger.
     *
     *  @return `false` if nothing was logged.
     */
    bool log(int logLevel, char const *string) const;

    /** Print a log message to standard error and report to the system logger
     *  using `printf` format strings.
     *
     *  @return `false` if nothing was logged.
     */
    template <typename... Arguments>
    bool log(
      int logLevel,
      char const *format,
      Arguments const &... arguments
    ) const {
      return this->log(logLevel, String::format(format, arguments...).data());
    }

    /** Make a best effort to print a log message to standard error and report
     *  to the system logger, and catch (and trigger an assertion failure on)
     *  all exceptions.
     *
     *  @return `false` if nothing was logged.
     */
    bool tryToLog(int logLevel, char const *string) const noexcept;

    /** Make a best effort to print a log message to standard error and report
     *  to the system logger using `printf` format strings, and catch (and
     *  trigger an assertion failure on) all exceptions.
     *
     *  @return `false` if nothing was logged.
     */
    template <typename... Arguments>
    bool tryToLog(
      int logLevel,
      char const *format,
      Arguments const &... arguments
    ) const noexcept {
      return this->tryToLog(
        logLevel, String::format(format, arguments...).data()
      );
    }

    /// Deinitialize the logger.
    ~Logger();

  private:

    /** Initialize the logger.
     *
     *  @param  name
     *          The name to use for the system logger; cannot be empty.
     *  @param  maxLogLevel
     *          The maximum log level to print to standard error.
     *  @param  maxSystemLogLevel
     *          The maximum log level to report to the system logger.
     *  @throw  std::invalid_argument
     *          The name was empty.
     */
    explicit Logger(
      std::string name,
      int maxLogLevel = defaultMaxLogLevel,
      int maxSystemLogLevel = defaultMaxSystemLogLevel
    );

    /** Report a log message to the system logger.
     *
     *  @return `false` if nothing was reported.
     */
    bool report(int logLevel, char const *string) const;

    /// The name associated with the system logger.
    std::string const name;

  #if BOOST_OS_WINDOWS

    /// A handle to the event log.
    Handle<HANDLE, nullptr> const eventLog;

  #endif

    /// The maximum log level for logging to standard error.
    int const maxLogLevel;

    /// The maximum log level for logging to the system logger.
    std::atomic<int> maxSystemLogLevel;

  };

  /** Print log message to standard error and report to the system logger using
   *  `printf` format strings.
   *
   *  @return `false` if nothing was logged.
   */
  template <typename... Arguments>
  bool log(
    int logLevel,
    char const *format,
    Arguments const &... arguments
  ) {
    return Logger::getInstance().log(logLevel, format, arguments...);
  }

  /** Make a best effort to print a log message to standard error and report to
   *  the system logger using `printf` format strings, and catch (and trigger an
   *  assertion failure on) all exceptions.
   *
   *  @return `false` if nothing was logged.
   */
  template <typename... Arguments>
  bool tryToLog(
    int logLevel,
    char const *format,
    Arguments const &... arguments
  ) noexcept {
    return Logger::getInstance().tryToLog(logLevel, format, arguments...);
  }

}

#endif
