/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       April 2020
 */

#if !defined(DCP_Registry_)
  #define DCP_Registry_

  #if BOOST_OS_WINDOWS

    #include <dcp/handle.hh>

    #include <windows.h>

    #include <string>
    #include <system_error>

namespace DCP::Registry {

  struct Iterator {

    explicit Iterator(HKEY key) noexcept :
    key(key)
    {}

    size_t getCount() const noexcept {
      return count;
    }

    virtual std::string getNext() = 0;

    virtual ~Iterator() = default;

  protected:

    HKEY key;
    DWORD index{};
    DWORD count{};
    DWORD maxSize{};
    std::string buffer{};

  };

  struct NameIterator : Iterator {

    explicit NameIterator(HKEY key);

    std::string getNext() override;

  };

  struct SubkeyIterator : Iterator {

    explicit SubkeyIterator(HKEY key);

    std::string getNext() override;

  };

  using Key = Handle<const HKEY, static_cast<HKEY>(nullptr)>;

  /// Return true if the key exists.
  bool keyExists(HKEY key, char const *subkey = "") noexcept;

  /// Rename the registry key tree, or throw if unsuccessful.
  void renameKey(char const *newName, HKEY key, char const *subkey = "");

  /// Open and return the registry key, or the invalid key if unsuccessful.
  /// If `write` is true, open with write permissions and create if not found.
  std::pair<Key, std::error_code> tryToGetKey(
    HKEY key,
    char const *subkey = "",
    bool writable = false
  ) noexcept;

  /// Remove the key and all keys/values below it.
  bool removeKey(HKEY key, char const *subkey = "");

  /// Return true if the value exists.
  bool valueExists(
    HKEY key,
    char const *subkey = "",
    char const *name = ""
  ) noexcept;

  /// Set the string value, or throw if unsuccessful. Assumes subkey exists.
  void setStringValue(
    char const *string,
    HKEY key,
    char const *subkey = "",
    char const *name = ""
  );

  /// Get the string value, or the empty string if unsuccessful.
  /// This will throw in the event of a buffer allocation error.
  std::pair<std::string, std::error_code> tryToGetStringValue(
    HKEY key,
    char const *subkey = "",
    char const *name = ""
  );

  /// Remove the value.
  bool removeValue(HKEY key, char const *subkey = "", char const *name = "");

}

  #endif
#endif
