/**
 *  @file
 *  @author     Wes Garland, wes@sparc.network
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2018
 */

#if !defined(DCP_Signal_)
  #define DCP_Signal_

  #include <dcp/logger.hh>

  #include <boost/optional.hpp>

  #include <array>
  #include <map>

namespace DCP::Signal {

  /** A signal handler that sets a callback for specific signal numbers, and
   *  restores original callbacks for those signal numbers on destruction.
   */
  struct Handler final {

    using Callback = void (*)(int);

    template <typename... Numbers>
    explicit Handler(Callback callback, Numbers... numbers) {
      try {
        std::array<int, sizeof...(numbers)> numberArray = {numbers...};
        for (auto number: numberArray) {
          if (auto oldCallback = registerCallback(number, callback)) {
            original[number] = *oldCallback;
          }
        }
        log(LogLevel::debug, "Signal Handler: constructed");
      } catch (...) {
        deinitialize();
        throw;
      }
    }

    Handler(Handler const &) = delete;
    Handler(Handler const &&) = delete;

    Handler &operator =(Handler const &) = delete;
    Handler &operator =(Handler const &&) = delete;

    ~Handler();

  private:

    static boost::optional<Callback> registerCallback(
      int number,
      Callback callback
    );

    void deinitialize();

    std::map<int, Callback> original{};

  };

  #if !BOOST_OS_WINDOWS

  bool send(pid_t process, int signal);

  #endif

}

#endif
