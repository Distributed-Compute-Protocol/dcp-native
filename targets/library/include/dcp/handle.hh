/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2020
 */

#if !defined(DCP_Handle_)
  #define DCP_Handle_

  #include <cstdint>
  #include <memory>
  #include <utility>

namespace DCP {

  /// A handle value type that can clean up after itself via reference count.
  template <typename Value, Value invalid>
  struct Handle final {

    /// Construct an invalid handle.
    Handle() :
    Handle(invalid)
    {}

    Handle(Handle const &) = default;

    /// Move the handle and reset it to 'invalid'.
    Handle([[maybe_unused]] Handle &&handle) noexcept :
    sp{
  #if !defined(__clang_analyzer__) // Suppress analyzer: false positive leak
      std::exchange(handle.sp,
        std::shared_ptr<void>(toPointer(invalid), [](void *) {})
      )
  #endif
    }
    {}

    /** Construct a handle to an unowned value, doing nothing when the last
     *  handle goes out of scope.
     */
    explicit Handle(Value value) :
    Handle(value, [](Value) {})
    {}

    /** Construct a handle that performs a destruction operation on the
     *  underlying value when the last handle goes out of scope.
     *
     *  @param  value
     *          The value to manage.
     *  @param  destroy
     *          A callable object that takes the value and performs a
     *          destruction operation on it.
     */
    template <typename Destroy>
    explicit Handle(Value value, Destroy destroy) :
    sp(
      toPointer(value),
      [destroy](void *p) {
        Value const v = toValue(p);
        if (v != invalid) {
          destroy(v);
        }
      }
    )
    {}

    Handle &operator =(Handle const &) = default;

    /// Move the handle and reset it to 'invalid'.
    Handle &operator =(Handle &&handle) noexcept {
      if (this != &handle) {
  #if !defined(__clang_analyzer__) // Suppress analyzer: false positive leak
        sp = std::exchange(handle.sp,
          std::shared_ptr<void>(toPointer(invalid), [](void *) {})
        );
  #endif
      }
      return *this;
    }

    // NOLINTNEXTLINE(hicpp-explicit-conversions)
    operator Value() const noexcept {
      return toValue(sp.get());
    }

    ~Handle() = default;

  private:

    static_assert(sizeof(Value) <= sizeof(void *),
      "The Value type cannot be larger than a void * in order to be stored"
      " inside a std::shared_ptr for reference-counted resource management."
    );

    static void *toPointer(Value value) noexcept {
  #if BOOST_OS_UNIX || BOOST_OS_MACOS
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wold-style-cast"
  #endif
      // NOLINTNEXTLINE(performance-no-int-to-ptr)
      return reinterpret_cast<void *>((uintptr_t)value);
  #if BOOST_OS_UNIX || BOOST_OS_MACOS
    #pragma GCC diagnostic pop
  #endif
    }

    static Value toValue(void *pointer) noexcept {
  #if BOOST_OS_UNIX || BOOST_OS_MACOS
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wold-style-cast"
  #endif
      return (Value)(reinterpret_cast<uintptr_t>(pointer));
  #if BOOST_OS_UNIX || BOOST_OS_MACOS
    #pragma GCC diagnostic pop
  #endif
    }

    std::shared_ptr<void> sp;

  };

}

#endif
