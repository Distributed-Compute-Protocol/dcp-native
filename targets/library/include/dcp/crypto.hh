/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       May 2021
 */

#if !defined(DCP_Crypto_)
  #define DCP_Crypto_

  #if BOOST_OS_WINDOWS

    #include <string>

namespace DCP::Crypto {

  /** Encrypt a string using CryptProtectData().
   *
   *  @param    string
   *            A non-empty string to encrypt.
   *  @return   The hexadecimal encrypted string.
   */
  std::string encrypt(std::string string);

  /** Decrypt a string via CryptUnprotectData().
   *
   *  @param    encrypted
   *            A string encrypted by a call to encrypt().
   *  @return   The decrypted, non-empty string.
   */
  std::string decrypt(std::string const &encrypted);

}

  #endif
#endif
