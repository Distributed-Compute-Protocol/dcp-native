/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2021
 */

#if !defined(DCP_Window_StaticControl_)
  #define DCP_Window_StaticControl_

  #if BOOST_OS_WINDOWS

    #include <windows.h>

namespace DCP::Window::StaticControl {

  /** Set the bitmap for the static control and return a handle to the resulting
   *  bitmap owned by the control.
   *  Pass `nullptr` to destroy the bitmap in the control.
   *
   *  To understand the design of this function, see:
   *  https://devblogs.microsoft.com/oldnewthing/20140219-00/?p=1713
   */
  HBITMAP setBitmap(HWND control, HBITMAP bitmap = nullptr) noexcept;

}

  #endif
#endif
