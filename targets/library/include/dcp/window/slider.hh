/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2021
 */

#if !defined(DCP_Window_Slider_)
  #define DCP_Window_Slider_

  #if BOOST_OS_WINDOWS

    #include <windows.h>

    #include <cstdint>

namespace DCP::Window {

  struct Slider final {

    explicit Slider(HWND control, uint16_t low, uint16_t high);

    operator HWND() const {
      return control;
    }

    void setPosition(uint16_t position);

    uint16_t getPosition() const;

  private:

    HWND const control;
    uint16_t const low;
    uint16_t const high;

  };

}

  #endif
#endif
