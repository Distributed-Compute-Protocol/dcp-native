/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2021
 */

#if !defined(DCP_Window_Link_)
  #define DCP_Window_Link_

  #if BOOST_OS_WINDOWS

    #include <windows.h>

namespace DCP::Window::Link {

  bool handleMessage(UINT message, WPARAM wParam, LPARAM lParam);

}

  #endif
#endif
