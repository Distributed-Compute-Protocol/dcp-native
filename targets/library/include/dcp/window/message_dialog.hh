/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       February 2021
 */

#if !defined(DCP_Window_MessageDialog_)
  #define DCP_Window_MessageDialog_

  #if BOOST_OS_WINDOWS

    #include <windows.h>

    #include <string>

namespace DCP::Window {

  struct MessageDialog {

    MessageDialog(UINT const type) :
    type(type)
    {}

    virtual int show(
      HWND parent,
      char const *caption,
      char const *message
    ) const;

    virtual ~MessageDialog();

  protected:

    UINT const type;

  };

}

  #endif
#endif
