/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2023
 */

#if !defined(DCP_Window_ComboBox_)
  #define DCP_Window_ComboBox_

  #if BOOST_OS_WINDOWS

    #include <windows.h>

    #include <cstdint>
    #include <stdexcept>

namespace DCP::Window {

  struct ComboBox final {

    template <typename Strings>
    explicit ComboBox(HWND const control, Strings const &strings) :
    control(control)
    {
      for (auto const &string: strings) {
        auto const result = SendMessage(
          control, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)string
        );
        if (result == CB_ERRSPACE) {
          throw std::runtime_error("Insufficient space to add list element.");
        }
        if (result == CB_ERR) {
          throw std::runtime_error("Error adding list element.");
        }
      }
    }

    operator HWND() const {
      return control;
    }

    void setSelection(size_t const index) {
      assert(index <= (WPARAM)-1);
      auto const result = SendMessage(
        control, CB_SETCURSEL, (WPARAM)index, (LPARAM)0
      );
      if (result == CB_ERR) {
        throw std::runtime_error("Error setting list selection.");
      }
    }

    size_t getSelection() const {
      auto const result = SendMessage(
        control, CB_GETCURSEL, (WPARAM)0, (LPARAM)0
      );
      if (result == CB_ERR) {
        throw std::runtime_error("Error getting list selection.");
      }
      assert(result <= SIZE_MAX);
      return (size_t)result;
    }

  private:

    HWND const control;

  };

}

  #endif
#endif
