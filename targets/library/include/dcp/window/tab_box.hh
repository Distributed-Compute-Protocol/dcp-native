/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2021
 */

#if !defined(DCP_Window_TabBox_)
  #define DCP_Window_TabBox_

  #if BOOST_OS_WINDOWS

    #include <dcp/window.hh>

    #include <windows.h>

    #include <string>
    #include <vector>

namespace DCP::Window {

  /// Wraps Windows tab control functionality.
  struct TabBox final {

    explicit TabBox(HWND control);

    operator HWND() const {
      return control;
    }

    size_t getTabCount() const;

    /**
     *  @param  id
     *          The ID of the child dialog, in the current executable, to use.
     *  @param  name
     *          The label for the tab; cannot be empty.
     *  @param  callback
     *          The function called to handle messages for the tab.
     */
    void appendTab(UINT id, std::string name, DLGPROC callback);

    /** @param  index
     *          The 0-based index of the tab to select; must be less than
     *          the value returned by getTabCount().
     */
    void selectTab(size_t index);

    /** @param  index
     *          The 0-based index of the tab to get; must be less than
     *          the value returned by getTabCount().
     */
    HWND getTab(size_t index) const;

    /// Respond to window messages.
    bool handleMessage(UINT message, WPARAM wParam, LPARAM lParam);

  private:

    void updateSelection();

    HWND control{};

    HWND parent{};

    std::vector<HWND> tabs{};

  };

}

  #endif
#endif
