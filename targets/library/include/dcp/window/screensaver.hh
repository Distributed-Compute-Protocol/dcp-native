/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#if !defined(DCP_Window_Screensaver_)
  #define DCP_Window_Screensaver_

  #if BOOST_OS_WINDOWS

    #include <dcp/color.hh>
    #include <dcp/handle.hh>
    #include <dcp/time.hh>

    #include <windows.h>

namespace DCP::Window {

  /// A screensaver implementation that paints a moving bitmap in a window.
  struct Screensaver final {

    Screensaver(
      HWND window,
      Color::Value backgroundColor,
      Handle<HBITMAP, nullptr> const &bitmap,
      Time::Milliseconds timeIncrement,
      double angleIncrement
    );

    Screensaver(Screensaver const &) = delete;
    Screensaver(Screensaver const &&) = delete;

    Screensaver &operator =(Screensaver const &) = delete;
    Screensaver &operator =(Screensaver const &&) = delete;

    /// Clear the window.
    void clear();

    /// Advance the screensaver.
    void update();

    /// Respond to window messages.
    bool handleMessage(UINT message, WPARAM wParam, LPARAM lParam);

    ~Screensaver();

  private:

    double const angleIncrement = 0.0;
    double angle = 0.0;

    Handle<UINT_PTR, 0> const windowTimer;
    RECT const windowClientRect;
    Handle<HDC, nullptr> const windowDC;
    Handle<HBITMAP, nullptr> const bitmap;
    Handle<HDC, nullptr> const sourceDC;
    Handle<HGDIOBJ, nullptr> const oldSourceObject;
    SIZE const bitmapSourceSize = {0, 0};
    SIZE const bitmapSize = {0, 0};
    RECT bitmapRect;
    Handle<HBRUSH, nullptr> const background;

  };

}

  #endif
#endif
