/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       February 2021
 */

#if !defined(DCP_Window_DismissableMessageDialog_)
  #define DCP_Window_DismissableMessageDialog_

  #if BOOST_OS_WINDOWS

    #include <dcp/registry.hh>
    #include <dcp/window/message_dialog.hh>

namespace DCP::Window {

  struct DismissableMessageDialog final : MessageDialog {

    template <typename... Arguments>
    explicit DismissableMessageDialog(
      std::string name,
      UINT const defaultResult,
      Arguments &&... arguments
    ) :
    MessageDialog(std::forward<Arguments>(arguments)...),
    name(std::move(name)),
    defaultResult(defaultResult)
    {}

    int show(
      HWND parent,
      char const *caption,
      char const *message
    ) const override;

    void setDismissed(bool dismissed = true);

    bool getDismissed() const;

  private:

    static HKEY const key;
    static char const *const subkey;
    static char const *const string;

    std::string const name;
    UINT const defaultResult;

  };

}

  #endif
#endif
