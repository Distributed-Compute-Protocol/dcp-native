/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       December 2019
 */

#if !defined(DCP_FileReader_)
  #define DCP_FileReader_

  #include <boost/filesystem/path.hpp>

  #include <memory>

namespace DCP {

  /** A view on a file that exposes contents as a byte array.
   *
   *  The file is memory mapped, during which time a shareable lock is applied
   *  via `flock(...LOCK_SH...)` on Unix and `CreateFile(...FILE_SHARE_READ...)`
   *  on Windows.
   */
  struct FileReader final {

    FileReader(FileReader const &) = delete;
    FileReader(FileReader const &&) = delete;

    /** Construct the file reader.
     *
     *  @throw  std::system_error
     *          There was an error opening, inspecting, or memory mapping the
     *          file.
     */
    explicit FileReader(boost::filesystem::path const &filePath);

    FileReader &operator =(FileReader const &) = delete;
    FileReader &operator =(FileReader const &&) = delete;

    /**
     *  @return A pointer to the bytes contained in the file.
     */
    [[nodiscard]]
    char const *getData() const;

    /**
     *  @return The number of bytes contained in the file.
     */
    [[nodiscard]]
    size_t getSize() const;

    ~FileReader();

  private:

    struct Implementation;

    std::unique_ptr<Implementation> implementation{};

  };

}

#endif
