/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       July 2020
 */

#if !defined(DCP_Version_)
  #define DCP_Version_

  #include <ostream>

namespace DCP::Version {

  /// Print a version message to output.
  template <typename Output, typename Name>
  void print(Output &output, Name const &name) {
    output << name << " " << DCP_version << std::endl;
    output << "Copyright (c) 2018 Distributive Corp.";
    output << " All Rights Reserved.";
    output << std::endl;
  }

}

#endif
