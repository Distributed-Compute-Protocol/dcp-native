/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2020
 */

#if !defined(DCP_WriteBuffer_)
  #define DCP_WriteBuffer_

  #include <dcp/socket.hh>

  #include <functional>
  #include <queue>
  #include <string>

namespace DCP {

  /// A simple write buffer.
  struct WriteBuffer final {

    /// The callback function type for @ref pop that performs the write.
    using Write = std::function<void (char const *memory, size_t &size)>;

    WriteBuffer() = default;

    WriteBuffer(WriteBuffer const &) = delete;
    WriteBuffer(WriteBuffer const &&) = delete;

    WriteBuffer &operator =(WriteBuffer const &) = delete;
    WriteBuffer &operator =(WriteBuffer const &&) = delete;

    /** Pop bytes from the buffer via the `write` function.
     *
     *  @param  write
     *          A callback that writes from memory provided by the buffer, and
     *          updates the `size` argument to the number of bytes written.
     *  @note   Do not retain or free the pointer passed to the callback:
     *          it points to memory managed by the buffer and may be invalidated
     *          by subsequent @ref pop calls.
     *  @note   Any exception thrown by the `write` function will be propagated.
     */
    void pop(Write const &write);

    /** Push a string into the buffer.
     *
     *  @param  string
     *          A string to be pushed into the buffer.
     */
    void push(std::string &&string);

    /// Return whether the buffer is empty.
    [[nodiscard]]
    bool isEmpty() const;

    /** Perform a buffered write to the socket using this write buffer.
     *
     *  @param  socket
     *          The socket to write on.
     *  @param  string
     *          The string to write.
     *  @param  timeout
     *          The maximum time to block, in milliseconds.
     */
    void send(
      SOCKET socket,
      char const *string,
      Time::Milliseconds timeout = Socket::defaultTimeout
    );

    ~WriteBuffer();

  private:

    /// The strings in the buffer.
    std::queue<std::string> queue{};

    /// The offset into the first string.
    size_t offset = 0;

  };

}

#endif
