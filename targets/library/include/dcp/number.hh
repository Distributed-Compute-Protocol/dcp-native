/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#if !defined(DCP_Number_)
  #define DCP_Number_

  #include <boost/lexical_cast.hpp>
  #include <boost/numeric/conversion/cast.hpp>

namespace DCP::Number {

  /// Cast a number to another numeric type, clamping to the range.
  template <typename To, typename From>
  To clamp(From from) {
    try {
      return boost::numeric_cast<To>(from);
    } catch (boost::numeric::negative_overflow const &) {
      return std::numeric_limits<To>::lowest();
    } catch (boost::numeric::positive_overflow const &) {
      return std::numeric_limits<To>::max();
    }
  }

  /// Try to parse an integer from the string, clamping to the integer range.
  template <typename Integer>
  bool tryToParseInteger(std::string const &string, Integer &integer) {
    // Attempt signed cast first to prevent negative wrap-around to unsigned.
    if (
      intmax_t signedInteger = 0;
      boost::conversion::try_lexical_convert(string, signedInteger)
    ) {
      integer = clamp<Integer>(signedInteger);
      return true;
    }
    if (
      uintmax_t unsignedInteger = 0;
      boost::conversion::try_lexical_convert(string, unsignedInteger)
    ) {
      integer = clamp<Integer>(unsignedInteger);
      return true;
    }
    return false;
  }

}

#endif
