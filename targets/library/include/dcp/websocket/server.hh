/**
 *  @file
 *  @author     Severn Lortie, severn@distributive.network
 *  @date       June 2024
 */

#if !defined(DCP_WebsocketServer_)
  #define DCP_WebsocketServer_

  #include "connection.hh"
  #include <boost/asio.hpp>
  #include <boost/beast/core/flat_buffer.hpp>
  #include <boost/beast/core/multi_buffer.hpp>
  #include <boost/beast/websocket.hpp>
  #include <boost/optional.hpp>

  #include <queue>

namespace DCP::Websocket {

  struct Server final {

    /// Construct a Websocket Server.
    explicit Server(short unsigned port);

    Server(Server const &) = delete;
    Server(Server const &&) = delete;

    Server &operator =(Server const &) = delete;
    Server &operator =(Server const &&) = delete;

    /** Set the disconnect callback to the provided function.
     *
     *  @param  callback
     *          The disconnect callback.
     */
    void setDisconnectCallback(std::function<void ()> callback);

    /** Set the read callback to the provided function.
     *
     *  @param  callback
     *          The read callback.
     */
    void setReadCallback(
      std::function<void (boost::beast::flat_buffer &)> callback
    );

    /// Accept a new connection to on the port.
    void acceptConnection();

    /** Send a message to the peer.
     *
     *  @param  message
     *          The message string.
     */
    void send(std::string message);

    [[nodiscard]]
    short unsigned getPort() const;

    /** Get the active socket. Either the acceptor socket, the websocket, or
     *  none (-1).
     *
     *  @return The socket handle.
     */
    [[nodiscard]]
    boost::asio::detail::socket_type getSocket();

    /// Poll the WebsocketServer's event loop.
    void pollEventLoop();

    ~Server() = default;

  private:

    /// Handle the peer disconnecting from the server.
    void handleDisconnect();

    /// Continually service the message at the front of the sendQueue.
    void sendLoop();

    short unsigned port{};
    std::function<void ()> disconnectCallback{};
    std::function<void (boost::beast::flat_buffer &)> readCallback{};
    std::queue<std::string> sendQueue{};
    bool sendLoopActive{false};
    boost::asio::io_context ioContext{};
    boost::asio::ip::tcp::acceptor acceptor;
    boost::optional<boost::asio::ip::tcp::socket> socket{};
    boost::shared_ptr<Connection> connection{};
  };

}

#endif
