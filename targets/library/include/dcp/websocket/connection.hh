/**
 *  @file
 *  @author     Severn Lortie, severn@distributive.network
 *  @date       June 2024
 */

#if !defined(DCP_WebsocketConnection_)
  #define DCP_WebsocketConnection_

  #include <boost/asio.hpp>
  #include <boost/beast/core/flat_buffer.hpp>
  #include <boost/beast/core/multi_buffer.hpp>
  #include <boost/beast/websocket.hpp>
  #include <boost/enable_shared_from_this.hpp>

  #include <queue>

namespace DCP::Websocket {

  struct Connection final : boost::enable_shared_from_this<Connection> {

    /** Construct a Websocket Connection.
     *
     *  @param  websocket
     *          The websocket the Connection is using.
     */
    explicit Connection(
      boost::beast::websocket::stream<boost::asio::ip::tcp::socket> &&websocket
    );

    Connection(Connection const &) = delete;
    Connection(Connection const &&) = delete;

    Connection &operator =(Connection const &) = delete;
    Connection &operator =(Connection const &&) = delete;

    /** Set the read callback to the provided function.
     *
     *  @param  callback
     *          The read callback.
     */
    void setReadCallback(
      std::function<void (boost::beast::flat_buffer &)> callback
    );

    /** Set the disconnect callback to the provided function.
     *
     *  @param  callback
     *          The disconnect callback.
     */
    void setDisconnectCallback(std::function<void ()> callback);

    /** Get a handle to the underlying socket in use.
     *
     *  @return The socket handle.
     */
    [[nodiscard]]
    boost::asio::detail::socket_type getSocket();

    /** This function will send the message provided.
     *
     *  @param  message
     *          The message to send.
     *  @param  callback
     *          The callback to invoke once sen82ding is complete.
     */
    void send(std::string const &message, std::function<void ()> callback);

    ~Connection() = default;

  private:

    /** Once called, this function will ensure there is always a read task
     *  queued. All results of reading are sent to the read callback registered
     *  with setReadCallback().
     */
    void readLoop();

    /** Handle an error encountered during an async operation.
     *
     *  @param  error
     *          The error code that was thrown.
     */
    void handleError(boost::beast::error_code error);

    boost::beast::flat_buffer readBuffer{};
    boost::beast::multi_buffer sendBuffer{};
    std::function<void (boost::beast::flat_buffer &)> readCallback{};
    std::function<void ()> disconnectCallback{};
    boost::beast::websocket::stream<boost::asio::ip::tcp::socket> websocket;
  };

}

#endif
