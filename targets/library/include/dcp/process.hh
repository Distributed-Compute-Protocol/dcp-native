/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       December 2021
 */

#if !defined(DCP_Process_)
  #define DCP_Process_

  #include <dcp/handle.hh>
  #include <dcp/time.hh>

  #include <boost/filesystem/path.hpp>
  #include <boost/optional.hpp>

  #if BOOST_OS_WINDOWS
    #include <windows.h> // Include before other windows headers
  #endif

namespace DCP {

  /// A potentially long-running process that may have child processes.
  struct Process final {

  #if BOOST_OS_WINDOWS
    using ID = DWORD;
  #else
    using ID = pid_t;
  #endif

  #if BOOST_OS_WINDOWS
    struct IO {
      explicit IO(
        HANDLE const error = GetStdHandle(STD_ERROR_HANDLE),
        HANDLE const output = GetStdHandle(STD_OUTPUT_HANDLE),
        HANDLE const input = GetStdHandle(STD_INPUT_HANDLE)
      ) :
      input{input},
      output{output},
      error{error}
      {
        assert(input != INVALID_HANDLE_VALUE);
        assert(output != INVALID_HANDLE_VALUE);
        assert(error != INVALID_HANDLE_VALUE);
      }

      HANDLE input;
      HANDLE output;
      HANDLE error;
    };

    /// Execute a shell command asynchronously.
    static void command(
      char const *command,
      std::vector<std::string> const &arguments,
      bool administrate,
      bool show
    );

    /// Execute a shell command synchronously.
    [[nodiscard]]
    static long unsigned command(
      char const *command,
      std::vector<std::string> const &arguments,
      boost::optional<Time::Milliseconds> timeout,
      bool administrate,
      bool show
    );

    /// Execute an executable synchronously and get standard output and error.
    [[nodiscard]]
    static long unsigned execute(
      boost::filesystem::path const &executablePath,
      std::vector<std::string> const &arguments,
      std::string &output,
      std::string &error
    );

    /** Execute an executable synchronously, get standard output, and log
     *  error output.
     *
     *  @throw  std::runtime_error
     *          The exit code was non-zero.
     */
    static std::string execute(
      boost::filesystem::path const &executablePath,
      std::vector<std::string> const &arguments
    );

    /// Create a job.
    static Handle<HANDLE, nullptr> createJob();

    /// Create an event that can be passed to a child process.
    static Handle<HANDLE, nullptr> createEvent();
  #endif

    /// Start a process.
    explicit Process(
      /// The executable file path.
      boost::filesystem::path const &executablePath,
      /// Arguments to be passed to the executable.
      /// Do not include the executable path as the 0th argument.
      std::vector<std::string> const &arguments,
  #if BOOST_OS_WINDOWS
      /// An optional handle of a job event to attach the process to.
      Handle<HANDLE, nullptr> jobHandle = {},
      /// An optional handle of a start event fired by the process.
      Handle<HANDLE, nullptr> startEventHandle = {},
      /// Additional handles for the process to inherit.
      std::vector<HANDLE> additionalHandles = {},
      /// Optional standard IO handles to run the process in console mode.
      IO const *io = nullptr
  #else
      /// A null-terminated array of strings setting environment variables.
      char *const *environment,
      /// A callback to be called inside the child process between `fork` and
      /// `exec`; returns true if `exec` should proceed.
      /// Because this is invoked after `fork`, this function should be careful
      /// to not rely on assumptions that may not hold for the child process.
      /// For example, only the calling thread will exist in the child process,
      /// meaning that any locks from other threads may not have been released.
      std::function<bool ()> const &childCallback = {
        []() {
          return true;
        }
      }
  #endif
    );

    Process(Process const &) = delete;

    Process(Process &&process) noexcept :
    id{std::exchange(process.id, {})},
  #if BOOST_OS_WINDOWS
    threadID{std::exchange(process.threadID, {})},
    handle{std::move(process.handle)},
    threadHandle{std::move(process.threadHandle)},
    jobHandle{std::move(process.jobHandle)},
    startEventHandle{std::move(process.startEventHandle)},
    started{std::exchange(process.started, {})},
  #endif
    attached{std::exchange(process.attached, {})}
    {}

    Process &operator =(Process const &) = delete;

    Process &operator =(Process &&process) noexcept {
      if (this != &process) {
        id = std::exchange(process.id, {});
  #if BOOST_OS_WINDOWS
        threadID = std::exchange(process.threadID, {});
        handle = std::move(process.handle);
        threadHandle = std::move(process.threadHandle);
        jobHandle = std::move(process.jobHandle);
        startEventHandle = std::move(process.startEventHandle);
        started = std::exchange(process.started, {});
  #endif
        attached = std::exchange(process.attached, {});
      }
      return *this;
    }

    [[nodiscard]]
    ID getID() const noexcept {
      return id;
    }

  #if BOOST_OS_WINDOWS
    [[nodiscard]]
    boost::optional<long unsigned> getExitCode() const;

    /**
     *  @return `true` if either the start event is null or was fired, or
     *          `false` if the process already exited.
     */
    bool waitToStart() noexcept;
  #endif

    [[nodiscard]]
    bool isRunning() const noexcept;

    /**
     *  @return `true` if wait completed.
     */
    bool wait(boost::optional<Time::Milliseconds> timeout = {}) const noexcept;

    void interrupt();

    void terminate();

    void kill();

    void detach();

    /// Block until the process has finished.
    ~Process();

  private:

    /// The process ID.
    ID id{};

  #if BOOST_OS_WINDOWS
    /// The process thread ID.
    DWORD threadID{};

    /// The process handle.
    Handle<HANDLE, nullptr> handle{};

    /// The process thread handle.
    Handle<HANDLE, nullptr> threadHandle{};

    /// The process job handle.
    Handle<HANDLE, nullptr> jobHandle{};

    /** The process start event handle.
     *
     *  If non-null, this is expected to be fired by the process when started.
     */
    Handle<HANDLE, nullptr> startEventHandle{};

    bool started{};
  #endif

    bool attached{};

  };

}

#endif
