/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       August 2020
 */

#if !defined(DCP_Paths_)
  #define DCP_Paths_

  #if BOOST_OS_WINDOWS

    #include <boost/filesystem/path.hpp>

    #include <shlobj.h>

namespace DCP::Paths {

  boost::filesystem::path getSpecialDirectoryPath(REFKNOWNFOLDERID id);

  /** Use the installation path from the registry, if it and the directory
   *  it describes exists; failing that, use the default directory provided.
   *  @param    defaultDirectoryPath
   *            If non-empty, this directory must exist.
   */
  boost::filesystem::path getInstallDirectoryPath(
    boost::filesystem::path const &defaultDirectoryPath = {}
  );

  /** Get the path of the current executable.
   *
   *  @note   This may be a short DOS 8.3-style path if invoked by the system.
   */
  boost::filesystem::path const &getExecutablePath();

  boost::filesystem::path getProgramDataDirectoryPath();

  boost::filesystem::path getSystemDirectoryPath();

}

  #endif
#endif
