/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       December 2019
 */

#if !defined(DCP_LogLevel_)
  #define DCP_LogLevel_

/// Log levels that correspond to syslog standard priority codes.
namespace DCP::LogLevel {

  /**
   *  @internal
   *    If adding a value, update the `static_assert` in the source file as
   *    appropriate.
   */
  // NOLINTNEXTLINE(performance-enum-size)
  enum Enumeration : int {
    emergency,   ///< System is unusable.
    alert,       ///< Action must be taken immediately.
    critical,    ///< Critical conditions.
    error,       ///< Error conditions.
    warning,     ///< Warning conditions.
    notice,      ///< Normal but significant conditions.
    info,        ///< Informational.
    debug,       ///< Debug-level messages.
    trace        ///< Trace-level messages.
  };

}

#endif
