/**
 *  @file
 *  @author     Wes Garland, wes@sparc.network
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2018
 */

#if !defined(DCP_ReadBuffer_)
  #define DCP_ReadBuffer_

  #include <dcp/socket.hh>

  #include <cstddef>
  #include <functional>

namespace DCP {

  /// A simple read buffer.
  struct ReadBuffer final {

    /// The callback function type for @ref push that performs the read.
    using Read = std::function<bool (char *memory, size_t &size)>;

    /**
     *  @throw  std::bad_alloc
     *          Initial allocation failed.
     */
    ReadBuffer();

    ReadBuffer(ReadBuffer const &) = delete;
    ReadBuffer(ReadBuffer const &&) = delete;

    ReadBuffer &operator =(ReadBuffer const &) = delete;
    ReadBuffer &operator =(ReadBuffer const &&) = delete;

    /** Return a pointer to the first line in the buffer, if any, to be popped.
     *
     *  @return A pointer to a null-terminated line to be popped from the
     *          buffer, or null if a complete line could not be popped. Does not
     *          include the newline.
     *  @note   Do not retain or free the pointer returned:
     *          it points to memory managed by the buffer and may be invalidated
     *          by subsequent @ref push calls.
     */
    char const *pop();

    /** Push bytes into the buffer via the `read` function.
     *
     *  @param  read
     *          A callback that reads to memory provided by the buffer, updates
     *          the `size` argument to the number of bytes read, and returns a
     *          boolean indicating whether further reads are possible.
     *  @return Whether more reads are possible.
     *  @throw  std::overflow_error
     *          Failed due to insufficient space in buffer.
     *  @note   Do not retain or free the pointer passed to the callback:
     *          it points to memory managed by the buffer and may be invalidated
     *          by subsequent @ref push calls.
     *  @note   Any exception thrown by the `read` function will be propagated.
     */
    bool push(Read const &read);

    /** Perform a buffered read from the socket using this read buffer.
     *
     *  @param  socket
     *          The socket to read on.
     *  @param  string
     *          The string to populate.
     *  @param  timeout
     *          The maximum time to block, in milliseconds.
     *  @return Whether more reads are possible.
     */
    bool receive(
      SOCKET socket,
      std::string &string,
      Time::Milliseconds timeout = Socket::defaultTimeout
    );

    ~ReadBuffer();

  private:

    /// The minimum size of buffer memory allocations.
    static size_t const minAllocationSz;
    /// The factor to apply to the allocation for each reallocation.
    static size_t const powAllocationSz;
    /// The minimum available memory for a line.
    static size_t const minAvailableSz;
    /// The maximum number of wasted bytes at the start of the buffer.
    static size_t const maxWasteSz;

    /// The amount of memory allocated.
    size_t memSz = 0;
    /// The beginning of the memory allocation.
    char *mem = nullptr;
    /// The start of the current line we are reading.
    char *ln = nullptr;
    /// The pointer to the next byte to be read within a line.
    char *p = nullptr;

  };

}

#endif
