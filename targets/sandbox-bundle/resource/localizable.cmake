# Copyright (c) 2024 Distributive Corp. All Rights Reserved.

set(DCP_SANDBOX_BUNDLE_DISPLAY_ERROR "Please re-install the application.")
set(DCP_SANDBOX_BUNDLE_DISPLAY_ERROR_UNKNOWN
  "There was an error running DCP Worker"
)

set(DCP_SANDBOX_BUNDLE_DISPLAY_NAME "DCP Work")

set(DCP_SANDBOX_BUNDLE_DISPLAY_OK "OK")

set(DCP_SANDBOX_BUNDLE_DISPLAY_RUNNING "Doing DCP Work...")

set(DCP_SANDBOX_BUNDLE_DISPLAY_STOP "Stop")
