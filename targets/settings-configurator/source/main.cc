/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       July 2020
 */

#if BOOST_OS_WINDOWS

  #include <dcp/argument.hh>
  #include <dcp/logger.hh>
  #include <dcp/number.hh>
  #include <dcp/paths.hh>
  #include <dcp/process.hh>
  #include <dcp/registry.hh>
  #include <dcp/service.hh>
  #include <dcp/settings.hh>
  #include <dcp/settings/configurator.hh>
  #include <dcp/version.hh>

  #include <boost/exception/diagnostic_information.hpp>
  #include <boost/filesystem/operations.hpp>
  #include <boost/format.hpp>
  #include <boost/program_options.hpp>

  #include <shellapi.h>

  #include <cinttypes>
  #include <cstdlib>
  #include <iostream>

using namespace DCP::Settings::Configurator;

namespace boost::program_options {
  namespace {

    template <typename T, typename charT = char>
    struct counted_typed_value final : typed_value<T, charT> {

      explicit counted_typed_value(int unsigned const count) :
      typed_value<T, charT>(nullptr),
      count(count)
      {
        if (count > 1) {
          this->multitoken();
        }
      }

      int unsigned min_tokens() const override {
        return count;
      }

      int unsigned max_tokens() const override {
        return count;
      }

    private:

      int unsigned count;

    };

    template <typename T, typename charT = char>
    counted_typed_value<T, charT> *counted_value(int unsigned const count) {
      return new counted_typed_value<T, charT>(count);
    }

  }
}

namespace {

  /** Reflect registry changes immediately for the current user.
   *
   *  Some registry changes are not automatically reflected. The only supported
   *  way to reflect these changes is to read the changed registry value and
   *  then pass it to `SystemParametersInfo()` with the appropriate 'set'
   *  action. This approach covers registry changes that have been made already,
   *  such as via the installer.
   *
   *  @return `true` if updated or not found, or `false` on error (logged).
   */
  bool updateSystemParameter(
    DCP::Registry::Key const &key,
    char const *const name,
    UINT const action
  ) {
    assert(key != DCP::Registry::Key{});

    auto const &[string, error] = DCP::Registry::tryToGetStringValue(
      key, "", name
    );
    if (string.empty()) {
      DCP::log(DCP::LogLevel::warning,
        "No string \"%s\" read from registry (\"%s\"); skipping",
        name, error.message().data()
      );
      return true;
    }

    UINT value{};
    if (!DCP::Number::tryToParseInteger(string, value)) {
      DCP::log(DCP::LogLevel::error,
        "The registry string \"%s\" could not be parsed as an integer.",
        string.data()
      );
      return false;
    }

    SetLastError(ERROR_SUCCESS);
    if (!SystemParametersInfo(action, value, NULL, 0)) {
      DCP::log(DCP::LogLevel::error,
        "SystemParametersInfo() failed for action %u and value %u: %s",
        action, value, std::error_code(
          GetLastError(), std::system_category()
        ).message().data()
      );
      return false;
    }

    return true;
  }

}

int main(int argc, char *const argv[]) noexcept {
  assert(argc > 0);
  assert(argv);

  long unsigned exitCode{0};

  try {
    auto const executablePath = boost::filesystem::absolute(argv[0]);
    auto const executableFile = executablePath.filename().string();

    // Initialize argument variables.
    int maxLogLevel{DCP::Logger::defaultMaxLogLevel};
    int maxSystemLogLevel{DCP::Logger::defaultMaxSystemLogLevel};
    std::vector<std::vector<std::string>> defaultSettings{};
    std::vector<std::vector<std::string>> settingsToSet{};
    std::vector<std::vector<std::string>> settingsToUnset{};
    std::vector<std::vector<std::string>> fileSettings{};
    boost::optional<DCP::Time::Milliseconds> timeout{};
    std::string serviceName{};

    // Parse the arguments.
    std::string const &argumentsUsage{"[options]"};
    std::string const &argumentsDescription{
      "Save configuration changes and restart the service as necessary."
    };
    std::string argumentsError{};
    std::stringstream argumentsErrorHelp{};
    try {
      namespace po = boost::program_options;

      po::options_description od("Options");
      od.add_options()(
        "help,h", "Print help."
      )(
        "version,v", "Print version information."
      )(
        "debug,d", po::value(&maxLogLevel)->default_value(maxLogLevel),
        "Specify the maximum log level (corresponding to syslog priority level,"
        " or greater for verbose logging) for logging to standard error."
        " A negative number suppresses all standard error logging."
      )(
        "system-debug,s",
        po::value(&maxSystemLogLevel)->default_value(maxSystemLogLevel),
        "Specify the maximum log level (corresponding to syslog priority level,"
        " or greater for verbose logging) for logging to the system logger."
        " A negative number suppresses all system logging."
      )(
        "timeout,t", po::value(&timeout),
        "Specify the number of milliseconds to wait for the service restart."
        " If unspecified, will wait indefinitely."
      )(
        "default", po::counted_value<std::vector<std::string>>(3),
        "Default a setting, in the form:\n"
        "\"<path>\" \"<name>\" \"<string>\"\n"
        "This does not impact pre-existing values and occurs before `unset`."
      )(
        "unset", po::counted_value<std::vector<std::string>>(2),
        "Unset a setting, in the form:\n"
        "\"<path>\" \"<name>\"\n"
        "This occurs after `default` and before `set`."
      )(
        "set", po::counted_value<std::vector<std::string>>(3),
        "Set a setting, in the form:\n"
        "\"<path>\" \"<name>\" \"<string>\"\n"
        "This occurs after `unset`."
      )(
        "file", po::counted_value<std::vector<std::string>>(2),
        "Copy a file into the program data directory, in the form:\n"
        "\"<source-file-path>\" \"<program-data-subdirectory-relative-path>\"\n"
      )(
        "service", po::value(&serviceName), "Restart the specified service."
      );

      try {
        po::variables_map vm{};
        auto const p = po::command_line_parser(argc, argv).options(od).run();
        po::store(p, vm);
        po::notify(vm);

        for (po::option const &option: p.options) {
          if (option.string_key == "default") {
            assert(option.value.size() == 3);
            defaultSettings.push_back(option.value);
          } else if (option.string_key == "unset") {
            assert(option.value.size() == 2);
            settingsToUnset.push_back(option.value);
          } else if (option.string_key == "set") {
            assert(option.value.size() == 3);
            settingsToSet.push_back(option.value);
          } else if (option.string_key == "file") {
            assert(option.value.size() == 2);
            fileSettings.push_back(option.value);
          }
        }

        // Process arguments that cause the program to exit.
        if (vm.count("help")) {
          DCP::Argument::printHelp(std::cout,
            executableFile, argumentsUsage, argumentsDescription, od
          );
          return exitCode;
        }
        if (vm.count("version")) {
          DCP::Version::print(std::cout, executableFile);
          return exitCode;
        }
      } catch (po::required_option const &error) {
        argumentsError = boost::str(
          boost::format("Missing argument: %s") % error.what()
        );
        DCP::Argument::printHelp(argumentsErrorHelp,
          executableFile, argumentsUsage, argumentsDescription, od
        );
      } catch (po::error const &error) {
        argumentsError = boost::str(
          boost::format("Invalid argument: %s") % error.what()
        );
        DCP::Argument::printHelp(argumentsErrorHelp,
          executableFile, argumentsUsage, argumentsDescription, od
        );
      }
    } catch (...) {
      argumentsError = boost::str(
        boost::format("Error parsing arguments: %s")
        % boost::current_exception_diagnostic_information().data()
      );
    }

    // Initialize the logger.
    auto const &logger = DCP::Logger::getInstance(
      executablePath.stem().string(), maxLogLevel, maxSystemLogLevel
    );

    // Start try block that logs any exceptions.
    try {
      // Log the executable name and version.
      logger.log(DCP::LogLevel::info, "Running \"%s\", version %s...",
        executableFile.data(), DCP_version
      );

      // Log arguments.
      if (logger.willLog(DCP::LogLevel::debug)) {
        for (int argi{}; argi < argc; ++argi) {
          logger.log(DCP::LogLevel::debug, "Argument %i: %s", argi, argv[argi]);
        }
      }

      // Handle any argument errors.
      if (!argumentsError.empty()) {
        exitCode |= ExitCode::argumentError;
        logger.log(DCP::LogLevel::error, "%s", argumentsError.data());
        if (logger.getMaxLogLevel() < DCP::LogLevel::error) {
          std::cerr << argumentsError << std::endl;
        }
        std::cerr << argumentsErrorHelp.str();
        return exitCode;
      }

      // Copy files.
      try {
        auto const rootPath = (
          DCP::Paths::getProgramDataDirectoryPath().lexically_normal()
        );
        if (!rootPath.empty()) {
          boost::filesystem::create_directories(rootPath);
        }

        for (auto &setting: fileSettings) {
          assert(setting.size() == 2);
          auto const sourceString = setting[0];
          auto const destinationString = setting[1];

          try {
            boost::filesystem::path sourcePath{sourceString};
            boost::filesystem::path destinationPath{destinationString};

            destinationPath = boost::filesystem::absolute(
              destinationPath, rootPath
            ).lexically_normal();
            auto const pair = std::mismatch(
              rootPath.begin(), rootPath.end(), destinationPath.begin()
            );
            if (pair.first != rootPath.end()) {
              exitCode |= ExitCode::fileCopyError;
              logger.log(DCP::LogLevel::error,
                "Destination path \"%s\" not inside \"%s\"; skipping...",
                destinationPath.string().data(), rootPath.string().data()
              );
              continue;
            }

            if (sourceString.empty()) {
              logger.log(DCP::LogLevel::info,
                "No source file specified; deleting \"%s\"",
                destinationPath.string().data()
              );
              try {
                boost::filesystem::remove(destinationPath);
              } catch (...) {
                exitCode |= ExitCode::fileCopyError;
                logger.log(DCP::LogLevel::error, "Error deleting \"%s\": %s",
                  destinationPath.string().data(),
                  boost::current_exception_diagnostic_information().data()
                );
              }
              continue;
            }

            if (!boost::filesystem::exists(sourcePath)) {
              exitCode |= ExitCode::fileCopyError;
              logger.log(DCP::LogLevel::error,
                "Source file \"%s\" doesn't exist; skipping...",
                sourcePath.string().data()
              );
              continue;
            }

            if (boost::filesystem::equivalent(sourcePath, destinationPath)) {
              logger.log(DCP::LogLevel::warning,
                "Skipping copy; \"%s\" and \"%s\" refer to same file",
                sourcePath.string().data(), destinationPath.string().data()
              );
              continue;
            }

            logger.log(DCP::LogLevel::debug, "Copying file \"%s\" to \"%s\"",
              sourcePath.string().data(), destinationPath.string().data()
            );
            try {
              boost::filesystem::copy_file(
                sourcePath, destinationPath,
                boost::filesystem::copy_options::overwrite_existing
              );
            } catch (...) {
              exitCode |= ExitCode::fileCopyError;
              logger.log(DCP::LogLevel::error,
                "Error copying \"%s\" to \"%s\": %s",
                sourcePath.string().data(), destinationPath.string().data(),
                boost::current_exception_diagnostic_information().data()
              );
            }
          } catch (...) {
            exitCode |= ExitCode::fileCopyError;
            logger.log(DCP::LogLevel::error,
              "Error copying \"%s\" to \"%s\": %s",
              sourceString.data(), destinationString.data(),
              boost::current_exception_diagnostic_information().data()
            );
          }
        }
      } catch (...) {
        exitCode |= ExitCode::fileCopyError;
        logger.log(DCP::LogLevel::error,
          "Error creating destination root directory: %s",
          boost::current_exception_diagnostic_information().data()
        );
      }

      // Try to update the settings.
      bool settingsMaybeModified{};
      try {
        auto [settings, error] = DCP::Settings::Tree::get("", true);
        if (error) {
          throw std::system_error(error, "DCP::Settings::Tree::get() failed");
        }

        for (auto &setting: defaultSettings) {
          assert(setting.size() == 3);
          std::string settingPath = setting[0];
          std::string settingName = setting[1];
          std::string settingString = setting[2];
          try {
            logger.log(DCP::LogLevel::info, "Defaulting \"%s\\%s\" to \"%s\"",
              settingPath.data(), settingName.data(), settingString.data()
            );
            settings.set(settingPath.data(), settingName.data(),
              settingString.data(), false
            );
            settingsMaybeModified = true;
          } catch (...) {
            exitCode |= ExitCode::settingWriteError;
            logger.log(DCP::LogLevel::error,
              "Error defaulting \"%s\\%s\" to \"%s\": %s",
              settingPath.data(), settingName.data(), settingString.data(),
              boost::current_exception_diagnostic_information().data()
            );
          }
        }
        for (auto &setting: settingsToUnset) {
          assert(setting.size() == 2);
          std::string settingPath = setting[0];
          std::string settingName = setting[1];
          try {
            logger.log(DCP::LogLevel::info, "Unsetting \"%s\\%s\"",
              settingPath.data(), settingName.data()
            );
            settings.unset(settingPath.data(), settingName.data());
            settingsMaybeModified = true;
          } catch (...) {
            exitCode |= ExitCode::settingWriteError;
            logger.log(DCP::LogLevel::error,
              "Error unsetting \"%s\\%s\": %s",
              settingPath.data(), settingName.data(),
              boost::current_exception_diagnostic_information().data()
            );
          }
        }
        for (auto &setting: settingsToSet) {
          assert(setting.size() == 3);
          std::string settingPath = setting[0];
          std::string settingName = setting[1];
          std::string settingString = setting[2];
          try {
            logger.log(DCP::LogLevel::info, "Setting \"%s\\%s\" to \"%s\"",
              settingPath.data(), settingName.data(), settingString.data()
            );
            settings.set(settingPath.data(), settingName.data(),
              settingString.data()
            );
            settingsMaybeModified = true;
          } catch (...) {
            exitCode |= ExitCode::settingWriteError;
            logger.log(DCP::LogLevel::error,
              "Error setting \"%s\\%s\" to \"%s\": %s",
              settingPath.data(), settingName.data(), settingString.data(),
              boost::current_exception_diagnostic_information().data()
            );
          }
        }
      } catch (...) {
        exitCode |= ExitCode::settingWriteError;
        logger.log(DCP::LogLevel::error, "Error updating settings: %s",
          boost::current_exception_diagnostic_information().data()
        );
      }

      // Update current screensaver user settings to reflect registry settings.
      {
        auto [desktopKey, error] = DCP::Registry::tryToGetKey(
          HKEY_CURRENT_USER, "Control Panel\\Desktop"
        );
        if (desktopKey == DCP::Registry::Key{}) {
          logger.log(DCP::LogLevel::warning,
            "No desktop registry key (\"%s\"); skipping screensaver update",
            error.message().data()
          );
        } else {
          try {
            logger.log(DCP::LogLevel::debug,
              "Updating screensaver active state..."
            );
            if (
              updateSystemParameter(
                desktopKey, "ScreenSaveActive", SPI_SETSCREENSAVEACTIVE
              )
            ) {
              logger.log(DCP::LogLevel::debug,
                "Successfully updated screensaver active state"
              );
            } else {
              exitCode |= ExitCode::screensaverUpdateError;
              logger.log(DCP::LogLevel::error,
                "Failed to update screensaver active state"
              );
            }
          } catch (...) {
            exitCode |= ExitCode::screensaverUpdateError;
            logger.log(DCP::LogLevel::error,
              "Error updating screensaver active state: %s",
              boost::current_exception_diagnostic_information().data()
            );
          }

          try {
            logger.log(DCP::LogLevel::debug, "Updating screensaver timeout...");
            if (
              updateSystemParameter(
                desktopKey, "ScreenSaveTimeOut", SPI_SETSCREENSAVETIMEOUT
              )
            ) {
              logger.log(DCP::LogLevel::debug,
                "Successfully updated screensaver timeout"
              );
            } else {
              exitCode |= ExitCode::screensaverUpdateError;
              logger.log(DCP::LogLevel::error,
                "Failed to update screensaver timeout"
              );
            }
          } catch (...) {
            exitCode |= ExitCode::screensaverUpdateError;
            logger.log(DCP::LogLevel::error,
              "Error updating screensaver timeout: %s",
              boost::current_exception_diagnostic_information().data()
            );
          }
        }
      }

      // If a setting was modified and the service exists, restart it.
      if (!settingsMaybeModified) {
        logger.log(DCP::LogLevel::debug, "No settings modified");
      } else if (serviceName.empty()) {
        logger.log(DCP::LogLevel::debug,
          "Not restarting service: none specified"
        );
      } else {
        logger.log(DCP::LogLevel::debug,
          "Restarting service '%s'...", serviceName.data()
        );
        try {
          auto const &installDirectoryPath = (
            DCP::Paths::getInstallDirectoryPath(executablePath.parent_path())
          );
          assert(!installDirectoryPath.empty());
          assert(boost::filesystem::is_directory(installDirectoryPath));

          DCP::Service service(installDirectoryPath, serviceName);
          if (service.restart(timeout)) {
            logger.log(DCP::LogLevel::debug,
              "Successfully restarted service '%s'", serviceName.data()
            );
          } else {
            exitCode |= ExitCode::serviceRestartError;
            logger.log(DCP::LogLevel::error,
              "Failed to restart service '%s'", serviceName.data()
            );
          }
        } catch (...) {
          exitCode |= ExitCode::serviceRestartError;
          logger.log(DCP::LogLevel::error,
            "Error restarting service '%s': %s", serviceName.data(),
            boost::current_exception_diagnostic_information().data()
          );
        }
      }
    } catch (...) {
      exitCode |= ExitCode::internalError;
      logger.log(DCP::LogLevel::error, "Error: %s",
        boost::current_exception_diagnostic_information().data()
      );
    }
  } catch (...) {
    exitCode |= ExitCode::internalError;
    try {
      std::cerr << boost::current_exception_diagnostic_information();
      std::cerr << std::endl;
    } catch (...) {
      assert(false);
    }
  }

  return exitCode;
}

#else

  #include <cstdlib>

int main() noexcept {
  return EXIT_FAILURE;
}

#endif
