# Copyright (c) 2021 Distributive Corp. All Rights Reserved.

# Used in node scripts.
set(DCP_NODE_PATH)

set(_TARGET "${PROJECT_NAME}-node")

set(_FILES)
set(_PROGRAMS)
if(DCP_NODE)
  if(NOT NODE_EXECUTABLE)
    if(NOT NODE_ROOT)
      message(FATAL_ERROR "Target '${_TARGET}' failed: NODE_ROOT not specified")
    endif()

    set(NODE_EXECUTABLE_SUBDIRECTORY)
    if(NOT WIN32)
      set(NODE_EXECUTABLE_SUBDIRECTORY "/bin")
    endif()

    find_program(NODE_EXECUTABLE
      NAMES "node"
      PATHS "${NODE_ROOT}${NODE_EXECUTABLE_SUBDIRECTORY}"
      DOC "Node executable path"
      REQUIRED
      NO_DEFAULT_PATH
    )
  endif()
  message(STATUS "NODE_EXECUTABLE: ${NODE_EXECUTABLE}")

  execute_process(
    COMMAND "${NODE_EXECUTABLE}" "--version"
    OUTPUT_VARIABLE NODE_VERSION
    ERROR_QUIET
    OUTPUT_STRIP_TRAILING_WHITESPACE
  )
  message(STATUS "NODE_VERSION: ${NODE_VERSION}")

  if(WIN32)
    set(DCP_NODE_PATH ".\\")
  else()
    set(DCP_NODE_PATH "./")
  endif()

  list(APPEND _FILES "${NODE_EXECUTABLE}")
  list(APPEND _PROGRAMS "${NODE_EXECUTABLE}")
endif()

set(_SCRIPT_INPUTS "script/node${DCP_SCRIPT_EXTENSION}.in")
dcp_configure_files(_SCRIPT_OUTPUTS EXECUTABLE INPUTS ${_SCRIPT_INPUTS})
list(APPEND _FILES ${_SCRIPT_OUTPUTS})
list(APPEND _PROGRAMS ${_SCRIPT_OUTPUTS})

add_custom_target("${_TARGET}"
  ALL
  COMMAND "${CMAKE_COMMAND}" "-E" "make_directory" "${DCP_OUTPUT_DIRECTORY}"
  COMMAND
  "${CMAKE_COMMAND}" "-E" "copy_if_different" ${_FILES}
  "${DCP_OUTPUT_DIRECTORY}"
  VERBATIM
)
dcp_target_initialize("${_TARGET}")
unset(_FILES)

dcp_add_sources("${_TARGET}"
  FOLDER "Script Files"
  SUBDIRECTORY "script"
  FILES ${_SCRIPT_INPUTS}
)
unset(_SCRIPT_INPUTS)

dcp_add_sources("${_TARGET}"
  FOLDER "Script Files (Generated)"
  DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
  SUBDIRECTORY "script"
  FILES ${_SCRIPT_OUTPUTS}
)
unset(_SCRIPT_OUTPUTS)

install(
  PROGRAMS ${_PROGRAMS}
  DESTINATION "${DCP_INSTALL_EXECUTABLE_SUBDIRECTORY}"
  COMPONENT NODE
)
unset(_PROGRAMS)
