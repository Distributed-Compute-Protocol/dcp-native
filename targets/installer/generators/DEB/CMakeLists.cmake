# Copyright (c) 2024 Distributive Corp. All Rights Reserved.

set(DCP_INSTALLER_DEBIAN_NAME "${DCP_WORKER_PRODUCT_ID}" CACHE INTERNAL "")

set(DCP_INSTALLER_DEBIAN_DEPENDS)
if(NOT DCP_NODE)
  set(DCP_INSTALLER_DEBIAN_NODE_DEPENDS "nodejs (>= 18)")
  list(APPEND DCP_INSTALLER_DEBIAN_DEPENDS
    "${DCP_INSTALLER_DEBIAN_NODE_DEPENDS}"
  )
endif()
set(DCP_INSTALLER_DEBIAN_SERVICE_DEPENDS "systemctl | systemd")
list(APPEND DCP_INSTALLER_DEBIAN_DEPENDS
  "${DCP_INSTALLER_DEBIAN_SERVICE_DEPENDS}"
)
list(JOIN DCP_INSTALLER_DEBIAN_DEPENDS ", " DCP_INSTALLER_DEBIAN_DEPENDS)

# Delete the combined package maintainer scripts
file(REMOVE_RECURSE "${CMAKE_CURRENT_BINARY_DIR}/generators/DEB/control")

set(_CONTROL_OUTPUTS)
macro(dcp_package_debian_add_package_maintainer_scripts
  _COMPONENT_NAME _COMPONENT_SUBDIRECTORY
)
  set(_INPUTS)
  set(_OUTPUTS)

  set(_SUBPATH "generators/DEB/components/${_COMPONENT_SUBDIRECTORY}/control")

  macro(dcp_package_debian_add_package_maintainer_script _CONTROL)
    set(_INPUT "${_SUBPATH}/${_CONTROL}.in")
    dcp_configure_files(_OUTPUT EXECUTABLE INPUTS "${_INPUT}")
    list(APPEND _INPUTS "${_INPUT}")
    list(APPEND _OUTPUTS "${_OUTPUT}")

    # Append this script fragment to the combined package maintainer script
    # in the dpkg control files; this is used to set
    # CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA in the CPack configuration file.
    set(_CONTROL_OUTPUT
      "${CMAKE_CURRENT_BINARY_DIR}/generators/DEB/control/${_CONTROL}"
    )
    file(READ "${_OUTPUT}" _CONTENT)
    file(APPEND "${_CONTROL_OUTPUT}" "${_CONTENT}\n")
    file(APPEND "${_CONTROL_OUTPUT}" "# end: ${_INPUT}\n")
    list(APPEND _CONTROL_OUTPUTS "${_CONTROL_OUTPUT}")

    unset(_INPUT)
    unset(_OUTPUT)
  endmacro()

  dcp_package_debian_add_package_maintainer_script("postinst")
  dcp_package_debian_add_package_maintainer_script("preinst")
  dcp_package_debian_add_package_maintainer_script("prerm")
  dcp_package_debian_add_package_maintainer_script("postrm")

  set(_PREFIX "Generators/DEB/Components/${_COMPONENT_NAME}/Control Files")
  source_group(
    TREE "${CMAKE_CURRENT_SOURCE_DIR}/${_SUBPATH}"
    PREFIX "${_PREFIX}"
    FILES ${_INPUTS}
  )
  source_group(
    TREE "${CMAKE_CURRENT_BINARY_DIR}/${_SUBPATH}"
    PREFIX "${_PREFIX} (Generated)"
    FILES ${_OUTPUTS}
  )
  unset(_PREFIX)
  unset(_SUBPATH)

  list(APPEND DCP_INSTALLER_SOURCE_FILES ${_INPUTS} ${_OUTPUTS})
  unset(_INPUTS)
  unset(_OUTPUTS)
endmacro()

# Order matters - this is package maintainer script aggregation order
dcp_package_debian_add_package_maintainer_scripts("Supervisor" "supervisor")
dcp_package_debian_add_package_maintainer_scripts("DCP Evaluator Service"
  "dcp-evaluator"
)
dcp_package_debian_add_package_maintainer_scripts("DCP Worker Service"
  "dcp-worker"
)
dcp_package_debian_add_package_maintainer_scripts("DCP Namespace Service"
  "dcp"
)

set (_SYSTEMD_INPUT_FILES)
set (_SYSTEMD_OUTPUT_FILES)
macro(dcp_package_debian_add_systemd_service _COMPONENT_NAME)
  set(_SUBPATH "generators/DEB/components/${_COMPONENT_NAME}/configuration")
  set(_INPUT "${_SUBPATH}/${_COMPONENT_NAME}.service.in")
  set(_OUTPUT
    "${CMAKE_CURRENT_BINARY_DIR}/${_SUBPATH}/${_COMPONENT_NAME}.service"
  )

  configure_file("${_INPUT}" "${_OUTPUT}" USE_SOURCE_PERMISSIONS @ONLY)
  list(APPEND _SYSTEMD_OUTPUT_FILES "${_OUTPUT}")
  unset(_INPUT)
  unset(_OUTPUT)
  unset(_SUBPATH)
endmacro()
dcp_package_debian_add_systemd_service(dcp-evaluator)
dcp_package_debian_add_systemd_service(dcp-worker)
dcp_package_debian_add_systemd_service(dcp)

source_group(
  TREE "${CMAKE_CURRENT_SOURCE_DIR}"
  PREFIX "Generators/DEB/Components/Service/Configuration Files"
  FILES ${_SYSTEMD_INPUT_FILES}
)
source_group(
  TREE "${CMAKE_CURRENT_BINARY_DIR}/.."
  PREFIX "Generators/DEB/Components/Service/Configuration Files (Generated)"
  FILES ${_SYSTEMD_OUTPUT_FILES}
)
list(APPEND DCP_INSTALLER_SOURCE_FILES "${_SYSTEMD_INPUT_FILES}"
  "${_SYSTEMD_OUTPUT_FILES}"
)
if(DCP_INSTALLER_GENERATOR STREQUAL "DEB")
  install(
    FILES ${_SYSTEMD_OUTPUT_FILES}
    DESTINATION "etc/systemd/system"
    COMPONENT SERVICE
  )
endif()
unset(_SYSTEMD_INPUT_FILES)
unset(_SYSTEM_OUTPUT_FILES)

list(REMOVE_DUPLICATES _CONTROL_OUTPUTS)
file(
  CHMOD ${_CONTROL_OUTPUTS}
  FILE_PERMISSIONS
  OWNER_READ OWNER_EXECUTE
  GROUP_READ GROUP_EXECUTE
  WORLD_READ WORLD_EXECUTE
)
source_group(
  TREE "${CMAKE_CURRENT_BINARY_DIR}/generators/DEB/control"
  PREFIX "Generators/DEB/Control Files (Generated)"
  FILES ${_CONTROL_OUTPUTS}
)
list(APPEND DCP_INSTALLER_SOURCE_FILES ${_CONTROL_OUTPUTS})
unset(_CONTROL_OUTPUTS)

set(_SUBPATH "generators/DEB/configuration")
set(_INPUT "${_SUBPATH}/desktop.in")
set(_OUTPUT
  "${CMAKE_CURRENT_BINARY_DIR}/${_SUBPATH}/${DCP_WORKER_PRODUCT_ID}.desktop"
)
configure_file("${_INPUT}" "${_OUTPUT}" USE_SOURCE_PERMISSIONS @ONLY)
source_group(
  TREE "${CMAKE_CURRENT_SOURCE_DIR}/${_SUBPATH}"
  PREFIX "Generators/DEB/Configuration Files"
  FILES "${_INPUT}"
)
source_group(
  TREE "${CMAKE_CURRENT_BINARY_DIR}/${_SUBPATH}"
  PREFIX "Generators/DEB/Configuration Files (Generated)"
  FILES "${_OUTPUT}"
)
list(APPEND DCP_INSTALLER_SOURCE_FILES "${_INPUT}" "${_OUTPUT}")
if(DCP_INSTALLER_GENERATOR STREQUAL "DEB")
  install(
    FILES "${_OUTPUT}"
    DESTINATION "usr/share/applications"
    COMPONENT SHARED
  )
endif()
unset(_INPUT)
unset(_OUTPUT)
unset(_SUBPATH)

if(DCP_INSTALLER_GENERATOR STREQUAL "DEB")
  install(
    FILES "${PROJECT_SOURCE_DIR}/resource/dcp.png"
    DESTINATION "${DCP_INSTALL_SUBDIRECTORY}/share/icons/hicolor/48x48/apps"
    COMPONENT SHARED
  )
endif()
