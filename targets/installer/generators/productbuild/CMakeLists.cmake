# Copyright (c) 2024 Distributive Corp. All Rights Reserved.

list(
  APPEND CMAKE_MODULE_PATH
  "${CMAKE_CURRENT_SOURCE_DIR}/generators/productbuild/module"
)

set(_FILES "generators/productbuild/module/CPack.distribution.dist.in")
source_group(
  TREE "${CMAKE_CURRENT_SOURCE_DIR}/generators/productbuild/module"
  PREFIX "Generators/productbuild/Module Files"
  FILES ${_FILES}
)
list(APPEND DCP_INSTALLER_SOURCE_FILES ${_FILES})
unset(_FILES)

function(dcp_package_productbuild_add_component_script_files
  _COMPONENT_NAME _COMPONENT_SUBDIRECTORY
)
  set(_SUBPATH
    "generators/productbuild/components/${_COMPONENT_SUBDIRECTORY}/script"
  )

  set(_INPUTS
    "${_SUBPATH}/postflight.sh.in"
    "${_SUBPATH}/preflight.sh.in"
  )
  dcp_configure_files(_OUTPUTS EXECUTABLE INPUTS ${_INPUTS})

  source_group(
    TREE "${CMAKE_CURRENT_SOURCE_DIR}/${_SUBPATH}"
    PREFIX "Generators/productbuild/Components/${_COMPONENT_NAME}/Script Files"
    FILES ${_INPUTS}
  )
  source_group(
    TREE "${CMAKE_CURRENT_BINARY_DIR}/${_SUBPATH}"
    PREFIX
    "Generators/productbuild/Components/${_COMPONENT_NAME}/Script Files (Generated)"
    FILES ${_OUTPUTS}
  )
  list(APPEND DCP_INSTALLER_SOURCE_FILES ${_INPUTS} ${_OUTPUTS})

  set(DCP_INSTALLER_SOURCE_FILES ${DCP_INSTALLER_SOURCE_FILES} PARENT_SCOPE)
endfunction()

dcp_package_productbuild_add_component_script_files("Supervisor" "supervisor")

dcp_package_productbuild_add_component_script_files("Service" "service")
set(_SUBPATH "generators/productbuild/components/service/configuration")
set(_INPUT "${_SUBPATH}/service.plist.in")
set(_OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/${_SUBPATH}/${DCP_SERVICE_NAME}.plist")
configure_file("${_INPUT}" "${_OUTPUT}" USE_SOURCE_PERMISSIONS @ONLY)
source_group(
  TREE "${CMAKE_CURRENT_SOURCE_DIR}/${_SUBPATH}"
  PREFIX "Generators/productbuild/Components/Service/Configuration Files"
  FILES ${_INPUT}
)
source_group(
  TREE "${CMAKE_CURRENT_BINARY_DIR}/${_SUBPATH}"
  PREFIX
  "Generators/productbuild/Components/Service/Configuration Files (Generated)"
  FILES ${_OUTPUT}
)
list(APPEND DCP_INSTALLER_SOURCE_FILES ${_INPUT} ${_OUTPUT})
if(DCP_INSTALLER_GENERATOR STREQUAL "productbuild")
  install(
    FILES ${_OUTPUT}
    DESTINATION "Library/LaunchDaemons"
    COMPONENT SERVICE
  )
endif()
unset(_INPUT)
unset(_OUTPUT)
unset(_SUBPATH)
