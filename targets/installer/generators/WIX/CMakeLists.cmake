# Copyright (c) 2024 Distributive Corp. All Rights Reserved.

set(DCP_INSTALLER_WIX_UPGRADE_ID "93FBCD41-FDF7-40B9-A364-99FF4A9A140F"
  CACHE STRING "The WiX product GUID; must match the Omaha product GUID."
)
message(STATUS "DCP_INSTALLER_WIX_UPGRADE_ID: ${DCP_INSTALLER_WIX_UPGRADE_ID}")

set(DCP_INSTALLER_WIX_ACTIVE_SETUP_ID "623BA780-9CCC-43D7-8C6C-37DFC95BB35B")

function(dcp_package_wix_get_id _COMPONENT_VARIABLE _FILE_VARIABLE
  _COMPONENT _FILE
)
  # https://cmake.org/pipermail/cmake-developers/2013-October/020181.html
  string(REPLACE "-" "_" _WIX_ID "${_FILE}")
  set(${_COMPONENT_VARIABLE} "CM_CP_${_COMPONENT}.${_WIX_ID}" PARENT_SCOPE)
  set(${_FILE_VARIABLE} "CM_FP_${_COMPONENT}.${_WIX_ID}" PARENT_SCOPE)
endfunction()
dcp_package_wix_get_id(
  DCP_INSTALLER_WIX_SETTINGS_CONFIGURATOR_PROGRAM_COMPONENT_ID
  DCP_INSTALLER_WIX_SETTINGS_CONFIGURATOR_PROGRAM_FILE_ID
  "SHARED" "${DCP_SETTINGS_CONFIGURATOR_PROGRAM_FILE}"
)
dcp_package_wix_get_id(
  DCP_INSTALLER_WIX_SETTINGS_PROGRAM_COMPONENT_ID
  DCP_INSTALLER_WIX_SETTINGS_PROGRAM_FILE_ID
  "SETTINGS" "${DCP_SETTINGS_PROGRAM_FILE}"
)
dcp_package_wix_get_id(
  DCP_INSTALLER_WIX_EVALUATOR_PROGRAM_COMPONENT_ID
  DCP_INSTALLER_WIX_EVALUATOR_PROGRAM_FILE_ID
  "EVALUATOR" "${DCP_EVALUATOR_PROGRAM_FILE}"
)
dcp_package_wix_get_id(
  DCP_INSTALLER_WIX_SUPERVISOR_SETUP_SCRIPT_COMPONENT_ID
  DCP_INSTALLER_WIX_SUPERVISOR_SETUP_SCRIPT_FILE_ID
  "SUPERVISOR" "${DCP_SUPERVISOR_SETUP_SCRIPT_FILE}"
)
dcp_package_wix_get_id(
  DCP_INSTALLER_WIX_SANDBOX_SCRIPT_COMPONENT_ID
  DCP_INSTALLER_WIX_SANDBOX_SCRIPT_FILE_ID
  "SUPERVISOR" "${DCP_SANDBOX_SCRIPT_FILE}"
)

include("${CMAKE_CURRENT_SOURCE_DIR}/generators/WIX/resource/localizable.cmake")
set(_FILES
  "generators/WIX/resource/localizable.cmake"
  "generators/WIX/resource/banner.bmp"
  "generators/WIX/resource/dialog.bmp"
)
source_group(
  TREE "${CMAKE_CURRENT_SOURCE_DIR}/generators/WIX/resource"
  PREFIX "Generators/WIX/Resource Files"
  FILES ${_FILES}
)
list(APPEND DCP_INSTALLER_SOURCE_FILES ${_FILES})
unset(_FILES)

set(_INPUTS
  "generators/WIX/patch/main.xml.in"
  "generators/WIX/patch/omaha.xml.in"
)
dcp_configure_files(_OUTPUTS INPUTS ${_INPUTS})
source_group(
  TREE "${CMAKE_CURRENT_SOURCE_DIR}/generators/WIX/patch"
  PREFIX "Generators/WIX/Patch Files"
  FILES ${_INPUTS}
)
source_group(
  TREE "${CMAKE_CURRENT_BINARY_DIR}/generators/WIX/patch"
  PREFIX "Generators/WIX/Patch Files (Generated)"
  FILES ${_OUTPUTS}
)
list(APPEND DCP_INSTALLER_SOURCE_FILES ${_INPUTS} ${_OUTPUTS})
unset(_INPUTS)
unset(_OUTPUTS)

set(_INPUTS "generators/WIX/template/main.wxs.in")
dcp_configure_files(_OUTPUTS INPUTS ${_INPUTS})
source_group(
  TREE "${CMAKE_CURRENT_SOURCE_DIR}/generators/WIX/template"
  PREFIX "Generators/WIX/Template Files"
  FILES ${_INPUTS}
)
source_group(
  TREE "${CMAKE_CURRENT_BINARY_DIR}/generators/WIX/template"
  PREFIX "Generators/WIX/Template Files (Generated)"
  FILES ${_OUTPUTS}
)
list(APPEND DCP_INSTALLER_SOURCE_FILES ${_INPUTS} ${_OUTPUTS})
unset(_INPUTS)
unset(_OUTPUTS)
