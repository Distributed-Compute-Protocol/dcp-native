# Copyright (c) 2024 Distributive Corp. All Rights Reserved.

set(DCP_INSTALLER_WIX_ERROR_CAPTION_DOWNGRADE
  "A later version of [ProductName] is already installed. Setup will now exit."
)
set(DCP_INSTALLER_WIX_ERROR_CAPTION_LEGACY
  "A legacy version is already installed and must be manually uninstalled."
)
set(DCP_INSTALLER_WIX_ERROR_CAPTION_X64
  "This product only runs on 64-bit Windows."
)

# WiX Toolset expects a 493x58 image for the banner:
# https://cmake.org/cmake/help/latest/cpack_gen/wix.html#variable:CPACK_WIX_UI_BANNER
set(DCP_INSTALLER_WIX_IMAGE_BANNER
  "${CMAKE_CURRENT_SOURCE_DIR}/generators/WIX/resource/banner.bmp"
)
# WiX Toolset expects a 493x312 image for the dialog:
# https://cmake.org/cmake/help/latest/cpack_gen/wix.html#variable:CPACK_WIX_UI_DIALOG
set(DCP_INSTALLER_WIX_IMAGE_DIALOG
  "${CMAKE_CURRENT_SOURCE_DIR}/generators/WIX/resource/dialog.bmp"
)

set(DCP_INSTALLER_WIX_LANGUAGE_CODE "1033")
