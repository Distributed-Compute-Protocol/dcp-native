# Copyright (c) 2022 Distributive Corp. All Rights Reserved.

# CMake searches the CMAKE_BINARY_DIR first for CMakeGraphVizOptions.cmake,
# followed by CMAKE_SOURCE_DIR. If none in the latter location, create a
# reasonable default in the former.
if(NOT EXISTS "${CMAKE_SOURCE_DIR}/CMakeGraphVizOptions.cmake")
  file(
    WRITE "${CMAKE_BINARY_DIR}/CMakeGraphVizOptions.cmake"
    "set(GRAPHVIZ_CUSTOM_TARGETS ON)\n"
  )
endif()

# Ensure that a dependency diagram file exists in the event that the argument
# `--graphviz "${CMAKE_BINARY_DIR}/dot/dependencies.dot"` was not provided to
# CMake. Dependency graph generation is done after all targets have been
# generated, so it is impossible to know at configuration time whether it was
# specified. If it was, the default dependency diagram will be replaced.
set(_FILE "dot/dependencies.dot")
if(NOT EXISTS "${CMAKE_BINARY_DIR}/${_FILE}")
  file(
    WRITE "${CMAKE_BINARY_DIR}/${_FILE}"
    "digraph Message {\"No dependency graph generated\" [shape=box]}\n"
  )
endif()

set(_TARGET "${PROJECT_NAME}-dot")

add_custom_target("${_TARGET}"
  ALL
  COMMAND
  "${CMAKE_COMMAND}" "-E" "copy_directory" "${CMAKE_BINARY_DIR}/dot"
  "${DCP_OUTPUT_DIRECTORY}/${_TARGET}"
  VERBATIM
)
dcp_target_initialize("${_TARGET}")
