/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2022
 */

#include <dcp/test.hh>

#if BOOST_OS_WINDOWS
  #include <dcp/supervisor.hh>
#endif

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Supervisor)
  BOOST_AUTO_TEST_SUITE(tests)

#if BOOST_OS_WINDOWS

  BOOST_AUTO_TEST_CASE(hash, *boost::unit_test::timeout(timeout)) {
    std::string const &hash{
      DCP::Supervisor::hash(getExecutablePath().parent_path(), "key", "secret")
    };
    BOOST_CHECK_EQUAL(hash,
      "eh1-d53545f76198c4d405a9de163d574d9e66f431e9d418c8df98b386a0b5c93554"
    );
  }

#else

  BOOST_AUTO_TEST_CASE(empty) {}

#endif

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
