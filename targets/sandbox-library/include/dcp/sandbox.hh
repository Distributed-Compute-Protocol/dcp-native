/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       May 2023
 */

#if !defined(DCP_Sandbox_)
  #define DCP_Sandbox_

  #include <dcp/process.hh>
  #include <dcp/socket.hh>

  #include <vector>

namespace DCP {

  struct Sandbox final {

    explicit Sandbox(
      /// The directory in which to find the sandbox script.
      boost::filesystem::path const &directoryPath,
      /// Arguments to be passed directly to the evaluator.
      std::vector<std::string> const &arguments = {},
      /// The socket, if any.
      SOCKET socket = INVALID_SOCKET
    );

    [[nodiscard]]
    Process &getProcess() noexcept {
      return process;
    }

    [[nodiscard]]
    Process const &getProcess() const noexcept {
      return process;
    }

  private:

    Process process;

  };

}

#endif
