/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       May 2023
 */

#include <dcp/sandbox.hh>

#include <dcp/logger.hh>

#include <boost/exception/diagnostic_information.hpp>
#include <boost/filesystem/operations.hpp>

namespace DCP {

  namespace {

    Process createProcess(
      boost::filesystem::path const &directoryPath,
      std::vector<std::string> const &arguments,
      SOCKET const socket
    ) {
      assert(boost::filesystem::is_directory(directoryPath));

      auto const &scriptPath = directoryPath / DCP_Sandbox_scriptFile;
      assert(boost::filesystem::exists(scriptPath));

      auto const &logger = Logger::getInstance();
      auto const logLevel = std::to_string(logger.getMaxLogLevel());
      auto const systemLogLevel = std::to_string(logger.getMaxSystemLogLevel());

#if BOOST_OS_WINDOWS
      auto const socketString = std::to_string(socket);

      auto job = Process::createJob();
      assert(job);
      auto const jobString = std::to_string(
        reinterpret_cast<uintptr_t>(static_cast<HANDLE>(job))
      );
      logger.log(LogLevel::debug, "Created sandbox job %s", jobString.data());

      auto startEvent = Process::createEvent();
      assert(startEvent);
      auto const startEventString = std::to_string(
        reinterpret_cast<uintptr_t>(static_cast<HANDLE>(startEvent))
      );
      logger.log(LogLevel::debug, "Created sandbox start event %s",
        startEventString.data()
      );
#else
      logger.log(LogLevel::debug, "Using environment containing `DISPLAY=:0`");
      std::string display{"DISPLAY=:0"};
      char *const environment[] = {&display.front(), nullptr};
#endif

      std::vector<std::string> sandboxArguments{
#if !BOOST_OS_WINDOWS
        "--stdio",
#endif
        "--",
#if BOOST_OS_WINDOWS
        "--start-event", startEventString,
        "--socket", socketString,
#endif
        "--debug", logLevel,
        "--system-debug", systemLogLevel
      };
      sandboxArguments.insert(
        std::end(sandboxArguments), std::begin(arguments), std::end(arguments)
      );

#if BOOST_OS_WINDOWS
      std::vector<HANDLE> additionalHandles{};
      if (socket != INVALID_SOCKET) {
        additionalHandles.push_back(reinterpret_cast<HANDLE>(socket));
      }
#else
      if (socket != INVALID_SOCKET) {
        Socket::setCloseOnExec(socket, false);
      }
#endif

      return Process(scriptPath, sandboxArguments,
#if BOOST_OS_WINDOWS
        std::move(job),
        std::move(startEvent),
        std::move(additionalHandles)
#else
        environment,
        [socket]() {
          if (socket != INVALID_SOCKET) {
            try {
              // Use `socket` for standard I/O.
              // Note that the evaluator `--socket` argument doesn't work here,
              // because the spawn function in dcp-evaluator-start does not
              // preserve inheritance of other sockets by default on non-Windows
              // (DCP-3655).
              Socket::duplicate(socket, STDIN_FILENO);
              Socket::duplicate(socket, STDOUT_FILENO);
              Socket::duplicate(socket, STDERR_FILENO);
            } catch (...) {
              tryToLog(LogLevel::error,
                "Exception in sandbox child process callback: %s",
                boost::current_exception_diagnostic_information().data()
              );
              Socket::close(socket);
              return false;
            }
          }
          return true;
        }
#endif
      );
    }

  }

  Sandbox::Sandbox(
    boost::filesystem::path const &directoryPath,
    std::vector<std::string> const &arguments,
    SOCKET const socket
  ) :
  process{createProcess(directoryPath, arguments, socket)}
  {}

}
