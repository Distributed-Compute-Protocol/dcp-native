/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       May 2021
 */

#if BOOST_OS_WINDOWS

  #include <dcp/argument.hh>
  #include <dcp/crypto.hh>
  #include <dcp/logger.hh>
  #include <dcp/paths.hh>
  #include <dcp/process.hh>
  #include <dcp/registry.hh>
  #include <dcp/version.hh>

  #include <boost/exception/diagnostic_information.hpp>
  #include <boost/filesystem/operations.hpp>
  #include <boost/format.hpp>
  #include <boost/program_options.hpp>

  #include <iostream>

namespace {

  void readOrWriteSecureValue(
    char const *const name, std::string &value, bool const save
  ) {
    assert(name);

    static char const *const subkey{
      "Software\\" DCP_companyName "\\" DCP_productName "\\signing"
    };

    if (value.empty()) {
      auto const &[encrypted, error] = DCP::Registry::tryToGetStringValue(
        HKEY_CURRENT_USER, subkey, name
      );
      if (encrypted.empty()) {
        DCP::log(DCP::LogLevel::notice,
          "No \"%s\" value read from registry (\"%s\")",
          name, error.message().data()
        );
      } else {
        value = DCP::Crypto::decrypt(encrypted);
        DCP::log(DCP::LogLevel::info, "Read \"%s\" value from registry.", name);
      }
    } else {
      DCP::log(DCP::LogLevel::info, "Using provided \"%s\" value.", name);
      if (save) {
        DCP::Registry::setStringValue(
          DCP::Crypto::encrypt(value).data(), HKEY_CURRENT_USER, subkey, name
        );
        DCP::log(DCP::LogLevel::info, "Wrote \"%s\" value to registry.", name);
      }
    }
  }

}

int main(int argc, char *const argv[]) noexcept {
  assert(argc > 0);
  assert(argv);

  try {
    auto const executablePath = boost::filesystem::absolute(argv[0]);
    auto const executableFile = executablePath.filename().string();

    // Initialize argument variables.
    int maxLogLevel{DCP::Logger::defaultMaxLogLevel};
    int maxSystemLogLevel{DCP::Logger::defaultMaxSystemLogLevel};
    std::string reader{};
    std::string password{};
    std::string container{};
    std::string filePathString{};
    std::string description{};
    boost::filesystem::path signtoolPath{DCP_signtoolPath};
    signtoolPath.make_preferred();
    std::string signtoolPathString{signtoolPath.string()};
    boost::filesystem::path certificatePath{DCP_certificatePath};
    certificatePath.make_preferred();
    std::string certificatePathString{certificatePath.string()};
    bool save{};

    // Parse the arguments.
    std::string const &argumentsUsage{"[options]"};
    std::string const &argumentsDescription{"Sign a file."};
    std::string argumentsError{};
    std::stringstream argumentsErrorHelp{};
    try {
      namespace po = boost::program_options;

      po::options_description od("Options");
      od.add_options()(
        "help,h", "Print help."
      )(
        "version,v", "Print version information."
      )(
        "debug,d", po::value(&maxLogLevel)->default_value(maxLogLevel),
        "Specify the maximum log level (corresponding to syslog priority level,"
        " or greater for verbose logging) for logging to standard error."
        " A negative number suppresses all standard error logging."
      )(
        "system-debug,s",
        po::value(&maxSystemLogLevel)->default_value(maxSystemLogLevel),
        "Specify the maximum log level (corresponding to syslog priority level,"
        " or greater for verbose logging) for logging to the system logger."
        " A negative number suppresses all system logging."
      )(
        "reader", po::value(&reader),
        "Specify the reader name, which is saved to the registry, encrypted for"
        " the current user."
        " If not specified, it is read securely from the registry."
      )(
        "password", po::value(&password),
        "Specify the password, which is saved to the registry, encrypted for"
        " the current user."
        " If not specified, it is read securely from the registry."
      )(
        "container", po::value(&container),
        "Specify the container name, which is saved to the registry, encrypted"
        " for the current user."
        " If not specified, it is read securely from the registry."
      )(
        "save", po::bool_switch(&save),
        "Save provided credentials securely to the registry, for use in"
        " subsequent runs."
      )(
        "file", po::value(&filePathString),
        "Specify the path of the file to sign."
      )(
        "description", po::value(&description)->default_value(description),
        "Specify the description of the signed content."
      )(
        "signtool",
        po::value(&signtoolPathString)->default_value(signtoolPathString),
        "Specify the path of the signtool executable. Required to sign a file."
      )(
        "certificate",
        po::value(&certificatePathString)->default_value(certificatePathString),
        "Specify the path of the certificate file. Required to sign a file."
      );

      try {
        po::variables_map vm{};
        po::store(po::command_line_parser(argc, argv).options(od).run(), vm);
        po::notify(vm);

        // Process arguments that cause the program to exit.
        if (vm.count("help")) {
          DCP::Argument::printHelp(std::cout,
            executableFile, argumentsUsage, argumentsDescription, od
          );
          return EXIT_SUCCESS;
        }
        if (vm.count("version")) {
          DCP::Version::print(std::cout, executableFile);
          return EXIT_SUCCESS;
        }
      } catch (po::required_option const &error) {
        argumentsError = boost::str(
          boost::format("Missing argument: %s") % error.what()
        );
        DCP::Argument::printHelp(argumentsErrorHelp,
          executableFile, argumentsUsage, argumentsDescription, od
        );
      } catch (po::error const &error) {
        argumentsError = boost::str(
          boost::format("Invalid argument: %s") % error.what()
        );
        DCP::Argument::printHelp(argumentsErrorHelp,
          executableFile, argumentsUsage, argumentsDescription, od
        );
      }
    } catch (...) {
      argumentsError = boost::str(
        boost::format("Error parsing arguments: %s")
        % boost::current_exception_diagnostic_information().data()
      );
    }

    // Initialize the logger.
    auto const &logger = DCP::Logger::getInstance(
      executablePath.stem().string(), maxLogLevel, maxSystemLogLevel
    );

    // Start try block that logs any exceptions.
    try {
      // Log the executable name and version.
      logger.log(DCP::LogLevel::info, "Running \"%s\", version %s...",
        executableFile.data(), DCP_version
      );

      // Log arguments.
      if (logger.willLog(DCP::LogLevel::debug)) {
        for (int argi{}; argi < argc; ++argi) {
          logger.log(DCP::LogLevel::debug, "Argument %i: %s", argi, argv[argi]);
        }
      }

      // Handle any argument errors.
      if (!argumentsError.empty()) {
        logger.log(DCP::LogLevel::error, "%s", argumentsError.data());
        if (logger.getMaxLogLevel() < DCP::LogLevel::error) {
          std::cerr << argumentsError << std::endl;
        }
        std::cerr << argumentsErrorHelp.str();
        return EXIT_FAILURE;
      }

      readOrWriteSecureValue("reader", reader, save);
      readOrWriteSecureValue("password", password, save);
      readOrWriteSecureValue("container", container, save);

      boost::filesystem::path filePath{filePathString};
      filePath.make_preferred();
      if (filePath.empty()) {
        logger.log(DCP::LogLevel::info, "No file specified for signing.");
        return EXIT_SUCCESS;
      }
      if (!boost::filesystem::exists(filePath)) {
        throw std::runtime_error(
          boost::str(boost::format("File not found: %s") % filePath)
        );
      }

      signtoolPath = signtoolPathString;
      signtoolPath.make_preferred();
      if (!boost::filesystem::exists(signtoolPath)) {
        throw std::runtime_error(
          boost::str(boost::format("Signtool not found: %s") % signtoolPath)
        );
      }

      std::vector<std::string> arguments{
        "sign",
        "/fd", "sha256",
        "/td", "sha256",
        "/tr", "http://timestamp.comodoca.com/rfc3161",
        "/csp", "eToken Base Cryptographic Provider",
        "/n", "Distributive Corp"
      };

      certificatePath = certificatePathString;
      certificatePath.make_preferred();
      if (!certificatePath.empty()) {
        if (!boost::filesystem::exists(certificatePath)) {
          throw std::runtime_error(
            boost::str(
              boost::format("Certificate not found: %s") % certificatePath
            )
          );
        }
        arguments.push_back("/f");
        arguments.push_back(certificatePath.string());
      }

      if (!password.empty()) {
        if (container.empty()) {
          throw std::runtime_error(
            "Container must be provided along with password"
          );
        }
        // Undocumented signtool arguments from:
        // https://stackoverflow.com/a/54439759
        arguments.push_back("/k");
        arguments.push_back(
          boost::str(
            boost::format("[%s{{%s}}]=%s") % reader % password % container
          )
        );
      }

      std::string descriptionArgument{};
      if (!description.empty()) {
        arguments.push_back("/d");
        arguments.push_back(description);
      }

      arguments.push_back(filePath.string());

      // Show the password prompt if no password provided.
      auto const exitCode = DCP::Process::command(
        signtoolPath.string().data(), arguments, {}, false, password.empty()
      );
      if (exitCode) {
        logger.log(DCP::LogLevel::error,
          "Signtool command returned non-zero exit code %lu", exitCode
        );
      }
      return exitCode;
    } catch (...) {
      logger.log(DCP::LogLevel::error, "Error: %s",
        boost::current_exception_diagnostic_information().data()
      );
    }
  } catch (...) {
    try {
      std::cerr << boost::current_exception_diagnostic_information();
      std::cerr << std::endl;
    } catch (...) {
      assert(false);
    }
  }
  return EXIT_FAILURE;
}

#else

  #include <cstdlib>

int main() noexcept {
  return EXIT_FAILURE;
}

#endif
