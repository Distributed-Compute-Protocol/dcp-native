/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       September 2020
 */

#if !defined(DCP_Settings_Configuration_)
  #define DCP_Settings_Configuration_

  #if BOOST_OS_WINDOWS

    #include <dcp/color.hh>
    #include <dcp/handle.hh>
    #include <dcp/keystore.hh>
    #include <dcp/number.hh>
    #include <dcp/time.hh>

    #include <windows.h>

    #include <map>

namespace DCP::Settings {

  /// Represents the DCP-specific configuration.
  struct Configuration final {

    struct ComputeGroup final {

      enum struct IncludePublic {
        Always,
        Fallback,
        Never
      };

      explicit ComputeGroup(
        std::string joinKey,
        std::string joinSecret,
        std::string joinHash = ""
      );

      std::string const &getJoinKey() const {
        return joinKey;
      }

      std::string const &getJoinSecret() const {
        return joinSecret;
      }

      std::string const &getJoinHash() const {
        return joinHash;
      }

    private:

      std::string joinKey;
      std::string joinSecret;
      std::string joinHash;

    };

    static bool validateUrl(std::string const &string);

    Configuration();

    int getMaxSystemLogLevel() const;

    void setMaxSystemLogLevel(int logLevel);

    boost::optional<Color::Value> getScreensaverBackgroundColor() const {
      return screensaverBackgroundColor;
    }

    void setScreensaverBackgroundColor(Color::Value const color) {
      screensaverBackgroundColor = color;
    }

    void resetScreensaverBackgroundColor() {
      screensaverBackgroundColor = {};
    }

    boost::filesystem::path const &getScreensaverBitmapFilePath() const {
      return screensaverBitmapFilePath;
    }

    bool setScreensaverBitmapFilePath(boost::filesystem::path filePath);

    Handle<HBITMAP, nullptr> getScreensaverBitmap() const {
      return screensaverBitmap;
    }

    void resetScreensaverBitmap() {
      screensaverBitmapFilePath.clear();
      screensaverBitmap = {};
    }

    std::string const &getSchedulerUrl() const {
      return schedulerUrl;
    }

    bool setSchedulerUrl(std::string url) {
      if (url.empty() || validateUrl(url)) {
        schedulerUrl = std::move(url);
        return true;
      }
      return false;
    }

    bool getEvaluatorWebGPU() const {
      return evaluatorWebGPU;
    }

    void setEvaluatorWebGPU(bool const enabled) {
      evaluatorWebGPU = enabled;
    }

    Keystore const *getIdentityKeystore() const {
      return identityKeystore.get_ptr();
    }

    bool setIdentity(boost::filesystem::path const &filePath);

    void resetIdentity() {
      identityKeystore = defaultIdentityKeystore;
    }

    std::string const &getPaymentAddress() const {
      return paymentAddress;
    }

    bool setPaymentAddress(std::string address) {
      if (address.empty() || Keystore::validateAddress(address)) {
        paymentAddress = std::move(address);
        return true;
      }
      return false;
    }

    bool setPayment(boost::filesystem::path const &filePath);

    void resetPayment() {
      paymentAddress = "";
    }

    std::map<std::string, ComputeGroup> const &getComputeGroups() const {
      return computeGroups;
    }

    bool addComputeGroup(std::string name, ComputeGroup computeGroup) {
      auto const result = computeGroups.insert(
        {std::move(name), std::move(computeGroup)}
      );
      return result.second;
    }

    bool removeComputeGroup(std::string const &name) {
      auto const result = computeGroups.erase(name);
      return result > 0;
    }

    ComputeGroup::IncludePublic getIncludePublicComputeGroup() const {
      return computeGroupsIncludePublic;
    }

    void setIncludePublicComputeGroup(
      ComputeGroup::IncludePublic includePublicComputeGroup
    ) {
      computeGroupsIncludePublic = includePublicComputeGroup;
    }

    /** Invoke the configurator to save the settings.
     *  @pre      The configurator exists in `configuratorDirectoryPath`.
     *  @return   The configurator exit code.
     */
    long unsigned save(
      boost::filesystem::path const &configuratorDirectoryPath,
      char const *const serviceName,
      boost::optional<Time::Milliseconds> timeout
    ) const;

    virtual ~Configuration();

    static char const *const screensaverBitmapFileName;

    static char const *const schedulerUrlPath;
    static char const *const schedulerUrlName;

    static char const *const paymentAddressPath;
    static char const *const paymentAddressName;

    static char const *const leavePublicComputeGroupPath;
    static char const *const leavePublicComputeGroupName;

    static char const *const computeGroupsPath;

    static char const *const evaluatorWebGPUPath;
    static char const *const evaluatorWebGPUName;

    static char const *const screensaverBackgroundColorPath;
    static char const *const screensaverBackgroundColorName;

    static char const *const maxSystemLogLevelPath;
    static char const *const maxSystemLogLevelName;

    static char const *const identityKeystorePath;
    static char const *const identityKeystoreName;

    static char const *const defaultIdentityKeystorePath;
    static char const *const defaultIdentityKeystoreName;

  private:

    int maxSystemLogLevel;
    boost::filesystem::path screensaverBitmapFilePath{};
    Handle<HBITMAP, nullptr> screensaverBitmap{};
    boost::optional<Color::Value> screensaverBackgroundColor{};
    std::string schedulerUrl{};
    bool evaluatorWebGPU{};
    boost::optional<Keystore> defaultIdentityKeystore{};
    boost::optional<Keystore> identityKeystore{};
    std::string paymentAddress{};
    ComputeGroup::IncludePublic computeGroupsIncludePublic{
      ComputeGroup::IncludePublic::Always
    };
    std::map<std::string, ComputeGroup> computeGroups{};

  };

}

  #endif

#endif
