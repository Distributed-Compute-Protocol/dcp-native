/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       September 2023
 */

#if !defined(DCP_Settings_Configurator_)
  #define DCP_Settings_Configurator_

  #if BOOST_OS_WINDOWS

namespace DCP::Settings::Configurator {

  /// A class containing an enumeration of exit code bit masks.
  struct ExitCode {

    enum Enumeration : long unsigned {
      argumentError          = (1UL << 0), ///< Missing/invalid argument.
      fileCopyError          = (1UL << 1), ///< Failed to copy file.
      settingWriteError      = (1UL << 2), ///< Failed to write setting.
      settingReadError       = (1UL << 3), ///< Failed to read setting.
      screensaverUpdateError = (1UL << 4), ///< Failed to update screensaver.
      serviceRestartError    = (1UL << 5), ///< Failed to restart service.
      internalError          = (1UL << 6), ///< Internal error occurred.
    };

    ExitCode() = delete;

  };

}

  #endif

#endif
