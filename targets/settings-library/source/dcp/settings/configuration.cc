/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       September 2020
 */

#include <dcp/settings/configuration.hh>

#if BOOST_OS_WINDOWS

  #include <dcp/bitmap.hh>
  #include <dcp/logger.hh>
  #include <dcp/paths.hh>
  #include <dcp/process.hh>
  #include <dcp/settings.hh>

  #include <boost/algorithm/string.hpp>
  #include <boost/exception/diagnostic_information.hpp>
  #include <boost/filesystem/operations.hpp>
  #include <boost/uuid/random_generator.hpp>
  #include <boost/uuid/uuid.hpp>
  #include <boost/uuid/uuid_io.hpp>

  #include <shlwapi.h>
  #include <wininet.h>

namespace DCP::Settings {

  Configuration::ComputeGroup::ComputeGroup(
    std::string joinKey,
    std::string joinSecret,
    std::string joinHash
  ) :
  joinKey(std::move(joinKey)),
  joinSecret(std::move(joinSecret)),
  joinHash(std::move(joinHash))
  {
    log(LogLevel::debug,
      "Compute Group: constructed with"
      " join key \"%s\", join secret \"%s\", join hash \"%s\"",
      this->joinKey.data(), this->joinSecret.data(), this->joinHash.data()
    );
  }

  char const *const Configuration::screensaverBitmapFileName{
    "dcp-screensaver.bmp"
  };

  char const *const Configuration::schedulerUrlPath{
    "dcp-client\\dcp-config\\scheduler"
  };
  char const *const Configuration::schedulerUrlName{
    "location"
  };

  char const *const Configuration::paymentAddressPath{
    "dcp-client\\dcp-config\\worker"
  };
  char const *const Configuration::paymentAddressName{
    "paymentAddress"
  };

  char const *const Configuration::leavePublicComputeGroupPath{
    "dcp-client\\dcp-config\\worker"
  };
  char const *const Configuration::leavePublicComputeGroupName{
    "leavePublicGroup"
  };

  char const *const Configuration::computeGroupsPath{
    "dcp-client\\dcp-config\\worker\\computeGroups"
  };

  char const *const Configuration::evaluatorWebGPUPath{
    "dcp-evaluator"
  };
  char const *const Configuration::evaluatorWebGPUName{
    "webgpu"
  };

  char const *const Configuration::screensaverBackgroundColorPath{
    "dcp-screensaver"
  };
  char const *const Configuration::screensaverBackgroundColorName{
    "backgroundColor"
  };

  char const *const Configuration::identityKeystorePath{
    "keystore"
  };
  char const *const Configuration::identityKeystoreName{
    "id"
  };

  char const *const Configuration::defaultIdentityKeystorePath{
    "keystore\\default"
  };
  char const *const Configuration::defaultIdentityKeystoreName{
    "id"
  };

  char const *const Configuration::maxSystemLogLevelPath{
    "logging"
  };
  char const *const Configuration::maxSystemLogLevelName{
    "maxSystemLogLevel"
  };

  bool Configuration::validateUrl(std::string const &string) {
    return (
      string.size() <= INTERNET_MAX_URL_LENGTH &&
      PathIsURL(string.data())
    );
  }

  Configuration::Configuration() :
  maxSystemLogLevel(Logger::defaultMaxSystemLogLevel)
  {
    auto const &defaultScreensaverBitmapFilePath = (
      Paths::getProgramDataDirectoryPath() / screensaverBitmapFileName
    );
    if (!setScreensaverBitmapFilePath(defaultScreensaverBitmapFilePath)) {
      log(LogLevel::info, "The bitmap file \"%s\" could not be loaded.",
        defaultScreensaverBitmapFilePath.string().data()
      );
    }

    auto const &[settings, settingsError] = Settings::Tree::get();
    if (settingsError) {
      log(LogLevel::debug,
        "The settings could not be loaded (\"%s\"); using default settings",
        settingsError.message().data()
      );
      return;
    }

    {
      auto const &[setting, settingError] = settings.tryToGet(
        maxSystemLogLevelPath, maxSystemLogLevelName
      );
      if (setting.empty()) {
        log(LogLevel::debug,
          "No maximum system log level setting (\"%s\")",
          settingError.message().data()
        );
      } else if (!Number::tryToParseInteger(setting, maxSystemLogLevel)) {
        log(LogLevel::warning,
          "The maximum system log level setting could not be parsed; ignoring"
        );
      }
    }

    {
      auto const &[setting, settingError] = settings.tryToGet(
        screensaverBackgroundColorPath, screensaverBackgroundColorName
      );
      if (setting.empty()) {
        log(LogLevel::debug, "No background color setting (\"%s\")",
          settingError.message().data()
        );
      } else {
        try {
          screensaverBackgroundColor = Color::fromString(setting);
        } catch (...) {
          log(LogLevel::debug, "Error parsing color string \"%s\": %s",
            setting.data(),
            boost::current_exception_diagnostic_information().data()
          );
        }
        if (!screensaverBackgroundColor) {
          log(LogLevel::warning,
            "The background color setting could not be parsed; ignoring"
          );
        }
      }
    }

    {
      auto const &[setting, settingError] = settings.tryToGet(
        schedulerUrlPath, schedulerUrlName
      );
      if (setting.empty()) {
        log(LogLevel::debug, "No scheduler URL setting (\"%s\")",
          settingError.message().data()
        );
      } else if (!setSchedulerUrl(setting)) {
        log(LogLevel::warning,
          "The scheduler URL setting failed validation; ignoring"
        );
      }
    }

    {
      auto const &[setting, settingError] = settings.tryToGet(
        evaluatorWebGPUPath, evaluatorWebGPUName
      );
      if (setting.empty()) {
        log(LogLevel::debug, "No evaluator WebGPU setting (\"%s\")",
          settingError.message().data()
        );
      } else if (setting == "true") {
        setEvaluatorWebGPU(true);
      }
    }

    {
      auto const &[setting, settingError] = settings.tryToGet(
        defaultIdentityKeystorePath, defaultIdentityKeystoreName
      );
      if (setting.empty()) {
        log(LogLevel::error,
          "No default identity keystore setting (\"%s\")",
          settingError.message().data()
        );
      } else {
        defaultIdentityKeystore = Keystore::parse(setting);
        if (!defaultIdentityKeystore) {
          log(LogLevel::error,
            "The default identity keystore setting failed validation; ignoring"
          );
        }
      }
    }

    {
      auto const &[setting, settingError] = settings.tryToGet(
        identityKeystorePath, identityKeystoreName
      );
      if (setting.empty()) {
        log(LogLevel::debug,
          "No identity keystore setting (\"%s\"); using default",
          settingError.message().data()
        );
        identityKeystore = defaultIdentityKeystore;
      } else {
        identityKeystore = Keystore::parse(setting);
        if (!identityKeystore) {
          log(LogLevel::debug,
            "The identity keystore setting failed validation; using default"
          );
          identityKeystore = defaultIdentityKeystore;
        }
      }
    }

    {
      auto const &[setting, settingError] = settings.tryToGet(
        paymentAddressPath, paymentAddressName
      );
      if (setting.empty()) {
        log(LogLevel::debug,
          "No payment address setting (\"%s\")", settingError.message().data()
        );
      } else if (!setPaymentAddress(std::move(setting))) {
        log(LogLevel::warning,
          "The payment address setting failed validation; ignoring"
        );
      }
    }

    {
      auto const &[setting, settingError] = settings.tryToGet(
        leavePublicComputeGroupPath, leavePublicComputeGroupName
      );
      assert(computeGroupsIncludePublic == ComputeGroup::IncludePublic::Always);
      if (setting == "true") {
        computeGroupsIncludePublic = ComputeGroup::IncludePublic::Never;
      } else if (setting == "fallback") {
        computeGroupsIncludePublic = ComputeGroup::IncludePublic::Fallback;
      } else if (setting.empty()) {
        log(LogLevel::debug,
          "No 'leave public compute group' setting (\"%s\");"
          " including public compute group by default",
          settingError.message().data()
        );
      } else {
        log(LogLevel::warning,
          "The 'leave public compute group' setting failed validation;"
          " ignoring and including public compute group by default"
        );
      }
    }

    auto const &[
      computeGroupSettings, computeGroupSettingsError
    ] = Settings::Tree::get(computeGroupsPath);
    if (computeGroupSettingsError) {
      log(LogLevel::debug,
        "No compute groups settings (\"%s\"); using default settings",
        computeGroupSettingsError.message().data()
      );
    } else {
      log(LogLevel::debug, "Iterating compute group paths...");
      Settings::Tree::PathIterator pathIterator(computeGroupSettings);
      size_t const count = pathIterator.getCount();
      log(LogLevel::debug, "Adding %zu compute group(s)", count);
      for (size_t index = 0; index < count; ++index) {
        std::string const path{pathIterator.getNext()};
        log(LogLevel::debug, "Reading compute group: %s", path.data());

        std::string joinKey{};
        {
          auto [setting, settingError] = computeGroupSettings.tryToGet(
            path.data(), "joinKey"
          );
          if (setting.empty()) {
            log(LogLevel::debug, "No compute group join key setting (\"%s\")",
              settingError.message().data()
            );
          }
          boost::trim(setting);
          joinKey = std::move(setting);
        }

        std::string joinSecret{};
        {
          auto [setting, settingError] = computeGroupSettings.tryToGet(
            path.data(), "joinSecret"
          );
          if (setting.empty()) {
            log(LogLevel::debug,
              "No compute group join secret setting (\"%s\")",
              settingError.message().data()
            );
          }
          boost::trim(setting);
          joinSecret = std::move(setting);
        }

        std::string joinHash{};
        {
          auto [setting, settingError] = computeGroupSettings.tryToGet(
            path.data(), "joinHash"
          );
          if (setting.empty()) {
            log(LogLevel::debug, "No compute group join hash setting (\"%s\")",
              settingError.message().data()
            );
          }
          boost::trim(setting);
          joinHash = std::move(setting);
        }

        std::string name{};
        {
          auto [setting, settingError] = computeGroupSettings.tryToGet(
            path.data(), "name"
          );
          if (setting.empty()) {
            log(LogLevel::debug, "No compute group name setting (\"%s\")",
              settingError.message().data()
            );
          }
          boost::trim(setting);
          name = std::move(setting);
        }
        if (name.empty()) {
          name = joinKey;
        }

        auto const result = computeGroups.insert(
          {name, ComputeGroup{joinKey, joinSecret, joinHash}}
        );
        if (!result.second) {
          log(LogLevel::warning,
            "Duplicate compute group in settings; ignoring"
          );
        }
      }
    }
  }

  int Configuration::getMaxSystemLogLevel() const {
    return maxSystemLogLevel;
  }

  void Configuration::setMaxSystemLogLevel(int const logLevel) {
    maxSystemLogLevel = logLevel;
  }

  bool Configuration::setScreensaverBitmapFilePath(
    boost::filesystem::path filePath
  ) {
    if (Handle<HBITMAP, nullptr> bitmap = Bitmap::loadFile(filePath)) {
      screensaverBitmapFilePath = std::move(filePath);
      screensaverBitmap = bitmap;
      return true;
    }
    return false;
  }

  bool Configuration::setIdentity(boost::filesystem::path const &filePath) {
    if (boost::optional<Keystore> keystore = Keystore::read(filePath)) {
      identityKeystore = std::move(keystore);
      return true;
    }
    return false;
  }

  bool Configuration::setPayment(boost::filesystem::path const &filePath) {
    if (boost::optional<Keystore> keystore = Keystore::read(filePath)) {
      paymentAddress = keystore->getAddress();
      return true;
    }
    return false;
  }

  long unsigned Configuration::save(
    boost::filesystem::path const &configuratorDirectoryPath,
    char const *const serviceName,
    boost::optional<Time::Milliseconds> const timeout
  ) const {
    assert(boost::filesystem::is_directory(configuratorDirectoryPath));
    assert(!serviceName || *serviceName);

    boost::filesystem::path const configuratorPath{
      configuratorDirectoryPath / DCP_Settings_configuratorProgramFile
    };
    assert(boost::filesystem::exists(configuratorDirectoryPath));

    std::vector<std::string> arguments{};

    if (serviceName) {
      arguments.push_back("--service");
      arguments.push_back(serviceName);
    }

    if (timeout) {
      arguments.push_back("--timeout");
      arguments.push_back(std::to_string(*timeout));
    }

    const Logger &logger = Logger::getInstance();
    arguments.push_back("--debug");
    arguments.push_back(std::to_string(logger.getMaxLogLevel()));
    arguments.push_back("--system-debug");
    arguments.push_back(std::to_string(logger.getMaxSystemLogLevel()));

    arguments.push_back("--file");
    arguments.push_back(getScreensaverBitmapFilePath().string());
    arguments.push_back(screensaverBitmapFileName);

    arguments.push_back("--set");
    arguments.push_back(maxSystemLogLevelPath);
    arguments.push_back(maxSystemLogLevelName);
    arguments.push_back(std::to_string(getMaxSystemLogLevel()));

    boost::optional<Color::Value> color{getScreensaverBackgroundColor()};
    arguments.push_back("--set");
    arguments.push_back(screensaverBackgroundColorPath);
    arguments.push_back(screensaverBackgroundColorName);
    arguments.push_back(color ? Color::toString(*color) : "");

    arguments.push_back("--set");
    arguments.push_back(schedulerUrlPath);
    arguments.push_back(schedulerUrlName);
    arguments.push_back(getSchedulerUrl());

    arguments.push_back("--set");
    arguments.push_back(evaluatorWebGPUPath);
    arguments.push_back(evaluatorWebGPUName);
    arguments.push_back(getEvaluatorWebGPU() ? "true" : "");

    if (Keystore const *keystore = getIdentityKeystore()) {
      std::string const &keystoreString = keystore->stringify();
      if (!keystoreString.empty()) {
        arguments.push_back("--set");
        arguments.push_back(identityKeystorePath);
        arguments.push_back(identityKeystoreName);
        arguments.push_back(keystoreString);
      }
    }

    arguments.push_back("--set");
    arguments.push_back(paymentAddressPath);
    arguments.push_back(paymentAddressName);
    arguments.push_back(getPaymentAddress());

    arguments.push_back("--set");
    arguments.push_back(leavePublicComputeGroupPath);
    arguments.push_back(leavePublicComputeGroupName);
    switch (computeGroupsIncludePublic) {
    case ComputeGroup::IncludePublic::Never:
      arguments.push_back("true");
      break;
    case ComputeGroup::IncludePublic::Fallback:
      arguments.push_back("fallback");
      break;
    case ComputeGroup::IncludePublic::Always:
    default:
      arguments.push_back("");
    }

    arguments.push_back("--unset");
    arguments.push_back(computeGroupsPath);
    arguments.push_back("");

    boost::uuids::random_generator generator{};
    for (auto const &computeGroup: computeGroups) {
      std::string path = computeGroupsPath;
      path += "\\";
      auto const uuid = generator();
      path += boost::uuids::to_string(uuid);

      arguments.push_back("--set");
      arguments.push_back(path);
      arguments.push_back("name");
      arguments.push_back(computeGroup.first);

      arguments.push_back("--set");
      arguments.push_back(path);
      arguments.push_back("joinKey");
      arguments.push_back(computeGroup.second.getJoinKey());

      if (computeGroup.second.getJoinHash().empty()) {
        arguments.push_back("--set");
        arguments.push_back(path);
        arguments.push_back("joinSecret");
        arguments.push_back(computeGroup.second.getJoinSecret());
      } else {
        arguments.push_back("--set");
        arguments.push_back(path);
        arguments.push_back("joinHash");
        arguments.push_back(computeGroup.second.getJoinHash());
      }
    }

    log(LogLevel::debug, "Running command: \"%s\" %s",
      configuratorPath.string().data(), String::joinQuoted(arguments).data()
    );
    long unsigned const exitCode{
      Process::command(
        configuratorPath.string().data(), arguments, {}, true, false
      )
    };
    log(LogLevel::info, "Command returned exit code %lu", exitCode);
    return exitCode;
  }

  Configuration::~Configuration() = default;

}

#endif
