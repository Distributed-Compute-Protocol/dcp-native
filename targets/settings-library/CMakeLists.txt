# Copyright (c) 2023 Distributive Corp. All Rights Reserved.

set(DCP_SETTINGS_PROGRAM_NAME "${PROJECT_NAME}-settings" CACHE INTERNAL "")
set(DCP_SETTINGS_PROGRAM_FILE
  "${DCP_SETTINGS_PROGRAM_NAME}${CMAKE_EXECUTABLE_SUFFIX}" CACHE INTERNAL ""
)

set(DCP_SETTINGS_CONFIGURATOR_PROGRAM_NAME
  "${PROJECT_NAME}-settings-configurator" CACHE INTERNAL ""
)
set(DCP_SETTINGS_CONFIGURATOR_PROGRAM_FILE
  "${DCP_SETTINGS_CONFIGURATOR_PROGRAM_NAME}${CMAKE_EXECUTABLE_SUFFIX}"
  CACHE INTERNAL ""
)

set(_TARGET "${PROJECT_NAME}-settings-library")

add_library("${_TARGET}" STATIC)
dcp_target_initialize("${_TARGET}")

dcp_add_sources("${_TARGET}"
  FOLDER "Source Files"
  SUBDIRECTORY "source"
  FILES
  "source/dcp/settings/configuration.cc"
  "source/dcp/settings/configurator.cc"
)

dcp_add_sources("${_TARGET}"
  FOLDER "Include Files"
  SUBDIRECTORY "include"
  FILES
  "include/dcp/settings/configuration.hh"
  "include/dcp/settings/configurator.hh"
)

set_target_properties("${_TARGET}"
  PROPERTIES OUTPUT_NAME "${DCP_SETTINGS_PROGRAM_NAME}"
)

target_compile_definitions("${_TARGET}"
  PRIVATE
  "DCP_Settings_configuratorProgramFile=\"${DCP_SETTINGS_CONFIGURATOR_PROGRAM_FILE}\""
)

target_include_directories("${_TARGET}" PUBLIC "include")

target_link_libraries("${_TARGET}" PUBLIC "${PROJECT_NAME}-library")
