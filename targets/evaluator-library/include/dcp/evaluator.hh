/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       December 2019
 */

#if !defined(DCP_Evaluator_)
  #define DCP_Evaluator_

  #include <dcp/logger.hh>
  #include <dcp/socket.hh>

  #include <boost/filesystem/path.hpp>
  #include <boost/optional.hpp>

  #include <string>
  #include <vector>

namespace DCP {

  /** A thread-local singleton Javascript evaluator.
   *
   *  The Javascript global context defines the following functions:
   *    - `writeln`:
   *      A function that writes a string to the output socket.
   *    - `nextTimer`:
   *      A function that specifies when the next timer will become ready.
   *    - `onreadln`:
   *      A variable to hold a callback function to be invoked by
   *      @ref evaluateStream(SOCKET, Time::Milliseconds) when a line is read
   *      from input.
   *    - `ontimer`:
   *      A variable to hold a callback function to be invoked by
   *      @ref evaluateStream(SOCKET, Time::Milliseconds) when the timer becomes
   *      ready.
   *    - `die`:
   *      A function that, if invoked in
   *      @ref evaluateStream(SOCKET, Time::Milliseconds), terminates evaluation
   *      and prevents this evaluator from doing any further evaluation.
   */
  struct Evaluator {

    /// The name of the Evaluator program file.
    static char const *const programFile;

    /** Return the thread-local instance, constructing as necessary.
     *
     *  @param  output
     *          The output socket handle.
     *  @param  webGPU
     *          Whether to enable WebGPU.
     *  @param  webGPUDevice
     *          The name or substring of the GPU adapter device to use.
     *  @param  debuggingPort
     *          The port to listen for debugger connections on, if any.
     *  @param  implementationOptions
     *          Options to pass to the underlying implementation.
     *  @throw  std::invalid_argument
     *          If arguments are invalid or differ from those passed in the
     *          initial call.
     */
    static Evaluator &getInstance(
      Socket::Handle const &output = {},
      boost::optional<bool> webGPU = {},
      boost::optional<std::string> const &webGPUDevice = {},
      boost::optional<short unsigned> debuggingPort = {},
      std::vector<std::string> const &implementationOptions = {}
    );

    Evaluator(Evaluator const &) = delete;
    Evaluator(Evaluator const &&) = delete;

    Evaluator &operator =(Evaluator const &) = delete;
    Evaluator &operator =(Evaluator const &&) = delete;

    /// Get the output socket.
    [[nodiscard]]
    virtual Socket::Handle const &getOutput() const = 0;

    /// Get whether WebGPU is enabled.
    [[nodiscard]]
    virtual bool getWebGPU() const = 0;

    virtual int getExitCode() const = 0;

    /// Get debugging port, or none if debugging not enabled.
    [[nodiscard]]
    virtual boost::optional<short unsigned> getDebuggingPort() const = 0;

    /// Get the implementation options.
    [[nodiscard]]
    virtual
    std::vector<std::string> const &getImplementationOptions() const = 0;

    /** Evaluate the file.
     *
     *  @param  filePath
     *          The path of the Javascript file to read and evaluate.
     *  @return `true` if evaluation occurred, without error.
     */
    virtual bool evaluateFile(boost::filesystem::path const &filePath) = 0;

    /** Evaluate multiple files, in order.
     *
     *  @param  filePaths
     *          The paths of the Javavascript files to evaluate, in order.
     *  @return `true` if evaluation occurred to completion, without error.
     */
    template <typename FilePaths = std::vector<boost::filesystem::path>>
    bool evaluateFiles(FilePaths const &filePaths) {
      // std::all_of is not guaranteed to short-circuit.
      // NOLINTNEXTLINE(readability-use-anyofallof)
      for (auto const &filePath: filePaths) {
        if (!evaluateFile(filePath)) {
          return false;
        }
      }
      return true;
    }

    /** Evaluate the string.
     *
     *  @param  string
     *          A line of Javascript code to evaluate.
     *  @return `true` if evaluation occurred to completion, without error.
     */
    virtual bool evaluateString(char const *string) = 0;

    /// @copydoc evaluateString(char const *)
    bool evaluateString(std::string const &string) {
      return evaluateString(string.data());
    }

    /** Evaluate multiple strings, in order.
     *
     *  @param  strings
     *          The Javavascript strings to evaluate, in order.
     *  @return `true` if evaluation occurred to completion, without error.
     */
    template <typename Strings = std::vector<std::string>>
    bool evaluateStrings(Strings const &strings) {
      // std::all_of is not guaranteed to short-circuit.
      // NOLINTNEXTLINE(readability-use-anyofallof)
      for (auto const &string: strings) {
        if (!evaluateString(string)) {
          return false;
        }
      }
      return true;
    }

    /** Evaluate code read from an input socket.
     *
     *  This function runs a reactor that responds to the following events:
     *    - Receive a line of input   => trigger `onreadln` callback
     *    - Timer becomes ready       => trigger `ontimer` callback
     *    - Script invokes `die()`    => return
     *
     *  @param  input
     *          The input socket to read and evaluate Javascript code from.
     *  @param  maxTimeout
     *          The maximum reactor wait timeout, in milliseconds, before it
     *          loops again.
     *  @return `true` if evaluation occurred to completion, without error.
     */
    virtual bool evaluateStream(
      SOCKET input,
      Time::Milliseconds maxTimeout = Socket::defaultTimeout
    ) = 0;

    /** Convenience form of evaluate that does all evaluations in one call.
     *
     *  Performs file evaluations, then string evaluations, then input
     *  evaluation; returns `false` immediately if any of these fail.
     *
     *  @param  input
     *          The input socket to read and evaluate Javascript code from.
     *  @param  filePaths
     *          The paths of the Javavascript files to evaluate, in order.
     *  @param  strings
     *          The Javavascript strings to evaluate, in order.
     *  @param  maxTimeout
     *          The maximum reactor wait timeout, in milliseconds, before it
     *          loops again.
     *  @return `true` if evaluation occurred to completion, without error.
     */
    template <
      typename FilePaths = std::vector<boost::filesystem::path>,
      typename Strings = std::vector<std::string>
    >
    bool evaluate(
      SOCKET const input,
      FilePaths const &filePaths = {},
      Strings const &strings = {},
      Time::Milliseconds const maxTimeout = Socket::defaultTimeout
    ) {
      log(LogLevel::debug, "Evaluation starting...");
      if (!evaluateFiles(filePaths)) {
        log(LogLevel::debug, "File evaluation failed; exiting");
        return false;
      }
      if (!evaluateStrings(strings)) {
        log(LogLevel::debug, "String evaluation failed; exiting");
        return false;
      }
      if (!evaluateStream(input, maxTimeout)) {
        log(LogLevel::debug, "Input evaluation failed; exiting");
        return false;
      }
      log(LogLevel::debug, "Evaluation completed successfully");
      return true;
    }

    /// Blocking wait for a debugging connection and start the debugger.
    virtual void waitForDebugger() = 0;

    /** Terminate evaluation from any thread and prevent the evaluator from
     *  doing any further evaluation.
     *
     *  @return True if termination was fully successful, or false if any part
     *          of it failed.
     */
    virtual bool terminate() = 0;

    virtual ~Evaluator();

  protected:

    Evaluator() = default;

  };

}

#endif
