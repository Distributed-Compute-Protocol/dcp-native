/**
 *  @file
 *  @author     Severn Lortie, severn@distributive.network
 *  @date       April 2024
 */

#if !defined(DCP_V8_InspectorClient_)
  #define DCP_V8_InspectorClient_

  #include <dcp/websocket/server.hh>
  #include <dcp/v8/inspector_channel.hh>

  #include <v8-inspector.h>
  #include <v8-platform.h>

  #include <boost/beast/core.hpp>

  #include <atomic>

namespace DCP::V8 {

  struct InspectorClient final : v8_inspector::V8InspectorClient {

    /** Construct a new InspectorClient.
     *
     *  @param  platform
     *          The V8 platform instance.
     *  @param  context
     *          The V8 context.
     *  @param  port
     *          The port Chrome DevTools will connect to.
     */
    explicit InspectorClient(
      v8::Platform &platform,
      v8::Local<v8::Context> &context,
      short unsigned port
    );

    InspectorClient(InspectorClient const &) = delete;
    InspectorClient(InspectorClient const &&) = delete;

    InspectorClient &operator =(InspectorClient const &) = delete;
    InspectorClient &operator =(InspectorClient const &&) = delete;

    /// Poll the Websocket Server's event loop.
    void pollEventLoop();

    /// Block until a debugger connects and initializes.
    void waitForDebugger();

    /** Inherited InspectorClient method that gets called by the V8 thread when
     *  paused.
     */
    void runMessageLoopOnPause(int contextGroupID) override;

    /** Inherited InspectorClient method that gets called when Chrome DevTools
     *  has finished setup and is ready to start executing.
     */
    void runIfWaitingForDebugger(int contextGroupID) override;

    /** Inherited InspectorClient method that gets called when the debugger
     *  unpauses.
     */
    void quitMessageLoopOnPause() override;

    /// Accept a new debugger connection.
    void acceptConnection();

    /** Required by InspectorClient implementors.
     *
     *  @return The context group ID.
     */
    v8::Local<v8::Context> ensureDefaultContextInGroup(
      int contextGroupID
    ) override;

    [[nodiscard]]
    short unsigned getPort() const;

    /** Get a handle to the underlying socket in use.
     *
     *  @return The socket handle.
     */
    [[nodiscard]]
    boost::asio::detail::socket_type getSocket();

    /// Stops any blocking operations the InspectorClient is performing.
    void terminate();

    ~InspectorClient() override = default;

  private:

    /** Send a Chrome DevTools Protocol message to the Inspector.
     *
     *  @param  message
     *          The message to send
     */
    void dispatchProtocolMessage(boost::beast::flat_buffer const &message);

    /** Make V8 think that Chrome DevTools is connected to the inspector. This
     *  enables V8 to pause on breakpoints.
     */
    void fakeChromeDevToolsConnection();

    /** Convert a std::string to a V8Inspector StringView.
     *
     *  @param  string
     *          The string to convert.
     *  @return the StringView.
     */
    static v8_inspector::StringView convertToStringView(
      std::string const &string
    );

    /** The context group ID in the session.
     *
     *  Contexts in sessions are in groups, each with a unique non-zero ID.
     *  This code only creates a session with one context, so use group ID 1.
     */
    static int const sessionContextGroupID{1};

    Websocket::Server server;
    v8::Platform &platform;
    v8::Handle<v8::Context> context;
    v8::Isolate *isolate{nullptr};
    InspectorChannel channel;
    std::unique_ptr<v8_inspector::V8Inspector> inspector{};
    std::unique_ptr<v8_inspector::V8InspectorSession> session{};
    bool quitOnNextLoop{false};
    bool debuggerInitializationDone{false};
    std::atomic<bool> quitNow{false};

  };

}

#endif
