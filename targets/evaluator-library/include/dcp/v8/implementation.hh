/**
 *  @file
 *  @author     Wes Garland, wes@sparc.network
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2018
 */

#if !defined(DCP_V8_Implementation_)
  #define DCP_V8_Implementation_

  #include <dcp/socket.hh>

  #include <v8.h>

  #include <boost/optional.hpp>

namespace DCP::V8 {

  struct Evaluator;

  /// A singleton class that wraps the V8 Javascript engine.
  struct Implementation final {

    /// Return the static instance, constructing as necessary.
    static Implementation const &getInstance(
      std::vector<std::string> const &options = {}
    );

    Implementation(Implementation const &) = delete;
    Implementation(Implementation const &&) = delete;

    Implementation &operator =(Implementation const &) = delete;
    Implementation &operator =(Implementation const &&) = delete;

    /// Return the thread-local Evaluator instance, constructing as necessary.
    [[nodiscard]]
    Evaluator &getEvaluator(
      Socket::Handle const &output = {},
      boost::optional<bool> webGPU = {},
      boost::optional<std::string> const &webGPUDevice = {},
      boost::optional<short unsigned> debuggingPort = {}
    ) const;

    std::vector<std::string> const &getOptions() const {
      return options;
    }

    ~Implementation();

  private:

    explicit Implementation(std::vector<std::string> options);

    /// The V8 options.
    std::vector<std::string> const options{};

    /// The V8 platform instance.
    std::unique_ptr<v8::Platform> const platform{};

    /// The V8 array buffer allocator.
    std::unique_ptr<v8::ArrayBuffer::Allocator> arrayBufferAllocator{};

    /// The V8 array isolate creation parameters.
    v8::Isolate::CreateParams createParams{};

  };

}

#endif
