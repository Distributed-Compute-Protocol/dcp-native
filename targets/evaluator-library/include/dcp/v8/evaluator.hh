/**
 *  @file
 *  @author     Wes Garland, wes@sparc.network
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2018
 */

#if !defined(DCP_V8_Evaluator_)
  #define DCP_V8_Evaluator_

  #include <dcp/evaluator.hh>
  #include <dcp/socket/interrupter.hh>
  #include <dcp/v8/inspector_client.hh>
  #include <dcp/write_buffer.hh>

  #include <libplatform/libplatform.h>
  #include <v8.h>

  #include <napi.h>

  #include <atomic>

namespace DCP::V8 {

  /** A V8 implementation of DCP::Evaluator.
   *
   *  # V8 Evaluator
   *
   *  The V8 Evaluator is an implementation of the DCP::Evaluator interface
   *  using a V8 embedding.
   *
   *  ## Debugging Architecture
   *
   *  V8 has a built-in debugger called the Inspector. You can think of the
   *  Inspector like the Chrome DevTools (CDT) backend. CDT uses the
   *  [Chrome DevTools Protocol](https://chromedevtools.github.io/devtools-protocol/)
   *  to communicate with the Inspector. All debug requests and responses follow
   *  the format described in the protocol.
   *
   *  ### Connecting to the Inspector
   *
   *  V8 has no native way to listen for or send CDT messages. It leaves this
   *  job up to the application embedding V8 (the embedder). In the context of
   *  DCP, the native Evaluator is a V8 embedder. So, in order to use DevTools
   *  to debug the native Evaluator, we need to accomplish two things:
   *
   *  * Implement the necessary V8 interfaces to communicate with the
   *    Inspector.
   *  * Implement a simple Websocket server to send back and forth CDT
   *    messages.
   *
   *  Here is a diagram showing a high-level block diagram of the task:
   *
   *  @mermaid{dcp/v8/evaluator/debugging_protocol_forward}
   *
   *  Let's focus in on the interface classes. There are only two:
   *  InspectorClient and InspectorChannel.
   *
   *  * InspectorClient is responsible for reacting to Inspector events, namely
   *    InspectorClient::runMessageLoopOnPause(),
   *    InspectorClient::quitMessageLoopOnPause(), and
   *    InspectorClient::runIfWaitingForDebugger(). It can also send the
   *    Inspector protocol messages with
   *    InspectorClient::dispatchProtocolMessage().
   *  * InspectorChannel provides a communication channel for the
   *    Inspector by implementing InspectorChannel::sendResponse() and
   *    InspectorChannel::sendNotification().
   *
   *  @mermaid{dcp/v8/evaluator/debugging_interface_classes}
   *
   *  To learn about how these interfaces are used by V8, consider the following
   *  example. We have started up the Evaluator with debugging enabled, and a
   *  Chrome DevTools instance has connected to our Websocket Server. What takes
   *  place to kick off the session?
   *
   *  @mermaid{dcp/v8/evaluator/debugging_inspector_init}
   *
   *  Pictured above is the flow of messages between CDT and and the Inspector
   *  inside of V8. Initially, the Inspector and V8 will negotiate and share
   *  information about the session. Below is a table of some of the example
   *  initialization protocol messages.
   *
   *  | Method                        | Request                          | Response                                                 |
   *  |-------------------------------|----------------------------------|----------------------------------------------------------|
   *  | Runtime.enable                | {}                               | {}                                                       |
   *  | Debugger.enable               | {"maxScriptsCacheSize":10000000} | {"debuggerId":"1879962774923180722.2975528061218066022"} |
   *  | Debugger.setPauseOnExceptions | {"state":"none"}                 | {}                                                       |
   *
   *  Once everything is set up, CDT will send
   *  `Runtime.runIfWaitingForDebugger`. This request fires the corresponding
   *  InspectorClient::runIfWaitingForDebugger() on the InspectorClient. Once
   *  this method fires, we enter the main debug loop.
   *
   *  ### Operation During Script Execution
   *
   *  @mermaid{dcp/v8/evaluator/debugging_operation_during_execution}
   *
   *  The above diagram shows the operation of the debugger during script
   *  execution. The debugger can be in two main states, `paused` or `running`.
   *  The `paused` state is represented by executing
   *  InspectorClient::runMessageLoopOnPause(). In this function, we service V8
   *  background tasks with InspectorClient::pollEventLoop() and check for
   *  incoming CDT messages. All incoming CDT messages are forwarded to the V8
   *  inspector with InspectorClient::dispatchProtocolMessage(). Eventually, one
   *  of these messages will unpause the debugger, causing
   *  InspectorClient::quitMessageLoopOnPause() to be invoked.
   *
   *  ### Immediate Mode
   *
   *  So far, we have discussed the **Wait Mode** of the evaluator. To review,
   *  this is when CDT has to connect before any evaluation can start. This mode
   *  is useful, but will not suffice for debugging workers. The reason is that,
   *  after connecting to the evaluator, Workers expect to send and receive
   *  messages during their operation. They are in the *reactor* phase of the
   *  evaluator. This phase is when the Evaluator receives code or messages from
   *  a socket and then evaluates them. It "reacts" to input from the worker.
   *  However, waiting for a CDT connection blocks all main thread activity,
   *  including the reactor. To allow for debugging while connected to the
   *  worker, we need to let the evaluator immediately start executing and have
   *  CDT connect to it at a later time.
   *
   *  @mermaid{dcp/v8/evaluator/debugging_immediate_mode}
   *
   *  The above diagram describes the logical operation of the evaluator during
   *  Immediate Mode. Essentially, the evaluator executes code as normal until a
   *  CDT connection is established. If a breakpoint is encountered, the
   *  Evaluator will pause and wait for a resume request from CDT.
   *
   *  Achieving this behavior requires three steps:
   *
   *  1. Fool V8 into thinking it is connected to CDT before it actually is.
   *  2. Queue any messages from V8 before CDT connects.
   *  3. Add the Websocket server's sockets to the `select` statement in the
   *     Evaluator's reactor.
   *
   *  The first step is required due to an operational detail of V8. The engine
   *  will not react to any debugger statements in the script its executing if
   *  it is not connected to a CDT instance. This exists so breakpoints in
   *  JavaScript code you encounter while browsing the web don't actually pause
   *  your browser. Fortunately, making V8 think a CDT instance is connected is
   *  quite simple. We only need to send `Runtime.enable` and `Debugger.enable`
   *  protocol messages to it. After receipt of those messages, it will pause
   *  its execution on breakpoints.
   *
   *  The second step requires that outgoing V8 messages are queued when CDT is
   *  not connected. Queueing messages is required because CDT and V8 must
   *  remain in sync. For example, when V8 loads a script, it sends the message
   *  [Debugger.scriptParsed](https://chromedevtools.github.io/devtools-protocol/tot/Debugger/#event-scriptParsed).
   *  This message let's CDT know the script ID so it can request the source of
   *  it later. If this message is sent before CDT connects, and we don't send
   *  it later, the script source can never be loaded.
   *
   *  The third step is to include the Websocket Server's sockets in the
   *  `select` statement in the Evaluator's reactor pattern. The `select`
   *  statement blocks until there is activity on any of its input sockets, or a
   *  timeout is reached. When one has a message to read, the `select` will
   *  return. We need to add the Websocket Server's sockets to the list so we
   *  can break from the wait and service incoming CDT requests.
   *
   *  ### Quitting the Evaluator
   *
   *  The Evaluator uses a two-threaded mechanism to cleanly exit. A SIGINT
   *  handler is registered in the main thread. When it is called it sets an
   *  atomic `quitNow` variable to `true`. A watcher thread is spawned to poll
   *  the value of `quitNow`. If it is `true`, then Evaluator::terminate()
   *  is invoked on the Evaluator to begin clean shutdown.
   *
   *  @mermaid{dcp/v8/evaluator/debugging_quit}
   *
   *  Notably, the `exit()` function is never invoked. This means that ongoing
   *  operations *must* quit on their own for the program to naturally exit.
   *  Take the Websocket Server's `read()` method as an example. To read from
   *  the connected peer's input socket, we may be tempted to do something like:
   *  ```cpp
   *  void readFromSocketBad(&buffer) {
   *    socket.read(buffer); // This is a blocking call
   *  }
   *  ```
   *
   *  However, when the user sends the program a `SIGINT`, this blocking
   *  operation will prevent the program from exiting. For this reason, any
   *  blocking call needs to be rewritten as follows:
   *  ```cpp
   *  void readFromSocketGood(&buffer) {
   *    socket.non_blocking(true);
   *    socket.async_read(...);
   *    while (!quitNow) {
   *      io_context.poll();
   *    }
   *  }
   *  ```
   *
   *  If all blocking calls are written this way, the Evaluator can cleanly
   *  exit.
   */
  struct Evaluator final : DCP::Evaluator {

    /** Construct the Evaluator, including a global persistent context which
     *  will be used by the running code.
     *
     *  @throw  std::runtime_error
     *          The context couldn't be created.
     */
    explicit Evaluator(
      v8::Platform &platform,
      v8::Isolate::CreateParams const &createParams,
      Socket::Handle output,
      bool webGPU,
      std::string webGPUDevice,
      boost::optional<short unsigned> debuggingPort
    );

    Evaluator(Evaluator const &) = delete;
    Evaluator(Evaluator const &&) = delete;

    Evaluator &operator =(Evaluator const &) = delete;
    Evaluator &operator =(Evaluator const &&) = delete;

    [[nodiscard]]
    Socket::Handle const &getOutput() const override;

    [[nodiscard]]
    bool getWebGPU() const override;

    int getExitCode() const override;

    [[nodiscard]]
    boost::optional<short unsigned> getDebuggingPort() const override;

    [[nodiscard]]
    std::vector<std::string> const &getImplementationOptions() const override;

    bool evaluateFile(boost::filesystem::path const &filePath) override;

    bool evaluateString(char const *const string) override;

    bool evaluateStream(SOCKET in, Time::Milliseconds maxTimeout) override;

    bool terminate() override;

    void waitForDebugger() override;

    ~Evaluator() override;

  private:


    /** Used by Javascript code to initialize webgpu.
     *
     *  Calling this will initialize webgpu, and create a gpu device.
     *  Can only be called once, after which it will always return false
     *
     *  Returns to the JS context a promise that resolves with true when webgpu
     *  is initialized, or false if webgpu initialization failed.
     *
     */
    static void initWebGPU(
      v8::FunctionCallbackInfo<v8::Value> const &args
    ) noexcept;

    /** Used by Javascript code to implement a native print function.
     *
     *  Like `console.log`, this writes each argument to output, separated by
     *  spaces, followed by a newline. Non-string arguments are converted via
     *  `toString` for output, or skipped if conversion fails.
     */
    static void writeln(
      v8::FunctionCallbackInfo<v8::Value> const &args
    ) noexcept;

    /// Used by Javascript code to stop the reactor.
    static void die(
      [[maybe_unused]] v8::FunctionCallbackInfo<v8::Value> const &args
    ) noexcept;

    /** Used by Javascript code to set the evaluator's next wake time.
     *
     *  Setting this to zero will tell the reactor that there are no pending
     *  timers etc. Passing no arguments sets it to 'right now', which is
     *  useful for implementing `setTimeout(0)`,
     *  `window.requestAnimationFrame()`, etc.
     *
     *  @param  args
     *          When 0, the existing timer is cancelled; otherwise the timer
     *          is set to fire this many ms since the epoch. Timers in the
     *          past fire right away. When `args` is empty, the timer fires
     *          right away.
     */
    static void nextTimer(
      v8::FunctionCallbackInfo<v8::Value> const &args
    ) noexcept;

    /** Used by Javascript code to define the `ontimer` callback.
     *
     *  The function will be invoked by the reactor when there are timers that
     *  should need servicing based on the information provided to
     *  nextTimer().
     *
     *  @param  args
     *          The callback function.
     *  @note   The behaviour is undefined when the argument is not a
     *          function.
     */
    static void onTimer(
      v8::FunctionCallbackInfo<v8::Value> const &args
    ) noexcept;

    /** Used by Javascript code to define the `onreadln` callback.
     *
     *  The function will be invoked by the reactor every time a complete line
     *  of data is read from the input. If none, it will be evaluated.
     *
     *  @param  args
     *          The callback function.
     *  @note   The behaviour is undefined when the argument is not a
     *          function.
     */
    static void onReadln(
      v8::FunctionCallbackInfo<v8::Value> const &args
    ) noexcept;

    /** Reset the given callback function.
     *
     *  @param  callback
     *          The callback variable to reset.
     *  @param  args
     *          The callback function.
     *  @note   The behaviour is undefined when the argument is not a
     *          function.
     */
    void resetCallback(
      v8::Global<v8::Function> &callback,
      v8::FunctionCallbackInfo<v8::Value> const &args
    );

    /** Invoke the given callback function.
     *
     *  @param  callback
     *          The callback variable to invoke as a function.
     *  @param  argc
     *          The number of arguments.
     *  @param  argv
     *          The argument array.
     *  @return `true` if a callback was invoked, or `false` if empty.
     */
    bool invokeCallback(
      v8::Global<v8::Function> const &callback,
      int const argc,
      v8::Local<v8::Value> *const argv
    );

    /// Report a V8 exception.
    void reportException(v8::TryCatch const &tryCatch);

    /** Evaluate Javascript source.
     *
     *  @return `false` if there was a Javascript exception.
     */
    bool evaluateSource(v8::Local<v8::String> const &source);

    /** A reactor that implements
     *  @ref evaluateStream(SOCKET, Time::Milliseconds).
     *
     *  @return `false` if quit before started.
     */
    bool react(
      // NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
      SOCKET input, Time::Milliseconds maxTimeout
    );

    void flushWriteBuffer();

    /** Whether @ref die() has been called in the current evaluation,
     *  or evaluation was terminated.
     */
    std::atomic<bool> quitNow{false};

    /// The output socket.
    Socket::Handle const output{};

    // Whether WebGPU is enabled.
    bool webGPU{};

    // Specify the name or substring of the GPU adapter device to use.
    std::string webGPUDevice{};

    // Whether WebGPU has been initialized.
    bool webGPUInitialized{};

    bool dieFailure{};

    /** A socket added to inputs to Socket::wait calls so that @ref terminate
     *  can use it to interrupt blocking calls immediately.
     */
    Socket::Interrupter interrupter{};

    /// The write buffer.
    WriteBuffer writeBuffer{};

    /// The next wake time, in milliseconds since the epoch.
    Time::Milliseconds nextWakeTime{};

    /// The V8 platform.
    v8::Platform &platform;
    /// The V8 isolate.
    v8::Isolate &isolate;
    /// The Node-API environment.
    Napi::Env env{nullptr};

    /// The inspector (debugger) client.
    std::unique_ptr<InspectorClient> inspectorClient{};

    /// The onTimer callback, invoked from Javascript.
    v8::Global<v8::Function> onTimerCallback{};
    /// The onReadln callback, invoked from Javascript.
    v8::Global<v8::Function> onReadlnCallback{};

  };

}

#endif
