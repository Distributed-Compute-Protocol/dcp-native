/**
 *  @file
 *  @author     Severn Lortie, severn@distributive.network
 *  @date       April 2024
 */

#if !defined(DCP_V8_InspectorChannel_)
  #define DCP_V8_InspectorChannel_

  #include <dcp/websocket/server.hh>

  #include <v8-inspector.h>

namespace DCP::V8 {

  struct InspectorChannel final : v8_inspector::V8Inspector::Channel {

    /** Construct a new InspectorChannel.
     *
     *  @param  server
     *          A Websocket Server reference to inject.
     */
    explicit InspectorChannel(Websocket::Server &server);

    InspectorChannel(InspectorChannel const &) = delete;
    InspectorChannel(InspectorChannel const &&) = delete;

    InspectorChannel &operator =(InspectorChannel const &) = delete;
    InspectorChannel &operator =(InspectorChannel const &&) = delete;

    /** V8 Inspector calls when response is ready.
     *
     *  @param  callID
     *          Ignored.
     *  @param  message
     *          A V8Inspector StringBuffer containing the message.
     *          Can contain either utf-8 or utf-16.
     */
    void sendResponse(
      int callID,
      std::unique_ptr<v8_inspector::StringBuffer> message
    ) override;

    /** V8 Inspector calls when notification is ready.
     *
     *  @param  message
     *          A V8Inspector StringBuffer containing the message.
     *          Can contain either utf-8 or utf-16.
     */
    void sendNotification(
      std::unique_ptr<v8_inspector::StringBuffer> message
    ) override;

    /// Required to override this method, just a NOOP.
    void flushProtocolNotifications() override {}

    ~InspectorChannel() override = default;

  private:

    /** Send a message to the connected peer.
     *
     *  @param  message
     *          A V8Inspector StringBuffer containing the message. Can contain
     *          either utf-8 or utf-16. Message cannot be null.
     */
    void send(std::unique_ptr<v8_inspector::StringBuffer> const &message);

    Websocket::Server &server;

  };

}

#endif
