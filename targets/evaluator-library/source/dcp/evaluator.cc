/**
 *  @file
 *  @author     Wes Garland, wes@sparc.network
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2018
 */

#include <dcp/evaluator.hh>

#include <dcp/v8/evaluator.hh>
#include <dcp/v8/implementation.hh>

namespace DCP {

  char const *const Evaluator::programFile = DCP_Evaluator_programFile;

  Evaluator &Evaluator::getInstance(
    Socket::Handle const &output,
    boost::optional<bool> const webGPU,
    boost::optional<std::string> const &webGPUDevice,
    boost::optional<short unsigned> const debuggingPort,
    std::vector<std::string> const &implementationOptions
  ) {
    return V8::Implementation::getInstance(implementationOptions).getEvaluator(
      output, webGPU, webGPUDevice, debuggingPort
    );
  }

  Evaluator::~Evaluator() = default;

}
