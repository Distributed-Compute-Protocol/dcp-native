/**
 *  @file
 *  @author     Wes Garland, wes@sparc.network
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2018
 */

#include <dcp/v8/evaluator.hh>

#include <dcp/file_reader.hh>
#include <dcp/number.hh>
#include <dcp/read_buffer.hh>
#include <dcp/v8/implementation.hh>

#include <js_native_api_v8.h>

#include <boost/exception/diagnostic_information.hpp>
#include <boost/numeric/conversion/cast.hpp>

NAPI_MODULE_IMPORT Napi::Object Initialize(Napi::Env env, Napi::Object exports);

namespace DCP::V8 {

  namespace {

    /// Read into memory provided.
    bool readToMemory(
      SOCKET const input,
      char *const memory,
      size_t &size
    ) {
      assert(memory);
      bool const result = Socket::read(input, memory, size);
      if (Logger::getInstance().willLog(LogLevel::debug) && (0 < size)) {
        std::string string(memory, size);
        String::escape(string);
        log(LogLevel::debug, "rx %zu byte(s): %s", size, string.data());
      }
      return result;
    }

    /// Write from memory provided.
    void writeFromMemory(
      SOCKET const output,
      char const *const memory,
      size_t &size
    ) {
      assert(memory);
      Socket::write(output, memory, size);
      if (Logger::getInstance().willLog(LogLevel::debug) && (0 < size)) {
        std::string string(memory, size);
        String::escape(string);
        log(LogLevel::debug, "tx %zu byte(s): %s", size, string.data());
      }
    }

  }

  Evaluator::Evaluator(
    v8::Platform &platform,
    v8::Isolate::CreateParams const &createParams,
    Socket::Handle output,
    bool const webGPU,
    std::string webGPUDevice,
    boost::optional<short unsigned> const debuggingPort
  ) :
  output{
    [output{std::move(output)}]() {
      if (output == INVALID_SOCKET) {
        throw std::invalid_argument(
          "The evaluator output socket must be a valid socket"
        );
      }
      return output;
    }()
  },
  webGPU{webGPU},
  webGPUDevice{std::move(webGPUDevice)},
  platform{platform},
  isolate{
    [&createParams]() -> v8::Isolate & {
      v8::Isolate *const isolatePointer = v8::Isolate::New(createParams);
      if (!isolatePointer) {
        throw std::logic_error("The isolate could not be created");
      }
      return *isolatePointer;
    }()
  }
  {
    isolate.Enter();
    {
      v8::HandleScope handleScope(&isolate);

      v8::Local<v8::ObjectTemplate> global = v8::ObjectTemplate::New(
        &isolate
      );
      global->Set(
        v8::String::NewFromUtf8Literal(
          &isolate, "writeln", v8::NewStringType::kNormal
        ),
        v8::FunctionTemplate::New(&isolate, writeln)
      );
      global->Set(
        v8::String::NewFromUtf8Literal(
          &isolate, "onreadln", v8::NewStringType::kNormal
        ),
        v8::FunctionTemplate::New(&isolate, onReadln)
      );
      global->Set(
        v8::String::NewFromUtf8Literal(
          &isolate, "nextTimer", v8::NewStringType::kNormal
        ),
        v8::FunctionTemplate::New(&isolate, nextTimer)
      );
      global->Set(
        v8::String::NewFromUtf8Literal(
          &isolate, "ontimer", v8::NewStringType::kNormal
        ),
        v8::FunctionTemplate::New(&isolate, onTimer)
      );
      global->Set(
        v8::String::NewFromUtf8Literal(
          &isolate, "die", v8::NewStringType::kNormal
        ),
        v8::FunctionTemplate::New(&isolate, die)
      );
      global->Set(
        v8::String::NewFromUtf8Literal(
          &isolate, "initWebGPU", v8::NewStringType::kNormal
        ),
        v8::FunctionTemplate::New(&isolate, initWebGPU)
      );

      v8::Local<v8::Context> context = v8::Context::New(
        &isolate, nullptr, global
      );
      if (context.IsEmpty()) {
        throw std::runtime_error("Error creating context");
      }

      context->Enter();

      if (debuggingPort) {
        inspectorClient = std::make_unique<InspectorClient>(
          platform, context, *debuggingPort
        );
      }

      env = {new napi_env__(context)};

      // Create navigator.
      auto navigator = Napi::Object::New(env);
      navigator.DefineProperty(
        Napi::PropertyDescriptor::Value(
          Napi::String::New(env, "userAgent"),
          Napi::String::New(env, "dcp-evaluator/7"),
          static_cast<napi_property_attributes>(
            napi_writable | napi_configurable | napi_enumerable
          )
        )
      );
      env.Global().DefineProperty(
        Napi::PropertyDescriptor::Value(
          Napi::String::New(env, "navigator"), navigator,
          static_cast<napi_property_attributes>(
            napi_writable | napi_configurable | napi_enumerable
          )
        )
      );
    }
    log(LogLevel::debug, "V8 Evaluator: constructed");
  }

  Socket::Handle const &Evaluator::getOutput() const {
    return output;
  }

  bool Evaluator::getWebGPU() const {
    return webGPU;
  }

  int Evaluator::getExitCode() const {
    const auto failCode = 123;
    return dieFailure ? failCode : 0;
  }

  boost::optional<short unsigned> Evaluator::getDebuggingPort() const {
    if (!inspectorClient) {
      return boost::none;
    }
    return inspectorClient->getPort();
  }

  std::vector<std::string> const &Evaluator::getImplementationOptions() const {
    return Implementation::getInstance().getOptions();
  }

  bool Evaluator::evaluateFile(boost::filesystem::path const &filePath) {
    log(LogLevel::debug, "Evaluating file \"%s\"...",
      filePath.string().data()
    );
    v8::HandleScope handleScope(&isolate);
    v8::Local<v8::String> source;
    std::string const &filePathString = filePath.string();
    log(LogLevel::debug, "Opening file \"%s\" at time %llu...",
      filePathString.data(), Time::now()
    );
    {
      FileReader fileReader(filePath);
      log(LogLevel::debug, "Opened file \"%s\" at time %llu",
        filePathString.data(), Time::now()
      );
      if (
        !v8::String::NewFromUtf8(
          &isolate,
          fileReader.getData(),
          v8::NewStringType::kNormal,
          boost::numeric_cast<int>(fileReader.getSize())
        ).ToLocal(&source)
      ) {
        log(LogLevel::error, "Error creating V8 string from: %s", source);
        return false;
      }
      log(LogLevel::debug, "Closing file \"%s\" at time %llu...",
        filePathString.data(), Time::now()
      );
    }
    log(LogLevel::debug, "Closed file \"%s\" at time %llu",
      filePathString.data(), Time::now()
    );
    return evaluateSource(source);
  }

  bool Evaluator::evaluateString(char const *const string) {
    assert(string);
    log(LogLevel::debug, "Evaluating string \"%s\"...", string);
    v8::HandleScope handleScope(&isolate);
    if (
      v8::Local<v8::String> local;
      v8::String::NewFromUtf8(
        &isolate, string, v8::NewStringType::kNormal
      ).ToLocal(&local)
    ) {
      return evaluateSource(local);
    }
    log(LogLevel::error, "Error creating V8 string from: %s", string);
    return false;
  }

  bool Evaluator::evaluateStream(SOCKET in, Time::Milliseconds maxTimeout) {
    return react(in, maxTimeout);
  }

  bool Evaluator::terminate() {
    bool success = true;

    quitNow = true;

    if (isolate.IsExecutionTerminating()) {
      tryToLog(LogLevel::debug, "Evaluator already terminating...");
      return true;
    }

    if (inspectorClient) {
      tryToLog(LogLevel::debug,
        (
          "Terminating inspector client for evaluator with output socket"
          " %" DCP_Socket_print "..."
        ),
        static_cast<SOCKET>(output)
      );
      try {
        inspectorClient->terminate();
      } catch (...) {
        success = false;
        tryToLog(LogLevel::error,
          (
            "Error terminating inspector client for evaluator with output"
            " socket %" DCP_Socket_print ": %s"
          ),
          static_cast<SOCKET>(output),
          boost::current_exception_diagnostic_information().data()
        );
      }
    }

    tryToLog(LogLevel::debug,
      "Interrupting evaluator with output socket %" DCP_Socket_print "...",
      static_cast<SOCKET>(output)
    );
    if (!interrupter.invoke()) {
      success = false;
      tryToLog(LogLevel::warning,
        "Interruption of evaluator with output socket %" DCP_Socket_print
        " failed",
        static_cast<SOCKET>(output)
      );
    }

    tryToLog(LogLevel::debug,
      (
        "Terminating Javascript execution"
        " for evaluator with output socket %" DCP_Socket_print "..."
      ),
      static_cast<SOCKET>(output)
    );
    try {
      // Google V8 has a no-exception policy and is compiled without exceptions.
      // It's more likely to call `abort()`, which is why this is performed last
      // in the function. Catch exceptions just in case this policy ever changes
      // in future V8 implementations.
      isolate.TerminateExecution();
    } catch (...) {
      success = false;
      tryToLog(LogLevel::error,
        (
          "Error terminating Javascript execution for evaluator with output"
          " socket %" DCP_Socket_print ": %s"
        ),
        static_cast<SOCKET>(output),
        boost::current_exception_diagnostic_information().data()
      );
    }

    return success;
  }

  void Evaluator::waitForDebugger() {
    inspectorClient->waitForDebugger();
  }

  Evaluator::~Evaluator() {
    tryToLog(LogLevel::debug, "V8 Evaluator: destructing");

    inspectorClient.reset();

    if (webGPU && webGPUInitialized) {
      Napi::HandleScope handleScope(env);
    }

    {
      v8::HandleScope handleScope(&isolate);

      v8::Local<v8::Context> context = isolate.GetCurrentContext();
      context->Exit();
    }

    assert(static_cast<napi_env>(env)->refs == 1);
#if BOOST_OS_WINDOWS
    if (!webGPU) // TODO: Results in a hang on Windows (DCP-3487)
#endif
    {
      static_cast<napi_env>(env)->Unref();
    }

    onTimerCallback.Reset();
    onReadlnCallback.Reset();

    isolate.Exit();
    isolate.Dispose();

    // If anything is in the output buffer, give flushing it one last try.
    try {
      flushWriteBuffer();
    } catch (...) {
      tryToLog(LogLevel::notice, "Error flushing write buffer: %s",
        boost::current_exception_diagnostic_information().data()
      );
    }
  }

  void Evaluator::initWebGPU(
    v8::FunctionCallbackInfo<v8::Value> const &args
  ) noexcept {
    try {
      Evaluator &evaluator = Implementation::getInstance().getEvaluator();

      if (evaluator.webGPU && !evaluator.webGPUInitialized) {
        evaluator.webGPUInitialized = true;

        // Dawn implementation requires setImmediate be defined in
        // order to require a device. Introduced in dawn commit d5783b8.
        auto setImmediate = evaluator.env.Global().Has("setImmediate");
        if (!setImmediate) {
          log(LogLevel::debug,
            "setImmediate not defined when initializing webgpu"
          );
          args.GetReturnValue().Set(false);
          return;
        }

        log(LogLevel::debug, "WebGPU: initializing...");
        Napi::HandleScope scope(evaluator.env);

        // Initialize the JS environment for Dawn.
        auto exports = (
          Initialize(evaluator.env, Napi::Object::New(evaluator.env))
        );

        // Dawn commit `ebc8114aab` introduced a breaking change to
        // `Initialize`: instead of adding all global objects to the
        // environment's global object, it now adds them to a `globals`
        // property of the returned exports object. As such, if the returned
        // exports object has a "globals" property, copy all of its properties
        // into the environment's global object.
        if (exports.HasOwnProperty("globals")) {
          // Unfortunately, the property iterator isn't available in the
          // version of node-addon-api that we need to use.
          auto const &globals = exports.Get("globals").As<Napi::Object>();
          auto const &globalNames = globals.GetPropertyNames();
          uint32_t const globalNameCount{globalNames.Length()};
          for (
            uint32_t globalNameIndex{0U};
            globalNameIndex < globalNameCount;
            ++globalNameIndex
          ) {
            auto const &globalName = globalNames.Get(
              std::to_string(globalNameIndex)
            );
            if (globals.HasOwnProperty(globalName)) {
              evaluator.env.Global().DefineProperty(
                Napi::PropertyDescriptor::Value(
                  globalName, globals.Get(globalName),
                  static_cast<napi_property_attributes>(
                    napi_writable | napi_configurable | napi_enumerable
                  )
                )
              );
            }
          }
        }

        // Set the Dawn flags to be passed in,
        // in the array form of `["foo=a", "bar=b,c"]`
        auto flags = Napi::Array::New(evaluator.env);
        if (!evaluator.webGPUDevice.empty()) { // --webgpu-device= flag is set
          flags[0U] = Napi::String::New(evaluator.env,
            "adapter=" + evaluator.webGPUDevice
          );
          tryToLog(LogLevel::debug, "WebGPU: using GPU device: \"%s\"",
            evaluator.webGPUDevice.data()
          );
        } else {
          tryToLog(LogLevel::debug, "WebGPU: using the default GPU device");
        }

        // Call the Dawn "create" function to get the GPU instance.
        auto create = exports.Get("create").As<Napi::Function>();
        auto gpu = create.Call(
          evaluator.env.Global(), {flags}
        ).As<Napi::Object>();

        // Set navigator.gpu.
        evaluator.env.Global().Get("navigator").As<Napi::Object>()
        .DefineProperty(
          Napi::PropertyDescriptor::Value(
            Napi::String::New(evaluator.env, "gpu"), gpu,
            static_cast<napi_property_attributes>(
              napi_writable | napi_configurable | napi_enumerable
            )
          )
        );

        args.GetReturnValue().Set(true);
      }
      else {
        args.GetReturnValue().Set(false);
      }
    } catch (...) {
      tryToLog(LogLevel::warning, "initWebGPU: Error: %s",
        boost::current_exception_diagnostic_information().data()
      );
      args.GetReturnValue().Set(false);
    }
  }

  void Evaluator::writeln(
    v8::FunctionCallbackInfo<v8::Value> const &args
  ) noexcept {
    try {
      Evaluator &evaluator = Implementation::getInstance().getEvaluator();
      assert(&evaluator.isolate == args.GetIsolate());
      bool empty = true;
      for (int i = 0; i < args.Length(); ++i) {
        v8::HandleScope const handleScope(&evaluator.isolate);

        v8::String::Utf8Value const utf8String(&evaluator.isolate, args[i]);
        if (char const *const cString = *utf8String) {
          if (!empty) {
            evaluator.writeBuffer.push(" ");
          }
          evaluator.writeBuffer.push(
            std::string(cString,
              boost::numeric_cast<size_t>(utf8String.length())
            )
          );
          empty = false;
        } else {
          log(LogLevel::debug,
            "writeln: Argument %i could not be converted to a string.", i
          );
        }
      }
      evaluator.writeBuffer.push("\n");

      // If there is anything in the write buffer, attempt to flush it.
      evaluator.flushWriteBuffer();
    } catch (...) {
      // Log writeln errors at debug level, rather than warning, because they
      // may be due to the receiver having since legitimately disconnected.
      tryToLog(LogLevel::debug, "writeln: Error: %s",
        boost::current_exception_diagnostic_information().data()
      );
    }
  }

  void Evaluator::die(
    [[maybe_unused]] v8::FunctionCallbackInfo<v8::Value> const &args
  ) noexcept {
    try {
      Implementation::getInstance().getEvaluator().quitNow = true;

      Evaluator &evaluator = Implementation::getInstance().getEvaluator();
      evaluator.dieFailure = (
        (args.Length() == 0) ? false :
        args[0]->BooleanValue(&evaluator.isolate)
      );
      DCP::tryToLog(DCP::LogLevel::debug, "Evaluator terminating...");
      if (Implementation::getInstance().getEvaluator().terminate()) {
        DCP::log(DCP::LogLevel::debug, "Evaluator terminated");
      } else {
        DCP::log(DCP::LogLevel::warning,
          "Evaluator may not have fully terminated"
        );
      }

    } catch (...) {
      tryToLog(LogLevel::warning, "die: Error: %s",
        boost::current_exception_diagnostic_information().data()
      );
    }
  }

  void Evaluator::nextTimer(
    v8::FunctionCallbackInfo<v8::Value> const &args
  ) noexcept {
    try {
      Evaluator &evaluator = Implementation::getInstance().getEvaluator();
      assert(&evaluator.isolate == args.GetIsolate());
      v8::HandleScope handleScope(&evaluator.isolate);

      v8::Local<v8::Context> context = (
        evaluator.isolate.GetCurrentContext()
      );
      evaluator.nextWakeTime = (
        (args.Length() == 0) ? Time::now() :
        static_cast<Time::Milliseconds>(
          args[0]->NumberValue(context).FromMaybe(0.0)
        )
      );
    } catch (...) {
      tryToLog(LogLevel::warning, "nextTimer: Error: %s",
        boost::current_exception_diagnostic_information().data()
      );
    }
  }

  void Evaluator::onTimer(
    v8::FunctionCallbackInfo<v8::Value> const &args
  ) noexcept {
    try {
      Evaluator &evaluator = Implementation::getInstance().getEvaluator();
      evaluator.resetCallback(evaluator.onTimerCallback, args);
    } catch (...) {
      tryToLog(LogLevel::warning, "onTimer: Error: %s",
        boost::current_exception_diagnostic_information().data()
      );
    }
  }

  void Evaluator::onReadln(
    v8::FunctionCallbackInfo<v8::Value> const &args
  ) noexcept {
    try {
      Evaluator &evaluator = Implementation::getInstance().getEvaluator();
      evaluator.resetCallback(evaluator.onReadlnCallback, args);
    } catch (...) {
      tryToLog(LogLevel::warning, "onReadln: Error: %s",
        boost::current_exception_diagnostic_information().data()
      );
    }
  }

  void Evaluator::resetCallback(
    v8::Global<v8::Function> &callback,
    v8::FunctionCallbackInfo<v8::Value> const &args
  ) {
    assert(&isolate == args.GetIsolate());
    v8::HandleScope handleScope(&isolate);

    v8::Local<v8::Value> arg;
    if (args.Length() == 0) {
      arg = v8::Undefined(&isolate);
    } else {
      arg = args[0];
      if (!arg->IsFunction()) {
        arg = v8::Undefined(&isolate);
      }
    }
    callback.Reset(&isolate, v8::Local<v8::Function>::Cast(arg));
  }

  bool Evaluator::invokeCallback(
    v8::Global<v8::Function> const &callback,
    int const argc,
    v8::Local<v8::Value> *const argv
  ) {
    if (callback.IsEmpty()) {
      return false;
    }
    v8::HandleScope handleScope(&isolate);

    v8::Local<v8::Context> context = isolate.GetCurrentContext();
    v8::Local<v8::Function> function = v8::Local<v8::Function>::New(
      &isolate, callback
    );
    v8::Local<v8::Value> result;
    v8::TryCatch tryCatch(&isolate);
    if (
      !function->Call(context, context->Global(), argc, argv).ToLocal(
        &result
      )
    ) {
      reportException(tryCatch);
    }
    return true;
  }

  void Evaluator::reportException(v8::TryCatch const &tryCatch) {
    v8::HandleScope handleScope(&isolate);

    v8::String::Utf8Value exception(&isolate, tryCatch.Exception());
    char const *const exceptionString = (
      (*exception ? *exception : "Unknown exception")
    );
    assert(exceptionString);

    v8::Local<v8::Message> message = tryCatch.Message();
    if (message.IsEmpty()) {
      log(LogLevel::debug, exceptionString);
      return;
    }

    std::stringstream ss;
    v8::Local<v8::Context> context = isolate.GetCurrentContext();

    v8::String::Utf8Value fileName(
      &isolate, message->GetScriptOrigin().ResourceName()
    );
    if (*fileName) {
      int const lineNumber = message->GetLineNumber(context).FromMaybe(-1);
      ss << *fileName << ":" << lineNumber << ": ";
    }
    ss << exceptionString;

    if (
      v8::Local<v8::String> sourceLineLocal;
      message->GetSourceLine(context).ToLocal(&sourceLineLocal)
    ) {
      v8::String::Utf8Value sourceLine(&isolate, sourceLineLocal);
      if (*sourceLine) {
        ss << std::endl;
        ss << *sourceLine;

        int const start = message->GetStartColumn(context).FromMaybe(0);
        int const end = message->GetEndColumn(context).FromMaybe(
          Number::clamp<int>(strlen(*sourceLine))
        );
        int index = 0;
        if (index < end) {
          ss << std::endl;
          for (; index < start; ++index) {
            ss << ' ';
          }
          for (; index < end; ++index) {
            ss << '^';
          }
        }
      }
    }

    v8::Local<v8::Value> stackTraceString;
    if (
      tryCatch.StackTrace(context).ToLocal(&stackTraceString) &&
      stackTraceString->IsString() &&
      v8::Local<v8::String>::Cast(stackTraceString)->Length() > 0
    ) {
      v8::String::Utf8Value stackTrace(&isolate, stackTraceString);
      if (*stackTrace) {
        ss << std::endl;
        ss << *stackTrace;
      }
    }

    log(LogLevel::debug, "%s", ss.str().data());
  }

  bool Evaluator::evaluateSource(v8::Local<v8::String> const &source) {
    if (quitNow) {
      log(LogLevel::debug, "Skipping source evaluation: quitting...");
      return false;
    }

    bool success = true;
    {
      v8::HandleScope handleScope(&isolate);

      v8::Local<v8::Context> context = isolate.GetCurrentContext();
      v8::TryCatch tryCatch(&isolate);
      v8::Local<v8::Script> script;
      if (v8::Script::Compile(context, source).ToLocal(&script)) {
        v8::Local<v8::Value> result;
        if (!script->Run(context).ToLocal(&result)) {
          reportException(tryCatch);
          success = false;
        } else if (!result->IsUndefined()) {
          v8::String::Utf8Value utf8Result(&isolate, result);
          log(LogLevel::debug, "Evaluation result: %s",
            *utf8Result ? *utf8Result : "<result conversion failed>"
          );
        }
      } else {
        log(LogLevel::notice, "Error evaluating source");
        reportException(tryCatch);
        success = false;
      }
    }
    try {
      flushWriteBuffer();
    } catch (...) {
      log(LogLevel::notice, "Error flushing write buffer: %s",
        boost::current_exception_diagnostic_information().data()
      );
    }
    log(LogLevel::debug, "Finished evaluating source: %s",
      success ? "successful" : "exception thrown"
    );
    return success;
  }

  // NOLINTNEXTLINE(readability-function-cognitive-complexity)
  bool Evaluator::react(
    // NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
    SOCKET input, Time::Milliseconds maxTimeout
  ) {
    // The minimum reactor timeout, in milliseconds.
    // This value is based on the event loop timeout clamp in Chrome.
    static Time::Milliseconds const minTimeout = 4;

    if (quitNow) {
      log(LogLevel::debug, "Skipping reactor evaluation: quitting...");
      return false;
    }

    // Loop until quit, with the buffers flushed as much as possible.
    log(LogLevel::debug, "Reactor starting...");
    ReadBuffer readBuffer;
    bool didFinishReading = false;
    bool didFinishProcessing = false;
    do {
      // If not quitting, pop a line from the read buffer and process.
      bool didProcessLine = false;
      if (!quitNow) {
        if (char const *const ln = readBuffer.pop()) {
          log(LogLevel::trace, "Reactor processing line from read buffer...");
          v8::HandleScope handleScope(&isolate);
          if (
            v8::Local<v8::String> line;
            v8::String::NewFromUtf8(
              &isolate, ln, v8::NewStringType::kNormal
            ).ToLocal(&line)
          ) {
            v8::Local<v8::Value> arg(line);
            if (!invokeCallback(onReadlnCallback, 1, &arg)) {
              log(LogLevel::debug,
                "Reactor: no 'onreadln' callback defined; evaluating..."
              );
              evaluateSource(line);
            }
            didProcessLine = true;
          } else {
            log(LogLevel::error, "Error creating V8 string from: %s", ln);
          }
        }
      }

      // If not quitting, pump messages to prevent a backlog.
      // Note that the above code can set `quitNow` to true.
      bool didProcessTask = false;
      if (!quitNow) {
        log(LogLevel::trace, "Reactor processing tasks in message loop...");
        while (v8::platform::PumpMessageLoop(&platform, &isolate)) {
          didProcessTask = true;
          isolate.PerformMicrotaskCheckpoint();
        }
      }

      // If quitting, stop reading from the input socket.
      // Note that the above code can set `quitNow` to true.
      if (quitNow) {
        didFinishReading = true;
      }

      // Wait for socket readiness: flush the write buffer and determine
      // readiness of the input socket (if reading isn't yet finished).
      // If there is an exception due to an unrecoverable wait or write
      // error, the exception is propagated from this function: doing
      // anything further here is pointless if output doesn't work.
      Socket::SetTuple readySockets;
      do {
        // Determine which sockets to monitor. If none, break out of the
        // loop.
        Socket::SetTuple waitingSockets;
        if (!didFinishReading) {
          log(LogLevel::trace, "Reactor awaiting input...");
          waitingSockets.input.insert(input);
          waitingSockets.error.insert(input);
        }
        if (!writeBuffer.isEmpty()) {
          log(LogLevel::trace, "Reactor awaiting output...");
          waitingSockets.output.insert(output);
        }
        if (waitingSockets.input.empty() && waitingSockets.output.empty()) {
          log(LogLevel::trace, "Reactor neither awaiting input nor output");
          break;
        }

        // Add the interrupter socket to the inputs so that waiting is
        // stopped immediately on termination.
        waitingSockets.input.insert(interrupter);

        // If there is an inspector client, add its socket to the inputs.
        if (inspectorClient) {
          waitingSockets.input.insert(inspectorClient->getSocket());
        }

        // Determine wait timeout.
        Time::Milliseconds const start = Time::now();
        Time::Milliseconds const timeout = (
          // If quitting, don't block so that things can wrap up ASAP.
          // If a line or task was processed, don't block in case there is
          // another one ready to go on the next reactor iteration.
          (quitNow || didProcessLine || didProcessTask) ? 0 :
          // Get ready to serve the event as soon as they arrive.
          (isolate.HasPendingBackgroundTasks()) ? minTimeout :
          // If there is no next wake time, block for the maximum time.
          (!nextWakeTime) ? maxTimeout :
          // If the next wake time is past (or almost past), block for
          // the minimum time.
          (nextWakeTime < (start + minTimeout)) ? minTimeout :
          // Otherwise, block until the next wake time (or the maximum
          // time, if less).
          std::min(maxTimeout, nextWakeTime - start)
        );

        // Wait until a either monitored socket is ready or timeout elapses,
        // and allow unrecoverable wait exceptions to propagate.
        log(LogLevel::trace, "Reactor waiting for up to %llu ms...",
          timeout
        );
        readySockets = Socket::wait(std::move(waitingSockets), timeout);
        log(LogLevel::trace, "Reactor waited for %llu ms",
          (Time::now() - start)
        );

        // If an exceptional input was received, start quitting.
        if (!readySockets.error.empty()) {
          log(LogLevel::notice,
            "Reactor received an exceptional input condition; quitting..."
          );
          didFinishReading = quitNow = true;
        }

        // If there is an inspector client, service it if not quitting.
        if (inspectorClient && !quitNow) {
          if (
            readySockets.input.find(inspectorClient->getSocket()) !=
            readySockets.input.end()
          ) {
            inspectorClient->acceptConnection();
          }
          inspectorClient->pollEventLoop();
        }

        // If output is not currently writable, exit the loop.
        if (readySockets.output.find(output) == readySockets.output.end()) {
          log(LogLevel::trace, "Reactor not writing to the output socket");
          break;
        }

        // Write from the write buffer and allow unrecoverable write
        // exceptions to propagate.
        log(LogLevel::trace,
          "Reactor writing from the write buffer to the output socket..."
        );
        SOCKET const socket = output;
        writeBuffer.pop(
          [socket](char const *memory, size_t &size) {
            writeFromMemory(socket, memory, size);
          }
        );
      } while (!writeBuffer.isEmpty());

      // If not quitting, and the the timer is due, reset the next wake
      // time and invoke the timer callback.
      if (!quitNow && nextWakeTime && nextWakeTime <= Time::now()) {
        log(LogLevel::trace, "Reactor waking...");
        nextWakeTime = 0;
        if (!invokeCallback(onTimerCallback, 0, nullptr)) {
          log(LogLevel::notice,
            "Reactor timer triggered, but no `ontimer` callback defined"
          );
        }
      }

      // If the input socket is readable, read into the read buffer.
      // If there was an unrecoverable read error, reading is finished.
      if (readySockets.input.find(input) == readySockets.input.end()) {
        log(LogLevel::trace, "Reactor not reading from the input socket");
      } else {
        assert(!didFinishReading);
        log(LogLevel::trace,
          "Reactor reading from the input socket into the read buffer..."
        );
        try {
          if (
            !readBuffer.push(
              [input](char *memory, size_t &size) {
                return readToMemory(input, memory, size);
              }
            )
          ) {
            log(LogLevel::debug,
              "Reactor quitting after read buffer flushed: input closed"
            );
            didFinishReading = true;
          }
        } catch (std::system_error const &error) {
          log(LogLevel::error,
            "Reactor quitting after read buffer flushed: %s", error.what()
          );
          didFinishReading = true;
        }
      }

      // Determine if processing is finished.
      if (quitNow || (didFinishReading && !didProcessLine)) {
        didFinishProcessing = true;
      }
    } while (!writeBuffer.isEmpty() || !didFinishProcessing);
    log(LogLevel::debug, "Reactor finished: %s",
      quitNow ? "evaluation terminated" : "input finished"
    );
    return true;
  }

  void Evaluator::flushWriteBuffer() {
    assert(output != INVALID_SOCKET);
    while (
      !writeBuffer.isEmpty() &&
      !Socket::wait({{}, {output}, {}}, 0).output.empty()
    ) {
      log(LogLevel::trace,
        "Flushing the write buffer to the output socket..."
      );
      SOCKET const socket = output;
      writeBuffer.pop(
        [socket](char const *memory, size_t &size) {
          writeFromMemory(socket, memory, size);
        }
      );
    }
    log(LogLevel::trace,
      "%s contents of write buffer were flushed to the output socket",
      writeBuffer.isEmpty() ? "All" : "Not all"
    );
  }

}
