/**
 *  @file
 *  @author     Severn Lortie, severn@distributive.network
 *  @date       April 2024
 */

#include <dcp/v8/inspector_client.hh>

#include <dcp/logger.hh>

#include <libplatform/libplatform.h>

#include <thread>

namespace DCP::V8 {

  InspectorClient::InspectorClient(
    v8::Platform &platform,
    v8::Local<v8::Context> &context,
    short unsigned const port
  ) :
  server{port},
  platform{platform},
  context{context},
  isolate{context->GetIsolate()},
  channel{server},
  inspector{v8_inspector::V8Inspector::create(isolate, this)}
  {
    if (!inspector) {
      throw std::runtime_error("Inspector creation failed");
    }
    session = inspector->connect(
      sessionContextGroupID, &channel, v8_inspector::StringView()
    );
    context->SetAlignedPointerInEmbedderData(1, this);
    std::string const contextName{"inspector_client"};
    inspector->contextCreated(
      v8_inspector::V8ContextInfo(
        context, sessionContextGroupID, convertToStringView(contextName)
      )
    );
    fakeChromeDevToolsConnection();
    server.setReadCallback(
      [this](boost::beast::flat_buffer &buffer) {
        dispatchProtocolMessage(buffer);
      }
    );
    server.setDisconnectCallback(
      [&session = session]() {
        if (session) {
          // If the Chrome DevTools session disconnects from the user closing
          // or refreshing the tab, we let V8 continue as normal, while ignoring
          // already set breakpoints
          session->setSkipAllPauses(true);
          session->resume();
        }
      }
    );
    tryToLog(LogLevel::debug, "InspectorClient: constructed");
  }

  void InspectorClient::fakeChromeDevToolsConnection() {
    assert(session);
    std::string runtimeEnableMessage{
      R"({"id":1,)"
      R"("method":"Runtime.enable",)"
      R"("params":{}})"
    };
    std::string debuggerEnableMessage{
      R"({"id":2,)"
      R"("method":"Debugger.enable",)"
      R"("params":{"maxScriptsCacheSize":10000000}})"
    };
    session->dispatchProtocolMessage(
      convertToStringView(runtimeEnableMessage)
    );
    session->dispatchProtocolMessage(
      convertToStringView(debuggerEnableMessage)
    );
  }

  void InspectorClient::pollEventLoop() {
    server.pollEventLoop();
  }

  void InspectorClient::waitForDebugger() {
    acceptConnection();
    while (!quitNow && !debuggerInitializationDone) {
      pollEventLoop();
      std::this_thread::yield();
    }
  }

  void InspectorClient::acceptConnection() {
    server.acceptConnection();
  }

  void InspectorClient::runMessageLoopOnPause(
    [[maybe_unused]] int const contextGroupID
  ) {
    assert(contextGroupID == sessionContextGroupID);
    tryToLog(LogLevel::debug, "Inspector Client: Debugger paused");
    if (!debuggerInitializationDone) {
      acceptConnection();
    }
    while (!quitOnNextLoop && !quitNow) {
      while (v8::platform::PumpMessageLoop(&platform, isolate)) {}
      pollEventLoop();
      std::this_thread::yield();
    }
    quitOnNextLoop = false;
  }

  void InspectorClient::runIfWaitingForDebugger(
    [[maybe_unused]] int const contextGroupID
  ) {
    assert(contextGroupID == sessionContextGroupID);
    tryToLog(LogLevel::debug, "Inspector Client: Finished initialization");
    std::string breakReason{"Pause on first executable line"};
    std::string breakDetails{"Set by evaluator debugger"};
    session->schedulePauseOnNextStatement(
      convertToStringView(breakReason),
      convertToStringView(breakDetails)
    );
    debuggerInitializationDone = true;
  }

  void InspectorClient::quitMessageLoopOnPause() {
    tryToLog(LogLevel::debug, "Inspector Client: Debugger unpaused");
    quitOnNextLoop = true;
  }

  void InspectorClient::dispatchProtocolMessage(
    boost::beast::flat_buffer const &message
  ) {
    // First convert the stream to a std::string and then take std::string
    // to a V8 StringView.
    std::string const string{boost::beast::buffers_to_string(message.data())};
    session->dispatchProtocolMessage(convertToStringView(string));
  }

  v8::Local<v8::Context> InspectorClient::ensureDefaultContextInGroup(
    [[maybe_unused]] int const contextGroupID
  ) {
    assert(contextGroupID == sessionContextGroupID);
    return context;
  }

  short unsigned InspectorClient::getPort() const {
    return server.getPort();
  }

  boost::asio::detail::socket_type InspectorClient::getSocket() {
    return server.getSocket();
  }

  void InspectorClient::terminate() {
    quitNow = true;
  }

  v8_inspector::StringView InspectorClient::convertToStringView(
    std::string const &string
  ) {
    v8_inspector::StringView stringView(
      reinterpret_cast<uint8_t const *>(string.data()),
      string.size()
    );
    return stringView;
  }

}
