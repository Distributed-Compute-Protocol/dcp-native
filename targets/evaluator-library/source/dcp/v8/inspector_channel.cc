/**
 *  @file
 *  @author     Severn Lortie, severn@distributive.network
 *  @date       April 2024
 */

#include <dcp/v8/inspector_channel.hh>

#include <boost/locale/encoding_utf.hpp>

namespace DCP::V8 {

  InspectorChannel::InspectorChannel(Websocket::Server &server) :
  server{server}
  {}

  void InspectorChannel::send(
    std::unique_ptr<v8_inspector::StringBuffer> const &message
  ) {
    assert(message);
    v8_inspector::StringView const stringView{message->string()};

    // The string view can be 8 or 16 bit. If it is 16, we convert it to 8.
    std::string cstr;
    if (stringView.is8Bit()) {
      cstr = std::string(
        reinterpret_cast<char const *>(stringView.characters8()),
        stringView.length()
      );
    } else {
      auto const *utf16String = reinterpret_cast<char16_t const *>(
        stringView.characters16()
      );
      cstr = boost::locale::conv::utf_to_utf<char, char16_t>(
        utf16String, boost::locale::conv::default_method
      );
    }
    server.send(cstr);
  }

  void InspectorChannel::sendResponse(
    [[maybe_unused]] int const callID,
    std::unique_ptr<v8_inspector::StringBuffer> message
  ) {
    send(message);
  }

  void InspectorChannel::sendNotification(
    std::unique_ptr<v8_inspector::StringBuffer> message
  ) {
    send(message);
  }

}
