/**
 *  @file
 *  @author     Wes Garland, wes@sparc.network
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2018
 */

#include <dcp/v8/implementation.hh>

#include <dcp/logger.hh>
#include <dcp/v8/evaluator.hh>

#include <boost/format.hpp>

namespace DCP::V8 {

  Implementation const &Implementation::getInstance(
    std::vector<std::string> const &options
  ) {
    static Implementation const implementation(options);
    if (!options.empty() && options != implementation.options) {
      throw std::invalid_argument(
        "Invalid options; already instantiated with different options"
      );
    }
    return implementation;
  }

  Implementation::Implementation(std::vector<std::string> options) :
  options{std::move(options)},
  platform(v8::platform::NewDefaultPlatform())
  {
    for (auto const &option: this->options) {
      log(LogLevel::debug,
        "V8 Implementation: setting option(s): %s", option.data()
      );
      v8::V8::SetFlagsFromString(option.data(), option.size());
    }

    v8::V8::InitializePlatform(platform.get());
    try {
      v8::V8::Initialize();
      try {
        arrayBufferAllocator.reset(
          v8::ArrayBuffer::Allocator::NewDefaultAllocator()
        );
        createParams.array_buffer_allocator = arrayBufferAllocator.get();
      } catch (...) {
        v8::V8::Dispose();
        throw;
      }
    } catch (...) {
      v8::V8::DisposePlatform();
      throw;
    }

    log(LogLevel::debug, "V8 Implementation: constructed");
  }

  Evaluator &Implementation::getEvaluator(
    Socket::Handle const &output,
    boost::optional<bool> const webGPU,
    boost::optional<std::string> const &webGPUDevice,
    boost::optional<short unsigned> const debuggingPort
  ) const {
    thread_local Evaluator evaluator(
      *platform,
      createParams,
      output,
      webGPU ? *webGPU : false,
      webGPUDevice ? *webGPUDevice : "",
      debuggingPort
    );
    if (output != Socket::Handle{} && output != evaluator.getOutput()) {
      throw std::invalid_argument(
        boost::str(
          boost::format(
            "Invalid output socket %" DCP_Socket_print ";"
            " already instantiated with output socket %" DCP_Socket_print
          )
          % static_cast<SOCKET>(output)
          % static_cast<SOCKET>(evaluator.getOutput())
        )
      );
    }
    if (webGPU && *webGPU != evaluator.getWebGPU()) {
      throw std::invalid_argument(
        boost::str(
          boost::format(
            "Invalid webGPU value %i; already instantiated with webGPU %i"
          )
          % *webGPU
          % evaluator.getWebGPU()
        )
      );
    }
    if (debuggingPort && debuggingPort != evaluator.getDebuggingPort()) {
      if (evaluator.getDebuggingPort()) {
        throw std::invalid_argument(
          boost::str(
            boost::format(
              "Invalid debuggingPort value %hu;"
              " already instantiated with debuggingPort %hu"
            )
            % *debuggingPort
            % *evaluator.getDebuggingPort()
          )
        );
      }
      throw std::invalid_argument(
        boost::str(
          boost::format(
            "Invalid debuggingPort value %hu;"
            " already instantiated with no debuggingPort"
          )
          % *debuggingPort
        )
      );
    }
    return evaluator;
  }

  Implementation::~Implementation() {
    tryToLog(LogLevel::debug, "V8 Implementation: destructing");
    v8::V8::Dispose();
    v8::V8::DisposePlatform();
  }

}
