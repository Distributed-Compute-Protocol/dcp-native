# Copyright (c) 2024 Distributive Corp. All Rights Reserved.

dcp_add_test_target(TARGET_VARIABLE _TARGET)

dcp_add_sources("${_TARGET}"
  FOLDER "Source Files"
  SUBDIRECTORY "source"
  FILES "source/dcp/test/sandbox/tests.cc"
)

dcp_add_sources("${_TARGET}"
  COPY
  FOLDER "Data Files"
  SUBDIRECTORY "data"
  FILES "data/matmul.js"
)

target_link_libraries("${_TARGET}" PRIVATE "${PROJECT_NAME}-sandbox-library")
