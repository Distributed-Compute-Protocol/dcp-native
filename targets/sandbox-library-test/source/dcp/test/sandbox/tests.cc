/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       April 2023
 */

#include <dcp/read_buffer.hh>
#include <dcp/sandbox.hh>
#include <dcp/test.hh>

#include <boost/exception/diagnostic_information.hpp>

#include <thread>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(Sandbox)

  namespace {

    struct Fixture final {

      explicit Fixture(
        /// Arguments to be passed directly to the evaluator.
        std::vector<std::string> const &arguments = {},
        /// The string from the sandbox indicating success.
        /// Default: "sandboxLoaded".
        // NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
        char const *const successString = "sandboxLoaded",
        /// If specified, the string from the sandbox indicating failure.
        char const *const failureString = nullptr
      ) :
      socketPair{DCP::Socket::createPair()},
      sandbox(getExecutablePath().parent_path(), arguments, socketPair[1])
      {
        assert(successString);

        BOOST_CHECK(sandbox.getProcess().isRunning());

        // Perform the confirmation step.
        bool success = false;
        for (DCP::ReadBuffer readBuffer{};; ) {
          std::string buffer{};
          if (!readBuffer.receive(socketPair[0], buffer, 100)) {
            BOOST_FAIL("The read buffer failed to receive");
            break;
          }
          if (!buffer.empty()) {
            DCP::log(DCP::LogLevel::debug, "rx: %s", buffer.data());
            if (failureString && strstr(buffer.data(), failureString)) {
              break;
            }
            if (strstr(buffer.data(), successString)) {
              success = true;
              break;
            }
          }
        }
        BOOST_CHECK(success);
      }

      Fixture(Fixture const &) = delete;
      Fixture(Fixture const &&) = delete;

      Fixture &operator =(Fixture const &) = delete;
      Fixture &operator =(Fixture const &&) = delete;

      [[nodiscard]]
      DCP::Sandbox &getSandbox() noexcept {
        return sandbox;
      }

      [[nodiscard]]
      DCP::Sandbox const &getSandbox() const noexcept {
        return sandbox;
      }

      void shutDown() const {
        DCP::Socket::shutDown(socketPair[0]);
      }

      ~Fixture() {
        // Duplicate the Process destruction logic here to fail on any errors.
        try {
          sandbox.getProcess().wait();
          BOOST_CHECK(!sandbox.getProcess().isRunning());
        } catch (...) {
          DCP::log(DCP::LogLevel::error,
            "Exception while destroying sandbox test: %s",
            boost::current_exception_diagnostic_information().data()
          );
          BOOST_FAIL("Error cleaning up sandbox test");
        }
      }

    private:

      std::array<DCP::Socket::Handle, 2> socketPair{};

      DCP::Sandbox sandbox;

    };

  }

  BOOST_AUTO_TEST_SUITE(tests)

  BOOST_AUTO_TEST_CASE(shutDown, *boost::unit_test::timeout(timeout)) {
    Fixture fixture{};
    fixture.shutDown();
  }

  BOOST_AUTO_TEST_CASE(abort, *boost::unit_test::timeout(timeout)) {
    Fixture fixture{};
    auto &process = fixture.getSandbox().getProcess();
    process.interrupt();
    process.wait();
  }

  BOOST_AUTO_TEST_CASE(matmul, *boost::unit_test::timeout(timeout)) {
    auto const dataFilePath = (
      getExecutablePath().parent_path() / "dcp-sandbox-library-test-matmul.js"
    );
    Fixture fixture(
      {"--file", dataFilePath.string(), "--webgpu"},
      "MATMUL (2x4 X 4x2): 2,2,50,60,114,140",
      "An exception was thrown:"
    );
    fixture.shutDown();
  }

  BOOST_AUTO_TEST_SUITE_END()
  BOOST_AUTO_TEST_SUITE_END()

}
