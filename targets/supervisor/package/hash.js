/**
 *  @file       hash.js
 *  @brief      Generate a compute group join hash from a join key and secret.
 *  @author     Jason Erb, jason@distributive.network
 *  @author     Wes Garland, wes@distributive.network
 *  @date       December 2021
 */
'use strict';

async function main() {
  const argv = process.argv.slice(2);
  if (argv.length != 2) {
    throw new Error("Incorrect argument number: expected join key and secret");
  }
  const key = argv[0];
  const secret = argv[1];
  const hash = require('dcp/compute-groups').calculateJoinHash(
    {'joinKey': key, 'joinSecret': secret}
  );
  process.stdout.write(hash);
}

function abort(error) {
  console.error(process.env.DCP_DEBUG ? error : error.message);
  process.exit(2);
}

process.exitCode = 1;
process.on('unhandledRejection', abort);
process.on('uncaughtException', abort);
require('dcp-client');
main().then(() => process.exit(0));
