/**
 *  @file       keystore.js
 *  @brief      Generate a keystore.
 *  @author     Jason Erb, jason@distributive.network
 *  @author     Eddie Roosenmaallen <eddie@distributive.network>
 *  @date       October 2020
 */
'use strict';

async function main() {
  const { Keystore } = require('dcp/wallet');
  const keystore = await new Keystore(null, '');
  keystore.label = require('os').hostname();
  const json = JSON.stringify(keystore);
  process.stdout.write(json);
}

function abort(error) {
  console.error(process.env.DCP_DEBUG ? error : error.message);
  process.exit(2);
}

process.exitCode = 1;
process.on('unhandledRejection', abort);
process.on('uncaughtException', abort);
require('dcp-client');
main().then(() => process.exit(0));
