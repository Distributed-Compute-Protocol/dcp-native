/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2022
 */

#include <dcp/supervisor.hh>

#if BOOST_OS_WINDOWS

  #include <dcp/process.hh>

  #include <boost/algorithm/string.hpp>
  #include <boost/filesystem/operations.hpp>

namespace DCP::Supervisor {

  std::string hash(
    boost::filesystem::path const &directoryPath,
    std::string const &key,
    std::string const &secret
  ) {
    assert(!directoryPath.empty());
    assert(boost::filesystem::is_directory(directoryPath));

    auto output = Process::execute(
      directoryPath / DCP_Supervisor_hashScriptFile, {key, secret}
    );
    boost::trim(output);
    return output;
  }

}

#endif
