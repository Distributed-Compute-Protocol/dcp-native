/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       January 2022
 */

#if !defined(DCP_Supervisor_)
  #define DCP_Supervisor_

  #if BOOST_OS_WINDOWS

    #include <boost/filesystem/path.hpp>

    #include <string>

namespace DCP::Supervisor {

  /** Generate a compute group hash.
   *
   *  @param  directoryPath
   *          Cannot be empty; must be an existing directory.
   *  @param  key
   *          The join key.
   *  @param  secret
   *          The join secret.
   *  @return The compute group hash string.
   */
  std::string hash(
    boost::filesystem::path const &directoryPath,
    std::string const &key,
    std::string const &secret
  );

}

  #endif

#endif
