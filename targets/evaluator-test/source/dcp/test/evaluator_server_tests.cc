/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#include <dcp/test.hh>
#include <dcp/test/evaluator_server_fixture.hh>

#include <boost/format.hpp>

#include <chrono>
#include <thread>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(evaluatorServerTests)

  BOOST_AUTO_TEST_CASE(run, *boost::unit_test::timeout(timeout)) {
    // Host/connection combinations; ensure that both IPv4 and IPv6 work.
    std::vector<std::pair<char const *, char const *>> const combinations{
      {"::", "localhost"},
      {"::", "127.0.0.1"},
      {"0.0.0.0", "localhost"},
      {"0.0.0.0", "127.0.0.1"},
      {"127.0.0.1", "127.0.0.1"},
      {"127.0.0.1", "localhost"},
      {"localhost", "localhost"}
    };
    for (auto const &[host, connection]: combinations) {
      DCP::log(DCP::LogLevel::info,
        "Connecting to '%s' via '%s'...", host, connection
      );

      EvaluatorServerFixture fixture{host};
      auto const &address = fixture.getAddress();

      auto const socket = DCP::Socket::connect(
        connection, std::to_string(address.getPort()).data()
      );
      BOOST_REQUIRE_NE(socket, INVALID_SOCKET);

      DCP::Socket::send(socket, "writeln(42)\n");
      DCP::Socket::send(socket, "die()\n");

      std::string receivedString{};
      auto const received = DCP::Socket::receive(socket, receivedString,
        EvaluatorServerFixture::defaultTimeout
      );
      BOOST_CHECK(received);
      BOOST_CHECK_EQUAL(receivedString.data(), "42");
    }
  }

  BOOST_AUTO_TEST_CASE(concurrency, *boost::unit_test::timeout(timeout)) {
    static size_t const concurrency{2};

    EvaluatorServerFixture fixture(nullptr, concurrency);
    auto const &address = fixture.getAddress();

    // Make the maximum number of connections to the server + one.
    // There are no guarantees about which connection will happen first.
    std::set<DCP::Socket::Handle> sockets;
    BOOST_REQUIRE_LT(concurrency, std::numeric_limits<size_t>::max());
    for (size_t index = 0; index < concurrency + 1; ++index) {
      auto socket = DCP::Socket::connect(
        "localhost", std::to_string(address.getPort()).data()
      );
      BOOST_REQUIRE_NE(socket, INVALID_SOCKET);
      sockets.insert(socket);
    }

    // Ensure that one connection didn't make it through.
    size_t total{0U};
    size_t connected{0U};
    size_t disconnected{0U};
    for (auto iterator = sockets.begin(); iterator != sockets.end(); ) {
      auto const socket = *iterator;
      DCP::Socket::send(
        socket,
        boost::str(boost::format("writeln(%i)\n") % total).data(),
        EvaluatorServerFixture::defaultTimeout
      );
      std::string receivedString{};
      bool received{false};
      try {
        received = DCP::Socket::receive(
          socket, receivedString, EvaluatorServerFixture::defaultTimeout
        );
      } catch (...) {
        BOOST_CHECK_EQUAL(total, concurrency);
      }
      if (received) {
        BOOST_CHECK_EQUAL(receivedString, std::to_string(total));
        ++connected;
        ++iterator;
      } else {
        ++disconnected;
        iterator = sockets.erase(iterator);
      }
      ++total;
    }
    BOOST_CHECK_EQUAL(connected, concurrency);
    BOOST_CHECK_EQUAL(disconnected, 1);

    // Remove one evaluator from the server.
    {
      BOOST_REQUIRE(!sockets.empty());
      auto socket = *sockets.begin();
      BOOST_REQUIRE_NE(socket, INVALID_SOCKET);
      DCP::Socket::send(socket, "die()\n",
        EvaluatorServerFixture::defaultTimeout
      );
      std::this_thread::sleep_for(std::chrono::seconds(3));
    }

    // Add another and ensure that it succeeds.
    {
      auto socket = DCP::Socket::connect(
        "localhost", std::to_string(address.getPort()).data()
      );
      BOOST_REQUIRE_NE(socket, INVALID_SOCKET);
      DCP::Socket::send(
        socket,
        boost::str(boost::format("writeln(%i)\n") % total).data(),
        EvaluatorServerFixture::defaultTimeout
      );
      std::string receivedString{};
      bool const received = DCP::Socket::receive(
        socket, receivedString, EvaluatorServerFixture::defaultTimeout
      );
      BOOST_CHECK(received);
      BOOST_CHECK_EQUAL(receivedString, std::to_string(total));
    }
  }

  BOOST_AUTO_TEST_CASE(profile, *boost::unit_test::timeout(timeout)) {
    static size_t const count{20};

    auto const start = std::chrono::steady_clock::now();
    {
      EvaluatorServerFixture fixture{};
      auto const &address = fixture.getAddress();

      std::set<DCP::Socket::Handle> sockets{};
      for (size_t index{0}; index < count; ++index) {
        auto const socket = DCP::Socket::connect(
          "localhost", std::to_string(address.getPort()).data()
        );
        BOOST_REQUIRE_NE(socket, INVALID_SOCKET);
        sockets.insert(socket);

        auto const &string = boost::str(
          boost::format("%i: %" DCP_Socket_print)
          % index
          % static_cast<SOCKET>(socket)
        );
        DCP::log(DCP::LogLevel::info, "Created connection %s", string.data());

        DCP::Socket::send(
          socket,
          boost::str(boost::format("writeln('%s')\n") % string).data(),
          EvaluatorServerFixture::defaultTimeout
        );
        std::string receivedString;
        BOOST_CHECK(
          DCP::Socket::receive(socket, receivedString,
            EvaluatorServerFixture::defaultTimeout
          )
        );
        BOOST_CHECK_EQUAL(receivedString, string);
      }
    }
    auto const finish = std::chrono::steady_clock::now();
    auto const elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
      finish - start
    ).count();
    DCP::log(DCP::LogLevel::info, "Elapsed time: %lu ms", elapsed);
  }

  BOOST_AUTO_TEST_SUITE_END()

}
