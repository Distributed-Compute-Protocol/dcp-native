/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       April 2024
 */

#include <dcp/test/evaluator_fixture.hh>

#include <dcp/evaluator.hh>
#include <dcp/test.hh>

#include <boost/exception/diagnostic_information.hpp>

namespace DCP::Test {

  DCP::Process EvaluatorFixture::createProcess(
    // NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
    char const *const host,
    char const *const port,
    size_t const concurrency,
#if !BOOST_OS_WINDOWS
    std::function<bool ()> const &childCallback,
#endif
    SOCKET const socket
  ) {
#if BOOST_OS_WINDOWS
    assert(job);
    assert(startEvent);
#else
    std::vector<char *> environment{nullptr};
    assert(!environment.empty());
#endif
    assert(port);

    auto const &executableDirectoryPath = getExecutablePath().parent_path();
    auto const &logger = DCP::Logger::getInstance();

    std::vector<std::string> arguments{
#if BOOST_OS_WINDOWS
      "--start-event", std::to_string(
        reinterpret_cast<uintptr_t>(static_cast<HANDLE>(startEvent))
      ),
#endif
      "--debug", std::to_string(logger.getMaxLogLevel()),
      "--system-debug", std::to_string(logger.getMaxSystemLogLevel()),
      "--port", port,
      "--concurrency", std::to_string(concurrency),
      "--socket", std::to_string(socket)
    };
    if (host) {
      arguments.emplace_back("--address");
      arguments.emplace_back(host);
    }

#if BOOST_OS_WINDOWS
    std::vector<HANDLE> additionalHandles{};
    if (socket != INVALID_SOCKET) {
      additionalHandles.push_back(reinterpret_cast<HANDLE>(socket));
    }
#else
    if (socket != INVALID_SOCKET) {
      DCP::Socket::setCloseOnExec(socket, false);
    }
#endif

    return DCP::Process(
      executableDirectoryPath / DCP::Evaluator::programFile, arguments,
#if BOOST_OS_WINDOWS
      job, startEvent, additionalHandles
#else
      &environment.front(), childCallback
#endif
    );
  }

  void EvaluatorFixture::destroyProcess(DCP::Process &process, bool interrupt) {
    // Duplicate the Process destruction logic here to fail on any errors.
    try {
      if (interrupt) {
        process.interrupt();
      }
      process.wait();
      BOOST_CHECK(!process.isRunning());
    } catch (...) {
      DCP::log(DCP::LogLevel::error,
        "Exception while destroying evaluator process fixture: %s",
        boost::current_exception_diagnostic_information().data()
      );
      BOOST_FAIL("Error cleaning up evaluator process fixture");
    }
  }

}
