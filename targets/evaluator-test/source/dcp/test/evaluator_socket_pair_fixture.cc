/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       April 2024
 */

#include <dcp/test/evaluator_socket_pair_fixture.hh>

#include <dcp/read_buffer.hh>
#include <dcp/test.hh>
#include <dcp/write_buffer.hh>

namespace DCP::Test {

  std::array<
    DCP::Time::Milliseconds, 4
  > const EvaluatorSocketPairFixture::delays{1, 10, 100, 1000};

  EvaluatorSocketPairFixture::EvaluatorSocketPairFixture(
#if !BOOST_OS_WINDOWS
    std::function<bool ()> const &childCallback
#endif
  ) :
  process{
    createProcess(nullptr, "", 0U,
#if !BOOST_OS_WINDOWS
      childCallback,
#endif
      socketPair[1]
    )
  }
  {
    assert(socketPair[0] != INVALID_SOCKET);
    assert(socketPair[1] != INVALID_SOCKET);

    socketPair[1] = {};
  }

  void EvaluatorSocketPairFixture::send(char const *const string) const {
    assert(string);

    DCP::WriteBuffer{}.send(socketPair[0], string);
  }

  void EvaluatorSocketPairFixture::receive(char const *const string) const {
    assert(string);

    for (DCP::ReadBuffer readBuffer{};; ) {
      std::string buffer{};
      if (!readBuffer.receive(socketPair[0], buffer, 100)) {
        BOOST_FAIL("The read buffer failed to receive");
        break;
      }
      if (!buffer.empty()) {
        BOOST_CHECK_EQUAL(buffer.data(), string);
        break;
      }
    }
  }

  void EvaluatorSocketPairFixture::shutDown() const {
    DCP::Socket::shutDown(socketPair[0]);
  }

}
