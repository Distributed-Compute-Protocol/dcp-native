/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       April 2024
 */

#include <dcp/test/evaluator_server_fixture.hh>

#include <dcp/test.hh>

#include <thread>

namespace DCP::Test {

  EvaluatorServerFixture::EvaluatorServerFixture(
    char const *const host,
    size_t const concurrency
  ) :
  address{
    [host]() {
      auto const available = DCP::Socket::getAvailableAddress(host);
      BOOST_REQUIRE(available);
      return *available;
    }()
  },
  process{
    createProcess(
      address.getIP().data(),
      std::to_string(address.getPort()).data(), concurrency
    )
  }
  {
    // Wait for server to start listening.
#if BOOST_OS_WINDOWS
    this->process.waitToStart();
#endif
    std::this_thread::sleep_for(std::chrono::seconds(2));
    BOOST_REQUIRE(this->process.isRunning());
  }

}
