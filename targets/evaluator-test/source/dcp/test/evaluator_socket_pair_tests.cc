/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2020
 */

#include <dcp/test.hh>
#include <dcp/test/evaluator_socket_pair_fixture.hh>

#include <boost/numeric/conversion/cast.hpp>

#include <chrono>
#include <thread>

namespace DCP::Test {

  BOOST_AUTO_TEST_SUITE(evaluatorSocketPairTests)

#if !BOOST_OS_WINDOWS
  BOOST_AUTO_TEST_CASE(falseCallback, *boost::unit_test::timeout(timeout)) {
    EvaluatorSocketPairFixture fixture(
      []() {
        return false;
      }
    );
    DCP::log(DCP::LogLevel::debug, "Test process.falseCallback: starting...");
    auto &process = fixture.getProcess();
    process.wait();
    BOOST_CHECK(!process.isRunning());
    DCP::log(DCP::LogLevel::debug, "Test process.falseCallback: ending...");
  }
#endif

  BOOST_AUTO_TEST_CASE(shutDown, *boost::unit_test::timeout(timeout)) {
    EvaluatorSocketPairFixture fixture{};
    DCP::log(DCP::LogLevel::debug, "Test process.shutDown: starting...");
    auto &process = fixture.getProcess();
    BOOST_REQUIRE(process.isRunning());
    fixture.send("writeln('ready');\n");
    fixture.receive("ready");
    fixture.shutDown();
    DCP::log(DCP::LogLevel::debug, "Test process.shutDown: ending...");
  }

  BOOST_AUTO_TEST_CASE(interrupt, *boost::unit_test::timeout(timeout)) {
    EvaluatorSocketPairFixture fixture{};
    DCP::log(DCP::LogLevel::debug, "Test process.interrupt: starting...");
    auto &process = fixture.getProcess();
    BOOST_REQUIRE(process.isRunning());
    process.interrupt();
    process.wait();
    DCP::log(DCP::LogLevel::debug, "Test process.interrupt: ending...");
  }

  BOOST_AUTO_TEST_CASE(interruptTwice, *boost::unit_test::timeout(timeout)) {
    EvaluatorSocketPairFixture fixture{};
    DCP::log(DCP::LogLevel::debug, "Test process.interruptTwice: starting...");
    auto &process = fixture.getProcess();
    BOOST_REQUIRE(process.isRunning());
    process.interrupt();
    process.wait();
    process.interrupt();
    DCP::log(DCP::LogLevel::debug, "Test process.interruptTwice: ending...");
  }

  BOOST_TEST_DECORATOR(
    *boost::unit_test::timeout(
      timeout * boost::numeric_cast<int unsigned>(
        EvaluatorSocketPairFixture::delays.size()
      )
    )
  )
  BOOST_DATA_TEST_CASE(interruptAfterDelay,
    EvaluatorSocketPairFixture::delays, delay
  ) {
    EvaluatorSocketPairFixture fixture{};
    DCP::log(DCP::LogLevel::debug,
      "Test process.interruptAfterDelay: starting..."
    );
    DCP::log(DCP::LogLevel::debug, "Sleeping for %lu millisecond(s)...", delay);
    std::this_thread::sleep_for(std::chrono::milliseconds(delay));
    DCP::log(DCP::LogLevel::debug, "Interrupting process...");
    auto &process = fixture.getProcess();
    BOOST_REQUIRE(process.isRunning());
    process.interrupt();
    process.wait();
    DCP::log(DCP::LogLevel::debug,
      "Test process.interruptAfterDelay: ending..."
    );
  }

  BOOST_AUTO_TEST_SUITE_END()

}
