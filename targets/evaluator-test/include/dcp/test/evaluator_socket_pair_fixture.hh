/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       April 2024
 */

#if !defined(DCP_Test_EvaluatorSocketPairFixture_)
  #define DCP_Test_EvaluatorSocketPairFixture_

  #include <dcp/test/evaluator_fixture.hh>

namespace DCP::Test {

  struct EvaluatorSocketPairFixture final : EvaluatorFixture {

    /// Delays to be used in parameterized tests.
    static std::array<DCP::Time::Milliseconds, 4> const delays;

    explicit EvaluatorSocketPairFixture(
#if !BOOST_OS_WINDOWS
      std::function<bool ()> const &childCallback = {
        []() {
          return true;
        }
      }
#endif
    );

    EvaluatorSocketPairFixture(EvaluatorSocketPairFixture const &) = delete;
    EvaluatorSocketPairFixture(EvaluatorSocketPairFixture const &&) = delete;

    EvaluatorSocketPairFixture &operator =(
      EvaluatorSocketPairFixture const &
    ) = delete;
    EvaluatorSocketPairFixture &operator =(
      EvaluatorSocketPairFixture const &&
    ) = delete;

    [[nodiscard]]
    DCP::Process &getProcess() noexcept {
      return process;
    }

    void send(char const *const string) const;

    void receive(char const *const string) const;

    void shutDown() const;

    ~EvaluatorSocketPairFixture() override {
      destroyProcess(process);
    }

  private:

    std::array<DCP::Socket::Handle, 2> socketPair{DCP::Socket::createPair()};

    DCP::Process process;

  };

}

#endif
