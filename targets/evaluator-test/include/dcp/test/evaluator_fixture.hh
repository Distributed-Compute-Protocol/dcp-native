/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       April 2024
 */

#if !defined(DCP_Test_EvaluatorFixture_)
  #define DCP_Test_EvaluatorFixture_

  #include <dcp/process.hh>
  #include <dcp/socket.hh>

namespace DCP::Test {

  struct EvaluatorFixture {

    EvaluatorFixture() = default;

    EvaluatorFixture(EvaluatorFixture const &) = delete;
    EvaluatorFixture(EvaluatorFixture const &&) = delete;

    EvaluatorFixture &operator =(EvaluatorFixture const &) = delete;
    EvaluatorFixture &operator =(EvaluatorFixture const &&) = delete;

    virtual ~EvaluatorFixture() = default;

  protected:

    DCP::Process createProcess(
      char const *const host,
      char const *const port,
      size_t const concurrency,
#if !BOOST_OS_WINDOWS
      std::function<bool ()> const &childCallback = {
        []() {
          return true;
        }
      },
#endif
      SOCKET const socket = INVALID_SOCKET
    );

    void destroyProcess(DCP::Process &process, bool interrupt = false);

  private:

#if BOOST_OS_WINDOWS
    DCP::Handle<HANDLE, nullptr> job{DCP::Process::createJob()};
    DCP::Handle<HANDLE, nullptr> startEvent{DCP::Process::createEvent()};
#endif

  };

}

#endif
