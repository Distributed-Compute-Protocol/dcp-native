/**
 *  @file
 *  @author     Jason Erb, jason@distributive.network
 *  @date       April 2024
 */

#if !defined(DCP_Test_EvaluatorServerFixture_)
  #define DCP_Test_EvaluatorServerFixture_

  #include <dcp/test/evaluator_fixture.hh>

namespace DCP::Test {

  struct EvaluatorServerFixture final : EvaluatorFixture {

    static DCP::Time::Milliseconds const defaultTimeout{200};

    explicit EvaluatorServerFixture(
      char const *const host = nullptr,
      size_t const concurrency = 0U
    );

    EvaluatorServerFixture(EvaluatorServerFixture const &) = delete;
    EvaluatorServerFixture(EvaluatorServerFixture const &&) = delete;

    EvaluatorServerFixture &operator =(EvaluatorServerFixture const &) = delete;
    EvaluatorServerFixture &operator =(
      EvaluatorServerFixture const &&
    ) = delete;

    [[nodiscard]]
    DCP::Socket::Address const &getAddress() const noexcept {
      return address;
    }

    ~EvaluatorServerFixture() override {
      destroyProcess(process, true);
    }

  private:

    DCP::Socket::Address address;

    DCP::Process process;

  };

}

#endif
