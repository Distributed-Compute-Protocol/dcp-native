# Copyright (c) 2020 Distributive Corp. All Rights Reserved.

set(_RESOURCE_INPUTS
  "resource/app.manifest.in"
  "resource/definitions.h.in"
)
dcp_configure_files(_RESOURCE_OUTPUTS INPUTS ${_RESOURCE_INPUTS})

set(_TARGET "${PROJECT_NAME}-evaluator")

add_executable("${_TARGET}")
dcp_target_initialize("${_TARGET}")

dcp_add_sources("${_TARGET}"
  FOLDER "Source Files"
  SUBDIRECTORY "source"
  FILES "source/main.cc"
)

dcp_add_sources("${_TARGET}"
  FOLDER "Resource Files"
  SUBDIRECTORY "resource"
  FILES
  ${_RESOURCE_INPUTS}
  "resource/resource.h"
  "resource/resource.rc"
)
unset(_RESOURCE_INPUTS)

dcp_add_sources("${_TARGET}"
  FOLDER "Resource Files (Generated)"
  DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
  SUBDIRECTORY "resource"
  FILES ${_RESOURCE_OUTPUTS}
)
unset(_RESOURCE_OUTPUTS)

add_custom_command(TARGET "${_TARGET}" PRE_BUILD VERBATIM)

set_target_properties("${_TARGET}"
  PROPERTIES OUTPUT_NAME "${DCP_EVALUATOR_PROGRAM_NAME}"
)

target_include_directories("${_TARGET}"
  PRIVATE "${CMAKE_CURRENT_BINARY_DIR}/resource"
)

target_link_libraries("${_TARGET}" PRIVATE "${PROJECT_NAME}-evaluator-library")

install(
  TARGETS "${_TARGET}"
  RUNTIME DESTINATION "${DCP_INSTALL_EXECUTABLE_SUBDIRECTORY}"
  COMPONENT EVALUATOR
)
