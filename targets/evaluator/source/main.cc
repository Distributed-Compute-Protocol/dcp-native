/**
 *  @file
 *  @brief      A command-line interface for a native Javascript evaluator.
 *  @author     Wes Garland, wes@sparc.network
 *  @author     Jason Erb, jason@distributive.network
 *  @date       March 2018
 */

#include <dcp/argument.hh>
#include <dcp/evaluator.hh>
#if BOOST_OS_WINDOWS
  #include <dcp/number.hh>
#endif
#include <dcp/process.hh>
#if BOOST_OS_WINDOWS
  #include <dcp/settings.hh>
#endif
#include <dcp/signal.hh>
#include <dcp/socket.hh>
#include <dcp/version.hh>

#include <boost/exception/diagnostic_information.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/format.hpp>
#include <boost/program_options.hpp>
#include <boost/thread/scoped_thread.hpp>
#include <boost/thread/thread.hpp>

#include <atomic>
#include <csignal>
#include <iostream>
#include <thread>

// https://lists.gnu.org/archive/html/bug-gnulib/2008-02/msg00126.html
#if BOOST_OS_MACOS
  #include <crt_externs.h>
  #define environ (*_NSGetEnviron())
#endif

namespace {

  /// True if the program should quit now.
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  std::atomic<bool> quitNow{false};

#if BOOST_OS_WINDOWS

  /// Handler for console control event.
  BOOL handleConsoleCtrl(DWORD const type) {
    DCP::log(DCP::LogLevel::debug, "Handling console control event: %lu", type);
    quitNow = true;
    DCP::log(DCP::LogLevel::debug, "Quitting now...");
    return true;
  }

#else

  /// Handler for signal.
  extern "C" void handleSignal([[maybe_unused]] int signal) {
    quitNow = true;
  }

#endif

}

// NOLINTNEXTLINE(readability-function-cognitive-complexity)
int main(int argc, char *const argv[]) noexcept {
  assert(argc > 0);
  assert(argv);

  try {
    auto const executablePath = boost::filesystem::absolute(argv[0]);
    auto const executableFile = executablePath.filename().string();

    // Initialize argument variables.
    int maxLogLevel{DCP::Logger::defaultMaxLogLevel};
    int maxSystemLogLevel{DCP::Logger::defaultMaxSystemLogLevel};
#if BOOST_OS_WINDOWS
    auto startEventNumber = reinterpret_cast<uintptr_t>(nullptr);
#endif
    SOCKET socketNumber{INVALID_SOCKET};
    bool webGPU{};
    boost::optional<std::string> webGPUDevice{};
    std::vector<std::string> implementationOptions{};
    boost::optional<std::string> host{};
    std::string port{};
    size_t concurrency{};
    uint64_t timeoutSeconds{
      DCP::Socket::defaultTimeout / DCP::Time::millisecondsPerSecond
    };
    boost::optional<uint64_t> shutdownTimeoutSeconds{};
    std::vector<std::string> filePathStrings{};
    std::vector<std::string> strings{};
    boost::optional<short unsigned> debuggingPort{};
    bool debuggingWait{};

#if BOOST_OS_WINDOWS
    // Set argument variable defaults based on global settings.
    {
      auto const &settings = DCP::Settings::Tree::get().first;

      char const *const path{"dcp-evaluator"};
      webGPU = (settings.tryToGet(path, "webgpu").first == "true");
      DCP::Number::tryToParseInteger(
        settings.tryToGet(path, "concurrency").first, concurrency
      );

      auto implementationOption = settings.tryToGet("v8", "options").first;
      if (!implementationOption.empty()) {
        implementationOptions.emplace_back(std::move(implementationOption));
      }
    }
#endif

    // Parse the arguments.
    std::string const &argumentsUsage{"[options] [<file.js> <file.js> ...]"};
    std::string const &argumentsDescription{
      "Perform Javascript evaluation ('evaluation mode'), or run a server\n"
      "('server mode') that can spawn evaluators, each in evaluation mode.\n"
      "\n"
      "In evaluation mode, code is evaluated in the following order:\n"
      " * Evaluate any code files in the order specified.\n"
      " * Evaluate any code strings in the order specified.\n"
      " * Start the reactor, which evaluates code from the input specified.\n"
      "\n"
      "The following additional functions are available to call:\n"
      " * die(failure):\n"
      "   Terminate evaluation, determine exit code.\n"
      " * nextTimer(when):\n"
      "   Specify when the next timer will become ready.\n"
      " * onreadln(callback):\n"
      "   Pass the function to be called by the reactor when a line is read.\n"
      " * ontimer(callback):\n"
      "   Pass the function to be called by the reactor when the timer fires.\n"
      " * writeln(line):\n"
      "   Write a line to the output socket."
    };
    std::string argumentsError{};
    std::stringstream argumentsErrorHelp{};
    try {
      namespace po = boost::program_options;

#if BOOST_OS_WINDOWS
      std::vector<uintptr_t> startEventNumbers{};
#endif
      std::vector<SOCKET> socketNumbers{};

      po::options_description od{"Options"};
      od.add_options()(
        "help,h", "Print help."
      )(
        "version,v", "Print version information."
      )(
        "debug,d", po::value(&maxLogLevel)->default_value(maxLogLevel),
        "Specify the maximum log level (corresponding to syslog priority level,"
        " or greater for verbose logging) for logging to standard error."
        " A negative number suppresses all standard error logging."
      )(
        "system-debug,s",
        po::value(&maxSystemLogLevel)->default_value(maxSystemLogLevel),
        "Specify the maximum log level (corresponding to syslog priority level,"
        " or greater for verbose logging) for logging to the system logger."
        " A negative number suppresses all system logging."
      )(
        "socket", po::value(&socketNumbers),
        "Specify the socket for the reactor to read from, and force the"
        " evaluator to run in evaluation mode."
        " If more than one is specified, the last one is used."
#if !BOOST_OS_WINDOWS
        " If unspecified in evaluation mode, the program uses standard I/O."
#endif
      )(
        "address,a", po::value(&host),
        "Specify the host name or address to listen at if in server mode."
        " Ignored in evaluation mode."
      )(
        "port,p", po::value(&port),
        "Specify the port number or service name to listen on."
        " If specified, the program runs in server mode, whereby each incoming"
        " connection on the port spawns an evaluator in evaluation mode."
        " Ignored in evaluation mode."
#if !BOOST_OS_WINDOWS
        " If unspecified, the evaluator will run in evaluation mode."
#endif
      )(
        "concurrency,n", po::value(&concurrency)->default_value(concurrency),
        "Specify the maximum number of evaluators, or 0 for unbounded."
        " Ignored in evaluation mode."
#if BOOST_OS_WINDOWS
        " Defaults to the global setting, or 0 if none."
#endif
      )(
        "webgpu",
        "Enable experimental WebGPU support. Use at your own risk."
#if BOOST_OS_WINDOWS
        " Ignored if WebGPU is already enabled via global setting."
#endif
      )(
        "webgpu-device", po::value(&webGPUDevice),
        "Specify the name or substring of the GPU adapter device to use."
        " Ignored if WebGPU is not enabled."
      )(
        "options,o", po::value(&implementationOptions),
        "Specify options to pass to the underlying implementation."
        " More than one can be specified."
      )(
        "timeout,t", po::value(&timeoutSeconds)->default_value(timeoutSeconds),
        "Specify the maximum time to wait for communication from the input,"
        " when the event loop is empty, in seconds."
      )(
        "shutdown-timeout", po::value(&shutdownTimeoutSeconds),
        "Specify the maximum time to wait for the evaluator server to shut down"
        " after interruption, in seconds. After this timeout, any child"
        " processes are detached and will continue to run."
      )(
        "file,f", po::value(&filePathStrings),
        "Specify a code file to evaluate. More than one can be specified."
      )(
        "code,c", po::value(&strings),
        "Specify a code string to evaluate. More than one can be specified."
      )(
        "debugging-port", po::value(&debuggingPort),
        "Specify the port to listen on for a debugging connection."
        " If unspecified, debugging is not enabled."
      )(
        "debugging-wait", po::bool_switch(&debuggingWait),
        "Block evaluation while waiting for a connection on the debugging port."
        " Ignored if debugging is not enabled."
      );
#if BOOST_OS_WINDOWS
      od.add_options()(
        "start-event", po::value(&startEventNumbers),
        "Specify the handle to trigger an event for when the evaluator starts."
        " If unspecified, no event is triggered."
        " If more than one is specified, the last one is used."
      );
#endif

      po::positional_options_description pod{};
      pod.add("file", -1);

      try {
        po::variables_map vm{};
        po::store(
          po::command_line_parser(argc, argv).options(od).positional(pod).run(),
          vm
        );
        po::notify(vm);

        // Process arguments that cause the program to exit.
        if (vm.count("help")) {
          DCP::Argument::printHelp(std::cout,
            executableFile, argumentsUsage, argumentsDescription, od
          );
          return EXIT_SUCCESS;
        }
        if (vm.count("version")) {
          DCP::Version::print(std::cout, executableFile);
          return EXIT_SUCCESS;
        }

        // Using a `bool_switch` option type would cause the default value to be
        // overwritten by either `true` or `false`, depending on the presence or
        // absence of the flag, respectively. Instead, the variable is set to
        // `true` only if the flag is present, thereby preserving the default
        // value in the absence of the flag.
        if (vm.count("webgpu")) {
          webGPU = true;
        }

#if BOOST_OS_WINDOWS
        if (!startEventNumbers.empty()) {
          startEventNumber = startEventNumbers.back();
        }
#endif

        if (!socketNumbers.empty()) {
          socketNumber = socketNumbers.back();
        }
#if BOOST_OS_WINDOWS
        if ((socketNumber == INVALID_SOCKET) && port.empty()) {
          throw po::error(
            "Windows requires either socket or port to be specified"
          );
        }
#endif
      } catch (po::required_option const &error) {
        argumentsError = boost::str(
          boost::format("Missing argument: %s") % error.what()
        );
        DCP::Argument::printHelp(argumentsErrorHelp,
          executableFile, argumentsUsage, argumentsDescription, od
        );
      } catch (po::error const &error) {
        argumentsError = boost::str(
          boost::format("Invalid argument: %s") % error.what()
        );
        DCP::Argument::printHelp(argumentsErrorHelp,
          executableFile, argumentsUsage, argumentsDescription, od
        );
      }
    } catch (...) {
      argumentsError = boost::str(
        boost::format("Error parsing arguments: %s")
        % boost::current_exception_diagnostic_information().data()
      );
    }

    // Initialize the logger.
    auto const &logger = DCP::Logger::getInstance(
      executablePath.stem().string(), maxLogLevel, maxSystemLogLevel
    );

    // Start try block that logs any exceptions.
    try {
      // Log the executable name and version.
      logger.log(DCP::LogLevel::info, "Running \"%s\", version %s...",
        executableFile.data(), DCP_version
      );

      // Log arguments.
      if (logger.willLog(DCP::LogLevel::debug)) {
        for (int argi{}; argi < argc; ++argi) {
          logger.log(DCP::LogLevel::debug, "Argument %i: %s", argi, argv[argi]);
        }
      }

      // Handle any argument errors.
      if (!argumentsError.empty()) {
        logger.log(DCP::LogLevel::error, "%s", argumentsError.data());
        if (logger.getMaxLogLevel() < DCP::LogLevel::error) {
          std::cerr << argumentsError << std::endl;
        }
        std::cerr << argumentsErrorHelp.str();
        return EXIT_FAILURE;
      }

#if BOOST_OS_WINDOWS
      // Remove "ignore" signal handler, if added.
      SetConsoleCtrlHandler(nullptr, false);

      // Add custom signal handler.
      SetLastError(ERROR_SUCCESS);
      if (!SetConsoleCtrlHandler(handleConsoleCtrl, true)) {
        logger.log(DCP::LogLevel::error, "SetConsoleCtrlHandler() failed: %s",
          std::error_code(
            GetLastError(), std::system_category()
          ).message().data()
        );
        return EXIT_FAILURE;
      }

      // If start event handle provided, trigger it.
      if (startEventNumber) {
        logger.log(DCP::LogLevel::debug,
          "Setting start event for event number %" PRIuPTR "...",
          startEventNumber
        );
        DCP::Handle<HANDLE, nullptr> const startEvent(
          reinterpret_cast<HANDLE>(startEventNumber), CloseHandle
        );

        SetLastError(ERROR_SUCCESS);
        if (!SetEvent(startEvent)) {
          logger.log(DCP::LogLevel::error, "SetEvent() failed: %s",
            std::error_code(
              GetLastError(), std::system_category()
            ).message().data()
          );
          return EXIT_FAILURE;
        }
      }
#else
      DCP::Signal::Handler signalHandler{handleSignal, SIGINT, SIGTERM};
#endif

      if ((socketNumber != INVALID_SOCKET) || port.empty()) {
        if (!port.empty()) {
          assert(socketNumber != INVALID_SOCKET);
          logger.log(DCP::LogLevel::debug,
            "Ignoring port; socket %" DCP_Socket_print " specified",
            socketNumber
          );
          port.clear();
        }
        assert(port.empty());

        DCP::Socket::Handle input{};
        DCP::Socket::Handle output{};
        if (socketNumber == INVALID_SOCKET) {
#if BOOST_OS_WINDOWS
          assert(false); // This should have been checked above.
          throw std::invalid_argument(
            "Windows requires either port or socket to be specified"
          );
#else
          // Treat standard input and output as sockets.
          input = DCP::Socket::Handle(fileno(stdin));
          output = DCP::Socket::Handle(fileno(stdout));

          // Unblock the sockets.
          logger.log(DCP::LogLevel::debug, "Unblocking I/O sockets...");
          DCP::Socket::unblock(input);
          DCP::Socket::unblock(output);
          logger.log(DCP::LogLevel::debug, "Unblocked I/O sockets");
#endif
        } else {
          // Unblock the socket.
          logger.log(DCP::LogLevel::debug,
            "Unblocking socket %" DCP_Socket_print "...", socketNumber
          );
          DCP::Socket::unblock(socketNumber);
          logger.log(DCP::LogLevel::debug,
            "Unblocked socket %" DCP_Socket_print, socketNumber
          );

          input = output = DCP::Socket::Handle(
            socketNumber, DCP::Socket::close
          );
        }

        // Construct the thread-local evaluator instance.
        auto &evaluator = DCP::Evaluator::getInstance(
          output, webGPU, webGPUDevice, debuggingPort, implementationOptions
        );

        // Create a thread that monitors quitNow; when it becomes true,
        // terminate evaluation.
        static_assert(std::atomic<bool>::is_always_lock_free,
          "Platform not supported:"
          " std::atomic<bool> not guaranteed to be lock-free."
        );
        boost::strict_scoped_thread<> thread(
          [&evaluator]() noexcept {
            try {
              static auto const sleepTimeout = std::chrono::milliseconds(20);
              while (!quitNow) {
                std::this_thread::sleep_for(sleepTimeout);
              }
              DCP::tryToLog(DCP::LogLevel::debug, "Evaluator terminating...");
              if (evaluator.terminate()) {
                DCP::log(DCP::LogLevel::debug, "Evaluator terminated");
              } else {
                DCP::log(DCP::LogLevel::warning,
                  "Evaluator may not have fully terminated"
                );
              }
            } catch (...) {
              DCP::tryToLog(DCP::LogLevel::warning,
                "Exception in quit monitoring thread: %s",
                boost::current_exception_diagnostic_information().data()
              );
            }
          }
        );

        // Block the main thread and perform the evaluation.
        try {
          logger.log(DCP::LogLevel::info,
            "Evaluating (press Ctrl+C to quit)..."
          );

          if (debuggingPort) {
            logger.log(DCP::LogLevel::info,
              "Debugging enabled. Listening on port %d...", *debuggingPort
            );
            if (debuggingWait) {
              logger.log(DCP::LogLevel::info,
                "Debugging wait enabled."
                " Blocking evaluation until debugger connects..."
              );
              evaluator.waitForDebugger();
            }
          }

          evaluator.evaluate(
            input, filePathStrings, strings,
            timeoutSeconds * DCP::Time::millisecondsPerSecond
          );

          logger.log(DCP::LogLevel::debug, "Evaluation finished");
        } catch (...) {
          logger.tryToLog(DCP::LogLevel::error,
            "Exception while evaluating: %s",
            boost::current_exception_diagnostic_information().data()
          );
        }

        // Wrap up the thread.
        quitNow = true;

        return evaluator.getExitCode();
      }
      assert(socketNumber == INVALID_SOCKET);
      assert(!port.empty());

      static DCP::Time::Milliseconds const listenTimeout{200};

      logger.log(DCP::LogLevel::info,
        "Listening for connections (press Ctrl+C to quit)..."
      );

      std::list<std::pair<DCP::Socket::Handle, DCP::Process>> children{};

      // No need to do anything for the listen callback.
      auto const listenCallback = [](
        DCP::Socket::Handle const &, short unsigned
      ) {};

      // Define the accept callback that starts an evaluator process for the
      // accepted socket.
      auto const acceptCallback = [
        &concurrency, &children, &executablePath, argc, argv
      ](DCP::Socket::Handle const &socket) {
        if (!quitNow && (socket != INVALID_SOCKET)) {
          // Prune dead processes.
          for (auto iterator = children.begin(); iterator != children.end(); ) {
            auto const &child = *iterator;
            child.second.wait(0);
            if (child.second.isRunning()) {
              ++iterator;
            } else {
              DCP::log(DCP::LogLevel::debug,
                "Pruning dead process %lu", child.second.getID()
              );
              iterator = children.erase(iterator);
            }
          }

          // If not at capacity, create the process.
          assert(!concurrency || (children.size() <= concurrency));
          if (concurrency && (children.size() >= concurrency)) {
            DCP::log(DCP::LogLevel::info,
              "Maximum concurrency of %zu reached; discarding connection",
              concurrency
            );
          } else {
            // Create the argument vector, reserve enough space to hold all of
            // the parent arguments (not including the initial "executable path"
            // argument) plus any additional arguments to be appended, and copy
            // the parent arguments in.
            std::vector<std::string> arguments{};
            assert(argc > 0);
            arguments.reserve(
              boost::numeric_cast<size_t>(argc) - 1U // Exclude initial element
#if BOOST_OS_WINDOWS
              + 2U // For `--start-event <value>` to be appended
#endif
              + 2U // For `--socket <value>` to be appended
            );
            // Copy all parent arguments, excluding the initial one.
            arguments.insert(arguments.end(), argv + 1, argv + argc);

#if BOOST_OS_WINDOWS
            // Create the child start event and append the corresponding
            // argument to the argument vector.
            auto const startEvent = DCP::Process::createEvent();
            {
              auto startEventString = std::to_string(
                reinterpret_cast<uintptr_t>(static_cast<HANDLE>(startEvent))
              );
              DCP::log(DCP::LogLevel::debug,
                "Child process start event: %s", startEventString.data()
              );
              arguments.emplace_back("--start-event");
              arguments.emplace_back(std::move(startEventString));
            }
#endif

            // Ensure the child socket is handled appriopriately and append the
            // corresponding argument to the argument vector.
            assert(socket != INVALID_SOCKET);
#if BOOST_OS_WINDOWS
            std::vector<HANDLE> additionalHandles{};
            additionalHandles.push_back(
              reinterpret_cast<HANDLE>(static_cast<SOCKET>(socket))
            );
#else
            DCP::Socket::setCloseOnExec(socket, false);
#endif
            {
              auto socketString = std::to_string(static_cast<SOCKET>(socket));
              DCP::log(DCP::LogLevel::debug,
                "Child process socket: %s", socketString.data()
              );
              arguments.emplace_back("--socket");
              arguments.emplace_back(std::move(socketString));
            }

            // Create the child process and append it to the children vector.
            children.emplace_back(socket,
              DCP::Process(executablePath, arguments,
#if BOOST_OS_WINDOWS
                {}, startEvent, additionalHandles
#else
                // Use the built-in `environ` array.
                environ
#endif
              )
            );
          }
        }
        if (quitNow) {
          DCP::log(DCP::LogLevel::debug, "Listener stopping");
        }
        return !quitNow;
      };

      // Perform blocking listen until `quitNow` is true.
      auto const success = DCP::Socket::listen(
        host ? host->data() : nullptr, port.data(),
        listenCallback, acceptCallback, nullptr, listenTimeout
      );
      if (success) {
        logger.log(DCP::LogLevel::info,
          "Evaluator server completed successfully."
        );
      } else {
        logger.log(DCP::LogLevel::warning,
          "Evaluator server completed unsuccessfully."
        );
      }

      // For each child process, create a thread that does a blocking abort.
      logger.log(DCP::LogLevel::debug,
        "Starting child process thread group..."
      );
      boost::thread_group threads{};
      for (auto &child: children) {
        threads.create_thread(
          [&shutdownTimeoutSeconds, &child]() noexcept {
            auto const processID = child.second.getID();

            DCP::tryToLog(DCP::LogLevel::debug,
              "Interrupting child process %lu...", processID
            );
            try {
              child.second.interrupt();
            } catch (...) {
              DCP::tryToLog(DCP::LogLevel::error,
                "Failed to interrupt child process: %s",
                boost::current_exception_diagnostic_information().data()
              );
            }

            if (shutdownTimeoutSeconds) {
              DCP::tryToLog(DCP::LogLevel::debug,
                "Waiting up to %" PRIu64 " seconds for child process %lu...",
                *shutdownTimeoutSeconds, processID
              );
              child.second.wait(
                *shutdownTimeoutSeconds * DCP::Time::millisecondsPerSecond
              );
              if (child.second.isRunning()) {
                child.second.detach();
                DCP::tryToLog(DCP::LogLevel::warning,
                  "Child process %lu detached", processID
                );
                return;
              }
            } else {
              DCP::tryToLog(DCP::LogLevel::debug,
                "Waiting for child process %lu...", processID
              );
              child.second.wait();
            }

            DCP::tryToLog(DCP::LogLevel::debug,
              "Child process %lu ended", processID
            );
          }
        );
      }

      // Wait for all child processes to finish.
      logger.log(DCP::LogLevel::debug, "Joining child process thread group...");
      threads.join_all();

      return success ? EXIT_SUCCESS : EXIT_FAILURE;
    } catch (...) {
      logger.log(DCP::LogLevel::error, "Error: %s",
        boost::current_exception_diagnostic_information().data()
      );
      return EXIT_FAILURE;
    }
  } catch (...) {
    try {
      std::cerr << boost::current_exception_diagnostic_information();
      std::cerr << std::endl;
    } catch (...) {
      assert(false);
    }
    return EXIT_FAILURE;
  }
}
