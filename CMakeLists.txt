# Copyright (c) 2019 Distributive Corp. All Rights Reserved.

cmake_minimum_required(VERSION "3.27" FATAL_ERROR)

# Have find_package use <UPPERCASE>_ROOT variables.
cmake_policy(SET CMP0144 NEW)

# Read the version from the version file.
# CMake only supports dot-separated integer version components.
file(READ "${CMAKE_CURRENT_SOURCE_DIR}/VERSION.txt" DCP_VERSION)
string(STRIP "${DCP_VERSION}" DCP_VERSION)

# Create the project.
project("dcp"
  VERSION "${DCP_VERSION}"
  DESCRIPTION "The native portion of the Distributive Compute Protocol."
  LANGUAGES "C" "CXX"
)

# Confirm that the version has 4 components.
if(
  (PROJECT_VERSION_MAJOR STREQUAL "") OR
  (PROJECT_VERSION_MINOR STREQUAL "") OR
  (PROJECT_VERSION_PATCH STREQUAL "") OR
  (PROJECT_VERSION_TWEAK STREQUAL "")
)
  message(
    FATAL_ERROR
    "The version must have 4 non-negative integer components: ${DCP_VERSION}"
  )
endif()

# Set DCP_VERSION to omit the tweak version if 0, and append prerelease and
# optional build information.
string(
  JOIN "." DCP_VERSION
  ${PROJECT_VERSION_MAJOR}
  ${PROJECT_VERSION_MINOR}
  ${PROJECT_VERSION_PATCH}
)
if(NOT PROJECT_VERSION_TWEAK EQUAL 0)
  # A non-zero tweak component always denotes an alpha version; therefore,
  # 1.2.3.1-alpha and 1.2.3.1 are equivalent. Use the former here for clarity.
  string(APPEND DCP_VERSION ".${PROJECT_VERSION_TWEAK}-alpha")
endif()
find_package(Git REQUIRED)
set(_VERSION_BUILD)
execute_process(
  COMMAND "${GIT_EXECUTABLE}" "rev-parse" "--short" "HEAD"
  WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
  OUTPUT_VARIABLE _GIT_COMMIT
  ERROR_VARIABLE _GIT_COMMIT_ERROR
  OUTPUT_STRIP_TRAILING_WHITESPACE
)
if(_GIT_COMMIT)
  list(APPEND _VERSION_BUILD "${_GIT_COMMIT}")
else()
  message(WARNING "Could not get git commit: ${_GIT_COMMIT_ERROR}")
endif()
unset(_GIT_COMMIT)
unset(_GIT_COMMIT_ERROR)
execute_process(
  COMMAND "${GIT_EXECUTABLE}" "diff-index" "--name-only" "HEAD" "--"
  WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
  OUTPUT_VARIABLE _GIT_COMMIT_DIRTY
  ERROR_QUIET
  OUTPUT_STRIP_TRAILING_WHITESPACE
)
if(_GIT_COMMIT_DIRTY)
  list(APPEND _VERSION_BUILD "uncommitted")
endif()
unset(_GIT_COMMIT_DIRTY)
if(_VERSION_BUILD)
  list(JOIN _VERSION_BUILD "." _VERSION_BUILD)
  string(APPEND DCP_VERSION "+${_VERSION_BUILD}")
endif()
unset(_VERSION_BUILD)
message(STATUS "DCP_VERSION: ${DCP_VERSION}")

# Set up IDE target folders.
if(NOT DEFINED CMAKE_FOLDER)
  if(MSVC)
    set(_TARGETS_FOLDER "")
  else()
    set(_TARGETS_FOLDER "Targets")
  endif()
  set(CMAKE_FOLDER "${_TARGETS_FOLDER}")

  set_property(GLOBAL PROPERTY USE_FOLDERS ON)
  set_property(GLOBAL PROPERTY PREDEFINED_TARGETS_FOLDER "${_TARGETS_FOLDER}")

  unset(_TARGETS_FOLDER)
endif()

# Set C++ settings.
message(STATUS "CMAKE_CXX_COMPILER_ID: ${CMAKE_CXX_COMPILER_ID}")
message(STATUS "CMAKE_CXX_COMPILER_VERSION: ${CMAKE_CXX_COMPILER_VERSION}")
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_STANDARD "17")
set(CMAKE_CXX_STANDARD_INCLUDE_DIRECTORIES
  ${CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES}
)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_VISIBILITY_PRESET "hidden")

# Export compile commands.
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# Set RPATH settings.
set(CMAKE_BUILD_WITH_INSTALL_RPATH ON)
if(APPLE)
  set(CMAKE_INSTALL_RPATH "@executable_path")
elseif(UNIX)
  set(CMAKE_INSTALL_RPATH "$ORIGIN")
endif()

# Set default install settings.
set(CMAKE_INSTALL_DEFAULT_COMPONENT_NAME "SHARED")

# Set OS.
message(STATUS "CMAKE_SYSTEM_NAME: ${CMAKE_SYSTEM_NAME}")
if(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
  set(DCP_OS "MacOS")
else()
  set(DCP_OS "${CMAKE_SYSTEM_NAME}")
endif()
message(STATUS "DCP_OS: ${DCP_OS}")

# Set architecture.
message(STATUS "CMAKE_SYSTEM_PROCESSOR: ${CMAKE_SYSTEM_PROCESSOR}")
string(TOLOWER "${CMAKE_SYSTEM_PROCESSOR}" DCP_ARCHITECTURE)
if(DCP_ARCHITECTURE STREQUAL "aarch64")
  set(DCP_ARCHITECTURE "arm64")
elseif(NOT DCP_ARCHITECTURE STREQUAL "arm64")
  set(DCP_ARCHITECTURE "x64")
endif()
message(STATUS "DCP_ARCHITECTURE: ${DCP_ARCHITECTURE}")
set(CMAKE_VS_PLATFORM_TOOLSET_HOST_ARCHITECTURE "${DCP_ARCHITECTURE}")

# Set configuration types.
# For single-config generators (eg. "Unix Makefiles"), default to Release.
set(CMAKE_CONFIGURATION_TYPES "Release" "MinSizeRel" "RelWithDebInfo" "Debug")
get_property(_IS_MULTI_CONFIG GLOBAL PROPERTY GENERATOR_IS_MULTI_CONFIG)
if(_IS_MULTI_CONFIG)
  set(CMAKE_CONFIGURATION_TYPES "${CMAKE_CONFIGURATION_TYPES}"
    CACHE STRING "Semicolon-separated list of supported configuration types."
    FORCE
  )
  message(STATUS "Build configuration to be selected at build time")
else()
  if(NOT CMAKE_BUILD_TYPE IN_LIST CMAKE_CONFIGURATION_TYPES)
    list(GET CMAKE_CONFIGURATION_TYPES 0 CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "${CMAKE_BUILD_TYPE}" CACHE STRING "" FORCE)
  endif()
  set_property(
    CACHE CMAKE_BUILD_TYPE
    PROPERTY HELPSTRING "The build configuration."
  )
  set_property(
    CACHE CMAKE_BUILD_TYPE
    PROPERTY STRINGS ${CMAKE_CONFIGURATION_TYPES}
  )
  message(STATUS "CMAKE_BUILD_TYPE: ${CMAKE_BUILD_TYPE}")
endif()
unset(_IS_MULTI_CONFIG)

# Set imported configuration mappings.
set(CMAKE_MAP_IMPORTED_CONFIG_DEBUG Debug Release "")
set(CMAKE_MAP_IMPORTED_CONFIG_MINSIZEREL MinSizeRel Release "")
set(CMAKE_MAP_IMPORTED_CONFIG_RELEASE Release "")
set(CMAKE_MAP_IMPORTED_CONFIG_RELWITHDEBINFO RelWithDebInfo Release "")

# Set the global MSVC runtime library default (ignored if not MSVC).
set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")

# Prevent spurious install_name_tool warnings on Xcode:
# https://gitlab.kitware.com/cmake/cmake/-/issues/21854
set(CMAKE_XCODE_ATTRIBUTE_OTHER_CODE_SIGN_FLAGS "-o linker-signed")

# Set binary output directories.
set(DCP_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/artifacts")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${DCP_OUTPUT_DIRECTORY}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${DCP_OUTPUT_DIRECTORY}")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${DCP_OUTPUT_DIRECTORY}")
foreach(_CONFIGURATION_TYPE IN ITEMS ${CMAKE_CONFIGURATION_TYPES})
  string(TOUPPER "${_CONFIGURATION_TYPE}" _CONFIGURATION_SUFFIX)
  set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_${_CONFIGURATION_SUFFIX}
    "${DCP_OUTPUT_DIRECTORY}/${_CONFIGURATION_TYPE}"
  )
  set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_${_CONFIGURATION_SUFFIX}
    "${DCP_OUTPUT_DIRECTORY}/${_CONFIGURATION_TYPE}"
  )
  set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_${_CONFIGURATION_SUFFIX}
    "${DCP_OUTPUT_DIRECTORY}/${_CONFIGURATION_TYPE}"
  )
  unset(_CONFIGURATION_SUFFIX)
endforeach()
# Note that this contains a generator expression, so can only be used in
# contexts where generator expressions are permitted.
set(DCP_OUTPUT_DIRECTORY "${DCP_OUTPUT_DIRECTORY}/$<CONFIG>")

# Set default directory for automatic "install" target.
if(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
  set(CMAKE_INSTALL_PREFIX "${CMAKE_CURRENT_BINARY_DIR}/install"
    CACHE PATH "Install path prefix, prepended onto install directories."
    FORCE
  )
endif()

# Set install paths.
if(UNIX)
  # Set the install prefix to root to allow installation to multiple paths
  # (eg. on MacOS: /opt/dcp and /Applications).
  set(DCP_INSTALL_PREFIX "/")
  set(DCP_INSTALL_SUBDIRECTORY "opt/dcp")
  set(DCP_INSTALL_EXECUTABLE_SUBDIRECTORY "${DCP_INSTALL_SUBDIRECTORY}/bin")
else()
  # Use a flat install tree, inside the default install prefix.
  set(DCP_INSTALL_PREFIX "")
  set(DCP_INSTALL_SUBDIRECTORY ".")
  set(DCP_INSTALL_EXECUTABLE_SUBDIRECTORY ".")
endif()
set(DCP_INSTALL_DIRECTORY "${DCP_INSTALL_PREFIX}${DCP_INSTALL_SUBDIRECTORY}")
set(DCP_INSTALL_EXECUTABLE_DIRECTORY
  "${DCP_INSTALL_PREFIX}${DCP_INSTALL_EXECUTABLE_SUBDIRECTORY}"
)

# Add option to disable extra compiler warnings.
option(DCP_WARNINGS "Add extra compiler warning checks." ON)
if(DCP_WARNINGS)
  message(STATUS "DCP_WARNINGS: ON (Adding extra compiler warning checks)")
else()
  message(STATUS "DCP_WARNINGS: OFF (Not adding extra compiler warning checks)")
endif()

# Add option to treat warnings as errors.
option(DCP_WARNINGS_AS_ERRORS "Treat warnings as errors." ON)
if(DCP_WARNINGS_AS_ERRORS)
  message(STATUS "DCP_WARNINGS_AS_ERRORS: ON (Treating warnings as errors)")
else()
  message(
    STATUS "DCP_WARNINGS_AS_ERRORS: OFF (Not treating warnings as errors)"
  )
endif()

# Add option to use clang-tidy.
option(DCP_CLANG_TIDY "Use clang-tidy." OFF)
if(DCP_CLANG_TIDY)
  message(STATUS "DCP_CLANG_TIDY: ON")
  if(NOT CLANG_TIDY_EXECUTABLE)
    find_program(CLANG_TIDY_EXECUTABLE
      NAMES "clang-tidy"
      DOC "Clang-tidy executable path"
      REQUIRED
    )
  endif()
  message(STATUS "CLANG_TIDY_EXECUTABLE: ${CLANG_TIDY_EXECUTABLE}")
  set(CMAKE_CXX_CLANG_TIDY "${CLANG_TIDY_EXECUTABLE}"
    "-extra-arg=-Wno-unknown-warning-option"
    "-extra-arg=-Wno-unused-command-line-argument"
    "-header-filter=\.hh$"
  )
  if(
    (CMAKE_CXX_COMPILER_ID STREQUAL "GNU") AND
    (DCP_ARCHITECTURE STREQUAL "x64")
  )
    # /usr/lib/gcc/x86_64-linux-gnu/11/include/xmmintrin.h:130:19: error:
    # use of undeclared identifier '__builtin_ia32_*' [clang-diagnostic-error]
    list(APPEND CMAKE_CXX_CLANG_TIDY "-extra-arg=-mno-sse2")
  endif()
  if(DCP_WARNINGS_AS_ERRORS)
    list(APPEND CMAKE_CXX_CLANG_TIDY "-warnings-as-errors=*")
  endif()
else()
  message(STATUS "DCP_CLANG_TIDY: OFF")
endif()

# Add option to enable tests.
option(DCP_TEST "Enable tests." ON)
if(DCP_TEST)
  message(STATUS "DCP_TEST: ON (Tests enabled)")
else()
  message(STATUS "DCP_TEST: OFF (Tests not enabled)")
endif()

# For compilers that support it, add option to add mutation test target.
if(NOT CMAKE_CXX_COMPILER_ID MATCHES ".*Clang")
  set(DCP_MUTATION OFF)
  message(STATUS "DCP_MUTATION: OFF (Mutation testing only supported on Clang)")
else()
  option(DCP_MUTATION "Enable mutation targets." OFF)
  if(DCP_MUTATION)
    message(STATUS "DCP_MUTATION: ON (Mutation targets enabled)")
    if(CMAKE_CXX_COMPILER_VERSION STREQUAL "")
      message(
        FATAL_ERROR "Cannot mutate: Compiler version could not be obtained"
      )
    endif()
    string(REPLACE "." ";" _COMPILER_VERSION "${CMAKE_CXX_COMPILER_VERSION}")
    list(GET _COMPILER_VERSION 0 _COMPILER_VERSION_MAJOR)
    find_program(MULL_RUNNER
      NAMES "mull-runner-${_COMPILER_VERSION_MAJOR}"
      PATHS "/usr/bin" "/usr/local/bin"
      DOC "Mull runner executable path"
      REQUIRED
    )
    find_file(MULL_PLUGIN
      NAMES "mull-ir-frontend-${_COMPILER_VERSION_MAJOR}"
      PATHS "/usr/lib" "/usr/local/lib"
      DOC "Mull plugin path"
      REQUIRED
    )
    unset(_COMPILER_VERSION_MAJOR)
    unset(_COMPILER_VERSION)
  else()
    message(STATUS "DCP_MUTATION: OFF (Mutation targets not enabled)")
  endif()
endif()

# For compilers that support it, add option to add coverage target.
if(
  (NOT CMAKE_CXX_COMPILER_ID STREQUAL "GNU") AND
  (NOT CMAKE_CXX_COMPILER_ID MATCHES ".*Clang")
)
  set(DCP_COVERAGE OFF)
  message(STATUS "DCP_COVERAGE: OFF (Only supported on Clang and GCC)")
else()
  option(DCP_COVERAGE "Enable coverage target." OFF)
  if(DCP_COVERAGE)
    message(STATUS "DCP_COVERAGE: ON (Coverage target enabled)")
    if(UNIX)
      set(_COVERAGE_CLEAN_COMMAND
        "find" "."
        "-name" "*.gcda"
        "-type" "f"
        "-delete"
      )
    endif()
  else()
    message(STATUS "DCP_COVERAGE: OFF (Coverage target not enabled)")
  endif()
endif()

# Add option to perform code formatting functions.
option(DCP_FORMAT "Perform code formatting functions." ON)
if(DCP_FORMAT)
  message(STATUS "DCP_FORMAT: ON (Performing code formatting functions)")
else()
  message(STATUS "DCP_FORMAT: OFF (Not performing code formatting functions)")
endif()

# Add option to use provided Node or system.
option(DCP_NODE "Include Node." ON)
if(DCP_NODE)
  message(STATUS "DCP_NODE: ON (Node included)")
else()
  message(STATUS "DCP_NODE: OFF (Node not included)")
endif()

if(WIN32)
  # Add option to enable Omaha auto-update.
  option(DCP_OMAHA "Enable Omaha." OFF)
  if(DCP_OMAHA)
    message(STATUS "DCP_OMAHA: ON (Omaha enabled)")
  else()
    message(STATUS "DCP_OMAHA: OFF (Omaha not enabled)")
  endif()
else()
  set(DCP_OMAHA OFF)
  message(STATUS "DCP_OMAHA: OFF (Omaha not supported on this platform)")
endif()

# Set script extension.
if(WIN32)
  set(DCP_SCRIPT_EXTENSION ".bat")
else()
  set(DCP_SCRIPT_EXTENSION ".sh")
endif()

# Set non-localizable company identifiers.
set(DCP_COMPANY_LEGAL_NAME "Distributive Corp.")
set(DCP_COMPANY_NAME "Distributive")
set(DCP_COMPANY_ID "distributive")
set(DCP_COMPANY_ASSEMBLY_ID "${DCP_COMPANY_NAME}")
set(DCP_COMPANY_BUNDLE_ID "network.${DCP_COMPANY_ID}")

# Set non-localizable product identifiers.
set(DCP_PRODUCT_NAME "DCP")
set(DCP_PRODUCT_ID "dcp")
set(DCP_PRODUCT_ASSEMBLY_ID "${DCP_COMPANY_ASSEMBLY_ID}.DCP")
set(DCP_PRODUCT_BUNDLE_ID "${DCP_COMPANY_BUNDLE_ID}.dcp")

# Set non-localizable worker product identifiers.
set(DCP_WORKER_PRODUCT_NAME "${DCP_PRODUCT_NAME} Worker")
set(DCP_WORKER_PRODUCT_ID "${DCP_PRODUCT_ID}-worker")
set(DCP_WORKER_PRODUCT_ASSEMBLY_ID "${DCP_PRODUCT_ASSEMBLY_ID}.Worker")
set(DCP_WORKER_PRODUCT_BUNDLE_ID "${DCP_PRODUCT_BUNDLE_ID}.worker")

# Set user-facing, high-level "do work" symlink name.
set(DCP_WORK_SYMLINK "dcp-work")

# Find NPM.
option(NPM_VERBOSE "Enable verbose NPM build logging." OFF)
if(NPM_VERBOSE)
  message(STATUS "NPM_VERBOSE: ON (Verbose NPM build logging enabled)")
else()
  message(STATUS "NPM_VERBOSE: OFF (Verbose NPM build logging not enabled)")
endif()
option(NPM_AUDIT "Automatically perform NPM audit on each build." OFF)
if(NPM_AUDIT)
  message(STATUS "NPM_AUDIT: ON (NPM auditing enabled)")
else()
  message(STATUS "NPM_AUDIT: OFF (NPM auditing not enabled)")
endif()
if(NOT NPM_EXECUTABLE)
  find_program(NPM_EXECUTABLE
    NAMES "npm"
    DOC "NPM executable path"
  )
endif()
if(NPM_EXECUTABLE)
  message(STATUS "NPM_EXECUTABLE: ${NPM_EXECUTABLE}")
  set(NPM_COMMAND "${NPM_EXECUTABLE}" "i"
    "--only=production"
    "--progress=false"
  )
  if(NPM_VERBOSE)
    list(APPEND NPM_COMMAND "--loglevel" "silly")
  endif()
  if(NPM_AUDIT)
    set(NPM_AUDIT_COMMAND "${NPM_EXECUTABLE}" "audit")
  endif()
else()
  message(WARNING "NPM not found")
  set(NPM_EXECUTABLE "")
endif()

# Find Signtool.
if(WIN32)
  if(NOT SIGNTOOL_EXECUTABLE)
    # Use standard Windows environment variable to get Program Files path.
    set(_ENV_PROGRAM_FILES_X86 "ProgramFiles(x86)")
    set(_SIGNTOOL_MINIMUM_SDK_VERSION "10.0.19041.0")
    string(
      CONCAT _SIGNTOOL_PATH
      "$ENV{${_ENV_PROGRAM_FILES_X86}}\\"
      "Windows Kits\\10\\bin\\"
      "${_SIGNTOOL_MINIMUM_SDK_VERSION}\\${DCP_ARCHITECTURE}"
    )
    unset(_ENV_PROGRAM_FILES_X86)
    unset(_SIGNTOOL_MINIMUM_SDK_VERSION)

    find_program(SIGNTOOL_EXECUTABLE "signtool" PATHS "${_SIGNTOOL_PATH}")
    unset(_SIGNTOOL_PATH)
  endif()
  if(SIGNTOOL_EXECUTABLE)
    message(STATUS "SIGNTOOL_EXECUTABLE: ${SIGNTOOL_EXECUTABLE}")
  else()
    message(STATUS "Signtool not found")
    set(SIGNTOOL_EXECUTABLE "")
  endif()
endif()

if(DCP_TEST)
  enable_testing()
endif()

function(dcp_add_targets)

  function(dcp_get_targets _VARIABLE)
    function(dcp_get_directory_targets _VARIABLE _DIRECTORY)
      get_property(_SUBDIRECTORIES
        DIRECTORY "${_DIRECTORY}"
        PROPERTY SUBDIRECTORIES
      )
      foreach(_SUBDIRECTORY IN LISTS _SUBDIRECTORIES)
        dcp_get_directory_targets("${_VARIABLE}" "${_SUBDIRECTORY}")
      endforeach()

      get_directory_property(_SUBTARGETS
        DIRECTORY "${_DIRECTORY}"
        BUILDSYSTEM_TARGETS
      )
      set("${_VARIABLE}" "${${_VARIABLE}}" ${_SUBTARGETS} PARENT_SCOPE)
    endfunction()

    dcp_get_directory_targets("${_VARIABLE}" "${PROJECT_SOURCE_DIR}")
    list(REMOVE_ITEM "${_VARIABLE}" "${PROJECT_NAME}")
    set("${_VARIABLE}" "${${_VARIABLE}}" ${_SUBTARGETS} PARENT_SCOPE)
  endfunction()

  # Configures files and sets ${_OUTPUTS_VARIABLE} to the resulting full paths.
  # INPUTS: Relative paths of input files with last extension ".in"
  # EXECUTABLE: Indicates that the files are to be executable
  function(dcp_configure_files _OUTPUTS_VARIABLE)
    cmake_parse_arguments(PARSE_ARGV 1 "" "EXECUTABLE" "" "INPUTS")
    set(_OUTPUTS)
    foreach(_INPUT IN LISTS _INPUTS)
      get_filename_component(_OUTPUT_SUBDIRECTORY "${_INPUT}" DIRECTORY)
      get_filename_component(_OUTPUT_NAME "${_INPUT}" NAME_WLE)

      set(_OUTPUT "${_OUTPUT_SUBDIRECTORY}/${_OUTPUT_NAME}")
      if(NOT _INPUT STREQUAL "${_OUTPUT}.in")
        message(FATAL_ERROR "Input '${_INPUT}' invalid; must end with '.in'")
      endif()
      set(_OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/${_OUTPUT}")

      configure_file("${_INPUT}" "${_OUTPUT}" USE_SOURCE_PERMISSIONS @ONLY)

      if(_EXECUTABLE)
        file(
          CHMOD "${_OUTPUT}"
          FILE_PERMISSIONS
          OWNER_READ OWNER_EXECUTE
          GROUP_READ GROUP_EXECUTE
          WORLD_READ WORLD_EXECUTE
        )
      endif()

      list(APPEND _OUTPUTS "${_OUTPUT}")
      unset(_OUTPUT)
    endforeach()
    set(${_OUTPUTS_VARIABLE} ${_OUTPUTS} PARENT_SCOPE)
  endfunction()

  function(dcp_add_sources _TARGET)
    set(_VALUE_ARGUMENTS "FOLDER" "DIRECTORY" "SUBDIRECTORY")
    set(_LIST_ARGUMENTS "FILES")
    cmake_parse_arguments(
      PARSE_ARGV 1 "" "COPY" "${_VALUE_ARGUMENTS}" "${_LIST_ARGUMENTS}"
    )
    if(NOT DEFINED _DIRECTORY)
      set(_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}")
    endif()
    source_group(
      TREE "${_DIRECTORY}/${_SUBDIRECTORY}"
      PREFIX "${_FOLDER}"
      FILES ${_FILES}
    )
    target_sources("${_TARGET}" PRIVATE ${_FILES})
    if(_COPY)
      foreach(_FILE IN LISTS _FILES)
        get_filename_component(_NAME "${_FILE}" NAME)
        set(_DESTINATION "${DCP_OUTPUT_DIRECTORY}/${_TARGET}-${_NAME}")
        add_custom_command(
          TARGET "${_TARGET}"
          POST_BUILD
          COMMAND
          "${CMAKE_COMMAND}" "-E" "copy_if_different" "${_FILE}"
          "${_DESTINATION}"
          WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
          VERBATIM
        )
        set_property(
          TARGET "${_TARGET}" PROPERTY OBJECT_DEPENDS "${_DESTINATION}"
        )
        unset(_NAME)
        unset(_DESTINATION)
      endforeach()
    endif()
  endfunction()

  set(CMAKE_FOLDER "${CMAKE_FOLDER}/${PROJECT_NAME}")

  # Add target for building everything.
  add_custom_target("${PROJECT_NAME}")

  # Add resource files.
  include("${CMAKE_CURRENT_SOURCE_DIR}/resource/localizable.cmake")
  dcp_add_sources("${PROJECT_NAME}"
    FOLDER "Resource Files"
    SUBDIRECTORY "resource"
    FILES
    "resource/dcp.ico"
    "resource/dcp.png"
    "resource/localizable.cmake"
  )

  # Add script files.
  dcp_add_sources("${PROJECT_NAME}"
    FOLDER "Script Files"
    SUBDIRECTORY "script"
    FILES
    "script/build.bat"
    "script/build.sh"
    "script/bundle.sh"
    "script/deploy.sh"
    "script/generate.bat"
    "script/generate.cmake"
    "script/generate.sh"
    "script/iconify.sh"
    "script/subscribe.sh"
    "script/unsubscribe.sh"
  )

  # Add root directory files.
  dcp_add_sources("${PROJECT_NAME}"
    FILES
    ".clang-tidy"
    ".gitignore"
    ".gitlab-ci.yml"
    ".markdownlint.json"
    "CHANGELOG.md"
    "CONTRIBUTING.md"
    "LICENSE.txt"
    "README.md"
    "VERSION.txt"
  )

  # Do any general target initialization.
  # Call this after creating a subdirectory target.
  function(dcp_target_initialize _TARGET)
    if(DCP_WARNINGS)
      get_target_property(_TARGET_TYPE "${_TARGET}" TYPE)
      if(
        (_TARGET_TYPE MATCHES "LIBRARY$") OR
        (_TARGET_TYPE STREQUAL "EXECUTABLE")
      )
        target_link_libraries("${_TARGET}" PRIVATE "${PROJECT_NAME}-warnings")
      endif()
    endif()

    if(_COVERAGE_CLEAN_COMMAND)
      get_target_property(_TARGET_BINARY_DIR "${_TARGET}" BINARY_DIR)
      if(_TARGET_BINARY_DIR)
        add_custom_command(
          TARGET "${_TARGET}"
          PRE_BUILD
          COMMAND ${_COVERAGE_CLEAN_COMMAND}
          WORKING_DIRECTORY "${_TARGET_BINARY_DIR}"
          VERBATIM
        )
      endif()
    endif()

    set_target_properties("${_TARGET}" PROPERTIES DCP_INITIALIZED ON)
  endfunction()

  # Add an external; appends to `_EXTERNAL_FILES`.
  set(_EXTERNAL_FILES)
  macro(dcp_add_external _EXTERNAL)
    add_subdirectory("externals/${_EXTERNAL}")
    set(_EXTERNAL_FILE "externals/${_EXTERNAL}/CMakeLists.txt")
    source_group(
      TREE "${CMAKE_CURRENT_SOURCE_DIR}/externals/${_EXTERNAL}"
      PREFIX "Externals\\${_EXTERNAL}"
      FILES "${_EXTERNAL_FILE}"
    )
    list(APPEND _EXTERNAL_FILES "${_EXTERNAL_FILE}")
    unset(_EXTERNAL_FILE)
  endmacro()

  # Add an interface; appends to `_INTERFACE_FILES`.
  set(_INTERFACE_FILES)
  macro(dcp_add_interface _INTERFACE)
    add_subdirectory("interfaces/${_INTERFACE}")
    set(_INTERFACE_FILE "interfaces/${_INTERFACE}/CMakeLists.txt")
    source_group(
      TREE "${CMAKE_CURRENT_SOURCE_DIR}/interfaces/${_INTERFACE}"
      PREFIX "Interfaces\\${PROJECT_NAME}-${_INTERFACE}"
      FILES "${_INTERFACE_FILE}"
    )
    list(APPEND _INTERFACE_FILES "${_INTERFACE_FILE}")
    unset(_INTERFACE_FILE)
  endmacro()

  # Add a subdirectory target.
  function(dcp_add_target _SUBDIRECTORY)
    add_subdirectory("targets/${_SUBDIRECTORY}")
    string(REPLACE "/" "-" _TARGET "${_SUBDIRECTORY}")
    set(_TARGET "${PROJECT_NAME}-${_TARGET}")
    if(NOT TARGET "${_TARGET}")
      message(STATUS "Target '${_TARGET}' not added")
      return()
    endif()

    add_dependencies("${PROJECT_NAME}" "${_TARGET}")

    get_target_property(_TARGET_INITIALIZED "${_TARGET}" DCP_INITIALIZED)
    if(NOT _TARGET_INITIALIZED)
      message(AUTHOR_WARNING "Target '${_TARGET}' not initialized")
      return()
    endif()

    message(STATUS "Target '${_TARGET}' added and initialized")
  endfunction()

  # Add externals.
  set(FETCHCONTENT_QUIET OFF)
  dcp_add_external("boost")
  dcp_add_external("dawn")
  if(DCP_NODE)
    dcp_add_external("node")
  else()
    set(NODE_ROOT "")
  endif()
  dcp_add_external("node_addon_api")
  dcp_add_external("node_api")
  if(WIN32)
    dcp_add_external("nssm")
  else()
    set(NSSM_ROOT "")
  endif()
  if(DCP_OMAHA)
    dcp_add_external("omaha")
  else()
    set(OMAHA_ROOT "")
  endif()
  dcp_add_external("socket_pair")
  if(DCP_FORMAT)
    dcp_add_external("uncrustify")
  endif()

  # Add targets.
  # Note that ordering may be significant for dependency reasons.
  dcp_add_interface("boost")
  dcp_add_interface("node_addon_api")
  dcp_add_interface("node_api")
  dcp_add_interface("dawn")
  dcp_add_interface("options")
  dcp_add_interface("warnings")
  dcp_add_target("node")
  dcp_add_target("nssm")
  dcp_add_target("library")
  dcp_add_target("licenses")
  dcp_add_target("evaluator-library")
  dcp_add_target("evaluator")
  dcp_add_target("supervisor")
  dcp_add_target("supervisor-library")
  dcp_add_target("sandbox")
  dcp_add_target("sandbox-library")
  dcp_add_target("service-library")
  dcp_add_interface("service")
  dcp_add_target("settings-library")
  dcp_add_target("settings-configurator")
  dcp_add_target("settings")
  dcp_add_target("screensaver")
  dcp_add_target("sign")
  dcp_add_target("format")
  dcp_add_target("uninstaller-bundle")
  dcp_add_target("sandbox-bundle")
  dcp_add_target("supervisor-setup")
  dcp_add_target("installer")
  dcp_add_target("docker")

  # Put all externals in "Externals" source group.
  dcp_add_sources("${PROJECT_NAME}"
    FOLDER "Externals"
    SUBDIRECTORY "externals"
    FILES ${_EXTERNAL_FILES}
  )
  unset(_EXTERNAL_FILES)

  # Put all interfaces in "Interfaces" source group.
  dcp_add_sources("${PROJECT_NAME}"
    FOLDER "Interfaces"
    SUBDIRECTORY "interfaces"
    FILES ${_INTERFACE_FILES}
  )
  unset(_INTERFACE_FILES)

  if(DCP_TEST)
    dcp_add_target("check")
    dcp_add_target("coverage")

    if(CMAKE_CXX_CLANG_TIDY)
      string(
        CONCAT _CLANG_TIDY_CHECKS
        "-android-cloexec-pipe,"
        "-bugprone-empty-catch,"
        "-cert-err58-cpp," # Boost.Test macros generate this warning
        "-cppcoreguidelines-avoid-magic-numbers,"
        "-cppcoreguidelines-macro-usage,"
        "-readability-avoid-unconditional-preprocessor-if,"
        "-readability-magic-numbers,"
      )
      list(APPEND CMAKE_CXX_CLANG_TIDY "-checks=${_CLANG_TIDY_CHECKS}")
      unset(_CLANG_TIDY_CHECKS)
    endif()

    set(_TEST_FIXTURES_REQUIRED)
    if(_COVERAGE_CLEAN_COMMAND)
      set(_COVERAGE_SETUP_FIXTURE "coverage-setup")
      add_test(
        NAME "${_COVERAGE_SETUP_FIXTURE}"
        COMMAND ${_COVERAGE_CLEAN_COMMAND}
        WORKING_DIRECTORY "${PROJECT_BINARY_DIR}"
      )
      set_tests_properties("${_COVERAGE_SETUP_FIXTURE}"
        PROPERTIES FIXTURES_SETUP "${_COVERAGE_SETUP_FIXTURE}"
      )
      list(APPEND _TEST_FIXTURES_REQUIRED "${_COVERAGE_SETUP_FIXTURE}")
      unset(_COVERAGE_SETUP_FIXTURE)
    endif()

    function(dcp_add_test_target)
      cmake_parse_arguments(PARSE_ARGV 0 "" "" "TARGET_VARIABLE" "")

      get_filename_component(_NAME "${CMAKE_CURRENT_SOURCE_DIR}" NAME)

      set(_TARGET "${PROJECT_NAME}-${_NAME}")
      add_executable("${_TARGET}" EXCLUDE_FROM_ALL)
      dcp_target_initialize("${_TARGET}")

      if(_COVERAGE_CLEAN_COMMAND)
        get_target_property(_TARGET_BINARY_DIR "${_TARGET}" BINARY_DIR)
        if(_TARGET_BINARY_DIR)
          add_custom_command(
            TARGET "${_TARGET}"
            PRE_BUILD
            COMMAND ${_COVERAGE_CLEAN_COMMAND}
            WORKING_DIRECTORY "${_TARGET_BINARY_DIR}"
            VERBATIM
          )
        endif()
      endif()

      # Generate a source file that defines BOOST_TEST_MODULE so that Boost Test
      # generates a main function there.
      set(_MAIN "${CMAKE_CURRENT_BINARY_DIR}/source/main.cc")
      file(
        WRITE "${_MAIN}"
        "#define BOOST_TEST_MODULE \"${_TARGET}\"\n"
        "#include <dcp/test.hh>\n"
      )
      dcp_add_sources("${_TARGET}"
        FOLDER "Source Files (Generated)"
        DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
        SUBDIRECTORY "source"
        FILES "${_MAIN}"
      )
      unset(_MAIN)

      add_dependencies("${PROJECT_NAME}-check" "${_TARGET}")

      add_test(
        NAME "${_NAME}"
        COMMAND "${_TARGET}" "--catch_system_errors=yes" "--log_level=warning"
      )
      set_tests_properties("${_NAME}"
        PROPERTIES FIXTURES_REQUIRED "${_TEST_FIXTURES_REQUIRED}"
      )

      if(DEFINED _TARGET_VARIABLE)
        set("${_TARGET_VARIABLE}" "${_TARGET}" PARENT_SCOPE)
      endif()
    endfunction()

    function(dcp_add_mutation_target _TEST_TARGET)
      if(NOT DCP_MUTATION)
        return()
      endif()

      get_filename_component(_TARGET "${CMAKE_CURRENT_SOURCE_DIR}" NAME)
      set(_TARGET "${PROJECT_NAME}-${_TARGET}")

      add_custom_target("${_TARGET}"
        COMMAND "${MULL_RUNNER}" "$<TARGET_FILE:${_TEST_TARGET}>"
        WORKING_DIRECTORY "${DCP_OUTPUT_DIRECTORY}"
        VERBATIM
      )
      dcp_target_initialize("${_TARGET}")

      add_dependencies("${_TARGET}"
        "${_TEST_TARGET}"
        "${PROJECT_NAME}-mutation"
      )
    endfunction()

    dcp_add_target("mutation")
    foreach(_SUBDIRECTORY IN ITEMS
      "library"
      "evaluator-library"
      "evaluator"
      "sandbox-library"
      "supervisor-library"
    )
      dcp_add_target("${_SUBDIRECTORY}-test")
      dcp_add_target("${_SUBDIRECTORY}-mutation")
    endforeach()
  endif()

  dcp_add_target("dot")
  dcp_add_target("html")
endfunction()
dcp_add_targets()
