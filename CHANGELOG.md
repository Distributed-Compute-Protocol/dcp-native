# CHANGES

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [7.3.12] - 2024-12-17

### Changed

* dcp-worker version update

## [7.3.11] - 2024-11-29

### Changed

* Updated webgpu version

### Added

* --webgpu-device flag to specify the adapter flag in webgpu initialization

## [7.3.10] - 2024-11-4

### Changed

* New version for updated dcp-worker version

## [7.3.9] - 2024-10-31

### Fixed

* Fix npm issues introduced in previous version
* Fix CI for Ubuntu 20

## [7.3.8] - 2024-10-30

### Changed

* Remove packages from being hard-coded in the package.json, install latest
  versions in build
* Use updated CI for Ubuntu 20 compatibility

## [7.3.7] - 2024-10-22

### Fixed

* Add user to necessary groups for gpu access

## [7.3.5] - 2024-10-17

### Added

* Provide navigator.userAgent on global object

## [7.3.4] - 2024-10-16

### Fixed

* Fixed matmul test for updated dcp-client

## [7.3.3] - 2024-10-15

### Added

* Add override for dcp-client to version 4.4.17

## [7.3.2] - 2024-10-10

### Fixed

* Fixed Dockerfile for updated docker release 

## [7.3.0] - 2024-09-24

### Added

* Added `initWebGPU` as global symbol provided to the JS environment,
  to lazy-load webGPU

### Changed

* Update Dawn to 6721
* `die` kills the JS environment synchronously when called
* `die` takes a boolean argument for if the evaluator should exit with a
   non-zero exit code
* .deb installer overhaul to add evaluator to systemd, and include kernel
   limitations to what the services may do

### Removed

* Remove instantiation of gpu device in evaluator constructor

## [7.2.0] - 2024-09-11

### Added

* Integrate test results and coverage reporting into CI (DCP-4531)
* Add all test sources to Doxygen output, and make corrections as needed
  (DCP-4532)

### Changed

* Rename user-facing symlink, installed by `dcp-worker` native installer, from
  `dcp-worker` to `dcp-work` to avoid confusion with the NPM package of the same
  name

### Fixed

* Fix permissions error when running `dcp-work` symlink after Linux/MacOS
  install: make `/opt/dcp` readable with only `/opt/dcp/.dcp` locked down
  (DCP-4529)
* Add tree enumerability to `navigator.gpu` (DCP-4520)

## [7.1.1] - 2024-09-06

### Changed

* Revert Dawn to 5953, with cherry pick
  0ae5730e3faaa554b36fcc90ac65a9368a9dada4 for `navigator.gpu` tree writability
  and configurability (DCP-4499)

## [7.1.0] - 2024-09-04

### Changed

* Update Dawn to 6550 (DCP-4499)
* Update NPM `dcp-worker` package to 3.3.11

### Fixed

* Make `navigator.gpu` tree writeable and configurable (DCP-4499)
* Remove evaluator logging noise when log level is less than debug (DCP-4513)
* Work around `dcp-evaluator` shutdown access violation on MacOS when WebGPU is
  enabled (DCP-4519)

## [7.0.3] - 2024-08-29

### Fixed

* Remove erroneous single-quotes from release CI script

## [7.0.2] - 2024-08-29

### Changed

* Make Visual Studio 2022 a requirement on Windows
* Update NPM `dcp-worker` package to 3.3.10

### Fixed

* Fix release CI

## [7.0.1] - 2024-08-23

### Fixed

* Revert to upper-case CPack component names (DCP-4466)

## [7.0.0] - 2024-08-23

### Added

* Add Javascript debugger support to the evaluator (DCP-4285)
* Evaluator: allow multiple `--options` arguments (DCP-4301)
* Explicitly set minimum Windows version to Windows 10 (DCP-4311)
* Default the evaluator `concurrency` option to the value of the `concurrency`
  registry string value under the `dcp-evaluator` key on Windows (DCP-4315)
* Add `--address` option (short: `-a`) to evaluator for specifying the host
  name or address (DCP-1805), which can include IPv4 (DCP-4406) or IPv6
  addresses (DCP-4331)
* Add installer links to versions published to DCP Native Package Repository and
  Releases pages (DCP-4353)
* Add latest release permalink to README along with general updates and
  improvements
* Add `DCP_FORMAT` to allow disabling of code formatting checking, defaulted to
  `OFF`.
* Add documentation support for Mermaid diagrams
* Enable source browsing in the HTML documentation
* Deploy Linux installers to new apt repositories (DCP-4362) and provide
  `subscribe` and `unsubscribe` scripts for users to subscribe to or unsubscribe
  from the repositories
* Add all root-directory configuration files to sources, to appear in IDEs

### Changed

* Change evaluator to allow multiple `--socket` and `--start-event` (on Windows)
  arguments, with only the last one used, to simplify argument forwarding to
  child processes
* Improve `BOOST_TEST_MODULE` handling in tests (DCP-4364)
* Default CMake options that require external programs to `OFF`, and fail if the
  external program is absent when `ON` (DCP-4386)
* Ensure build product names match target names where possible, including
  changing target names from `DCP-*` to `dcp-*`, and factor target/interface
  names out to variables
* Rename `dcp-package` target and `DCP_PACKAGE*` CMake variables to
  `dcp-installer` and `DCP_INSTALLER*`, respectively (DCP-4367)
* Rename `configuration` installation component to `settings`
* Rename `DCP::Settings` class to `DCP::Settings::Tree`
* Rename `dcp-configuration` target and executable to `dcp-settings`
* Combine `dcp-configuration-library` and `dcp-configurator-library` targets
  into `dcp-settings-library`, and put all code in the `DCP::Settings` namespace
* Rename `dcp-configurator` target and executable to `dcp-settings-configurator`
* Rename `DCP_INSTALLER_COMPONENTIZED` to `DCP_INSTALLER_COMPONENTIZE`
* Move global documentation into appropriate targets and remove special
  knowledge from `dcp-html` target
* Absorb `Command` class into `Process` and add unit tests (DCP-4436)
* Standardize unit test naming and organization to eliminate potential namespace
  collisions (DCP-4471)
* Move all root-directory scripts into `script` directory

### Fixed

* Use correct semantic versions in change log, installer file name, and
  elsewhere (DCP-4347)
* Make `Evaluator::terminate()` more resilient against exceptions (DCP-4363)
* Fix CMake 3.30 errors and warnings (DCP-4376)
* Update Boost to 1.85.0 for B2 fix on MSVC (DCP-4389, DCP-4390)
* Fix blurry graphic in Windows installer (DCP-4403)
* Fix documentation deployment to GitLab Pages (DCP-4404)
* Change install component names to `snake_case` to fix and prevent CMake
  variable name conflicts (DCP-4466)
* Address `clang-tidy` warnings, and fix `clang-tidy` on Ubuntu 24.04 (DCP-4468)
* Make all markdown conform to markdownlint
* Fix `dcp-docker` target build when `DCP_INSTALLER` is `OFF`
* Fix versioning in Debian installers to use all 4 version components
* Update node-gyp to recognize Visual Studio 2022
* Update node-eventlog to fix a broken hash
* Update setup instructions

### Removed

* Remove `DCP::Argument::match()`; no longer needed with improvements to
  argument handling in the evaluator
* Remove unused functions from `DCP::String`

## [6.1.3] - 2024-07-05

### Fixed

* Fix supervisor script failure in some cases

## [6.1.2] - 2024-07-05

### Fixed

* Fix Windows install failure when Distributive registry key does not yet exit
  (DCP-4394)

## [6.1.1] - 2024-06-24

### Fixed

* Update Node to 20.15.0 in order to fix `dcp-worker` crash on Windows ARM64
  (DCP-4372)
* Work around incorrect handling of Unicode characters in license RTF by
  WiX Toolset 3.14.1 (DCP-4374)

## [6.1.0] - 2024-06-19

### Changed

* Update NPM `dcp-worker` package to 3.3.6

### Fixed

* Fix dependency relationship between DCP-evaluator and DCP-evaluator-test
  targets (DCP-4323)
* Fix `DCP::Logger::willLog()` and eliminate an extra copy of each sent and
  received string in non-Debug builds (DCP-4350)
* Fix incorrect logging of `DCP_HTML` CMake variable status
* Make Doxygen ignore `node_modules` directories

## [6.0.3] - 2024-05-22

### Fixed

* Remove erroneous/unnecessary `docker pull` and `docker push` surrounding
  `docker buildx imagetools create`

### Removed

* Remove unit test code from test coverage reporting

## [6.0.2] - 2024-05-21

### Changed

* Return Docker image builds to shell runners to enable better GPU capabilities

### Fixed

* Make `latest` Docker image properly multi-architecture (DCP-4281)

## [6.0.1] - 2024-05-15

### Changed

* Coalesce RUN commands in Dockerfile
* Ensure Docker images are built by Docker CI runners

## [6.0.0] - 2024-05-14

### Added

* Add Docker deploy and release steps to CI (DCP-4186)
* Add "arm64/v8" architecture to the Docker image (DCP-4202)
* Add `DCP_SUPERVISOR_SERVICE` environment variable argument for Docker
  container: if defined, run supervisor in service and sandbox server in
  foreground (DCP-4209)
* Add `DCP_NPM_UPDATE` environment variable argument for Docker container to
  indicate whether to update the `dcp-worker` NPM package (DCP-4200):
  * '2': update to bleeding-edge
  * '1': update to latest point release
  * otherwise: no update
* Add improved docker script output
* Add graphical branch coverage reporting (DCP-4230)
* Add mutation testing (DCP-4231)
* On Unix, install `dcp-worker-config.js` to "/opt/dcp/.dcp" for users to copy
  and modify (DCP-4412)

### Changed

* Revamp deployment and release CI stages to make it easier to QA prior to
  release, and encode formerly manual processes into CI release jobs (DCP-4198)
* Update boost to 1.85.0 and add locale
* Update NPM `dcp-worker` package to 3.3.3
* Reorganize tests:
  * One test target per tested target, with the test target appending `-test` to
    the tested target name
  * All test code is now inside the `DCP::Test` namespace
  * Test suites are used to namespace test within `DCP::Test`, whose namespacing
    is reflected in the directory structure
  * One leaf test suite per test source file
* Consolidate keystore generator into supervisor NPM package and rewrite to use
  the `dcp-client` Wallet API (DCP-4243)

### Fixed

* Ensure syslog() is called with accepted log level in order to prevent
  undefined behaviour (DCP-4206)
* Fix XDG_RUNTIME_DIR error (DCP-4207)
* Install vulkan-tools to Docker image (DCP-4208)
* Fix incorrect Debian installer dependencies specification

## [5.0.1] - 2024-03-14

### Added

* Add `DCP_PACKAGE_COMPONENTIZED` option, defaulted to `OFF`, to control whether
  to additionally generate an installation package for each component (Linux
  only)
* Add ability to configure evaluator arguments dynamically in docker image via
  `DCP_EVALUATOR_ARGUMENTS` environment variable, and remove hard-coded V8
  options (DCP-4152)

### Fixed

* Fix bug that could result in compute groups not being shown in the Windows
  configuration UI (DCP-4165)

## [5.0.0] - 2024-03-01

### Added

* Add support for Xcode 15 (DCP-3995)
* Add MacOS Apple Silicon (ARM64) support (DCP-3988)
* Add Windows 11 ARM64 support (DCP-4007)
* Add CMake option `DCP_NODE` for optionally excluding bundled Node in favour of
  using system-installed version (DCP-4134)
* Add minimum-viable features to MacOS installer, including a "DCP Worker.app"
  application bundle that runs the sandbox server that the service connects to
  in order to do work (DCP-171)
* Add minimum-viable features to Ubuntu installer, including a "DCP Worker"
  launcher icon that runs the sandbox server that the service connects to in
  order to do work (DCP-1394)
* Add component-based installers on Linux
* Add `DCP-sandbox-bundle` target to install an application bundle on MacOS that
  runs the sandbox server
* Add `DCP-docker` target that generates a minimum-viable worker `Dockerfile` by
  installing the ".deb" installer generated by the build (DCP-4124)

### Changed

* Update dcp-worker to 3.2.38 (DCP-3956)
* Make Screensaver installation component optional (DCP-3973)
* Standardize CMake booleans to use `ON` and `OFF`
* Improve CMake logging
* Rename `registrator` to `supervisor-setup`
* Factor registry settings accesses out to new `Settings` class
* Refactor and improve `Registry` class
* Rename builds to use OS (Linux/MacOS/Windows) and Architecture (arm64|x64)
  (DCP-3993)
* Update boost to 1.83.0 (DCP-3994)
* Rename `DCP-uninstaller` target to `DCP-uninstall-bundle`
* Update Windows Installer version from 3.1 to 5.0

### Fixed

* Log executable argument errors to logger (DCP-3941)
* Fix large license font in About tab (DCP-3961)
* Ensure that if `DCP_OMAHA` is off, Omaha is not included (DCP-3962)
* Make service restart more robust (DCP-3968)
* Fix erroneous handling of arguments of the form `--foo=x` and `-fx` in some
  cases in `dcp-evaluator` (DCP-3981)
* Fix `RelWithDebInfo` and `MinSizeRel` builds, pass build configuration to
  CPack correctly, change Omaha to use standard CMake configurations, and fix
  miscellaneous CMake warnings (DCP-4033)
* Fix potential dynamic library loading bug with `dcp-evaluator` executable
  produced by build (vs. install) (DCP-4034)
* Fix scripts to allow running via symlink (DCP-4133)
* Correctly handle conditional CMake options if condition unmet
* Make version variable logic more robust against git errors

### Removed

* Remove redundant "--install" option from `dcp-configurator`
* Remove "--load" and "-l" options from `dcp-evaluator` (DCP-3756)

## [4.0.2] - 2023-10-26

### Fixed

* Fix bug that prevented evaluator from loading dynamic libraries on Linux when
  binaries were relocated

## [4.0.1] - 2023-10-20

### Added

* Add experimental warning to `--webgpu` documentation in `dcp-evaluator` help

## [4.0.0] - 2023-10-18

### Added

* Add ability to enable experimental WebGPU support at runtime (DCP-3900) by:
  * Passing `--webgpu` to `dcp-evaluator`
  * Setting registry string value `HKLM\Distributive\DCP\dcp-evaluator\webgpu`
    to `true` (Windows only)
  * Checking a box in the settings UI (Windows only)
* Install Start Menu shortcuts for the sandbox server and the screensaver
  settings control panel (DCP-3529)

### Changed

* Update dcp-worker to 3.2.30-14

## [3.0.0] - 2023-10-13

### Added

* Add MacOS support for WebGPU
* Add `DCP_PACKAGE_VERBOSE` option for verbose CPack logging
* Add `dcp-keystore.sh` for generating a keystore on non-Windows

### Changed

* Switch to V8 10.2.154.15 provided by Node API via Node 18.12.1 and deprecate
  now-unused `dcp-evaluator` `--load`/`-l` option (DCP-2954, DCP-3478)
* Switch to Dawn 5953 WebGPU implementation and Node API bindings (DCP-3225)
* Update boost build to 0.4.0 to include a necessary patch
* "Public Groups" are now called "Global Groups" (DCP-3841)
* Move all script-based registry operations during install to single
  `dcp-registrator` script (DCP-3848)
* Change `dcp-keystore.bat` script to simply generate a keystore
* Change `dcp-configurator` arguments to remove special knowledge of screensaver
* Reduce global CMake variables (DCP-3860)
* Rename `DCP-...-test` targets and `dcp-...-test` executables to `DCP-test-...`
  and `dcp-test-...`, respectively, for better file ordering

### Fixed

* Remove spurious "PDCP Worker" entry in screensaver list (DCP-2605)
* Refactor build targets to fix potential dependency graph issues (DCP-3856,
  DCP-3858, DCP-3859)

## [2.5.0] - 2023-09-29

### Fixed

* Fix `nssm restart` by removing `AppNoConsole` registry setting (DCP-3865)

## [2.4.0] - 2023-09-28

### Fixed

* Remove "supervisor" string value in
  "HKLM\SOFTWARE\Distributive\DCP\dcp-client\dcp-config\rollout" registry key
  that prevents Supervisor 2

## [2.3.0] - 2023-09-27

### Changed

* Update dcp-worker to 3.2.30-11

## [2.2.5] - 2023-09-26

### Fixed

* Default dcp-client/dcp-config/evaluator/location registry key to
  `http://127.0.0.1:9000/` to work around `localhost` usage issues

## [2.2.4] - 2023-09-22

### Changed

* Restore dcp-worker to 3.2.30-3

### Fixed

* Default to Supervisor 1 until kinks are worked out of Supervisor 2

## [2.2.3] - 2023-09-21

### Fixed

* Roll back dcp-worker to 3.2.29 to work around scheduler change that prevented
  work from being done

## [2.2.2] - 2023-09-20

### Changed

* Update Node from 16 to 18 (DCP-3842)

## [2.2.1] - 2023-09-14

### Changed

* Deploy installers to Omaha with `is_critical` set to `false` to prevent
  over-eager updating (eg. during another install)

### Fixed

* Potentially fix service restart issue after install

## [2.2.0] - 2023-08-24

### Changed

* Disable Omaha by default, and explicitly enable in CI (DCP-3792)

### Fixed

* Fix Windows 11 install rollback issue (DCP-3791)

### Removed

* Remove legacy "Kings Distributed Systems" registry migration

## [2.1.0] - 2023-08-10

### Added

* Add firewall exception to MSI for evaluator executable (DCP-3775)

## [2.0.0] - 2023-08-06

### Added

* Add `--socket` option to evaluator to allow execution in non-server mode
  (DCP-3549)
* ADD `--shutdown-timeout` option to evaluator
* Add sandbox process implementation for non-Windows (DCP-3567)
* Add `DCP_SHOW_INCLUDES` option for MSVC (DCP-3770)
* Add warning when NPM package builds are disabled, eg. for development
  (DCP-3765)
* Add `Socket::setCloseOnExec` and `Socket::getAvailablePort`

### Changed

* Switch from multi-thread to multi-process evaluator server (DCP-1525)
* Make screensaver run the sandbox server in its own process (DCP-3548)
* Ensure all sandbox execution is done through `dcp-evaluator-start` and remove
  all sandbox environment file knowledge from DCP Native (DCP-3577)
* Replace `Command::executeAndWaitForOutput` by adding a "console" mode to
  `Process`
* Update bundled node executable to 16.20.1
* Improve exception safety and error handling

### Fixed

* Ensure COM is uninitialized properly (DCP-3556)
* Fix target dependencies (DCP-3519)
* Use `NULL` instead of `INVALID_HANDLE_VALUE` for invalid registry key
* Avoid potential initialization fiasco with `DismissableMessageDialog::key`

### Removed

* Remove `DCP_NODE_VERSION_CHECK`; unnecessary single-source-of-truth violation

## [1.4.4] - 2023-08-03

### Changed

* Update `dcp-worker` to 3.2.30-3

## [1.4.3] - 2023-08-01

### Changed

* Update `dcp-worker` to 3.2.30-2

## [1.4.2] - 2023-07-31

### Changed

* Update `dcp-worker` to 3.2.30-1

## [1.4.1] - 2023-07-27

### Added

* Add `--omit=dev` and `--omit=peer` to NPM builds

### Changed

* Update dcp-keystore using npm 8.19.2

## [1.4.0] - 2023-07-26

### Changed

* Update `dcp-worker` to 3.2.29 and `dcp-client` to 4.2.32
* Update bundled `node` executable to 16.20.1

### Removed

* Remove `DCP_NODE_VERSION_CHECK` and associated code; we already control the
  version of Node that gets bundled by the installer, and updating node required
  changes in two places instead of just one

## [1.3.1] - 2023-07-17

### Changed

* Nothing; this release is strictly to test auto-update on Windows

## [1.3.0] - 2023-07-15

### Changed

* Improve the development and release process documented in `CONTRIBUTING.md`

### Fixed

* Apply Omaha security fix (DCP-3676)

## [1.2.8] - 2023-06-08

### Changed

* Update `dcp-worker` to 3.2.28-2

### Fixed

* Fix `dcp-supervisor` build for updated Node/NPM

## [1.2.7] - 2023-05-29

### Changed

* Update `dcp-worker` to 3.2.28-1

## [1.2.6] - 2023-05-18

### Changed

* Update `dcp-worker` to 3.2.27, and `dcp-client` to 4.2.30

### Removed

* Remove `--public-group-fallback` from `dcp-supervisor.bat`, now that the value
  is correctly read from the registry

## [1.2.5] - 2023-05-01

### Changed

* Update `dcp-worker` to prod-20230427 branch head

## [1.2.4] - 2023-03-27

### Added

* Default evaluator `readln` to evaluating the line (DCP-3520)

### Changed

* Make the V8 Javascript implementation get V8 options from the configuration if
  none specified (DCP-3523)
* Rename `dcp-service` to `dcp-supervisor` 1.0.0
* Make `dcp-supervisor.bat` use default logging by default, and only log to the
  Event Viewer when run as a service (DCP-3536)
* Build everything to a configuration directory within the "artifacts" directory
  (DCP-3537)
* Update `dcp-worker` to 3.2.24

### Fixed

* Revert default `max-node-space-size` setting (DCP-3486)
* Fix potential evaluator crashes in case of malformed strings (DCP-3521,
  DCP-3522)

## [1.2.3] - 2023-02-16

### Fixed

* Fix channel deployment as per change to Omaha API

## [1.2.2] - 2023-02-16

### Changed

* Use registry-defined V8 options for all V8 execution on Windows (DCP-3488),
  and default these to include setting `max-node-space-size` to 4.5GB (DCP-3486)
* Update `dcp-service` version to 0.38.0 and update its `dcp-worker` dependency
  to 3.2.20

## [1.2.1] - 2022-12-08

### Changed

* Update `dcp-service` version to 0.37.0 and update its `dcp-worker` dependency
  to the prod-20221207 branch head

### Fixed

* Fix for clang-tidy on Ubuntu 22.04 (DCP-3455)

## [1.2.0] - 2022-11-08

### Changed

* Replace BabylonNative Node-API and Node-Addon-API implementations with those
  from NodeJS, with a minimum of changes from BabylonNative (DCP-2955)
* Update V8 to 10.5.218.8 for non-WebGPU builds (DCP-3366)
* Update `dcp-service` version to 0.36.0 and update its `dcp-worker` dependency
  to the prod-20221101 branch head

## [1.1.1] - 2022-10-27

### Changed

* Update `dcp-service` version to 0.35.0 and update its `dcp-worker` dependency
  to 3.2.15

## [1.1.0] - 2022-09-30

### Added

* Add Raspberry Pi support (DCP-3333)

### Removed

* Remove icudtl.dat file on Linux and MacOS (DCP-3343)

### Changed

* Update `dcp-service` version to 0.34.0 and update its `dcp-worker` dependency
  to the prod-20220919 branch head

## [1.0.2] - 2022-09-09

### Changed

* Update `dcp-service` version to 0.32.0 and update its `dcp-worker` dependency
  to the prod-20220907 branch head

## [1.0.1] - 2022-09-08

### Changed

* Update `dcp-service` version to 0.31.0 and update its `dcp-worker` dependency
  to the prod-20220907 branch head

## [1.0.0] - 2022-09-08

### Changed

* Rename "Kings Distributed Systems" to "Distributive" in the application and
  updater, and migrate all registry settings on install (DCP-2682)
* Update `dcp-service` version to 0.29.0 and replace `dcp-service-worker` with
  consolidated `dcp-worker`

### Fixed

* Make Debian installer functional for a basic evaluator install (DCP-3305)
* Make MacOS installer functional for a basic evaluator install (DCP-3307); note
  that this does not yet include an uninstall script (DCP-3310)

## [0.12.2] - 2022-08-18

### Changed

* Update `dcp-service` version to 0.28.0 and update its `dcp-service-worker`
  dependency to 1.3.11

### Fixed

* Remove dcp-screensaver.scr on Windows uninstall (DCP-3266)

## [0.12.1] - 2022-08-09

### Added

* Add experimental WebGPU builds to the package build stage

### Changed

* Update `dcp-service` version to 0.27.0 and update its `dcp-service-worker`
  dependency to 1.3.10

### Removed

* Remove redundant SHA256 checksum files from package, now that GitLab
  automatically gives the SHA256 checksum for each file in the package

## [0.12.0] - 2022-07-02

### Changed

* Update V8 to 8.4.371.15 (DCP-2633)
* Update default screensaver graphic and background color (DCP-3153)
* Update `dcp-service` version to 0.26.0 and update its `dcp-service-worker`
  dependency to the prod-20220718 branch head

### Added

* Add MacOS support (DCP-3166)

## [0.11.5] - 2022-05-26

### Changed

* Allow V8 options to be configured at run time (DCP-2657)
* Flatten target tree and make executable and target names match (DCP-2667)
* Replace super-build with simpler single-phase build (DCP-2680)
* Update `dcp-service` version to 0.25.0 and update its `dcp-service-worker`
  dependency to the prod-20220524 branch head
* Fix warnings for clang-tidy version that ships with Ubuntu 21.10 (DCP-2655)
* Change default "all" target to build all targets with the exception of
  "DCP-package"
* Change release process to deploy to beta channel (DCP-2800)
* Update default screensaver graphic

### Added

* Add `V8_OPTIONS` CMake cache variable for specifying command-line options for
  V8 (DCP-2657)

## [0.11.4] - 2022-03-21

### Changed

* Update `dcp-service` version to 0.20.0 and update its `dcp-service-worker`
  dependency to the prod-20220321 branch head
* RunMicroTask queue after pumping the message loop in reactor (DCP-2650)
* Add experimental WebGPU support to Windows (DCP-2603)
* Update Node to 14 (DCP-2635)
* Demote evaluator write failure log level from 'warning' to 'debug' (DCP-2645)

### Fixed

* Make DCP-check target use address sanitizer suppressions (DCP-2631)
* Make compute group hash generation faster and no longer do a full dcp-client
  initialization (DCP-2632)
* Remove GPU.$setPlatform from WebGPU environment (DCP-2633)

## [0.11.3] - 2022-02-07

### Changed

* Update `dcp-service` version to 0.17.0 and update its `dcp-service-worker`
  dependency to the prod-20220202-hot branch head

## [0.11.2] - 2022-01-24

### Added

* Add unit test for compute group join hash generation (DCP-2569)
* Add `npm audit` call after each `npm ci` call in the build (DCP-2563), and add
  `NPM_AUDIT` option to allow enabling or disabling (DCP-2571)
* Add instructions for updating `dcp-service-worker`

### Changed

* Update `dcp-service` version to 0.15.0 and update its `dcp-service-worker`
  dependency to the develop branch head
* Remove redundant registry reads from `"dcp-service.bat"` script (DCP-2549)
* Update default screensaver graphic

### Fixed

* Fix a compute group join hash generation bug that could cause incorrect hashes
  in the event of an error (DCP-2547), and improve error logging
* Fix `node-eventlog` dependency vulnerabilities (DCP-2564)

## [0.11.1] - 2021-12-13

### Changed

* Strip leading and trailing whitespace from compute group name, join key, join
  secret, and join hash on input (DCP-2529)
* Allow pre-release tags by updating the release process to only deploy if a tag
  of the form x.x.x is pushed to the release branch

## [0.11.0] - 2021-12-10

### Changed

* Update `dcp-service` version to 0.10.1 and update its `dcp-service-worker`
  dependency to the prod-20211208 branch head

### Added

* Add Compute Groups tab to UI (DCP-2467, DCP-2502)

## [0.10.7] - 2021-11-18

### Changed

* Update `dcp-service` version to 0.8.0 and update its `dcp-service-worker`
  dependency to the prod-20211108 branch head

## [0.10.6] - 2021-11-17

### Fixed

* Improve read buffering speed via exponential reallocation (DCP-2413)

### Changed

* Change to 4-component version, where the last component is non-zero for
  pre-release builds (DCP-2463)

## [0.10.5] - 2021-10-05

### Changed

* Update `dcp-service` version to 0.7.2 and update its `dcp-service-worker`
  dependency to the release branch head

## [0.10.4] - 2021-10-05

### Fixed

* Persist screensaver timeout between installs (DCP-2346)
* Remove maximum read buffer size limitation to allow very long lines (DCP-2235)
* Speed up build time by eliminating unnecessary N-API submodule cloning
* Fix WiX build warnings

### Changed

* Update Omaha build version to 0.0.5

## [0.10.3] - 2021-09-14

### Changed

* Update `dcp-service` version to 0.7.1 and update its `dcp-service-worker`
  dependency to the release branch head

## [0.10.2] - 2021-09-13

### Fixed

* Fix screensaver bitmap: remove white line at top

### Removed

* Remove NSIS support (DCP-2286)

## [0.10.1] - 2021-09-10

### Changed

* Update `dcp-service` version to 0.7.0 and update its `dcp-service-worker`
  dependency to the release branch head
* Update screensaver bitmap

## [0.10.0] - 2021-09-09

### Added

* Add Omaha integration for automatic updates on Windows (DCP-1778)
* Add deploy step to CI that uploads MSI to update server (DCP-2267)

## [0.8.8] - 2021-08-24

### Added

* Add `DCP_TEST` CMake variable to allow disabling of test targets
* Add `DCP_SERVICE_PACKAGE_BUILD` and `DCP_KEYSTORE_PACKAGE_BUILD` CMake
  variables to allow disabling of NPM CI builds for debugging (DCP-2079)
* Add initial, experimental WebGPU support (DCP-1989 sub-tickets DCP-1990,
  DCP-2000, DCP-2001, DCP-2005, DCP-2014, DCP-2015, DCP-2019, DCP-2023,
  DCP-2025, DCP-2029, DCP-2038, DCP-2076, DCP-2156)

### Changed

* Update `dcp-service` version to 0.6.1 and update its `dcp-service-worker`
  dependency to the develop branch head

## [0.8.7] - 2021-08-10

### Changed

* Update `dcp-service` version to 0.6.0 and update its `dcp-service-worker`
  dependency to the develop branch

## [0.8.6] - 2021-08-03

### Changed

* Update `dcp-service` version to 0.5.2 and update its `dcp-service-worker`
  dependency

## [0.8.5] - 2021-07-08

### Fixed

* Replace non-stable Node URL with stable one (DCP-2004)

### Changed

* Update `dcp-service` version to 0.5.1 and update its `dcp-service-worker`
  dependency

## [0.8.4] - 2021-06-30

### Changed

* Update `dcp-service` version to 0.5.0; change its `dcp-service-worker`
  dependency to `release`

## [0.8.3] - 2021-06-28

### Changed

* Update `dcp-service` version to 0.4.0; change its `dcp-service-worker`
  dependency to `prod-20210628`

## [0.8.2] - 2021-06-24

### Changed

* Update `dcp-service-worker` to latest version

## [0.8.1] - 2021-06-22

### Added

* Add `"dcp-evaluator.bat"` to optional "Evaluator" install component on Windows
  to allow running the evaluator server for debugging (DCP-1974)

### Changed

* Update `dcp-service` version to 0.3.0; change its `dcp-service-worker`
  dependency back to using the `release` branch now that changes have been
  merged

## [0.8.0] - 2021-06-08

### Added

* Add `NPM_VERBOSE` CMake option for NPM build debugging
* Add `DCP_TARGET_FOLDERS` to documentation

### Fixed

* Fix incorrect system requirements in `"README.md"`
* Disable `DCP_TARGET_FOLDERS` on MSVC

### Changed

* Replace screensaver graphic: ensure all background is black
* Update `dcp-service` version to 0.2.0; change its `dcp-service-worker`
  dependency to use the `beta-release/lakehead-20210609` branch instead of
  `release`

## [0.7.11] - 2021-06-04

### Changed

* CI:
  * Improve YAML consistency
  * Fix hard-coded package directory
  * Update for directory tree changes in dcp-native-ci
  * Update externals

## [0.7.10] - 2021-06-03

## [0.7.9] - 2021-06-03

### Changed

* CI: Factor platform-specific templates out to dcp-native-ci

## [0.7.8] - 2021-06-03

### Added

* Include Debug artifacts in package (DCP-1395)

## [0.7.7] - 2021-06-03

### Changed

* Update `dcp-service-worker` to latest
* Update externals to latest releases
* Update default screensaver bitmap
* Improve CMake logging
* CI:
  * Fold "upload" stage into "package" to reduce release time and remove
  "curlimages/curl:latest" image from chain
  * Factor duplicated code out of .gitlab-ci.yml

### Removed

* Exclude Doxygen from Debug build

## [0.7.6] - 2021-06-01

### Changed

* CI: Use same Linux image for package stage as build stage

## [0.7.5] - 2021-06-01

### Changed

* Refactor `".gitlab-ci.yml"`

## [0.7.4] - 2021-05-31

### Changed

* Refactor `".gitlab-ci.yml"`

## [0.7.3] - 2021-05-28

### Changed

* Rename installed aggregated license files from "LICENSE" to "LICENSES" to
  avoid file conflicts when building in-source (DCP-1861)
* Only produce RTF aggregated licenses on Windows (DCP-1861)
* Improve separation between generation and build steps, such that the
  "artifacts" directory strictly includes build results and can be safely
  deleted without requiring an explicit regeneration step to repopulate
  (DCP-1871)
* Update keystore and service packages; the latter includes some logging
  improvements in `dcp-service-worker` (DCP-1546)
* Update strings in UI to reflect branding changes (DCP-1878)

### Added

* Add "DCP-licenses" target to aggregate licenses (DCP-1861)
* Add placeholder dependency graph to allow for builds without
  `"generate.cmake"` (DCP-1871)
* Add `NODE_EXECUTABLE`, `NODE_ROOT`, `NSSM_EXECUTABLE`, and `NSSM_ROOT` CMake
  variables to documentation (DCP-1860)

### Removed

* Remove Node 12 and NSSM from build requirements (DCP-1860)
* Remove redundant `DCP-configurator` dependency on `comctl32`

### Fixed

* Make cache variable usage more robust against "falsy" values

## [0.7.2] - 2021-05-27

### Changed

* Move Node and NSSM into externals to ensure exact versions used (DCP-1860)
* Restrict DCP-keystore target to Windows builds

### Added

* Add `"CHANGELOG.md"` to DCP target sources and Doxygen documentation

### Removed

* Remove Chocolatey NSSM install from Windows CI (DCP-1860)

### Fixed

* Use correct redistributable version of NSSM (DCP-1860)

## [0.7.1] - 2021-05-22

### Changed

* CI: express explicit dependency between release and upload jobs
* CI: name jobs for platform, then build configuration

## [0.7.0] - 2021-05-22

### Added

* Add DCP-sign target and switch to using it for executable signing (DCP-1771)
* Add `DCP::Crypto` (DCP-1771)
* Add `getKnownFolderPath()` to public `DCP::Paths` interface
* Add bound port to socket listen callbacks (DCP-1755)
* Add MSI hooks for Omaha auto-updates (DCP-1778)
* Add CMake `DCP_TARGET_FOLDERS` option for nesting target folders in IDEs
* Add `".DS_Store"` files to `".gitignore"`
* Add `"CONTRIBUTING.md"`, containing coding guidelines (DCP-1832)
* Add `"CONTRIBUTING.md"`, `"LICENSE.txt"`, and Graphviz dot dependency graph to
  Doxygen documentation as "Related pages"
* Add `setFont()` (DCP-1852) and `setIcon()` to `DCP::Window`

### Changed

* Update Node from 10 to 12 (DCP-1790)
* Move Boost and Uncrustify builds to external repositories that publish
  binaries through GitLab Releases, and change the build to download those
  binaries (DCP-1768)
* Replace `dcp-service-worker` submodule with an inline `dcp-service` package
  that includes it as a dependency
* Update `dcp-service-worker` to latest commit in `release` branch and change
  calling code to reflect changes in package tree structure (DCP-1844)
* Change `DCP::Command` to take a vector of argument strings and ensure they are
  all quoted instead of requiring quoting in the calling code
* Revamp `DCP::Configuration` exit codes:
  * Remove `configurationFailure` and indicate failure to run the configurator
    separately from the enumeration
  * Combine all argument errors into a single `argumentError`
  * Add `screensaverUpdateError` and return it when the configurator cannot
    do a live update to screensaver settings (DCP-1778)
* Move `DCP-warnings` interface target to its own file, and factor compiler
  options out to new `DCP-options` interface target
* Potentially speed NPM builds by specifying `--only=production` and
  `--progress=false`
* Make the Graphviz dot component a requirement for the `DCP-doxygen` target
* Improve CI:
  * Build directly into source directory on CI to shorten paths on Windows
  * Update Linux docker image, and remove now-redundant installs from the
    `".gitlab-ci.yml"` script
  * Only zip the `"artifacts"` directory in the `package` stage
* Aggregate all external licenses into the license installed with the software,
  and shown in the installer and About screen (DCP-1852)
* Force CTest to run in its own process
* Update and improve `"README.md"` documentation
* Improve release script robustness
* Improve code naming, readability, structure, reuse, and consistency
* Improve user error messages
* Improve error logging

### Removed

* Remove DCP-graphviz target, replaced with CMake `--graphviz` argument in
  `"generate.cmake"`, to prevent Xcode segfaults (DCP-1834)
* Remove unnecessary `DCP-keystore` dependency on `DCP-configurator`
* Remove unnecessary Python requirement from `"README.md"`
* Remove `DCP::Test::getDataDirectoryPath`

### Fixed

* Fix intermittent concurrency errors in tests (DCP-1755, DCP-1779)
* Prevent `std::string::front()` from being called on an empty string
* Fix Power Settings sleep timeout detection (DCP-1833)
* Make `DCP::Command` log an error rather than showing a dialog
* Fix CMake issues:
  * Replace `CMAKE_SOURCE_DIR` and `CMAKE_BINARY_DIR` with `PROJECT_SOURCE_DIR`
    and `PROJECT_BINARY_DIR` for correct behaviour if DCP Native is included in
    other projects
  * Ensure `DCP_COVERAGE` is not set for non-Unix platforms
  * Disable `DCP_TEST_BUILD` on Xcode due to recursive build error
  * Add `VERBATIM` to all `add_custom_command()` calls
* Fix `GIT_CLONE_PATH` in CI:
  * Allow concurrent builds by including `${CI_CONCURRENT_ID}`
  * Fix GitLab Runner configuration so that `${CI_BUILDS_DIR}` can be used
    (<https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4167#note_186682451>)
  * Use `${CI_PROJECT_ID}` instead of `${CI_PROJECT_NAME}`
* Build NPM packages using `--build-from-source` by default (which fixes the
  node-eventlog `npm install` build in the dcp-service package); can be
  overridden with `DCP_KEYSTORE_PACKAGE_BUILD_FROM_SOURCE` and
  `DCP_SERVICE_PACKAGE_BUILD_FROM_SOURCE` (DCP-1826)
* Work around class hierarchy bug in Doxygen by adding an explicit base class
  namespace
* Ensure screensaver and screensaver settings dialog exit with failure, the
  latter with a general user message, on top-level exception (DCP-1857)
* Properly handle exception when creating tab in screensaver settings dialog
