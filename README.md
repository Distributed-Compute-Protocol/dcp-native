# INTRODUCTION

DCP Native is the natively-implemented portion of the Distributive Compute
Protocol.

## Releases

Binary releases can be found on the
[Releases page](https://gitlab.com/Distributed-Compute-Protocol/dcp-native/-/releases),
with the latest release
[here](https://gitlab.com/Distributed-Compute-Protocol/dcp-native/-/releases/permalink/latest).

## Installing

To install the DCP Native Worker suite of software:

* Windows:
  * Download `dcp-worker-<version>-windows-<architecture>.msi` from the links
    above, double-click, and follow the on-screen instructions.
* MacOS:
  * Download `dcp-worker-<version>-macos-<architecture>.pkg` from the links
    above, double-click, and follow the on-screen instructions.
* Linux:
  * Make sure your system is supported. Linux support includes Ubuntu 22.04 and
    later. To check, enter:
    ```bash
    cat /etc/lsb-release
    ```
  * For WebGPU workloads, install `vulkan-tools` or `vulkan-sdk` by running:
    ```bash
    sudo apt install vulkan-tools
    ```
  * To subscribe to the Distributive apt repository for automatic updates, enter
    the following in the terminal after navigating to the repository directory:
    ```bash
    script/subscribe.sh
    ```
    After subscribing, enter the following to install the DCP Native worker:
    ```bash
    sudo apt install dcp-worker
    ```
  * To install manually, download
    `dcp-worker-<version>-linux-<architecture>.deb` from the links above, then
    navigate to the download directory in the terminal and enter the following:
    ```
    sudo dpkg --install dcp-worker-$VERSION-linux-$ARCHITECTURE.deb
    ```
    where `$VERSION` and `$ARCHITECTURE` are replaced with the version and
    architecture in the file name.

## Uninstalling

To uninstall the DCP Native Worker suite of software:

* Windows:
  * In the Remove Programs control panel, select "DCP Worker" and uninstall.
* MacOS:
  * Run "Uninstall DCP Worker" inside the DCP folder inside Applications.
* Linux:
  * If installed via apt, enter the following into the terminal:
    ```bash
    sudo apt remove dcp-worker
    ```
    After uninstalling, optionally enter the following in the terminal, after
    navigating to the repository directory, to unsubscribe from the apt
    repository:
    ```bash
    script/unsubscribe.sh
    ```
  * For a manual installation, enter the following in the terminal:
    ```bash
    sudo dpkg --remove dcp-worker
    ```

## Configuring

To configure the installation:

* Windows:
  * In the settings window that appears at the end of the install, configure the
    payment address and any other settings and click Save, then Close.
* Unix:
  * In the terminal, enter the following:
    ```bash
    sudo --user dcp cp /opt/dcp/.dcp/dcp-worker-config.js /opt/dcp/.dcp/dcp-config.js
    sudo --user dcp nano /opt/dcp/.dcp/dcp-config.js
    ```
  * Edit the payment address and any other settings according to the comments,
    and save.
  * Restart the service:
    * MacOS:
      ```bash
      sudo launchctl unload /Library/LaunchDaemons/network.distributive.dcp.worker.plist
      sudo launchctl load /Library/LaunchDaemons/network.distributive.dcp.worker.plist
      ```
    * Linux:
      ```bash
      sudo systemctl restart dcp
      ```

## Running

To run the worker from an installation:

* Windows:
  * In the Start Menu, navigate to the DCP folder and double-click the "DCP
    Work" shortcut.
* Debian Linux (Ubuntu):
  * `sudo systemctl start dcp`.

Any other binaries and scripts can be run directly from locations in which they
can be found:

* The install location, which is one of the following:
  * Windows: `C:\Program Files\Distributive\DCP`
  * Linux: `/opt/dcp/bin`
* The build output directory (see the Building section, below)
* The "dcp-native" directory downloaded and unzipped from the
  [Releases page](https://gitlab.com/Distributed-Compute-Protocol/dcp-native/-/releases)
  or the
  [Package Repository](https://gitlab.com/Distributed-Compute-Protocol/dcp-native/-/packages)

### Binaries

* `dcp-evaluator`: The basic Javascript evaluator, which is a line-based REPL
  by default. Can be run in "evaluation" or "server" mode. Run
  `dcp-evaluator --help` for more information. This program is based on the V8
  JavaScript engine and the Dawn WebGPU library, and is a proprietary, secure,
  environment. It is not Node.js.

### Node.js Programs

* `dcp-worker`: This program requests work from the DCP Scheduler, and
  schedules it for evaluation in a dcp-evaluator sandbox.
* `dcp-evaluator-start`: This program determines the configuration for and
  launches the dcp-evaluator.
* `dcp-evaluator-manager`: This program hooks various system monitors and uses
  them to determine whether or not the dcp-evaluator should be running and 
  whether it should be allowed to increase workload.

### Batch Files (Windows)

* `dcp-sandbox`: Runs the evaluator under an environment that implements the
  worker protocol and full Javascript environment for communication with
  dcp-worker.
* `dcp-supervisor`: Runs the worker supervisor, which is responsible for getting
  work from the scheduler, starting sandboxes, and sending the work to the
  sandboxes for computation.

### Systemd Units (Debian / Ubuntu Linux)

* `dcp`: Starts dcp-evaluator and dcp-worker. Both of thsee units need to be
  running in order to do work. The dcp-evaluator unit can be stopped at any
  time to give cycles back to other processes on the system. The dcp-worker
  unit uses almost no resources and can be left running all the time. In a 
  system with frequent up/down cycling of the dcp-evaluator unit, keeping the
  dcp-worker alive improves the system's efficiency by storing work inputs in
  memory, and keeping the DCP Scheduler updated as to its state.
* `dcp-evaluator`: Runs the evaluator under an environment that implements the
  worker protocol and full Javascript environment for communication with the
  supervisor.
* `dcp-worker`: Runs the worker supervisor, which is responsible for getting
  work from the scheduler, starting sandboxes, and sending the work to the
  sandboxes for computation.

## Building

### Setting Up

#### Software

The following software is required:

* [Git](https://git-scm.com/downloads) is required for downloads of external
  dependencies. On Windows, select "Use external OpenSSH" (see "SSH Keys"
  section, below).
* [CMake](https://cmake.org/download/) (>= 3.26) is required for building on all
  platforms. If the CMake version provided by your Ubuntu distribution is too
  old, please visit the [Kitware APT Repository page](https://apt.kitware.com/)
  and follow the instructions provided.
* NPM, as part of [Node](https://nodejs.org/download/release/) (use the latest
  Node 20.x version) is required for building the `dcp-supervisor` target. It is
  recommended to *not* use NVM, which tends to cause problems due to its
  non-standard paths.
* [Python 3](https://www.python.org) (>= 3.10, or >= 3.11 if building Windows
  ARM64) is required by the `dcp-supervisor` build target. If using Python >=
  3.12, install `distutils` by running `pip install setuptools`. Note that if
  building any external libraries, some have specific Python version
  requirements.
* [Xcode](https://developer.apple.com/xcode/) is required on MacOS, along with
  the Command Line Tools.
* [Visual Studio](https://visualstudio.microsoft.com/vs/) (2022) is required on
  Windows, with the following components:
  * Under "Workloads", select "Desktop development with C++".
  * Under "Individual components":
    * Search for ".NET framework SDK" and select the newest (4.8.1 at the time
      of writing).
    * Search for "Windows SDK" and if one is not selected, select the newest
      (Windows 11 SDK (10.0.26100.0) or Windows 10 SDK (10.0.20348.0) at the
      time of writing).

The following software is optional:

* `clang-tidy` is required if `DCP_CLANG_TIDY` is `ON`.
* The `gcovr` Python3 module is required if `DCP_COVERAGE` is `ON`. This can be
  installed by running `pip install gcovr`.
* [Doxygen](https://www.doxygen.nl/download.html) and
  [Graphviz](https://graphviz.org/download/#executable-packages) are required if
  `DCP_HTML` is `ON`. For Macs with Apple Silicon, it is recommended to install
  these via MacPorts.
* [WiX Toolset](https://wixtoolset.org/docs/wix3/) 3.14.1 is required on Windows
  if `DCP_INSTALLER` is `ON`. This requires .NET Framework 3.5.1, which can be
  enabled in the "Windows Features" control panel.
* [mull](https://github.com/mull-project/mull) is required on platforms using a
  Clang compiler if `DCP_MUTATION` is `ON`.

#### SSH Keys

In order to push to repositories, SSH keys must be configured. To configure
Windows to use your private key automatically:

* Follow these steps to Windows OpenSSH:
  <https://davidaugustat.com/windows/windows-11-setup-ssh>
* Tell git to use it by entering the following into the terminal:
  `git config --global core.sshCommand C:/Windows/System32/OpenSSH/ssh.exe`

#### Windows Security

In order to stop Windows Security from slowing down builds, you can add your
source and build trees as
[exclusions](https://support.microsoft.com/en-us/windows/add-an-exclusion-to-windows-security-811816c0-4dfd-af4a-47e4-c301afe13b26).

### Getting The Code

To clone the
[DCP Native repository](https://gitlab.com/Distributed-Compute-Protocol/dcp-native),
enter the following:
```bash
git clone git@gitlab.com:Distributed-Compute-Protocol/dcp-native.git
```

### Quick Start

To build everything using default settings, invoke the following command (with
the ellipsis denoting optional build arguments):

* **Unix:**
  ```bash
  script/build.sh ...
  ```
* **Windows:**
  ```
  script\build.bat ...
  ```

Build arguments are those accepted by CMake (see
<https://cmake.org/cmake/help/latest/manual/cmake.1.html#build-a-project>).
Important build arguments include:

* `--target <target>` for the build target.
* `--config <Debug|MinSizeRel|Release|RelWithDebInfo>` for the build
  configuration; only applies to multi-configuration project types, such as
  "Ninja Multi-Config", "Visual Studio 16 2019", or "Xcode".
  Multiple-configuration build type generators are recommended since they allow
  (a) use of multiple build configurations in the same build directory, and (b)
  switching build configuration at build time vs. generation time. See
  [CMake Build Configurations](https://cmake.org/cmake/help/latest/manual/cmake-buildsystem.7.html#build-configurations)
  for more information on build configurations in CMake. Note that pre-built
  libraries are only provided for `Debug` and `Release`, requiring manual builds
  for `MinSizeRel` and `RelWithDebInfo`.
* `--clean-first` to clean prior build products before the build.
* `-- <argument> [<argument> ...]`, where each `<argument>` is an argument
  accepted by the native project type. Note the space after `--`.

All builds are generated into the output directory
`"<build>/artifacts/<configuration>"` where:

* `<build>` refers to the build directory, which defaults to the `"build"`
subdirectory of the directory containing this file.
* `<configuration>` refers to the build configuration.

### External Dependencies

DCP Native has two types of external code dependencies: binary (pre-built), and
source. By default, these are downloaded and cached as necessary.

#### Binary Dependencies

Projects governing these builds have names of the form `*-build` under
`https://gitlab.com/Distributed-Compute-Protocol` and include:

* [Boost](https://gitlab.com/Distributed-Compute-Protocol/boost-build)
* [Dawn](https://gitlab.com/Distributed-Compute-Protocol/dawn-build) (optional)
* [Node](https://gitlab.com/Distributed-Compute-Protocol/node-build)
* [Omaha](https://gitlab.com/Distributed-Compute-Protocol/omaha-build) (Windows
  only)
* [Uncrustify](https://gitlab.com/Distributed-Compute-Protocol/uncrustify-build)
  (optional)

In the event that you want to develop with local builds of these dependencies,
perform the following steps:

* Clone and build the dependency by following the instructions in the
  dependency's `"README.md"` file. The build will place the build products in a
  `"build/install"` subdirectory of the repository directory.
* Once the dependency build is completed, specify the full path of the
  `"build/install"` subdirectory as the `-D <DEPENDENCY>_ROOT` value in the
  CMake arguments as described below.

#### Source Dependencies

Projects governing these sources are repository forks that include:

* [node-addon-api](https://github.com/Distributive-Network/node-addon-api)
* [socketpair](https://github.com/Distributive-Network/socketpair)

In the event that you want to develop with local clones of these repositories,
specify the full path of the repository as the `-D <DEPENDENCY>_ROOT` value in
the CMake arguments as described below.

### CMake Arguments

To specify non-default CMake settings, the generation step can be invoked
manually with the folowing command (with the ellipsis denoting optional CMake
arguments):

* **Unix:**
  ```bash
  script/generate.sh ...
  ```
* **Windows:**
  ```
  script\generate.bat ...
  ```

CMake arguments are listed
[here](https://cmake.org/cmake/help/latest/manual/cmake.1.html#options). Notable
arguments include:

* `-G <generator> [-A <generator platform, if applicable>]`, as listed on the
  [cmake-generators](https://cmake.org/cmake/help/latest/manual/cmake-generators.7.html)
  page. Some notes on specific platforms:
  * On Linux, the default is the "Unix Makefiles" generator. However, the
    "Ninja Multi-Config" generator is recommended for fast
    multiple-configuration builds.
  * On Windows:
    * Visual Studio 2019 is the minimum version that supports x64 builds:
      ```
      -G "Visual Studio 16 2019" -A x64
      ```
    * Visual Studio 2022 is the minimum version that supports arm64 builds:
      ```
      -G "Visual Studio 17 2022" -A arm64
      ```
* `-D <variable[=value]>` to set a CMake variable. Notable variables used by
  this project include:
  * `BOOST_*`:
    * `BOOST_ROOT=<path/to/boost>` to fulfill the Boost binary dependency
      locally (see the "Binary Dependencies" section).
  * `CMAKE_*`:
    * `CMAKE_BUILD_TYPE=<Debug|MinSizeRel|Release|RelWithDebInfo>`
      (single-configuration project types only, such as "Unix Makefiles") to set
      the build configuration (default: `Release`). Note that pre-built
      libraries are only provided for `Debug` and `Release`, requiring manual
      builds for `MinSizeRel` and `RelWithDebInfo`.
  * `DAWN_*`:
    * `DAWN_ROOT=<path/to/dawn>` to fulfill the Dawn binary dependency locally
      (see the "Binary Dependencies" section).
  * `DCP_*`:
    * `DCP_CLANG_TIDY=ON` to enable `clang-tidy` to perform additional static
      analysis on the code.
    * `DCP_COVERAGE=ON` (Mac and Linux only) to enable the `dcp-coverage`
      build target.
    * `DCP_FORMAT=OFF` to disable code formatting functions. If `ON`:
      * `DCP_FORMAT_FIX=ON` to tell the `dcp-format` build target to
        automatically format code; otherwise, formatting errors fail the build.
    * `DCP_HTML=ON` to enable the `dcp-html` build target, which generates HTML
      documentation.
    * `DCP_INSTALLER=ON` to enable the `dcp-installer` target. If `ON`:
      * `DCP_INSTALLER_COMPONENTIZE=ON` (Linux only) to additionally build an
        installer for each component.
      * `DCP_INSTALLER_SIGN=ON` (Windows only) to sign installer executables
        with the Distributive certificate. For more information on signing,
        refer to the documentation of the `dcp-installer` build target.
    * `DCP_MUTATION=ON` (Clang only) to enable the `dcp-mutation` and
      `dcp-*-mutation` build targets.
    * `DCP_NODE=OFF` to use the system-provided Node.
    * `DCP_OMAHA=ON` (Windows only) to enable Omaha auto-updates.
    * `DCP_SANITIZER_ADDRESS=ON` (Mac and Linux only) to enable the Clang
      address sanitizer (Mac and Linux only).
    * `DCP_SANITIZER_LEAK=ON` (Linux only) to enable the Clang leak sanitizer.
    * `DCP_SANITIZER_MEMORY=ON` (Linux only) to enable the Clang memory
      sanitizer. Note that this is mutually exclusive with the address, leak,
      and thread sanitizers and cannot be enabled simultaneously.
    * `DCP_SANITIZER_THREAD=ON` (Mac and Linux only) to enable the Clang thread
      sanitizer. Note that this is mutually exclusive with coverage data
      generation and the address and leak sanitizers, and cannot be enabled
      simultaneously.
    * `DCP_SANITIZER_UNDEFINED=ON` (Mac and Linux only) to enable the Clang
      undefined behavior sanitizer.
    * `DCP_SIGN_CERTIFICATE_FILE=<path/to/certificate/file>` (Windows only) to
      specify the path to the certificate file (`".cer"`) to use for signing.
    * `DCP_SHOW_INCLUDES=ON` (Visual Studio only) to show the include tree in
      the build output. This can be very time-consuming.
    * `DCP_TEST=OFF` to disable test build targets.
    * `DCP_WARNINGS_AS_ERRORS=OFF` to not treat warnings as errors.
  * `NODE_*`:
    * `NODE_EXECUTABLE=<path/to/node/executable>` for specifying a Node
      executable to use; by default, will look in `NODE_ROOT`, if set.
    * `NODE_ROOT=<path/to/node>` for specifying a Node download to use.
  * `NODE_API_*`:
    * `NODE_API_ROOT=<path/to/Node-API>` to fulfill the Node-API source
      dependency locally (see the "Source Dependencies" section).
  * `NODE_ADDON_API_*`:
    * `NODE_ADDON_API_ROOT=<path/to/Node-Addon-API>` to fulfill the
      Node-Addon-API source dependency locally (see the "Source Dependencies"
      section).
  * `NPM_*`:
    * `NPM_AUDIT=ON` to run `npm audit` as part of the build.
    * `NPM_VERBOSE=ON` to enable verbose NPM build logging.
  * `NSSM_*`:
    * `NSSM_EXECUTABLE=<path/to/nssm/executable>` for specifying an NSSM
      executable to use; by default, will look in the `"win64"` subdirectory of
      `NSSM_ROOT`, if set.
    * `NSSM_ROOT=<path/to/nssm>` for specifying an NSSM download to use.
  * `OMAHA_*`:
    * `OMAHA_ROOT=<path/to/omaha>` (Windows only) to fulfill the Omaha binary
      dependency locally (see the "Binary Dependencies" section).
  * `SOCKET_PAIR_*`:
    * `SOCKET_PAIR_ROOT=<path/to/socketpair>` to fulfill the SocketPair source
      dependency locally (see the "Source Dependencies" section).
  * `UNCRUSTIFY_*`:
    * `UNCRUSTIFY_EXECUTABLE=<path/to/uncrustify>` for specifying a specific
      Uncrustify executable to use; by default, will look in the `bin`
      subdirectory of `UNCRUSTIFY_ROOT`, if set.
    * `UNCRUSTIFY_ROOT=<path/to/uncrustify>` to fulfill the Uncrustify binary
      dependency locally (see the "Binary Dependencies" section).
* `-U <variable>` to unset a CMake variable.

Variables need only be supplied on the initial generation; variable values are
cached for subsequent runs until modified (via `-D`) or unset (via `-U`).

After the generate script has run, the `build` script can be invoked normally
to build.

### Out-of-Source Builds

To build into another directory, navigate to the desired directory and invoke
the following to generate (with the ellipsis denoting optional CMake arguments,
and `$REPOSITORY_PATH` denoting the path to the repository):
```
cmake ... -P "$REPOSITORY_PATH/script/generate.cmake"
```
Followed by the CMake build command (with the ellipsis denoting optional build
arguments):
```
cmake --build . ...
```

### Building Using the Native Build Project

The native build project can be used directly for builds by navigating to the
build directory (`"build"` by default) and performing the normal native build
steps, such as:

* For the "Unix Makefile" generator, run `make`.
* For an IDE generator (eg. Xcode, Visual Studio), open `"dcp.<extension>"` and
  built normally from within the IDE. **Note:** if the `"CMakeLists.txt"` file
  is changed during development, the first subsequent build in the IDE may cause
  the project to reload as the build is regenerated with the new CMake settings,
  thus requiring a second build.

### Build Targets

Information about specific build targets follows.

If no targets are specified when building, the default target is built. Because
this is the target incrementally built by CPack when building the
`dcp-installer` target, some targets are excluded from the default target. These
include `dcp-installer` itself, which would cause recursion, and non-essential
targets that perform a potentially lengthy action each build, such as
`dcp-check` and `dcp-format`.

#### dcp

The `DCP` build target builds all of the other build targets.

#### dcp-check

The `dcp-check` build target builds and runs the unit tests defined in the
`dcp-test-*` build targets.

#### dcp-coverage

On Mac and Linux, the `dcp-coverage` build target outputs a `gcovr` test
coverage report directory called `"dcp-coverage"` to the output directory, with
main file `"index.hml"`.

#### dcp-docker

On Linux, the `dcp-docker` build target generates a worker Dockerfile.

The following environment variables can be specified when running the docker
image:

* `DCP_NPM_UPDATE` updates the dcp-worker NPM package as follows:
  * 2: update to bleeding edge version
  * 1: update to latest point version
  * otherwise (or undefined): use the bundled version
* `DCP_SUPERVISOR_SERVICE`, if non-empty, indicates that the supervisor will
  be run as a service and the sandbox server will be run in the foreground,
  whereby Docker run arguments are passed to the sandbox server. Otherwise,
  the sandbox server is run in the background via `inetd` and the supervisor
  is run in the foreground, with Docker run arguments passed to the supervisor.
* `DCP_EVALUATOR_ARGUMENTS` (if `DCP_SUPERVISOR_SERVICE` is empty) provides
  arguments to the evaluator.

#### dcp-evaluator

The `dcp-evaluator` build target outputs the command-line `dcp-evaluator`
executable to the output directory.

#### dcp-format

The `dcp-format` build target uses
[Uncrustify](https://github.com/uncrustify/uncrustify) to perform inline code
formatting or format checking.

#### dcp-html

The `dcp-html` build target outputs a Doxygen documentation directory called
`"dcp-html"` to the output directory, with main file `"index.html"`.

#### dcp-installer

The `dcp-installer` build target outputs an installer to the output directory.

In order to sign the Windows installer, the following setup steps must be
completed:

* Install
  [Windows 10 SDK](https://developer.microsoft.com/en-us/windows/downloads/windows-10-sdk/)
  so that `signtool.exe` is present.
* Install and set up
  [SafeNet Authentication Client](https://knowledge.digicert.com/generalinformation/INFO1982.html).
* Plug in the USB dongle. This will need to be plugged in during each signed
  build.
* Follow [these steps](https://stackoverflow.com/a/54439759) to:
  * Export the certificate to a `".cer"` file.
  * Determine the reader and container names, to be used in the next step.
* Each user who will be performing a build must save the reader and container
  names (from the prior step), along with the USB dongle password (ask an
  administrator), to the HKCU hive in the registry in an encrypted form by
  running the following command, where `dcp-sign` is the executable built by the
  `dcp-sign` target:
  ```
  dcp-sign --save --reader "$READER" --container "$CONTAINER" --password "$PASSWORD"
  ```
  **NOTE: MAKE SURE** to enter the credentials correctly. The USB dongle will
  fail to work after a total of 15 incorrect attempts.
* Set the following CMake variables:
  * `DCP_INSTALLER_SIGN=ON`.
  * `DCP_SIGN_CERTIFICATE_FILE=<path/to/certificate/file>`, where
  `"<path/to/certificate/file>"` is the path to the certificate file exported
  above.

#### dcp-library, dcp-*-library

The `dcp-library` and `dcp-*-library` build targets output static libraries to
the output directory.

#### dcp-licenses

The `dcp-licenses` build target aggregates `"LICENSE.txt"` with the licenses of
each external library and outputs the results to `"LICENSES.txt"` (and
`"LICENSES.rtf"` on Windows) in the output directory.

#### dcp-node

The `dcp-node` build target outputs the `node` executable to the output
directory, along with a `"node.bat"` script on Windows responsible for running
it with the proper V8 settings from the registry.

#### dcp-nssm

On Windows, the `dcp-nssm` build target outputs the `nssm` executable to the
output directory. This program is used for managing services.

#### dcp-sandbox

The `dcp-sandbox` build target outputs a `dcp-sandbox` script to the output
directory. This script runs the sandbox server. If the `dcp-supervisor` script
is running (either manually or via service), it will connect to the sandbox
server and work will start to be done.

#### dcp-screensaver

On Windows, the `dcp-screensaver` build target outputs the
`"dcp-screensaver.scr"` screensaver executable to the output directory. The
screensaver runs a sandbox server; see the `dcp-sandbox` target.

To run the screensaver, pass `/s` to the `"dcp-screensaver.scr"` executable.

To make Windows invoke the screensaver, for development:

* For the output directory, grant the following permissions to "LOCAL SERVICE":
  * Read & execute
  * List folder contents
  * Read
* To run the screensaver for the logged-in user, run `gpedit` and:
  * Navigate to "User Configuration", "Administrative Templates",
    "Control Panel", then "Personalization".
  * Open "Force a specific screen saver", click "Enabled", enter the full
    path of  `"dcp-screensaver.scr"` and click OK.
  * Open "Screen saver timeout", click "Enabled", select the number of seconds
    before the screensaver should appear, and click OK.
  * Open "Enable screen saver", click "Enabled", and click OK.
* To run the screensaver when no user is logged in, run `regedt32` and:
  * Navigate to "HKEY_USERS", ".DEFAULT", "Control Panel", then "Desktop".
  * Create a string (REG_SZ) key called "SCRNSAVE.EXE" and set the value to the
    full path of `"dcp-screensaver.scr"`.
  * Create a string (REG_SZ) key called "ScreenSaveTimeOut" and set the value
    to the number of seconds before the screensaver should appear.
  * Create a string (REG_SZ) key called "ScreenSaveActive" and set the value to
    1 to enable (or 0 to disable).

#### dcp-settings

On Windows, the `dcp-settings` build target outputs the `dcp-settings`
executable to the output directory. This program runs the settings UI.

#### dcp-settings-configurator

On Windows, the `dcp-settings-configurator` build target outputs the
command-line `dcp-settings-configurator` executable to the output directory.
This program is used for saving configuration settings and elevates permissions
as necessary to permit writing to the registry.

#### dcp-sign

The `dcp-sign` build target builds the `dcp-sign` executable to the output
directory.

#### dcp-supervisor

The `dcp-supervisor` build target is responsible for building the
`dcp-supervisor` Node package to the output directory. This program is
responsible for fetching work from the scheduler and connecting to the sandbox
server for sandboxes to do it.

In order to update the `dcp-worker` package dependency:

* Navigate to the `"targets/supervisor/package"` repository subdirectory.
* Update the version in `"package.json"` as necessary. For the `dcp-supervisor`
  version:
  * If the `dcp-worker` major version is changed, increment the major version
    and reset the patch version to 0.
  * Otherwise, if the `dcp-worker` minor version is changed, or security issues
    have been fixed, increment the minor version and reset the patch version to
    0.
  * Otherwise, increment the patch version only.
* Run `npm install dcp-worker`.
* Commit the results. The `"node_modules"` folder can be deleted.

#### dcp-supervisor-setup

On Windows, the `dcp-supervisor-setup` build target outputs the
`dcp-supervisor-setup` script to the output directory. This script is run by the
installer to set up all supervisor defaults on the system.

#### dcp-*-bundle

On MacOS, the `dcp-*-bundle` targets generate application bundles to be included
in the install.

#### dcp-*-test

The `dcp-*-test` build targets each build a `dcp-*-test` unit test executable in
the output directory that can accept the normal
[Boost Test runtime parameters](https://www.boost.org/doc/libs/release/libs/test/doc/html/boost_test/runtime_config.html).

## Debugging Evaluation

**IMPORTANT**: You *need* to have opened the devtools at least once (or visit
this [link](devtools://devtools/bundled/inspector.html)) for the below steps to
work.

For architectural details of how the debugger works, see the
[DCP::V8::Evaluator detail documentation](https://distributed-compute-protocol.gitlab.io/dcp-native/struct_d_c_p_1_1_v8_1_1_evaluator.html#details).

### Debugger Modes

The debugger can either wait for you to connect to it over Chrome DevTools
before executing code (**wait mode**), or it can begin immediately (**immediate
mode**). This behavior is analogous to `inspect` vs. `inspect-brk` if you have
used Node's debugging functionality. **Wait mode** is useful for debugging
sandbox setup scripts. **Immediate mode** is for debugging work functions or any
code that is sent to the evaluator after setup files are run.

### Debugging in Wait Mode

To connect Chrome DevTools to a running evaluator, follow the below steps:

* Launch the evaluator with options:
  ```
  --debugging-port $PORT --debugging-wait
  ```
* Visit
  `devtools://devtools/bundled/inspector.html?experiments=true&v8only=true&ws=127.0.0.1:<port>`.

There is a bug in Chromium whereby if the DevTools have not been loaded before,
`devtools://` links are treated as invalid.
[Upstream issue](https://issues.chromium.org/issues/40068914).

### Debugging in Immediate Mode

* Launch the evaluator with options:
  ```
  --debugging-port $PORT
  ```
* Visit
  `devtools://devtools/bundled/inspector.html?experiments=true&v8only=true&ws=127.0.0.1:<port>`.

You won't see any code until a breakpoint is encountered.

### Debugging Sandbox Setup Scripts in Immediate Mode

This is the same procedure as before, except we are going to load the Sandbox
Setup scripts in.

* Launch the sandbox with options:
  ```
  --stdio -- --debugging-port $PORT --debugging-wait
  ```
* Visit
  `devtools://devtools/bundled/inspector.html?experiments=true&v8only=true&ws=127.0.0.1:<port>`

### Debugging the Worker in Immediate Mode

* Launch the sandbox with options:
  ```
  --stdio -- --debugging-port $PORT
  ```
* Launch the supervisor with options:
  ```
  -o console -c 1 -v
  ```
* Visit
  `devtools://devtools/bundled/inspector.html?experiments=true&v8only=true&ws=127.0.0.1:<port>`

*Caveat*: When launching the worker with more than one sandbox, evaluator
debugger port numbers will conflict and cause errors. This will be addressed
with the future addition of Standalone Worker Protocol support for debugging.
