# Copyright (c) 2021 Distributive Corp. All Rights Reserved.

set(_INTERFACE "${PROJECT_NAME}-options")

# Add an interface that sets compile options.
add_library("${_INTERFACE}" INTERFACE)

target_compile_features("${_INTERFACE}"
  INTERFACE "cxx_std_${CMAKE_CXX_STANDARD}"
)

if(WIN32)
  target_compile_definitions("${_INTERFACE}"
    INTERFACE
    "_CRT_NONSTDC_NO_DEPRECATE"
    "_CRT_SECURE_NO_WARNINGS"
    "_WINSOCK_DEPRECATED_NO_WARNINGS"
    "NOMINMAX"
    "WIN32_LEAN_AND_MEAN"
  )

  # https://learn.microsoft.com/en-us/cpp/porting/modifying-winver-and-win32-winnt?view=msvc-170
  set(_WINDOWS_VERSION 0x0A00) # Windows 10
  target_compile_definitions("${_INTERFACE}"
    INTERFACE
    "_WIN32_WINNT=${_WINDOWS_VERSION}"
    "WINVER=${_WINDOWS_VERSION}"
  )
  unset(_WINDOWS_VERSION)
endif()

if(MSVC)
  option(DCP_SHOW_INCLUDES "Show include tree in build output." OFF)
  if(DCP_SHOW_INCLUDES)
    message(
      STATUS "DCP_SHOW_INCLUDES: ON (Showing include tree in build output)"
    )
    target_compile_options("${_INTERFACE}" INTERFACE "/showIncludes")
  else()
    message(
      STATUS "DCP_SHOW_INCLUDES: OFF (Not showing include tree in build output)"
    )
  endif()

  target_compile_options("${_INTERFACE}" INTERFACE "/utf-8")
else()
  set(DCP_SHOW_INCLUDES OFF)
  message(STATUS "DCP_SHOW_INCLUDES: OFF (Not supported by this compiler)")
endif()

if(DCP_MUTATION)
  if(NOT CMAKE_CXX_COMPILER_ID MATCHES ".*Clang")
    message(
      AUTHOR_WARNING
      "DCP_MUTATION is only permitted on Clang."
      " CMAKE_CXX_COMPILER_ID: ${CMAKE_CXX_COMPILER_ID}"
    )
  elseif(NOT MULL_PLUGIN)
    message(AUTHOR_WARNING "DCP_MUTATION requires Mull; plugin not found")
  else()
    target_compile_options("${_INTERFACE}" INTERFACE "-O0" "-g")
    target_compile_options("${_INTERFACE}"
      INTERFACE
      "-fexperimental-new-pass-manager"
      "-fpass-plugin=${MULL_PLUGIN}"
      "-grecord-command-line"
    )
    if(DCP_COVERAGE)
      target_compile_options("${_INTERFACE}"
        INTERFACE
        "-fcoverage-mapping"
        "-fprofile-instr-generate"
      )
    endif()
  endif()
endif()

if(DCP_COVERAGE)
  if(
    (NOT CMAKE_CXX_COMPILER_ID STREQUAL "GNU") AND
    (NOT CMAKE_CXX_COMPILER_ID MATCHES ".*Clang")
  )
    message(
      AUTHOR_WARNING
      "DCP_COVERAGE is only permitted on GCC or Clang."
      " CMAKE_CXX_COMPILER_ID: ${CMAKE_CXX_COMPILER_ID}"
    )
  else()
    target_compile_options("${_INTERFACE}" INTERFACE "-O0" "-g")
    target_compile_options("${_INTERFACE}" INTERFACE "--coverage")
    target_link_options("${_INTERFACE}" INTERFACE "--coverage")
  endif()
endif()

if(
  (CMAKE_CXX_COMPILER_ID STREQUAL "GNU") OR
  (CMAKE_CXX_COMPILER_ID MATCHES ".*Clang")
)
  set(_SANITIZERS)

  option(DCP_SANITIZER_ADDRESS "Enable address sanitizer." OFF)
  if(DCP_SANITIZER_ADDRESS)
    message(STATUS "DCP_SANITIZER_ADDRESS: ON (Address sanitizer enabled)")
    list(APPEND _SANITIZERS "address")
  else()
    message(STATUS "DCP_SANITIZER_ADDRESS: OFF (Address sanitizer not enabled)")
  endif()

  if(CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
    set(DCP_SANITIZER_LEAK OFF)
  else()
    option(DCP_SANITIZER_LEAK "Enable leak sanitizer." OFF)
  endif()
  if(DCP_SANITIZER_LEAK)
    message(STATUS "DCP_SANITIZER_LEAK: ON (Leak sanitizer enabled)")
    list(APPEND _SANITIZERS "leak")
  else()
    message(STATUS "DCP_SANITIZER_LEAK: OFF (Leak sanitizer not enabled)")
  endif()

  option(DCP_SANITIZER_UNDEFINED "Enable undefined sanitizer." OFF)
  if(DCP_SANITIZER_UNDEFINED)
    message(STATUS "DCP_SANITIZER_UNDEFINED: ON (Undefined sanitizer enabled)")
    list(APPEND SANITIZERS "undefined")
  else()
    message(
      STATUS "DCP_SANITIZER_UNDEFINED: OFF (Undefined sanitizer not enabled)"
    )
  endif()

  option(DCP_SANITIZER_THREAD "Enable thread sanitizer." OFF)
  if(DCP_SANITIZER_THREAD)
    message(STATUS "DCP_SANITIZER_THREAD: ON (Thread sanitizer enabled)")
    if(DCP_SANITIZER_ADDRESS OR DCP_SANITIZER_LEAK)
      message(
        FATAL_ERROR
        "Thread sanitizer incompatible with address or leak sanitizers; set"
        " DCP_SANITIZER_THREAD=OFF, DCP_SANITIZER_ADDRESS=OFF, and/or"
        " DCP_SANITIZER_LEAK=OFF"
      )
    endif()
    if(DCP_COVERAGE)
      message(FATAL_ERROR
        "Thread sanitizer incompatible with coverage; set"
        " DCP_SANITIZER_THREAD=OFF and/or DCP_COVERAGE=OFF"
      )
    endif()
    list(APPEND _SANITIZERS "thread")
  else()
    message(STATUS "DCP_SANITIZER_THREAD: OFF (Thread sanitizer not enabled)")
  endif()

  option(DCP_SANITIZER_MEMORY "Enable memory sanitizer." OFF)
  if(DCP_SANITIZER_MEMORY)
    message(STATUS "DCP_SANITIZER_MEMORY: ON (Memory sanitizer enabled)")
    if(DCP_SANITIZER_ADDRESS OR DCP_SANITIZER_LEAK OR DCP_SANITIZER_THREAD)
      message(
        FATAL_ERROR
        "Memory sanitizer incompatible with address, thread, or leak"
        " sanitizers; set DCP_SANITIZER_MEMORY=OFF, DCP_SANITIZER_ADDRESS=OFF,"
        " DCP_SANITIZER_LEAK=OFF, and/or DCP_SANITIZER_THREAD=OFF"
      )
    endif()
    list(APPEND _SANITIZERS "memory")
  else()
    message(STATUS "DCP_SANITIZER_MEMORY: OFF (Memory sanitizer not enabled)")
  endif()

  list(JOIN _SANITIZERS "," _SANITIZERS)
  if(NOT _SANITIZERS STREQUAL "")
    target_compile_options("${_INTERFACE}"
      INTERFACE "-fsanitize=${_SANITIZERS}"
    )
    target_link_options("${_INTERFACE}" INTERFACE "-fsanitize=${_SANITIZERS}")
  endif()

  unset(_SANITIZERS)
else()
  message(STATUS "DCP_SANITIZER_ADDRESS: OFF (Unsupported by this compiler)")
  message(STATUS "DCP_SANITIZER_LEAK: OFF (Unsupported by this compiler)")
  message(STATUS "DCP_SANITIZER_UNDEFINED: OFF (Unsupported by this compiler)")
  message(STATUS "DCP_SANITIZER_THREAD: OFF (Unsupported by this compiler)")
  message(STATUS "DCP_SANITIZER_MEMORY: OFF (Unsupported by this compiler)")
endif()
