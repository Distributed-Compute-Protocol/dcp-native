@echo off
REM
REM @file         generate.bat
REM               Generate the project into "<repository>/build".
REM @author       Jason Erb, jason@distributive.network
REM @date         December 2019

setLocal enableExtensions

if not defined CMAKE (
  set CMAKE=cmake
) else (
  echo Using CMake: %CMAKE%
)

pushd "%~dp0"
set "SD=%cd%"
pushd ".."
if not exist "build" mkdir "build"
pushd "build"
start "" /i /b /wait "%CMAKE%" %* -P "%SD%\generate.cmake"
popd
popd
popd
