#!/bin/sh
#
# @file         unsubscribe.sh
#               Unsubscribe from the apt repository.
# @author       Jason Erb, jason@distributive.network
# @date         August 2024

set -e

if [ ! "$(uname)" = "Linux" ]; then
  echo "This script is only supported on Linux." >&2
  exit 1
fi

if [ "$#" -eq 0 ]; then
  CHANNEL=
  CHANNEL_DASH=
elif [ "$#" -eq 1 ] && { [ "$1" = "alpha" ] || [ "$1" = "beta" ]; }; then
  CHANNEL="$1"
  CHANNEL_DASH="${CHANNEL}-"
else
  echo "Usage: $0 [alpha|beta]" >&2
  exit 1
fi

sudo rm /etc/apt/keyrings/distributive-${CHANNEL_DASH}pubkey.asc || true
sudo rm /etc/apt/sources.list.d/${CHANNEL_DASH}distributive.list || true
sudo apt update

echo >&2
if [ -z "${CHANNEL}" ]; then
  echo "Unsubscribed from the Distributive apt repository." >&2
else
  echo "Unsubscribed from the Distributive '${CHANNEL}' apt repository." >&2
fi
