#!/bin/bash
#
# @file         iconify.sh
#               Generate a MacOS application ICNS file from a PNG.
#               Adapted from: https://stackoverflow.com/a/31883126
# @author       Jason Erb, jason@distributive.network
# @date         September 2022

set -e

if [ ! "$(uname)" = "Darwin" ]; then
  echo "This script is only supported on MacOS." >&2
  exit 1
fi

if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <png file path> <icon name>" >&2
  exit 1
fi
PNG=$1
ICON=$2

rm -rf "./${ICON}.iconset"
mkdir "./${ICON}.iconset"

# Normal screen icons
for SIZE in 16 32 64 128 256 512; do
  sips -z ${SIZE} ${SIZE} "${PNG}" \
   --out "./${ICON}.iconset/icon_${SIZE}x${SIZE}.png"
done

# Retina display icons
for SIZE in 32 64 256 512; do
  sips -z ${SIZE} ${SIZE} "${PNG}" \
   --out "./${ICON}.iconset/icon_$(expr ${SIZE} / 2)x$(expr ${SIZE} / 2)x2.png"
done

# Make a multi-resolution icon
iconutil -c icns -o "./${ICON}.icns" "./${ICON}.iconset"

rm -rf "./${ICON}.iconset"
