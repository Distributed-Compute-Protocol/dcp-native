#!/bin/sh
#
# @file         subscribe.sh
#               Subscribe to the apt repository.
# @author       Jason Erb, jason@distributive.network
# @date         July 2024

set -e

if [ ! "$(uname)" = "Linux" ]; then
  echo "This script is only supported on Linux." >&2
  exit 1
fi

if [ "$#" -eq 0 ]; then
  CHANNEL=
  CHANNEL_DOT=
  CHANNEL_DASH=
elif [ "$#" -eq 1 ] && { [ "$1" = "alpha" ] || [ "$1" = "beta" ]; }; then
  CHANNEL="$1"
  CHANNEL_DOT="${CHANNEL}."
  CHANNEL_DASH="${CHANNEL}-"
else
  echo "Usage: $0 [alpha|beta]" >&2
  exit 1
fi

wget --quiet -O - http://${CHANNEL_DOT}apt.distributive.network/gpg-pubkey.asc | sudo tee /etc/apt/keyrings/distributive-${CHANNEL_DASH}pubkey.asc

echo "deb [signed-by=/etc/apt/keyrings/distributive-${CHANNEL_DASH}pubkey.asc arch=$( dpkg --print-architecture )] http://${CHANNEL_DOT}apt.distributive.network $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/${CHANNEL_DASH}distributive.list

sudo apt update

echo >&2
if [ -z "${CHANNEL}" ]; then
  echo "Subscribed to the Distributive apt repository." >&2
else
  echo "Subscribed to the Distributive '${CHANNEL}' apt repository." >&2
  echo >&2
  echo "WARNING: The '${CHANNEL}' repository is unsupported for public use." >&2
  echo "Software in this repository is untested, unlikely to work properly," >&2
  echo "and may delete your data or otherwise damage your computer." >&2
fi
echo >&2
echo "To unsubscribe from this repository, run:" >&2
echo >&2
echo "$(dirname "$(realpath "$0")")/unsubscribe.sh ${CHANNEL}" >&2
echo >&2
