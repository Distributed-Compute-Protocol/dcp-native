#!/bin/sh
#
# @file         build.sh
#               Build the project into "<repository>/build".
# @author       Jason Erb, jason@distributive.network
# @date         December 2019

set -e

if [ ! "${CMAKE}" ]; then
  CMAKE=cmake
else
  echo "Using CMake: ${CMAKE}"
fi

cd $(dirname "$(realpath "$0")")
SD="${PWD}"
cd ".."
mkdir -p "build"
cd "build"
"${CMAKE}" -P "${SD}/generate.cmake"
"${CMAKE}" --build "." "$@"
