#!/bin/bash
#
# @file         bundle.sh
#               Compile applescript into a MacOS application bundle.
# @author       Jason Erb, jason@distributive.network
# @date         January 2024

set -e

if [ ! "$(uname)" = "Darwin" ]; then
  echo "This script is only supported on MacOS." >&2
  exit 1
fi

if [ "$#" -ne 3 ]; then
  echo "Usage: $0 <application name> <applescript path> <icon path>" >&2
  exit 1
fi
APPLICATION_NAME=$1
APPLESCRIPT_PATH=$2
ICON_PATH=$3

APPLICATION_FILE="${APPLICATION_NAME}.app"

pushd $(dirname "$(realpath "$0")")
SD="$(pwd)"
popd

# Compile the applescript into an application bundle.
osacompile -o "${APPLICATION_FILE}" "${APPLESCRIPT_PATH}"

# Rename "applet" to the application name, and generate icons.
pushd "${APPLICATION_FILE}/Contents"
defaults write "${PWD}/Info.plist" \
 "CFBundleExecutable" -string "${APPLICATION_NAME}"
defaults write "${PWD}/Info.plist" \
 "CFBundleIconFile" -string "${APPLICATION_NAME}"
chmod +r "${PWD}/Info.plist"
if [ -f "MacOS/applet" ]; then
  mv -f "MacOS/applet" "MacOS/${APPLICATION_NAME}"
fi
pushd Resources
if [ -f "applet.rsrc" ]; then
  mv -f "applet.rsrc" "${APPLICATION_NAME}.rsrc"
fi
rm -f "applet.icns"
"${SD}/iconify.sh" "${ICON_PATH}" "${APPLICATION_NAME}"
popd
popd

# Reflect the icon update.
touch -c "${APPLICATION_FILE}"
