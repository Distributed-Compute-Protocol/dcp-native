# Copyright (c) 2020 Distributive Corp. All Rights Reserved.

cmake_minimum_required(VERSION "3.16" FATAL_ERROR)

set(_ARGUMENT_COUNT "1")
while(_ARGUMENT_COUNT LESS CMAKE_ARGC)
  set(_ARGUMENT "${CMAKE_ARGV${_ARGUMENT_COUNT}}")
  if(_ARGUMENT MATCHES "^-P")
    break()
  endif()
  list(APPEND _ARGUMENTS "${_ARGUMENT}")
  math(EXPR _ARGUMENT_COUNT "${_ARGUMENT_COUNT} + 1")
endwhile()

execute_process(
  COMMAND
  "${CMAKE_COMMAND}"
  "--graphviz" "dot/dependencies.dot"
  "${CMAKE_CURRENT_LIST_DIR}/.."
  ${_ARGUMENTS}
  WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
  RESULT_VARIABLE _RESULT
)
if(_RESULT)
  message(FATAL_ERROR "Failure generating: error ${_RESULT}")
endif()
