#!/bin/bash
#
# @file         deploy.sh
#               Push a tag, resulting in a deployment.
# @author       Jason Erb, jason@distributive.network
# @date         April 2021

set -e

cd $(dirname "$(realpath "$0")")
cd ..

CHANGES=$(git status --porcelain=v1 2> /dev/null)
if [ ! -z "${CHANGES}" ]; then
  echo "${CHANGES}" >&2
  echo "Uncommitted changes; aborting..." >&2
  exit 1
fi

VERSION=$(cat VERSION.txt)
if [ -z "${VERSION}" ]; then
  echo "Version not found in 'VERSION.txt'; aborting..." >&2
  exit 1
fi
if [[ ! "${VERSION}" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
  echo "Version ${VERSION} failed regex match; aborting..." >&2
  exit 1
fi
IFS='.'; VERSION_ARRAY=(${VERSION}); unset IFS
if [ ! "${#VERSION_ARRAY[@]}" = "4" ]; then
  echo "Version must have 4 components; found ${#VERSION_ARRAY[@]}" >&2
  exit 1
fi

BRANCH=$(git branch --show-current)
if [ ! "${VERSION_ARRAY[3]}" = "0" ]; then
  VERSION="${VERSION}-alpha"
  TAG="${VERSION}"
else
  if [ ! "${BRANCH}" = "release" ]; then
    echo "On '${BRANCH}' branch instead of 'release'; aborting..." >&2
    exit 1
  fi
  TAG="${VERSION_ARRAY[0]}.${VERSION_ARRAY[1]}.${VERSION_ARRAY[2]}"
fi

git fetch --tags
git tag -a "${TAG}" HEAD -m "Tag ${TAG}; Version ${VERSION}"
git push origin "${BRANCH}" "${TAG}"
