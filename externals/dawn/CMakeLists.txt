# Copyright (c) 2023 Distributive Corp. All Rights Reserved.

set(_ROOT "${CMAKE_CURRENT_BINARY_DIR}/install")
set(DAWN_ROOT "${_ROOT}" CACHE PATH "The Dawn root directory.")
message(STATUS "DAWN_ROOT: ${DAWN_ROOT}")
if(NOT DAWN_ROOT STREQUAL "${_ROOT}")
  message(STATUS "Not downloading Dawn; custom root specified.")
  return()
endif()
unset(_ROOT)

# 5.1.0
if(DCP_OS STREQUAL "Linux")
  if(DCP_ARCHITECTURE STREQUAL "arm64")
    set(_DOWNLOAD_PACKAGE_FILE_ID "155912480")
    set(_DOWNLOAD_SHA256
      "99b6ae50b6c4ca6f27d7969477db96ab7144c554a44576c612d8a5e325feacb3"
    )
  elseif(DCP_ARCHITECTURE STREQUAL "x64")
    set(_DOWNLOAD_PACKAGE_FILE_ID "155891345")
    set(_DOWNLOAD_SHA256
      "44e82e9dcc1181527c7ff8a02be47d50f0b2cba33297e07d281754aabdce6e67"
    )
  else()
    message(FATAL_ERROR "No Dawn download for ${DCP_OS} ${DCP_ARCHITECTURE}.")
  endif()
elseif(DCP_OS STREQUAL "MacOS")
  if(DCP_ARCHITECTURE STREQUAL "arm64")
    set(_DOWNLOAD_PACKAGE_FILE_ID "155902696")
    set(_DOWNLOAD_SHA256
      "f0a5a7170085caeada9eca620a5b250f779263179d06101aa9a1c522e760f518"
    )
  elseif(DCP_ARCHITECTURE STREQUAL "x64")
    set(_DOWNLOAD_PACKAGE_FILE_ID "155898652")
    set(_DOWNLOAD_SHA256
      "b2b719935246bef81dcfa0f0b869ae7c015832fc4808c4a9a2dda94527c1ebc9"
    )
  else()
    message(FATAL_ERROR "No Dawn download for ${DCP_OS} ${DCP_ARCHITECTURE}.")
  endif()
elseif(DCP_OS STREQUAL "Windows")
  if(DCP_ARCHITECTURE STREQUAL "x64")
    set(_DOWNLOAD_PACKAGE_FILE_ID "155908542")
    set(_DOWNLOAD_SHA256
      "3c945f4a86a1e3f3da5024e7fbec864baa59258ca01dd6c58e7a96fedb59cd3c"
    )
  else()
    message(FATAL_ERROR "No Dawn download for ${DCP_OS} ${DCP_ARCHITECTURE}.")
  endif()
else()
  message(FATAL_ERROR "No Dawn download for ${DCP_OS}.")
endif()

string(
  CONCAT _DOWNLOAD_URL
  "https://gitlab.com/Distributed-Compute-Protocol/dawn-build/-/"
  "package_files/${_DOWNLOAD_PACKAGE_FILE_ID}/download"
)

include("${CMAKE_ROOT}/Modules/FetchContent.cmake")

FetchContent_Declare(dawn
  PREFIX "${CMAKE_CURRENT_BINARY_DIR}"
  SOURCE_DIR "${DAWN_ROOT}"
  URL "${_DOWNLOAD_URL}"
  URL_HASH "SHA256=${_DOWNLOAD_SHA256}"
  DOWNLOAD_EXTRACT_TIMESTAMP OFF
  TLS_VERIFY ON
)
FetchContent_MakeAvailable(dawn)
